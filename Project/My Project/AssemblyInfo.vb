﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("eAlert-Admin")> 
<Assembly: AssemblyDescription("Administrator Module")> 
<Assembly: AssemblyCompany("Altius Infosystem Pvt Ltd")> 
<Assembly: AssemblyProduct("eAlert-Admin")> 
<Assembly: AssemblyCopyright("Copyright ©  2011")> 
<Assembly: AssemblyTrademark("eAlert")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("07e0c3e0-f1d7-4e66-9b63-6af16e3b6a75")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.0.0")> 
<Assembly: AssemblyFileVersion("1.0.0.0")> 
