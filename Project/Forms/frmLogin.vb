﻿#Region "Imports"
Imports System
Imports System.Data
Imports System.Data.OleDb
Imports System.Threading
Imports System.Globalization
Imports System.Text
#End Region

Public Class frmLogin
    Dim mStrComp() As String
    Dim mStrLoc() As String
    'Dim mDataset1 As Long
    Dim mDataset2 As New DataSet()
    Dim mserver As String
    Dim NextShift As String



    Private Sub frmLogin_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormAlign(Me)

        Dim appProc() As Process
        Dim strModName, strProcName As String

        strModName = Process.GetCurrentProcess.MainModule.ModuleName
        strProcName = System.IO.Path.GetFileNameWithoutExtension(strModName)

        appProc = Process.GetProcessesByName(strProcName)

        ' ''If appProc.Length >= 1 Then
        ' ''    MessageBox.Show("There is an instance of this application running.")
        ' ''    End
        ' ''End If

        mUserLocation = ""
        mdiMain.btnMachineData.Enabled = False
        mdiMain.btnLogout.Enabled = False

        Fill_Company_Details()
        mStrComp = Split(cmbCompCode.Text, " - ")
        Fill_Location_Details()
        mStrLoc = Split(cmbLocCode.Text, " - ")
        mCompCode = ""
        mLocCode = ""
        Get_Server_Date()
        'Auto_update()
        
        'mdiMain.Hide() 'Mail Send that time mdi form hide
    End Sub

    Public Sub Get_Server_Date()
        SSQL = ""
        SSQL = "Select Convert(Varchar(10),GetDate(),103)"
        lblServerDate.Text = ReturnSingleValue(SSQL)
    End Sub

    Public Sub Fill_Location_Details()
        SSQL = ""
        SSQL = "Select LocCode + ' - ' + LocName From Location_Mst Where CompCode='" & Trim(mStrComp(0)) & "'"

        mDataSet = Nothing
        mDataSet = ReturnMultipleValue(SSQL)

        cmbLocCode.Items.Clear()

        If mDataSet.Tables(0).Rows.Count <= 0 Then Exit Sub
        Dim iRow As Integer = 0

        Do
            cmbLocCode.Items.Add(Trim(mDataSet.Tables(0).Rows(iRow)(0)))
            cmbLocCode.SelectedIndex = 0
            iRow += 1
            While iRow > mDataSet.Tables(0).Rows.Count - 1 : Exit Sub : End While

        Loop
    End Sub
    Public Sub Fill_Company_Details()
        SSQL = ""
        SSQL = "Select CompCode + ' - ' + CompName From Company_Mst"

        mDataSet = Nothing
        mDataSet = ReturnMultipleValue(SSQL)

        cmbCompCode.Items.Clear()

        If mDataSet.Tables(0).Rows.Count <= 0 Then Exit Sub
        Dim iRow As Integer = 0

        Do
            cmbCompCode.Items.Add(Trim(mDataSet.Tables(0).Rows(iRow)(0)))
            cmbCompCode.SelectedIndex = 0
            iRow += 1
            While iRow > mDataSet.Tables(0).Rows.Count - 1 : Exit Sub : End While

        Loop
    End Sub
    Private Sub Cancel_A_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_A.Click
        mdiMain.btnMachineData.Enabled = False
        mdiMain.btnLogout.Enabled = False
        Me.Close()
    End Sub
    

    Private Sub OK_A_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_A.Click
        mvarLoginUser = ""
        mUserLocation = ""


        'Check Date Value Start
        mvarLoginUser = ""
        mUserLocation = ""
        Dim q As String = ""
        ''Dim Dt As DataTable = New DataTable()
        'SSQL = "Select * from Verification_Mst"
        'q = ReturnSingleValue(SSQL)
        'Dim Verification_Date As String
        'Dim Verification_Date1 As String
        'If q = "" Then
        '    SSQL = "Select Col_M1,Col_d1,Col_Y1 from Value_Mst"
        '    mDataSet = ReturnMultipleValue(SSQL)
        '    If mDataSet.Tables(0).Rows.Count > 0 Then
        '        Verification_Date = (2 & "-" & 6 & "-" & 2021).ToString()
        '        SSQL = "select convert(varchar,getdate(),105)"
        '        Verification_Date1 = ReturnSingleValue(SSQL)
        '        If (Convert.ToDateTime(Verification_Date) >= Convert.ToDateTime(Verification_Date1)) Then
        '            'Skip
        '        Else
        '            Dim Ins As String = "Insert Into Verification_Mst (Value1) values ('redf#sdfdsfdsf9890834==')"
        '            InsertDeleteUpdate(Ins)
        '            MessageBox.Show("Date Expired....", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
        '            'Return
        '            'Me.Close()
        '            'Application.Exit()
        '            Exit Sub
        '        End If

        '    Else
        '        MessageBox.Show("Date Expired....", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
        '        'Return
        '        'Application.Exit()
        '        Exit Sub
        '    End If
        'Else
        '    'MessageBox.Show("Your Licence Key has been Expired. Update your Licence Key", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
        '    MessageBox.Show("Date Expired....", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
        '    'Return
        '    'Application.Exit()
        '    Exit Sub
        'End If
        ''Check Date Value End













        If Trim(txtAdminName.Text) = "" Then
            MessageBox.Show("Please enter user name.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtAdminName.Focus()
            Exit Sub
        End If
        If Trim(txtAdminPwd.Text) = "" Then
            MessageBox.Show("Please enter password.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtAdminPwd.Focus()
            Exit Sub
        End If

        SSQL = ""
        SSQL = "Select Count(UserName) from [Raajco_Rights]..MstUsers "
        SSQL = SSQL & " Where UserCode='" & txtAdminName.Text & "'"
        SSQL = SSQL & " And Password='" & UTF8Encryption(txtAdminPwd.Text) & "'"

        Dim mRetVal As Integer
        mRetVal = ReturnSingleValue(SSQL)
        If mRetVal <= 0 Then
            'Call OK_U_Click(sender, e)
            MessageBox.Show("Please try again.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtAdminName.Focus()
            Exit Sub
        End If
        mdiMain.btnMachineData.Enabled = True
        mdiMain.btnLogout.Enabled = True
        
        mdiMain.statusUser.Text = UCase(Trim(txtAdminName.Text))
        mdiMain.statusServerDate.Text = lblServerDate.Text
        mvarLoginUser = UCase(Trim(txtAdminName.Text))
        Me.Close()
        'mdiMain.Hide() Mail Send that time mdi form hide
    End Sub

    Private Sub OK_U_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_U.Click
        mvarLoginUser = ""
        mUserLocation = ""

        txtUserName.Text = txtAdminName.Text
        txtUserPwd.Text = txtAdminPwd.Text
        If Trim(cmbCompCode.Text) = "" Then
            MessageBox.Show("Please select company details.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbCompCode.Focus()
            Exit Sub
        End If
        If Trim(cmbLocCode.Text) = "" Then
            MessageBox.Show("Please select location details.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbLocCode.Focus()
            Exit Sub
        End If
        If Trim(txtAdminName.Text) = "" Then
            MessageBox.Show("Please enter user name.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtAdminName.Focus()
            Exit Sub
        End If
        If Trim(txtAdminPwd.Text) = "" Then
            MessageBox.Show("Please enter user password.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtAdminPwd.Focus()
            Exit Sub
        End If

        Dim iStr1() As String
        Dim istr2() As String

        iStr1 = Trim(cmbCompCode.Text).Split("-")
        istr2 = Trim(cmbLocCode.Text).Split("-")
        SSQL = ""
        SSQL = "Select Count(UserName) from User_Login "
        SSQL = SSQL & " Where CompCode='" & Trim(iStr1(0)) & "'"
        SSQL = SSQL & " And LocCode='" & Trim(istr2(0)) & "'"
        SSQL = SSQL & " And UserName='" & UCase(Trim(txtAdminName.Text)) & "'"
        SSQL = SSQL & " And Password='" & Trim(Encryption(UCase(Trim(txtAdminPwd.Text)))) & "'"

        Dim mRetVal As Integer

        mRetVal = ReturnSingleValue(SSQL)
        If mRetVal <= 0 Then
            MessageBox.Show("Please try again.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtAdminName.Focus()
            Exit Sub
        End If

        mCompCode = cmbCompCode.Text
        mLocCode = cmbLocCode.Text
        Dim qry As String = ""
        qry = "Select Data from Rights where CompCode = '" & Trim(iStr1(0)) & "' and LocCode = '" & Trim(istr2(0)) & "' and Username = '" & UCase(Trim(txtUserName.Text)) & "'"
        mDataSet = ReturnMultipleValue(qry)
        If mDataSet.Tables(0).Rows.Count > 0 Then
            mCompCode = cmbCompCode.Text
            mLocCode = cmbLocCode.Text
            
            mdiMain.btnLogout.Enabled = True
            mdiMain.btnMachineData.Enabled = True
            mdiMain.statusLocation.Text = UCase(Trim(istr2(0)))
            mdiMain.statusUser.Text = UCase(Trim(txtUserName.Text))
            mdiMain.statusServerDate.Text = lblServerDate.Text
            mvarLoginUser = UCase(Trim(txtUserName.Text))
            mUserLocation = Trim(istr2(0))
            Me.Close()
            'mdiMain.Hide() Mail Send that time mdi form hide

        End If
    End Sub

    Private Sub Cancel_U_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_U.Click
        mdiMain.btnMachineData.Enabled = False
        mdiMain.btnLogout.Enabled = False
        Me.Close()
    End Sub

    Private Sub LogoPictureBox_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub lblServerDate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblServerDate.Click

    End Sub

    Private Shared Function UTF8Encryption(ByVal password As String) As String
        Dim strmsg As String = String.Empty
        Dim encode As Byte() = New Byte(password.Length - 1) {}
        encode = Encoding.UTF8.GetBytes(password)
        strmsg = Convert.ToBase64String(encode)
        Return strmsg
    End Function

End Class
