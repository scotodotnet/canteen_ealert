﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormDistributeBtn
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.RbtOTDistribute = New System.Windows.Forms.RadioButton
        Me.RbtBonusDistribute = New System.Windows.Forms.RadioButton
        Me.btnopen = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'RbtOTDistribute
        '
        Me.RbtOTDistribute.AutoSize = True
        Me.RbtOTDistribute.Location = New System.Drawing.Point(107, 36)
        Me.RbtOTDistribute.Name = "RbtOTDistribute"
        Me.RbtOTDistribute.Size = New System.Drawing.Size(95, 17)
        Me.RbtOTDistribute.TabIndex = 0
        Me.RbtOTDistribute.TabStop = True
        Me.RbtOTDistribute.Text = "OT Distribution"
        Me.RbtOTDistribute.UseVisualStyleBackColor = True
        '
        'RbtBonusDistribute
        '
        Me.RbtBonusDistribute.AutoSize = True
        Me.RbtBonusDistribute.Location = New System.Drawing.Point(107, 60)
        Me.RbtBonusDistribute.Name = "RbtBonusDistribute"
        Me.RbtBonusDistribute.Size = New System.Drawing.Size(106, 17)
        Me.RbtBonusDistribute.TabIndex = 1
        Me.RbtBonusDistribute.TabStop = True
        Me.RbtBonusDistribute.Text = "Other Distribution"
        Me.RbtBonusDistribute.UseVisualStyleBackColor = True
        '
        'btnopen
        '
        Me.btnopen.Location = New System.Drawing.Point(107, 96)
        Me.btnopen.Name = "btnopen"
        Me.btnopen.Size = New System.Drawing.Size(75, 23)
        Me.btnopen.TabIndex = 2
        Me.btnopen.Text = "Open"
        Me.btnopen.UseVisualStyleBackColor = True
        '
        'FormDistributeBtn
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(260, 133)
        Me.Controls.Add(Me.btnopen)
        Me.Controls.Add(Me.RbtBonusDistribute)
        Me.Controls.Add(Me.RbtOTDistribute)
        Me.Name = "FormDistributeBtn"
        Me.Text = "Choose Distribution Type"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents RbtOTDistribute As System.Windows.Forms.RadioButton
    Friend WithEvents RbtBonusDistribute As System.Windows.Forms.RadioButton
    Friend WithEvents btnopen As System.Windows.Forms.Button
End Class
