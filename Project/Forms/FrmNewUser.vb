﻿#Region "Imports"
Imports System
Imports System.Data
Imports System.Data.OleDb
Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.Windows.Forms
Imports CrystalDecisions.CrystalReports.Engine
#End Region
Public Class FrmNewUser
    Dim mStatus As Long
    Private mImageFile As Image
    Private mImageFilePath As String
    Dim iStr1() As String
    Dim iStr2() As String
    Dim iStr3() As String
    Dim mStrComp() As String
    Dim mStrLoc() As String
    Private Sub FrmNewUser_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormAlign(Me)
        Fill_Company_Details()
        mStrComp = Split(cmbCompCode.Text, " - ")
        Load_grid()
        'Fill_Location_Details()
        'mStrLoc = Split(cmbLocCode.Text, " - ")
        
    End Sub
    Public Sub Fill_Company_Details()
        SSQL = ""
        SSQL = "Select CompCode + ' | ' + CompName as [Name] from Company_Mst Order By CompName"

        mDataSet = ReturnMultipleValue(SSQL)
        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                cmbCompCode.Items.Clear()
                For iRow = 0 To .Rows.Count - 1
                    cmbCompCode.Items.Add(.Rows(iRow)(0))
                Next
            End If
        End With
    End Sub
    Public Sub Fill_Location_Details()
        mStrComp = Split(cmbCompCode.Text, " | ")
        SSQL = ""
        SSQL = "Select LocCode + ' - ' + LocName From Location_Mst Where CompCode='" & Trim(mStrComp(0)) & "'"

        mDataSet = Nothing
        mDataSet = ReturnMultipleValue(SSQL)

        cmbLocCode.Items.Clear()

        If mDataSet.Tables(0).Rows.Count <= 0 Then Exit Sub
        Dim iRow As Integer = 0

        Do
            cmbLocCode.Items.Add(Trim(mDataSet.Tables(0).Rows(iRow)(0)))
            cmbLocCode.SelectedIndex = 0
            iRow += 1
            While iRow > mDataSet.Tables(0).Rows.Count - 1 : Exit Sub : End While

        Loop
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub cmbCompCode_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbCompCode.SelectedIndexChanged
        Fill_Location_Details()
        'mStrLoc = Split(cmbLocCode.Text, " - ")
    End Sub

    Private Sub OK_U_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_U.Click
        mStrComp = Split(cmbCompCode.Text, " | ")
        mStrLoc = Split(cmbLocCode.Text, " - ")
        If Trim(txtUserName.Text) = "" Then
            MessageBox.Show("Please enter user Name.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtUserName.Focus()
        ElseIf Trim(txtUserPwd.Text) = "" Then
            MessageBox.Show("Please enter user Password.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtUserPwd.Focus()
        ElseIf Trim(cmbCompCode.Text) = "" Then
            MessageBox.Show("Please select the Company Code.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbCompCode.Focus()

        ElseIf Trim(cmbLocCode.Text) = "" Then
            MessageBox.Show("Please select the password.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbLocCode.Focus()
        Else
            SSQL = ""
            SSQL = SSQL & "Select UserName from User_Login where UserName='" & Trim(txtUserName.Text) & "' and CompCode='" & Trim(mStrComp(0)) & "' and LocCode='" & Trim(mStrLoc(0)) & "'"
            Dim User_val As String = Nothing
            User_val = ReturnSingleValue(SSQL)
            If Trim(User_val) = Nothing Then
                Dim ins As String = "Insert into User_Login (CompCode,LocCode,UserName,Password) values ('" & Trim(mStrComp(0)) & "','" & Trim(mStrLoc(0)) & "','" & Trim(txtUserName.Text) & "','" & Trim(Encryption(UCase(Trim(txtUserPwd.Text)))) & "')"
                mStatus = InsertDeleteUpdate(ins)
                If mStatus > 0 Then
                    MessageBox.Show("Saved Successfully.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    txtUserName.Text = ""
                    txtUserPwd.Text = ""
                    Load_grid()
                End If
            Else
                Dim ds As DialogResult
                ds = MessageBox.Show("User Name Already Exist. Do you want to Save it ?", "Altius", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
                If ds = DialogResult.Yes Then
                    Dim qry As String = "Update User_Login set Password='" & Trim(Encryption(UCase(Trim(txtUserPwd.Text)))) & "' where UserName='" & Trim(txtUserName.Text) & "' and CompCode='" & Trim(mStrComp(0)) & "' and LocCode='" & Trim(mStrLoc(0)) & "'"
                    mStatus = InsertDeleteUpdate(qry)
                    If mStatus > 0 Then
                        MessageBox.Show("Updated Successfully.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        txtUserName.Text = ""
                        txtUserPwd.Text = ""
                        Load_grid()
                    End If
                Else
                    txtUserName.Text = ""
                    txtUserPwd.Text = ""
                End If
            End If
        End If
    End Sub

    Private Sub Cancel_U_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_U.Click
        txtUserName.Text = ""
        txtUserPwd.Text = ""
    End Sub
    Private Sub Load_grid()
        DataGridView1.Rows.Clear()
        SSQL = ""
        SSQL = SSQL & "select * from User_Login Order by CompCode,LocCode,UserName  Asc"
        mDataSet = ReturnMultipleValue(SSQL)
        If (mDataSet.Tables(0).Rows.Count > 0) Then
            For ia As Integer = 0 To mDataSet.Tables(0).Rows.Count - 1
                Dim n As Integer = DataGridView1.Rows.Add()
                DataGridView1.Rows(n).Cells(1).Value = mDataSet.Tables(0).Rows(n)("LocCode").ToString()
                DataGridView1.Rows(n).Cells(2).Value = mDataSet.Tables(0).Rows(n)("UserName").ToString()

            Next

        End If
    End Sub

    Private Sub cmbLocCode_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbLocCode.SelectedIndexChanged

        
    End Sub

    Private Sub DataGridView1_CellDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellDoubleClick
        If (e.RowIndex >= -1) And (e.ColumnIndex >= -1) Then
            txtUserName.Text = DataGridView1.Rows(e.RowIndex).Cells(2).Value
            'cmbLocCode.Text = DataGridView1.Rows(e.RowIndex).Cells(1).Value
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
Recheck:
        Dim select_user As String = ""
        For is_val As Integer = 0 To DataGridView1.Rows.Count - 1
            select_user = Convert.ToString(DataGridView1.Rows(is_val).Cells(0).Value)
            If select_user = "True" Then
                Dim ins As String = "Delete from User_Login where UserName='" & DataGridView1.Rows(is_val).Cells(2).Value & "'"
                mStatus = InsertDeleteUpdate(ins)
                Load_grid()
                GoTo recheck

            End If
        Next

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        FrmPermission.MdiParent = mdiMain
        FrmPermission.Show()
        FrmPermission.BringToFront()
    End Sub
End Class