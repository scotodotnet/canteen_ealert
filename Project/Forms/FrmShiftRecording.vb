﻿Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Globalization
Public Class FrmShiftRecording
    Private con As SqlConnection
    Private cmd As SqlCommand
    Private sda As SqlDataAdapter
    Private PageSelect As String = "1"
    Private EmpNo As String
    Private NextShift As String
    Private Category As String
    Private ServerDate As String
    Private nextDateField As String
    Private k_val As String
    Private Ascii As Boolean = False
    Private myTable As DataTable
    Private endDate As DateTime, startDate As DateTime
    Private year As Integer
    Private month As Integer
    Private year1 As Integer
    Private month1 As Integer
    Dim mStatus As Long
    Private forward As DateTime
    Private Sub FrmShiftRecording_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Fill_Company_Details()
        year = DateTime.Today.Year
        month = DateTime.Today.Month
        forward = DateTime.Today.AddMonths(1)
        year1 = forward.Year
        month1 = forward.Month

        startDate = New DateTime(year, month, 1)
        endDate = New DateTime(year1, month1, 1)

        updateCalendar()
        dataGridView1.Columns(0).Width = 50
        dataGridView1.Columns(1).Width = 50
        dataGridView1.Columns(2).Width = 50
        dataGridView1.Columns(3).Width = 50
        dataGridView1.Columns(4).Width = 50
        dataGridView1.Columns(5).Width = 50
        dataGridView1.Columns(6).Width = 50
        Fill_Shiftname()
        Fill_OptionType()
        'newEmp_Details()
        Fill_Days()
        'Auto_update()
        addCombo()
        Leave_load()
        Leave_load1()
        Leave_load2()
    End Sub
    Public Sub Fill_Company_Details()
        SSQL = ""
        SSQL = "Select CompCode + ' | ' + CompName as [Name],CompCode from Company_Mst Order By CompName"
        cmbCompCode.Items.Clear()
        mDataSet = ReturnMultipleValue(SSQL)
        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                cmbCompCode.DataSource = mDataSet.Tables(0)
                cmbCompCode.DisplayMember = "[Name]"
                cmbCompCode.ValueMember = "CompCode"
                'cmbCompCode.Items.Clear()
                'For iRow = 0 To .Rows.Count - 1
                '    cmbCompCode.Items.Add(.Rows(iRow)(0))
                'Next
            End If
        End With
    End Sub
    Private Sub updateCalendar()
        Dim allDates As List(Of DateTime) = getdatesbetween(startDate, endDate)
        myTable = getdata()
        dataGridView1.DataSource = filldates(myTable, allDates)
        lblCurrentDate.Text = (CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(allDates(0).Month) & " " & allDates(0).Year).ToString()
        'lblCurrentDate.Text = (CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(allDates(0).Month) & " ") + allDates(0).Year
    End Sub
    Private Sub addCombo()
        cmbWeekReg.Items.Clear()
        cmbWeekReg.Items.Add("Sunday")
        cmbWeekReg.Items.Add("Monday")
        cmbWeekReg.Items.Add("Tuesday")
        cmbWeekReg.Items.Add("Wednesday")
        cmbWeekReg.Items.Add("Thursday")
        cmbWeekReg.Items.Add("Friday")
        cmbWeekReg.Items.Add("Saturday")
    End Sub
    Private Sub Leave_load()
        Dim dt As DataTable
        Dim qry As String
        Dim sda As SqlDataAdapter
        qry = "Select ID,FieldValue from LeaveRoutine order by ID Asc"
        mDataSet = ReturnMultipleValue(qry)
        cmbShift1.DataSource = mDataSet.Tables(0)
        cmbShift1.DisplayMember = "FieldValue"
        cmbShift1.ValueMember = "ID"
    End Sub
    Private Sub Leave_load1()
        Dim qry As String
        Dim dt As DataTable
        Dim sda As SqlDataAdapter
        qry = "Select ID,FieldValue from LeaveRoutine order by ID Asc"
        mDataSet = ReturnMultipleValue(qry)
        cmbshift2.DataSource = mDataSet.Tables(0)
        cmbshift2.DisplayMember = "FieldValue"
        cmbshift2.ValueMember = "ID"
    End Sub
    Private Sub Leave_load2()
        Dim qry As String
        Dim dt As DataTable
        Dim sda As SqlDataAdapter
        qry = "Select ID,FieldValue from LeaveRoutine order by ID Asc"
        mDataSet = ReturnMultipleValue(qry)
        cmbshift3.DataSource = mDataSet.Tables(0)
        cmbshift3.DisplayMember = "FieldValue"
        cmbshift3.ValueMember = "ID"
    End Sub
    Private Function getdata() As DataTable
        Dim dt As New DataTable()
        Dim dc1 As New DataColumn("Sunday", Type.[GetType]("System.Int32"))
        Dim dc2 As New DataColumn("Monday", Type.[GetType]("System.Int32"))
        Dim dc3 As New DataColumn("Tuesday", Type.[GetType]("System.Int32"))
        Dim dc4 As New DataColumn("Wednesday", Type.[GetType]("System.Int32"))
        Dim dc5 As New DataColumn("Thursday", Type.[GetType]("System.Int32"))
        Dim dc6 As New DataColumn("Friday", Type.[GetType]("System.Int32"))
        Dim dc7 As New DataColumn("Saturday", Type.[GetType]("System.Int32"))

        dt.Columns.Add(dc1)
        dt.Columns.Add(dc2)
        dt.Columns.Add(dc3)
        dt.Columns.Add(dc4)
        dt.Columns.Add(dc5)
        dt.Columns.Add(dc6)
        dt.Columns.Add(dc7)

        Return dt
    End Function
    Private Function filldates(ByVal dt As DataTable, ByVal alldates As List(Of DateTime)) As DataTable
        dt.Rows.Clear()
        Dim week As DataRow = dt.NewRow()
        For Each curentDate As DateTime In alldates
            If curentDate.DayOfWeek = DayOfWeek.Sunday Then
                'loop every 7
                dt.Rows.Add(week)
                'add the existing week
                'start a new week
                week = dt.NewRow()
            End If
            week(curentDate.DayOfWeek.ToString()) = curentDate.Day
        Next
        dt.Rows.Add(week)
        Return dt
    End Function
    Private Sub Fill_OptionType()
        Dim dt As New DataSet()
        Dim Ssql As String = "Select ShiftType, Value from ShiftAllocation order by ID Asc"
        mDataSet = ReturnMultipleValue(Ssql)
        CmbShiftTypeReg.Items.Clear()
        CmbShiftTypeReg.DataSource = mDataSet.Tables(0)
        CmbShiftTypeReg.DisplayMember = "ShiftType"
        CmbShiftTypeReg.ValueMember = "Value"
    End Sub
    Private Sub newEmp_Details()
        Dim dquery As New DataTable()
        Dim dt As New DataSet()
        Dim Ssql As String = ""
        Dim Ssql1 As String = ""
        'cmbMacnoReg.Items.Clear()
        'cmbMacNoPer.Items.Clear()
        Ssql1 = "Select count(*) as number from Shiftrrecording where Ccode= '" & cmbCompCode.SelectedValue & "' and Lcode= '" & cmbLocCode.SelectedValue & "'"
        Dim da1 As New SqlDataAdapter(Ssql1, con)
        mDataSet = ReturnMultipleValue(Ssql1)
        If Convert.ToInt32(mDataSet.Tables(0).Rows(0)("number").ToString()) <= 0 Then
            'Ssql = "Select EmpNo from Employee_Mst where IsActive='Yes' and CatName='LABOUR' Order by MachineID Asc"
            Ssql = "Select EmpNo from Employee_Mst where IsActive='Yes' and CompCode= '" & cmbCompCode.SelectedValue & "' and LocCode='" & cmbLocCode.SelectedValue & "' Order by convert(numeric,EmpNo) Asc"
        ElseIf Convert.ToInt32(mDataSet.Tables(0).Rows(0)("number").ToString()) > 0 Then
            'Ssql = "Select EmpNo from Employee_Mst where IsActive='Yes' and CatName='LABOUR' and MachineID Not in (select MachineNo from Shiftrrecording) Order by MachineID Asc"
            Ssql = "Select EmpNo from Employee_Mst where IsActive='Yes' and CompCode='" & cmbCompCode.SelectedValue & "' and LocCode='" & cmbLocCode.SelectedValue & "' and EmpNo Not in (select MachineNo from Shiftrrecording where Ccode='" & cmbCompCode.SelectedValue & "' and Lcode='" & cmbLocCode.SelectedValue & "') Order by convert(numeric,EmpNo) Asc"
        End If
        sda = New SqlDataAdapter(Ssql, con)
        mDataSet = ReturnMultipleValue(Ssql)
        If mDataSet.Tables(0).Rows.Count > 0 Then



            cmbMacnoReg.DataSource = mDataSet.Tables(0)
            cmbMacnoReg.DisplayMember = "EmpNo"
            cmbMacnoReg.ValueMember = "EmpNo"
            cmbMacNoPer.DataSource = mDataSet.Tables(0)
            cmbMacNoPer.DisplayMember = "EmpNo"
            cmbMacNoPer.ValueMember = "EmpNo"
            txtEmpNameper.Clear()
            txtNamereg.Clear()
        End If
    End Sub
    Private Sub Fill_Days()
        Dim dt As New DataTable()
        Dim Ssql As String = "Select NoDays from ShiftDays"
        mDataSet = ReturnMultipleValue(Ssql)
        If mDataSet.Tables(0).Rows.Count > 0 Then
            txtDaysreg.Text = mDataSet.Tables(0).Rows(0)("NoDays").ToString()
        End If
    End Sub
    Private Function getdatesbetween(ByVal starttime As DateTime, ByVal endtime As DateTime) As List(Of DateTime)
        Dim thetime As DateTime = starttime
        Dim mydates As New List(Of DateTime)()
        mydates.Add(starttime)
        Dim nextDay As DateTime = starttime.AddDays(1)

        While endtime > nextDay
            mydates.Add(nextDay)

            nextDay = nextDay.AddDays(1)
        End While
        Return mydates
    End Function
    Private Sub Fill_Shiftname()
        Dim dt As New DataSet()
        Dim Ssql As String = "Select ShiftDetails,ShiftCode from Shiftdetails order by ShiftCode Asc"
        sda = New SqlDataAdapter(Ssql, con)
        mDataSet = ReturnMultipleValue(Ssql)
        cmbShiftnamereg.Items.Clear()
        cmbShiftNamePer.Items.Clear()
        If mDataSet.Tables(0).Rows.Count > 0 Then


            cmbShiftnamereg.DataSource = mDataSet.Tables(0)
            cmbShiftnamereg.DisplayMember = "ShiftDetails"
            cmbShiftnamereg.ValueMember = "ShiftCode"

            cmbShiftNamePer.DataSource = mDataSet.Tables(0)
            cmbShiftNamePer.DisplayMember = "ShiftDetails"
            cmbShiftNamePer.ValueMember = "ShiftCode"
        End If
    End Sub

    Private Sub cmbMacnoReg_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbMacnoReg.SelectedIndexChanged
        txtNamereg.Text = ""
    End Sub

    Private Sub cmbMacnoReg_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cmbMacnoReg.KeyDown
        If e.KeyCode = Keys.Enter Then
            If tabControl1.SelectedTab Is tabReg Then
                Category = "R"
            ElseIf tabControl1.SelectedTab Is tabPer Then
                Category = "P"
            End If
            If cmbCompCode.SelectedValue = "" Then
                MessageBox.Show("Select the Company Code.", "Altius InfoSystems", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            ElseIf cmbLocCode.SelectedValue = "" Then
                MessageBox.Show("Select the LocationCode.", "Altius InfoSystems", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Else


                Dim dt As New DataTable()
                Dim Ssql As String = "Select MachineNo from Shiftrrecording WHERE MachineNo='" & Trim(cmbMacnoReg.Text) & "' and Ccode='" & cmbCompCode.SelectedValue & "' and Lcode='" & cmbLocCode.SelectedValue & "'"
                sda = New SqlDataAdapter(Ssql, con)
                mDataSet = ReturnMultipleValue(Ssql)
                If mDataSet.Tables(0).Rows.Count = 0 Then
                    mDataSet = Nothing
                    btnEdit.Text = "Edit"
                Else
                    mDataSet = Nothing
                    btnEdit.Text = "Edit"
                    Call btnEdit_Click(sender, e)
                End If
                'Dim Ssql As String = ""
                Ssql = ""
                If btnEdit.Text = "Edit" Then
                    'Ssql = "Select FirstName + '' + LastName AS NAME from Employee_Mst WHERE EmpNo='" & cmbMacnoReg.Text.Trim() & "' AND " & " IsActive='Yes' and CatName='LABOUR'"
                    Ssql = "Select FirstName + '' + LastName AS NAME from Employee_Mst WHERE EmpNo='" & cmbMacnoReg.Text.Trim() & "' AND " & " IsActive='Yes' and CompCode = '" & cmbCompCode.SelectedValue & "' and LocCode = '" & cmbLocCode.SelectedValue & "'"
                    sda = New SqlDataAdapter(Ssql, con)
                    mDataSet = ReturnMultipleValue(Ssql)
                    
                    If mDataSet.Tables(0).Rows.Count > 0 Then


                        txtNamereg.Text = mDataSet.Tables(0).Rows(0)("NAME").ToString()
                    End If
                ElseIf btnEdit.Text = "New" Then
                    'Ssql = "Select EmpName,ShiftName,ShiftType,WeekOff,NoDays,Category,Shift1,Shift2,shift3,WeekStartDay,StartingDate,AutoRotateShift from Shiftrrecording Where MachineNo = '" & cmbMacnoReg.Text.Trim() & "' and MachineNo in (Select EmpNo from Employee_Mst where IsActive='Yes' and CatName='LABOUR')"
                    Ssql = "Select EmpName,ShiftName,ShiftType,WeekOff,NoDays,Category,Shift1,Shift2,shift3,WeekStartDay,StartingDate,AutoRotateShift from Shiftrrecording Where MachineNo = '" & cmbMacnoReg.Text.Trim() & "' and Ccode='" & cmbCompCode.SelectedValue & "' and Lcode='" & cmbLocCode.SelectedValue & "' and MachineNo in (Select EmpNo from Employee_Mst where IsActive='Yes' and CompCode = '" & cmbCompCode.SelectedValue & "' and LocCode = '" & cmbLocCode.SelectedValue & "')"

                    mDataSet = ReturnMultipleValue(Ssql)
                    If mDataSet.Tables(0).Rows.Count > 0 Then


                        If mDataSet.Tables(0).Rows(0)("Category").ToString() = "R" Then
                            txtNamereg.Text = mDataSet.Tables(0).Rows(0)("EmpName").ToString()
                            cmbShiftnamereg.SelectedValue = mDataSet.Tables(0).Rows(0)("ShiftName").ToString()
                            CmbShiftTypeReg.SelectedValue = mDataSet.Tables(0).Rows(0)("ShiftType").ToString()
                            cmbWeekReg.Text = mDataSet.Tables(0).Rows(0)("WeekOff").ToString()
                            txtDaysreg.Text = mDataSet.Tables(0).Rows(0)("NoDays").ToString()
                            txtstartreg.Text = DateTime.Today.ToShortDateString()
                            'cmbShift1.SelectedValue = mDataSet.Tables(0).Rows(0)("Shift1").ToString()
                            'cmbshift2.SelectedValue = dt.Rows(0)("Shift2").ToString()
                            'cmbshift3.SelectedValue = mDataSet.Tables(0).Rows(0)("Shift3").ToString()
                            txtStartDay.Text = mDataSet.Tables(0).Rows(0)("WeekStartDay").ToString()
                            txtstartreg.Text = mDataSet.Tables(0).Rows(0)("StartingDate").ToString()
                            If mDataSet.Tables(0).Rows(0)("AutoRotateShift").ToString() = "1" Then
                                checkBox1.Checked = True
                            Else
                                checkBox1.Checked = False
                            End If

                        Else
                            txtNamereg.Text = mDataSet.Tables(0).Rows(0)("EmpName").ToString()
                            cmbShiftnamereg.SelectedValue = mDataSet.Tables(0).Rows(0)("ShiftName").ToString()
                            cmbWeekReg.Text = mDataSet.Tables(0).Rows(0)("WeekOff").ToString()
                            txtStartDay.Text = mDataSet.Tables(0).Rows(0)("WeekStartDay").ToString()
                            txtstartreg.Text = mDataSet.Tables(0).Rows(0)("StartingDate").ToString()
                        End If
                    End If

                End If

            End If
            End If
    End Sub

    Private Sub btndays_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndays.Click
        If btndays.Text = "Edit" Then
            txtDaysreg.Enabled = True
            btndays.Text = "Save"
        ElseIf btndays.Text = "Save" Then
            txtDaysreg.Enabled = False
            btndays.Text = "Edit"
        End If
    End Sub
    Private Sub ChangeColor(ByVal DayValue As String)
        'dataGridView1.Rows[1].Cells[1].Selected=true;
        Dim day As String = ""
        For i As Integer = 0 To cmbWeekReg.Items.Count - 1
            day = cmbWeekReg.Items(i).ToString()
            dataGridView1.Columns(day).DefaultCellStyle.BackColor = System.Drawing.Color.White
        Next
        dataGridView1.Columns(DayValue).DefaultCellStyle.BackColor = System.Drawing.Color.Red
        'for (int i = 0; i < dataGridView1.Rows.Count-1; i++)
        '{
        '    MessageBox.Show(dataGridView1.Rows[i].Cells[DayValue].Value + "");
        '    dataGridView1.Rows[i].DefaultCellStyle.BackColor = System.Drawing.Color.Red;
        '}
    End Sub

    Private Sub cmbWeekReg_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbWeekReg.SelectedIndexChanged
        ChangeColor(cmbWeekReg.Text)
    End Sub

    Private Sub dataGridView1_CellClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dataGridView1.CellClick
        Dim tmpdate As String
        Dim ErrFlag As Boolean = False
        tmpdate = ((dataGridView1.CurrentCell.Value.ToString() & " ") + lblCurrentDate.Text)
        tmpdate = (Convert.ToDateTime(tmpdate).ToShortDateString())
        If Convert.ToDateTime(tmpdate) < Convert.ToDateTime(ServerDate) Then
            MessageBox.Show("Select the date properly.", "Altius InfoSystems", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            ErrFlag = True
            txtstartreg.Text = ""
        ElseIf (cmbWeekReg.Text = "") OrElse (cmbWeekReg.Text Is Nothing) Then
            MessageBox.Show("Select the Week off properly.", "Altius InfoSystems", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            ErrFlag = True
            txtstartreg.Text = ""
        ElseIf cmbWeekReg.Text = Convert.ToDateTime(tmpdate).DayOfWeek.ToString() Then
            MessageBox.Show("Select the date properly.", "Altius InfoSystems", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            ErrFlag = True
            txtstartreg.Text = ""
        End If
        If Not ErrFlag Then
            txtstartreg.Text = tmpdate
            txtStartDay.Text = Convert.ToDateTime(tmpdate).DayOfWeek.ToString()
        End If
    End Sub

    Private Sub btnprevious_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnprevious.Click
        startDate = startDate.AddMonths(-1)
        endDate = endDate.AddMonths(-1)
        updateCalendar()
    End Sub

    Private Sub btnForward_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnForward.Click
        startDate = startDate.AddMonths(1)
        endDate = endDate.AddMonths(1)
        updateCalendar()
    End Sub

    Private Sub cmbMacNoPer_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbMacNoPer.SelectedIndexChanged
        txtEmpNameper.Text = ""
    End Sub

    Private Sub cmbMacNoPer_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cmbMacNoPer.KeyDown
        If e.KeyCode = Keys.Enter Then
            Call Machine_KeyDown_clr()
            If tabControl1.SelectedTab Is tabReg Then
                Category = "R"
            ElseIf tabControl1.SelectedTab Is tabPer Then
                Category = "P"
            End If
            If tabControl1.SelectedTab Is tabReg Then
                Category = "R"
            ElseIf tabControl1.SelectedTab Is tabPer Then
                Category = "P"
            End If
            If cmbCompCode.SelectedValue = "" Then
                MessageBox.Show("Select the Company Code.", "Altius InfoSystems", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            ElseIf cmbLocCode.SelectedValue = "" Then
                MessageBox.Show("Select the LocationCode.", "Altius InfoSystems", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Else
                Dim dt As New DataTable()
                Dim Ssql As String = ""

                Ssql = "Select MachineNo from Shiftrrecording WHERE MachineNo='" & Trim(cmbMacNoPer.Text) & "' and Ccode='" & cmbCompCode.SelectedValue & "' and Lcode='" & cmbLocCode.SelectedValue & "'"
                sda = New SqlDataAdapter(Ssql, con)
                mDataSet = ReturnMultipleValue(Ssql)
                If mDataSet.Tables(0).Rows.Count = 0 Then
                    mDataSet = Nothing
                    btnEdit.Text = "Edit"
                Else
                    mDataSet = Nothing
                    btnEdit.Text = "Edit"
                    Call btnEdit_Click(sender, e)
                End If
                If btnEdit.Text = "Edit" Then
                    'Ssql = "Select FirstName + '' + LastName AS NAME from Employee_Mst WHERE EmpNo='" & cmbMacNoPer.Text.Trim() & "' AND " & " IsActive='Yes' and CatName='LABOUR'"
                    Ssql = "Select FirstName + '' + LastName AS NAME from Employee_Mst WHERE EmpNo='" & cmbMacNoPer.Text.Trim() & "' AND " & " IsActive='Yes' and CompCode = '" & cmbCompCode.SelectedValue & "' and LocCode = '" & cmbLocCode.SelectedValue & "'"
                    sda = New SqlDataAdapter(Ssql, con)
                    mDataSet = ReturnMultipleValue(Ssql)
                    txtEmpNameper.Text = mDataSet.Tables(0).Rows(0)("NAME").ToString()
                ElseIf btnEdit.Text = "New" Then
                    'Ssql = "Select EmpName,ShiftName,ShiftType,WeekOff,NoDays,Category,WeekStartDay,StartingDate from Shiftrrecording Where MachineNo = '" & cmbMacNoPer.Text.Trim() & "' and MachineNo in (Select EmpNo from Employee_Mst where IsActive='Yes' and CatName='LABOUR')"
                    Ssql = "Select EmpName,ShiftName,ShiftType,WeekOff,NoDays,Category,WeekStartDay,StartingDate from Shiftrrecording Where MachineNo = '" & cmbMacNoPer.Text.Trim() & "' and Ccode = '" & cmbCompCode.SelectedValue & "' and Lcode = '" & cmbLocCode.SelectedValue & "' and MachineNo in (Select EmpNo from Employee_Mst where IsActive='Yes' and CompCode = '" & cmbCompCode.SelectedValue & "' and LocCode = '" & cmbLocCode.SelectedValue & "')"
                    sda = New SqlDataAdapter(Ssql, con)
                    mDataSet = ReturnMultipleValue(Ssql)
                    If mDataSet.Tables(0).Rows(0)("Category").ToString() = "R" Then
                        txtEmpNameper.Text = mDataSet.Tables(0).Rows(0)("EmpName").ToString()
                        cmbShiftNamePer.SelectedValue = mDataSet.Tables(0).Rows(0)("ShiftName").ToString()

                        cmbWeekReg.Text = mDataSet.Tables(0).Rows(0)("WeekOff").ToString()
                        txtstartreg.Text = mDataSet.Tables(0).Rows(0)("StartingDate").ToString()
                        txtStartDay.Text = mDataSet.Tables(0).Rows(0)("WeekStartDay").ToString()
                    Else
                        txtEmpNameper.Text = mDataSet.Tables(0).Rows(0)("EmpName").ToString()
                        cmbShiftNamePer.SelectedValue = mDataSet.Tables(0).Rows(0)("ShiftName").ToString()
                        cmbWeekReg.Text = mDataSet.Tables(0).Rows(0)("WeekOff").ToString()
                        txtstartreg.Text = mDataSet.Tables(0).Rows(0)("StartingDate").ToString()
                        txtStartDay.Text = mDataSet.Tables(0).Rows(0)("WeekStartDay").ToString()
                    End If
                End If
                End If
            End If
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If btnEdit.Text = "Edit" Then
            'EditEmp_details()
        ElseIf btnEdit.Text = "New" Then
            'newEmp_Details()
        End If
        If btnEdit.Text = "Edit" Then
            'RegularCalender.Enabled = false;
            btnEdit.Text = "New"
        Else
            'RegularCalender.Enabled = true;
            btnEdit.Text = "Edit"
        End If
    End Sub
    Private Sub EditEmp_details()
        Dim Ssql As String
        'Ssql = "Select EM.EmpNo from Employee_Mst EM inner Join Shiftrrecording SR on " & " EM.EmpNo = SR.MachineNo where EM.IsActive='Yes' " & " And EM.CatName='LABOUR'"
        Ssql = "Select EM.EmpNo from Employee_Mst EM inner Join Shiftrrecording SR on " & " EM.EmpNo = SR.MachineNo where EM.IsActive='Yes' and EM.CompCode = '" & cmbCompCode.SelectedValue & "' and LocCode = '" & cmbLocCode.SelectedValue & "'"
        mDataSet = ReturnMultipleValue(Ssql)
        If mDataSet.Tables(0).Rows.Count > 0 Then


            'cmbMacnoReg.Items.Clear();
            cmbMacnoReg.DataSource = mDataSet.Tables(0)
            cmbMacnoReg.DisplayMember = "EmpNo"
            cmbMacnoReg.ValueMember = "EmpNo"
            'cmbMacNoPer.Items.Clear();
            cmbMacNoPer.DataSource = mDataSet.Tables(0)
            cmbMacNoPer.DisplayMember = "EmpNo"
            cmbMacNoPer.ValueMember = "EmpNo"
        End If
    End Sub
    Private Sub clr()
        dataGridView1.Columns(0).Width = 50
        dataGridView1.Columns(1).Width = 50
        dataGridView1.Columns(2).Width = 50
        dataGridView1.Columns(3).Width = 50
        dataGridView1.Columns(4).Width = 50
        dataGridView1.Columns(5).Width = 50
        dataGridView1.Columns(6).Width = 50
        'Fill_Shiftname()
        'Fill_WeekDays();
        'Fill_OptionType()
        newEmp_Details()
        Fill_Days()
        'Auto_update()
        addCombo()
        txtstartreg.Text = ""
        txtNamereg.Text = ""
        txtEmpNameper.Text = ""
        txtStartDay.Text = ""
        cmbShift1.SelectedValue = "0"
        cmbshift2.SelectedValue = "0"
        cmbshift3.SelectedValue = "0"
        checkBox1.Checked = False
        btnEdit.Text = "Edit"
    End Sub

    Private Sub Machine_KeyDown_clr()
        'dataGridView1.Columns(0).Width = 50
        'dataGridView1.Columns(1).Width = 50
        'dataGridView1.Columns(2).Width = 50
        'dataGridView1.Columns(3).Width = 50
        'dataGridView1.Columns(4).Width = 50
        'dataGridView1.Columns(5).Width = 50
        'dataGridView1.Columns(6).Width = 50
        'Fill_Shiftname()
        'Fill_WeekDays();
        'Fill_OptionType()
        'newEmp_Details()
        'Fill_Days()
        'Auto_update()
        'addCombo()
        txtstartreg.Text = ""
        txtNamereg.Text = ""
        txtEmpNameper.Text = ""
        txtStartDay.Text = ""
        cmbShift1.SelectedValue = "0"
        cmbshift2.SelectedValue = "0"
        cmbshift3.SelectedValue = "0"
        checkBox1.Checked = False
        btnEdit.Text = "Edit"
    End Sub

    Private Sub btnclr_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclr.Click
        clr()
    End Sub

    Private Sub Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel.Click
        Me.Close()
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim ErrFlag As Boolean = False
        Dim Autorotate As String
        Dim insertrec As String = ""
        If tabControl1.SelectedTab Is tabReg Then
            If (cmbMacnoReg.Text = "") OrElse (cmbMacnoReg.Text Is Nothing) Then
                MessageBox.Show("Enter the Employee No Properly", "Altius InfoSystems", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                ErrFlag = True
            ElseIf (txtNamereg.Text = "") OrElse (txtNamereg.Text Is Nothing) Then
                MessageBox.Show("Select the Employee No Properly", "Altius InfoSystems", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                ErrFlag = True
            ElseIf (cmbShiftnamereg.Text = "") OrElse (cmbShiftnamereg.Text Is Nothing) Then
                MessageBox.Show("Select the Shift name Properly", "Altius InfoSystems", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                ErrFlag = True
            ElseIf (CmbShiftTypeReg.Text = "") OrElse (CmbShiftTypeReg.Text Is Nothing) Then
                MessageBox.Show("Select the Shift Type Properly", "Altius InfoSystems", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                ErrFlag = True
            ElseIf (cmbWeekReg.Text = "") OrElse (cmbWeekReg.Text Is Nothing) Then
                MessageBox.Show("Select the week off Properly", "Altius InfoSystems", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                ErrFlag = True
            ElseIf (txtDaysreg.Text = "") OrElse (txtDaysreg.Text Is Nothing) OrElse (txtDaysreg.Text = "0") Then
                MessageBox.Show("Enter the shift Routine Days No Properly", "Altius InfoSystems", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                ErrFlag = True
            ElseIf (txtstartreg.Text = "") OrElse (txtstartreg.Text Is Nothing) Then
                MessageBox.Show("Enter the start date Properly", "Altius InfoSystems", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                ErrFlag = True
            ElseIf Convert.ToDateTime(txtstartreg.Text) < DateTime.Today Then
                MessageBox.Show("Enter the start date Properly", "Altius InfoSystems", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                ErrFlag = True
            ElseIf btndays.Text = "Save" Then
                MessageBox.Show("Save the shift Routine Days", "Altius InfoSystems", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                ErrFlag = True
            ElseIf cmbCompCode.SelectedValue = "" Then
                MessageBox.Show("Save the Company Code", "Altius InfoSystems", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                ErrFlag = True
            ElseIf cmbLocCode.SelectedValue = "" Then
                MessageBox.Show("Save the Location Code", "Altius InfoSystems", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                ErrFlag = True
            End If
            If checkBox1.Checked = True Then
                Autorotate = "1"
            Else
                Autorotate = "0"
            End If
            If Not ErrFlag Then
                Dim dt As New DataTable()
                Dim q As String = "Select MachineNo from Shiftrrecording where MachineNo = '" & cmbMacnoReg.Text.Trim() & "' and Ccode = '" & cmbCompCode.SelectedValue & "' and Lcode = '" & cmbLocCode.SelectedValue & "'"
                mDataSet = ReturnMultipleValue(q)
                If mDataSet.Tables(0).Rows.Count > 0 Then
                    EmpNo = mDataSet.Tables(0).Rows(0)("MachineNo").ToString()
                Else
                    EmpNo = "0"
                End If

                If EmpNo.Trim() = cmbMacnoReg.Text.Trim() Then
                    Dim dt1 As New DataTable()
                    Dim q_val As String = "Select ID,ShiftType,First,Second,Third from ShiftAllocation where Value='" & CmbShiftTypeReg.SelectedValue & "' and  (First='" & cmbShiftnamereg.SelectedValue & "' or Second = '" & cmbShiftnamereg.SelectedValue & "' " & " or Third='" & cmbShiftnamereg.SelectedValue & "')"

                    mDataSet = ReturnMultipleValue(q_val)
                    If mDataSet.Tables(0).Rows(0)("First").ToString() = cmbShiftnamereg.SelectedValue.ToString() Then
                        NextShift = mDataSet.Tables(0).Rows(0)("Second").ToString()
                    ElseIf mDataSet.Tables(0).Rows(0)("Second").ToString() = cmbShiftnamereg.SelectedValue.ToString() Then
                        NextShift = mDataSet.Tables(0).Rows(0)("Third").ToString()
                    ElseIf mDataSet.Tables(0).Rows(0)("Third").ToString() = cmbShiftnamereg.SelectedValue.ToString() Then
                        NextShift = mDataSet.Tables(0).Rows(0)("First").ToString()
                    Else
                        NextShift = "0"
                    End If

                    Dim enddate As DateTime
                    enddate = Convert.ToDateTime(txtstartreg.Text).AddDays(Convert.ToInt32(txtDaysreg.Text))
                    If enddate.DayOfWeek.ToString() = cmbWeekReg.Text Then
                        enddate = enddate.AddDays(1)
                    End If
                    Dim insertval As String = "Update Shiftrrecording set Category = 'R',ShiftName = '" & cmbShiftnamereg.SelectedValue & "',ShiftType='" & CmbShiftTypeReg.SelectedValue & "', " & " WeekOff='" & cmbWeekReg.Text & "',NoDays='" & txtDaysreg.Text & "',StartingDate=convert(Datetime,'" & txtstartreg.Text & "',105), " & " NextDate=convert(datetime,'" & enddate & "',105),NextShift='" & NextShift & "',Shift1='" & cmbShift1.SelectedValue & "',Shift2='" & cmbshift2.SelectedValue & "',Shift3='" & cmbshift3.SelectedValue & "',WeekStartDay='" & txtStartDay.Text & "',AutoRotateShift='" & Autorotate & "' where MachineNo='" & cmbMacnoReg.Text & "' and Ccode='" & cmbCompCode.SelectedValue & "' and Lcode = '" & cmbLocCode.SelectedValue & "'"

                    mStatus = InsertDeleteUpdate(insertval)
                    insertrec = "Insert into ShiftReport (MachineNo,EmpName,Category,Shiftname,ShiftType,Weekoff,StartingDate,NextDate,Ccode,Lcode) values ('" & cmbMacnoReg.Text & "','" & txtNamereg.Text & "','" & "R" & "','" & cmbShiftnamereg.SelectedValue & "','" & CmbShiftTypeReg.SelectedValue & "','" & cmbWeekReg.Text & "',convert(Datetime,'" & txtstartreg.Text & "',105),convert(datetime,'" & enddate & "',105),'" & cmbCompCode.SelectedValue & "','" & cmbLocCode.SelectedValue & "')"
                    mStatus = InsertDeleteUpdate(insertrec)
                    If mStatus > 0 Then
                        MessageBox.Show("Updated Successfully...!", "Altius InfoSystems", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
                        btnEdit.Text = "Edit"
                    End If
                    
                    clr()
                Else
                    Dim dt1 As New DataTable()
                    Dim q_val As String = "Select ID,ShiftType,First,Second,Third from ShiftAllocation where Value ='" & CmbShiftTypeReg.SelectedValue & "' and  (First='" & cmbShiftnamereg.SelectedValue & "' or Second ='" & cmbShiftnamereg.SelectedValue & "' or Third='" & cmbShiftnamereg.SelectedValue & "')"

                    mDataSet = ReturnMultipleValue(q_val)
                    If mDataSet.Tables(0).Rows(0)("First").ToString() = cmbShiftnamereg.SelectedValue.ToString() Then
                        NextShift = mDataSet.Tables(0).Rows(0)("Second").ToString()
                    ElseIf mDataSet.Tables(0).Rows(0)("Second").ToString() = cmbShiftnamereg.SelectedValue.ToString() Then
                        NextShift = mDataSet.Tables(0).Rows(0)("Third").ToString()
                    ElseIf dt1.Rows(0)("Third").ToString() = cmbShiftnamereg.SelectedValue.ToString() Then
                        NextShift = mDataSet.Tables(0).Rows(0)("First").ToString()
                    Else
                        NextShift = "0"
                    End If

                    Dim enddate As DateTime
                    enddate = Convert.ToDateTime(txtstartreg.Text).AddDays(Convert.ToInt32(txtDaysreg.Text))
                    If enddate.DayOfWeek.ToString() = cmbWeekReg.Text Then
                        enddate = enddate.AddDays(1)
                    End If
                    Dim insertval As String = "Insert into Shiftrrecording (MachineNo,EmpName,Category,ShiftName,ShiftType,WeekOff,NoDays,StartingDate,NextDate,NextShift,Shift1,Shift2,Shift3,WeekStartDay,AutoRotateShift,Ccode,Lcode) " & " Values('" & cmbMacnoReg.Text.Trim() & "','" & txtNamereg.Text.Trim() & "','R','" & cmbShiftnamereg.SelectedValue & "','" & CmbShiftTypeReg.SelectedValue & "','" & cmbWeekReg.Text & "','" & txtDaysreg.Text.Trim() & "',convert(Datetime,'" & txtstartreg.Text & "',105),convert(datetime,'" & enddate & "',105),'" & NextShift & "','" & cmbShift1.SelectedValue & "','" & cmbshift2.SelectedValue & "','" & cmbshift3.SelectedValue & "','" & txtStartDay.Text & "','" & Autorotate & "','" & cmbCompCode.SelectedValue & "','" & cmbLocCode.SelectedValue & "')"
                    mStatus = InsertDeleteUpdate(insertval)
                    insertrec = "Insert into ShiftReport (MachineNo,EmpName,Category,Shiftname,ShiftType,Weekoff,StartingDate,NextDate,Ccode, Lcode) values ('" & cmbMacnoReg.Text & "','" & txtNamereg.Text & "','" & "R" & "','" & cmbShiftnamereg.SelectedValue & "','" & CmbShiftTypeReg.SelectedValue & "','" & cmbWeekReg.Text & "',convert(Datetime,'" & txtstartreg.Text & "',105),convert(datetime,'" & enddate & "',105),'" & cmbCompCode.SelectedValue & "','" & cmbLocCode.SelectedValue & "')"

                    mStatus = InsertDeleteUpdate(insertrec)
                    If mStatus > 0 Then
                        MessageBox.Show("Added Successfully...!", "Altius InfoSystems", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
                        clr()
                    End If
                    

                End If

            End If
        ElseIf tabControl1.SelectedTab Is tabPer Then
            If (cmbMacNoPer.Text = "") OrElse (cmbMacNoPer.Text Is Nothing) Then
                MessageBox.Show("Enter the Employee No Properly", "Altius InfoSystems", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                ErrFlag = True
            ElseIf (cmbShiftNamePer.Text = "") OrElse (cmbShiftNamePer.Text Is Nothing) Then
                MessageBox.Show("Enter the Shift Name Properly", "Altius InfoSystems", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                ErrFlag = True
            ElseIf (cmbWeekReg.Text = "") OrElse (cmbWeekReg.Text Is Nothing) Then
                MessageBox.Show("Enter the Week off Properly", "Altius InfoSystems", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                ErrFlag = True
            ElseIf (txtEmpNameper.Text = "") OrElse (txtEmpNameper.Text Is Nothing) Then
                MessageBox.Show("Enter the Employee Name Properly", "Altius InfoSystems", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                ErrFlag = True
            ElseIf (txtstartreg.Text = "") OrElse (txtstartreg.Text Is Nothing) Then
                MessageBox.Show("Enter the Start date Properly", "Altius InfoSystems", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                ErrFlag = True
            ElseIf Convert.ToDateTime(txtstartreg.Text) < DateTime.Today Then
                MessageBox.Show("Enter the Start date Properly", "Altius InfoSystems", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                ErrFlag = True
            End If
            If Not ErrFlag Then
                Dim dt As New DataTable()
                Dim q As String = "Select MachineNo from Shiftrrecording where MachineNo = '" & cmbMacNoPer.Text.Trim() & "' and Ccode='" & cmbCompCode.SelectedValue & "' and Lcode='" & cmbLocCode.SelectedValue & "'"
                mDataSet = ReturnMultipleValue(q)
                
                If mDataSet.Tables(0).Rows.Count > 0 Then
                    EmpNo = mDataSet.Tables(0).Rows(0)("MachineNo").ToString()
                Else
                    EmpNo = "0"
                End If

                If EmpNo = cmbMacNoPer.Text Then
                    Dim insertval As String = "Update Shiftrrecording Set ShiftName='" & cmbShiftNamePer.SelectedValue & "',ShiftType=0,WeekOff= '" & cmbWeekReg.Text & "',NoDays=0,StartingDate=convert(Datetime,'" & txtstartreg.Text & "',105),NextDate=convert(datetime,'" & txtstartreg.Text & "',105),NextShift='" & cmbShiftNamePer.SelectedValue & "',WeekStartDay='" & txtStartDay.Text & "',AutoRotateShift='" & "0" & "',Category='" & "P" & "' where MachineNo='" & cmbMacNoPer.Text.Trim() & "' and Ccode='" & cmbCompCode.SelectedValue & "' and Lcode='" & cmbLocCode.SelectedValue & "'"
                    mStatus = InsertDeleteUpdate(insertval)
                    insertrec = "Insert into ShiftReport (MachineNo,EmpName,Category,Shiftname,ShiftType,Weekoff,StartingDate,NextDate,Ccode,Lcode) values ('" & cmbMacNoPer.Text & "','" & txtEmpNameper.Text & "','" & "P" & "','" & cmbShiftNamePer.SelectedValue & "','" & "0" & "','" & cmbWeekReg.Text & "',convert(Datetime,'" & txtstartreg.Text & "',105),convert(datetime,'" & endDate & "',105),'" & cmbCompCode.SelectedValue & "','" & cmbLocCode.SelectedValue & "')"

                    mStatus = InsertDeleteUpdate(insertrec)
                    If mStatus > 0 Then
                        MessageBox.Show("Updated Successfully...!", "Altius InfoSystems", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
                        clr()
                    End If
                    
                Else
                    Dim insertval As String = ""
                    insertval = "Insert into Shiftrrecording (MachineNo,EmpName,Category,ShiftName,ShiftType,WeekOff,NoDays,StartingDate,NextDate,NextShift,WeekStartDay,AutoRotateShift,Ccode,Lcode)"
                    insertval &= " Values('" & Trim(cmbMacNoPer.Text) & "','" & Trim(txtEmpNameper.Text) & "','P','" & cmbShiftNamePer.SelectedValue & "','0'"
                    insertval &= ",'" & cmbWeekReg.Text & "','0',convert(Datetime,'" & txtstartreg.Text & "',105),convert(datetime,'" & txtstartreg.Text & "',105),'" & cmbShiftNamePer.SelectedValue & "','" & txtStartDay.Text & "','" & "0" & "','" & cmbCompCode.SelectedValue & "','" & cmbLocCode.SelectedValue & "')"
                    mStatus = InsertDeleteUpdate(insertval)
                    insertrec = "Insert into ShiftReport (MachineNo,EmpName,Category,Shiftname,ShiftType,Weekoff,StartingDate,NextDate,Ccode,Lcode) values ('" & cmbMacNoPer.Text & "','" & txtEmpNameper.Text & "','" & "P" & "','" & cmbShiftNamePer.SelectedValue & "','" & "0" & "','" & cmbWeekReg.Text & "',convert(Datetime,'" & txtstartreg.Text & "',105),convert(datetime,'" & txtstartreg.Text & "',105),'" & cmbCompCode.SelectedValue & "','" & cmbLocCode.SelectedValue & "')"
                    mStatus = InsertDeleteUpdate(insertrec)
                    If mStatus > 0 Then
                        MessageBox.Show("Added Successfully...!", "Altius InfoSystems", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
                        clr()
                    End If


                End If
                End If
            End If
    End Sub

    Private Sub btnReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReport.Click
        mdiMain.Form_Load(FrmShiftRep)
        'FrmShiftRep.Show()
        'Me.Close()
    End Sub

    Private Sub cmbLocCode_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbLocCode.SelectedIndexChanged
        'clr()
    End Sub

    Private Sub cmbCompCode_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbCompCode.SelectedIndexChanged
        'clr()
    End Sub

    Private Sub cmbLocCode_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cmbLocCode.KeyDown
        If e.KeyCode = Keys.Enter Then
            newEmp_Details()
        End If

    End Sub

    Private Sub cmbCompCode_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cmbCompCode.KeyDown
        If e.KeyCode = Keys.Enter Then
            If Trim(cmbCompCode.Text) = "" Then Exit Sub
            Dim iStr() As String
            iStr = Split(Trim(cmbCompCode.Text), " | ")

            SSQL = ""
            SSQL = "Select LocCode + ' | ' + LocName as [Name],LocCode from Location_Mst Where Compcode='"
            SSQL = SSQL & "" & iStr(0) & "'"

            mDataSet = ReturnMultipleValue(SSQL)

            With mDataSet.Tables(0)
                cmbLocCode.DataSource = mDataSet.Tables(0)
                cmbLocCode.DisplayMember = "[Name]"
                cmbLocCode.ValueMember = "LocCode"
                If .Rows.Count > 0 Then
                    'cmbLocCode.Items.Clear()
                    'For iRow = 0 To .Rows.Count - 1
                    '    cmbLocCode.Items.Add(.Rows(iRow)(0))
                    'Next
                End If
            End With
        End If
    End Sub

    Private Sub btnHoliday_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHoliday.Click
        mdiMain.Form_Load(FrmNFh)
        'Me.Close()
    End Sub
End Class