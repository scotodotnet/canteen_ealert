﻿#Region "Imports"
Imports System
Imports System.Data
Imports System.Data.OleDb
#End Region
Public Class frmDesign
    Dim mStatus As Boolean
    Private Sub frmDepartment_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormAlign(Me)
        Fill_Grid_Details()
        mStatus = True
    End Sub
    Public Sub Fill_Grid_Details()
        With fgDesign
            .Clear()
            .Cols.Fixed = 0
            .Rows.Fixed = 0
            .Cols.Count = 1
            .Rows.Count = 1
            .Cols(0).Width = 4000
        End With
        mDataSet = New DataSet
        SSQL = ""
        SSQL = "Select DesignName from Designation_Mst Order By DesignName"

        mDataSet = ReturnMultipleValue(SSQL)

        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                fgDesign.Rows.Count = .Rows.Count + 1
                For irow = 0 To .Rows.Count - 1
                    fgDesign(irow, 0) = .Rows(irow)(0)
                Next
            End If
        End With

    End Sub
    Public Sub Save_Validation()
        mStatus = True
        For irow = 0 To fgDesign.Rows.Count - 1
            If UCase(Remove_Single_Quote(Trim(txtDesignName.Text))) = Trim(fgDesign(irow, 0)) Then
                mStatus = False
                MessageBox.Show("Designation Already Exists.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            End If
        Next
    End Sub
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Save_Validation()
        If mStatus = True Then
            SSQL = ""
            SSQL = "Insert into Designation_Mst Values( '" & UCase(Remove_Single_Quote(Trim(txtDesignName.Text))) & "')"
            Dim mSaveStatus As Long
            mSaveStatus = InsertDeleteUpdate(SSQL)
            If mSaveStatus > 0 Then
                txtDesignName.Text = ""
                Fill_Grid_Details()
                txtDesignName.Focus()
                Exit Sub
            End If
        End If
    End Sub
End Class
