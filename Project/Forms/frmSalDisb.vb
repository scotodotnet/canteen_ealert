﻿#Region "Imports"

Imports System
Imports System.Data
Imports System.Globalization
Imports System.Threading

#End Region

Public Class frmSalDisb

    Dim iStr1() As String
    Dim iStr2() As String

    Private Sub btnImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImport.Click

        Dim oXL As Object = CreateObject("Excel.Application")
        Dim oWBK As Object
        Dim oWS As Object
        Dim oRNG As Object
        Dim strReportPath As String = ""
        Dim mRowCount As Long = 0
        Dim sFilePath As String = "'"

        Try


            With dlgOpen

                .Title = "Select a File to Open"
                .Filter = "All Microsoft Office Excel Files|*.xls;*.xlsx"
                .DefaultExt = "*.xls"
                .FilterIndex = 1
                .FileName = ""
            End With

            If dlgOpen.ShowDialog = Windows.Forms.DialogResult.Cancel Then
                Exit Sub
            End If

            sFilePath = dlgOpen.FileName

            If sFilePath = "" Then Exit Sub

            If System.IO.File.Exists(sFilePath) = False Then
                Exit Sub
            End If

            Fill_Grid_Title()

            'strReportPath = Application.StartupPath & "\Salary\" & "Salary.xls"

            strReportPath = sFilePath

            If Not IO.File.Exists(strReportPath) Then
                Throw (New Exception("Unable to locate report file:" & vbCrLf & strReportPath))
                Exit Sub
            End If

            oWBK = oXL.Workbooks.Open(strReportPath)
            oWS = oXL.Worksheets(1)

            oRNG = oWS.Cells(1, 2)
            mRowCount = Val(oRNG.Value) + 2
            _flex.Rows = mRowCount - 1

            For iRow As Integer = 3 To mRowCount
                For iCol As Integer = 1 To 3
                    oRNG = oWS.Cells(iRow, iCol)
                    _flex.set_TextMatrix(iRow - 2, iCol - 1, oRNG.Value)
                Next
            Next

            oWBK.Close()

            Grid_Calculation()

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Finally
            oWBK = Nothing
            oXL = Nothing
        End Try
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Close()
    End Sub

    Private Sub frmSalDisb_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormAlign(Me)
        dtpPayFrom.Value = Now
        dtpPayTo.Value = Now
        dtpPayoutFrom.Value = Now
        dtpPayoutTo.Value = Now
        Fill_Company_Details()
        cmbCompCode.SelectedIndex = 0
        Fill_Pay_Period_Details()
        lblIPAddress.Text = "IP ADDRESS DETAILS"
        btnSave.Enabled = False
    End Sub

    Public Sub Fill_Pay_Period_Details()
        cmbPayPeriod.Items.Clear()
        SSQL = ""
        SSQL = "Select * from PayPeriod_Mst"
        mDataSet = ReturnMultipleValue(SSQL)
        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                For iRow = 0 To .Rows.Count - 1
                    cmbPayPeriod.Items.Add(.Rows(iRow)(0))
                Next
            End If
        End With
    End Sub

    Private Sub cmbCompCode_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCompCode.LostFocus
        If Trim(cmbCompCode.Text) = "" Then Exit Sub
        Dim iStr() As String
        iStr = Split(Trim(cmbCompCode.Text), " | ")

        SSQL = ""
        SSQL = "Select LocCode + ' | ' + LocName as [Name] from Location_Mst Where Compcode='"
        SSQL = SSQL & "" & iStr(0) & "'"

        mDataSet = ReturnMultipleValue(SSQL)

        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                cmbLocCode.Items.Clear()
                For iRow = 0 To .Rows.Count - 1
                    cmbLocCode.Items.Add(.Rows(iRow)(0))
                Next
            End If
        End With
    End Sub

    Public Sub Fill_Company_Details()
        SSQL = ""
        SSQL = "Select CompCode + ' | ' + CompName as [Name] from Company_Mst Order By CompName"

        mDataSet = ReturnMultipleValue(SSQL)
        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                cmbCompCode.Items.Clear()
                For iRow = 0 To .Rows.Count - 1
                    cmbCompCode.Items.Add(.Rows(iRow)(0))
                Next
            End If
        End With
    End Sub

    Public Sub Fill_Grid_Title()
        With _flex
            .Clear()
            .Rows = 1
            .Cols = 5
            .FixedCols = 0
            .FixedRows = 1
            .set_TextMatrix(0, 0, "EmpNo")
            .set_TextMatrix(0, 1, "Name")
            .set_TextMatrix(0, 2, "Salary")
            .set_TextMatrix(0, 3, "MacineId")
            .set_TextMatrix(0, 4, "Payout")
        End With
    End Sub

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        Fill_Grid_Title()
        dtpPayFrom.Value = Now
        dtpPayTo.Value = Now
        dtpPayoutFrom.Value = Now
        dtpPayoutTo.Value = Now
        txtNoEmp.Text = ""
        txtTotAmt.Text = ""
        txtPayoutCover.Text = ""
        txtPayoutAmt.Text = ""
        txtBalCover.Text = ""
        txtBalAmt.Text = ""
        lblIPAddress.Text = "IP ADDRESS DETAILS"
        cmbLocCode.Focus()
    End Sub

    Private Sub btnProcess_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProcess.Click
        Try
            If Trim(cmbCompCode.Text) = "" Then
                MessageBox.Show("Please Select Company Code.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
                cmbCompCode.Focus()
                Exit Sub
            End If

            If Trim(cmbLocCode.Text) = "" Then
                MessageBox.Show("Please Select Location Code.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
                cmbLocCode.Focus()
                Exit Sub
            End If

            iStr1 = Split(Trim(cmbCompCode.Text), " | ")
            iStr2 = Split(Trim(cmbLocCode.Text), " | ")

            SSQL = ""
            SSQL = "Select ExistingCode,MachineID from Employee_Mst Where CompCode='" & iStr1(0) & "'"
            SSQL &= " And LocCode='" & iStr2(0) & "'"

            mDataSet = Nothing
            mDataSet = ReturnMultipleValue(SSQL)

            If mDataSet.Tables(0).Rows.Count <= 0 Then Exit Sub

            For iTblRow As Integer = 0 To mDataSet.Tables(0).Rows.Count - 1
                For ifgRow As Integer = 1 To _flex.Rows - 1
                    If Trim(_flex.get_TextMatrix(ifgRow, 0)) = Trim(mDataSet.Tables(0).Rows(iTblRow)(0)) Then
                        _flex.set_TextMatrix(ifgRow, 3, Trim(mDataSet.Tables(0).Rows(iTblRow)(1)))
                    End If
                Next
            Next

            Dim mBtnName As String

            mBtnName = btnProcess.Text
            btnProcess.Text = "Wait..."

            'Get Log Details

            SSQL = ""
            SSQL = "Select MachineID,Max(Payout) as [Payout] from LogTime_SALARY "
            SSQL = SSQL & " Where Compcode='" & iStr1(0) & "'"
            SSQL = SSQL & " And LocCode='" & iStr2(0) & "'"
            SSQL = SSQL & " And IPAddress='" & Trim(lblIPAddress.Text) & "'"
            SSQL = SSQL & " And Payout >='" & dtpPayoutFrom.Value.ToString("yyyy/MM/dd") & "'"
            SSQL = SSQL & " And Payout <='" & dtpPayoutTo.Value.ToString("yyyy/MM/dd") & "'"
            SSQL = SSQL & " Group By MachineID"
            SSQL = SSQL & " Order By Max(Payout)"

            mDataSet = Nothing
            mDataSet = ReturnMultipleValue(SSQL)

            If mDataSet.Tables(0).Rows.Count <= 0 Then Exit Sub

            'Fill Grid Details

            For iTblRow = 0 To mDataSet.Tables(0).Rows.Count - 1
                For iGridRow = 1 To _flex.Rows - 1
                    If Trim(_flex.get_TextMatrix(iGridRow, 3)) = Trim(Decryption(mDataSet.Tables(0).Rows(iTblRow)("MachineID"))) Then
                        _flex.set_TextMatrix(iGridRow, 4, String.Format("{0:d/M/yyyy hh:mm tt}", mDataSet.Tables(0).Rows(iTblRow)("Payout")))
                    End If
                Next
            Next
            'Save Grid Details

            btnProcess.Text = mBtnName

            Grid_Calculation()

            btnSave.Enabled = True
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End Try

    End Sub

    Public Sub Grid_Calculation()

        If _flex.Rows = 1 Then Exit Sub

        txtNoEmp.Text = "0"
        txtTotAmt.Text = "0"
        txtPayoutCover.Text = "0"
        txtPayoutAmt.Text = "0"
        txtBalCover.Text = "0"
        txtBalAmt.Text = "0"

        Dim mRow As Integer = 0
1:
        For mRow = 1 To _flex.Rows - 1
            If _flex.get_TextMatrix(mRow, 0) = "" Then
                _flex.RemoveItem(mRow)
                GoTo 1
            End If
        Next

        For mRow = 1 To _flex.Rows - 1
            txtNoEmp.Text = Val(txtNoEmp.Text) + 1
            txtTotAmt.Text = Val(txtTotAmt.Text) + Val(_flex.get_TextMatrix(mRow, 2))
        Next

        For mRow = 1 To _flex.Rows - 1
            If Trim(_flex.get_TextMatrix(mRow, 4)) <> "" Then
                txtPayoutCover.Text = Val(txtPayoutCover.Text) + 1
                txtPayoutAmt.Text = Val(txtPayoutAmt.Text) + Val(_flex.get_TextMatrix(mRow, 2))
            End If
        Next

        txtBalCover.Text = Val(txtNoEmp.Text) - Val(txtPayoutCover.Text)
        txtBalAmt.Text = Val(txtTotAmt.Text) - Val(txtPayoutAmt.Text)

        'Thread.CurrentThread.CurrentCulture = New CultureInfo("hi-IN")
        'txtBalAmt.Text = String.Format("{0:C}", Convert.ToDouble(txtBalAmt.Text))

    End Sub

    Private Sub dtpPayoutTo_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpPayoutTo.LostFocus

        Try

            SSQL = ""
            SSQL = "Select isnull(EmpNo,'') as [EmpNo],isnull(ExistingCode,'') as [ExistingCode]"
            SSQL &= " ,isnull(FirstName,'') as [FirstName], isnull(MachineID,'') as [MachineID]"
            SSQL &= " ,isnull(Salary,'') as [Salary],isnull(Payout,'') as [Payout] "
            SSQL &= " from SalaryAck_Details Where CompCode='" & iStr1(0) & "'"
            SSQL &= " And LocCode='" & iStr2(0) & "'"

            mDataSet = Nothing
            mDataSet = ReturnMultipleValue(SSQL)

            If mDataSet.Tables(0).Rows.Count <= 0 Then
                btnImport.Enabled = True
                Exit Sub
            Else
                btnImport.Enabled = False
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Try

            If Trim(cmbCompCode.Text) = "" Then
                MessageBox.Show("Please Select Company Details.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
                cmbCompCode.Focus()
                Exit Sub
            End If

            If Trim(cmbLocCode.Text) = "" Then
                MessageBox.Show("Please Select Location Deatils", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
                cmbLocCode.Focus()
                Exit Sub
            End If

            If Trim(cmbPayPeriod.Text) = "" Then
                MessageBox.Show("Please Select Pay Period Deatils", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
                cmbPayPeriod.Focus()
                Exit Sub
            End If

            If _flex.Rows = 1 Then Exit Sub

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End Try



    End Sub

    Private Sub cmbLocCode_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbLocCode.LostFocus
        Try
            If Trim(cmbCompCode.Text) = "" Then
                MessageBox.Show("Please Select Company Code.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
                cmbCompCode.Focus()
                Exit Sub
            End If

            If Trim(cmbLocCode.Text) = "" Then
                MessageBox.Show("Please Select Location Code.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
                cmbLocCode.Focus()
                Exit Sub
            End If

            iStr1 = Split(Trim(cmbCompCode.Text), " | ")
            iStr2 = Split(Trim(cmbLocCode.Text), " | ")

            SSQL = ""
            SSQL = "Select IPAddress From IPAddress_Mst Where CompCode='" & Trim(iStr1(0)) & "'"
            SSQL = SSQL & " And LocCode='" & Trim(iStr2(0)) & "'"
            SSQL = SSQL & " And IPMode='SALARY'"

            mDataSet = Nothing
            mDataSet = ReturnMultipleValue(SSQL)

            lblIPAddress.Text = "IP ADDRESS DETAILS"
            If mDataSet.Tables(0).Rows.Count <= 0 Then Exit Sub

            lblIPAddress.Text = mDataSet.Tables(0).Rows(0)(0)

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End Try
    End Sub

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click

        If _flex.Rows < 2 Then Exit Sub

        Dim xls As Object = CreateObject("Excel.Application")

        Dim strFilename As String
        Dim intCol, intRow As Integer


        Dim strPath As String = Application.StartupPath & "\Excel"

        If xls Is Nothing Then
            MessageBox.Show("Excel is not installed on this machine.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        Try
            With xls
                .application.visible = True
                .SheetsInNewWorkbook = 1
                .Workbooks.Add()
                .Worksheets(1).Select()
                .Range("A1:I1").MergeCells = True
                .cells(1, 1).value = "SALARY DISBURSEMENT" & " - " & Trim(iStr2(0)) & " - " & cmbPayPeriod.Text
                '.cells(1, 2).value = " - " & Trim(cmbShift.Text)
                '.cells(1, 3).value = " - " & iStr2(0)
                '.cells(1, 4).value = " - " & dtpFromDate.Value.ToString("dd/MMM/yyyy")
                .cells(1, 4).EntireRow.Font.Bold = True


                Dim intI As Integer = 1
                Dim intK As Integer = 1

                For intCol = 0 To _flex.Cols - 1
                    .cells(2, intI).value = _flex.get_TextMatrix(0, intCol)
                    .cells(2, intI).EntireRow.Font.Bold = True
                    intI += 1
                Next

                intI = 3
                intK = 1

                For intRow = 1 To _flex.Rows - 1
                    intK = 1
                    For intCol = 0 To _flex.Cols - 1
                        .Cells(intI, intK).value = _flex.get_TextMatrix(intRow, intCol)
                        intK += 1
                    Next
                    intI += 1
                Next

                .Cells(intI + 1, 2).value = "No of Covers   :"
                .Cells(intI + 2, 2).value = "Total Amount   :"
                .Cells(intI + 3, 2).value = "Paid Covers    :"
                .Cells(intI + 4, 2).value = "Paid Amount    :"
                .Cells(intI + 5, 2).value = "Balance Covers :"
                .Cells(intI + 6, 2).value = "Balance Amount :"

                .Cells(intI + 1, 3).value = Trim(txtNoEmp.Text)
                .Cells(intI + 2, 3).value = Trim(txtTotAmt.Text)
                .Cells(intI + 3, 3).value = Trim(txtPayoutCover.Text)
                .Cells(intI + 4, 3).value = Trim(txtPayoutAmt.Text)
                .Cells(intI + 5, 3).value = Trim(txtBalCover.Text)
                .Cells(intI + 6, 3).value = Trim(txtBalAmt.Text)

                If Mid$(strPath, strPath.Length, 1) <> "\" Then
                    strPath = strPath & "\"
                End If
                strFilename = strPath & "Salary.xls"
                .Application.DisplayAlerts = False
                .ActiveCell.Worksheet.SaveAs(strFilename)
                .Application.DisplayAlerts = True
            End With
            System.Runtime.InteropServices.Marshal.ReleaseComObject(xls)
            xls = Nothing
            'MsgBox("Data's are exported to Excel Succesfully in '" & strFilename & "'", MsgBoxStyle.Information)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        ' The excel is created and opened for insert value. We most close this excel using this system        
        'Dim pro() As Process = System.Diagnostics.Process.GetProcessesByName("EXCEL")
        'For Each i As Process In pro
        '    i.Kill()
        'Next
    End Sub

    'Private Sub btnDownload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDownload.Click
    '    If Trim(cmbCompCode.Text) = "" Then
    '        MessageBox.Show("Please Select Company Code.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '        cmbCompCode.Focus()
    '        Exit Sub
    '    End If

    '    If Trim(cmbLocCode.Text) = "" Then
    '        MessageBox.Show("Please Select Location Code.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '        cmbLocCode.Focus()
    '        Exit Sub
    '    End If


    '    If lblIPAddress.Text = "IP ADDRESS DETAILS" Then Exit Sub

    '    downloadMachine_Details(iStr1(0), iStr2(0), Trim(lblIPAddress.Text), "LogTime_SALARY")

    'End Sub
End Class