﻿#Region "Imports"
Imports System
Imports System.Data
Imports System.Data.OleDb
Imports CrystalDecisions.CrystalReports.Engine
#End Region

Public Class frmMealsDetails

    Private iStr1() As String
    Private iStr2() As String
    Private iStr3() As String
    Private EMpName As String
    Dim mStatus As Long
    Private Check_Download_Clear_Error As Boolean = False
    Public axCZKEM1 As New zkemkeeper.CZKEM
    Private bIsConnected = False 'the boolean value identifies whether the device is connected
    Private iMachineNumber As Integer

    Private Sub frmMealsDetails_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormAlign(Me)
        Fill_Company_Details()
        cmbCompCode.SelectedIndex = 0
        cmbCompCode.Enabled = False
        cmbCompCode_LostFocus(sender, e)

        If Trim(mUserLocation) <> "" Then
            cmbLocCode.SelectedIndex = cmbLocCode.FindString(Trim(mUserLocation))
            cmbLocCode.Enabled = False

            Dim iStr1() As String
            Dim iStr2() As String
            Dim iYear() As String

            iStr1 = Split(Trim(cmbCompCode.Text), " | ")
            iStr2 = Split(Trim(cmbLocCode.Text), " | ")

        Else
            cmbLocCode.SelectedIndex = 0
        End If
        cmbLocCode_LostFocus(sender, e)
        Fill_Department_Details()
        Check_Download_Clear_Error = False

    End Sub
    Public Sub Fill_Department_Details()
        cmbDept.Items.Clear()
        SSQL = ""
        SSQL = "Select distinct deptname from Department_Mst"
        mDataSet = ReturnMultipleValue(SSQL)
        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                For iRow = 0 To .Rows.Count - 1
                    cmbDept.Items.Add(.Rows(iRow)(0))
                Next
            End If
        End With
    End Sub
    Public Sub Fill_Company_Details()
        SSQL = ""
        SSQL = "Select CompCode + ' | ' + CompName as [Name] from Company_Mst Order By CompName"

        mDataSet = ReturnMultipleValue(SSQL)
        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                cmbCompCode.Items.Clear()
                For iRow = 0 To .Rows.Count - 1
                    cmbCompCode.Items.Add(.Rows(iRow)(0))
                Next
            End If
        End With
    End Sub
    Private Sub cmbCompCode_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCompCode.LostFocus
        If Trim(cmbCompCode.Text) = "" Then Exit Sub
        Dim iStr() As String
        iStr = Split(Trim(cmbCompCode.Text), " | ")

        SSQL = ""
        SSQL = "Select LocCode + ' | ' + LocName as [Name] from Location_Mst Where Compcode='"
        SSQL = SSQL & "" & iStr(0) & "'"

        mDataSet = ReturnMultipleValue(SSQL)

        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                cmbLocCode.Items.Clear()
                For iRow = 0 To .Rows.Count - 1
                    cmbLocCode.Items.Add(.Rows(iRow)(0))
                Next
            End If
        End With
    End Sub
    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Close()
    End Sub

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        lblNetAmt.Text = "0.00"
        txtApproved.Text = ""
        txtGuestComp.Text = ""
        txtGuestName.Text = ""
        txtNoPerson.Text = ""

    End Sub


    Private Sub cmbTokenType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbTokenType.SelectedIndexChanged

        If (cmbTokenType.Text = "Staff") Then
            cmbWages.Enabled = True
            cmbDept.Enabled = True
            txtName.Enabled = True
            cmbTktNo.Enabled = True
            txtGuestComp.Enabled = False
            txtGuestName.Enabled = False
            Load_Employee_Type()
        ElseIf (cmbTokenType.Text = "Labour") Then
            cmbWages.Enabled = True
            cmbDept.Enabled = True
            txtName.Enabled = True
            cmbTktNo.Enabled = True
            txtGuestComp.Enabled = False
            txtGuestName.Enabled = False
            Load_Employee_Type()

        ElseIf (cmbTokenType.Text = "OT/WH") Then
            cmbWages.Enabled = True
            cmbDept.Enabled = True
            txtName.Enabled = True
            cmbTktNo.Enabled = True
            txtGuestComp.Enabled = False
            txtGuestName.Enabled = False
        Else
            cmbWages.Enabled = False
            cmbDept.Enabled = False
            txtName.Enabled = False
            cmbTktNo.Enabled = False
            txtGuestComp.Enabled = True
            txtGuestName.Enabled = True

        End If
    End Sub

    Private Sub cmbMealsType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbMealsType.SelectedIndexChanged

        Dim iStr1() As String
        Dim iStr2() As String

        iStr1 = Split(Trim(cmbCompCode.Text), " | ")
        iStr2 = Split(Trim(cmbLocCode.Text), " | ")

        lblNetAmt.Text = "0.00"


        SSQL = "select Amt from MealsMst where MealsTypes='" & cmbMealsType.Text & "' and Types='" & cmbTokenType.Text & "' "
        mDataSet = Nothing
        mDataSet = ReturnMultipleValue(SSQL)

        If mDataSet.Tables(0).Rows.Count > 0 Then
            With mDataSet.Tables(0)
                lblNetAmt.Text = mDataSet.Tables(0).Rows(0)("Amt")
            End With

        Else
            lblNetAmt.Text = "0.00"
        End If

       

    End Sub
    Private Sub LoadRegNo()
      


    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim AutoCode As String
        Dim Next_No As String
        Dim Zero_Join As String

        If Trim(txtNoPerson.Text) = "" Then
            MessageBox.Show("Please enter No of Persons.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbMealsType.Focus()
            Exit Sub
        End If

        If Trim(txtApproved.Text) = "" Then
            MessageBox.Show("Please enter Approved By.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtApproved.Focus()
            Exit Sub
        End If

        If (cmbTokenType.Text = "Company Guest") Then
            If Trim(txtGuestComp.Text) = "" Then
                MessageBox.Show("Please enter Guest company name.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
                txtGuestComp.Focus()
                Exit Sub
            End If

            If Trim(txtGuestName.Text) = "" Then
                MessageBox.Show("Please enter Guest name.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
                txtGuestName.Focus()
                Exit Sub
            End If
        End If

        If (cmbTokenType.Text = "Private Guest") Then
            If Trim(txtGuestComp.Text) = "" Then
                MessageBox.Show("Please enter Guest company name.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
                txtGuestComp.Focus()
                Exit Sub
            End If

            If Trim(txtGuestName.Text) = "" Then
                MessageBox.Show("Please enter Guest name.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
                txtGuestName.Focus()
                Exit Sub
            End If
        End If

        'AutoID start
        SSQL = "select CAST(RIGHT(Auto_Code,LEN(Auto_Code)-5) as decimal(18,0)) as Auto_Code from MealsDetails "
        SSQL = SSQL & "order by CAST(RIGHT(Auto_Code,LEN(Auto_Code)-5) as decimal(18,0)) desc"
        mDataSet = Nothing

        mDataSet = ReturnMultipleValue(SSQL)
        If mDataSet.Tables(0).Rows.Count > 0 Then
            With mDataSet.Tables(0)
                Next_No = mDataSet.Tables(0).Rows(0)("Auto_Code")
            End With

        Else
            Next_No = "0"

        End If

        Next_No = (Convert.ToDecimal(Next_No) + Convert.ToDecimal(1)).ToString()

        If Convert.ToDecimal(Next_No) <= 2 Then

            For iRow = 0 To Convert.ToDecimal(Next_No) - 3
                Zero_Join = Zero_Join & "0"
                AutoCode = "MESS" & "-" & Zero_Join & Next_No
            Next

        Else
            AutoCode = "MESS" & "-" & Next_No

        End If
        If AutoCode = Nothing Or AutoCode = "" Then
            AutoCode = "MESS" & "-" & Next_No
        End If
        'AutoID End

        If (cmbTokenType.Text = "Company Guest" Or cmbTokenType.Text = "Private Guest" Or cmbTokenType.Text = "Contract" Or cmbTokenType.Text = "Others") Then

            'Guest Category Entry
            Dim SdataSet As New DataSet
            Dim iStr1() As String


            iStr1 = Split(Trim(cmbCompCode.Text), " | ")
            iStr2 = Split(Trim(cmbLocCode.Text), " | ")
            'iStr3 = Split(Trim(cmbTktNo.Text), " -> ")


            SSQL = "insert into MealsDetails (Auto_Code,TokenType,TknNo,EmpName,WagesType,MealsType,Dates,"
            SSQL = SSQL & "Department,Approved,GuestComp,GuestName,NoofPerson,Amount,Ccode,Lcode) Values("
            SSQL = SSQL & "'" & AutoCode & "','" & cmbTokenType.Text & "','" & cmbTktNo.Text & "','','" & cmbWages.Text & "','" & cmbMealsType.Text & "',convert(datetime,'" & txtFromDate.Text & "',103),"
            SSQL = SSQL & "'" & cmbDept.Text & "','" & txtApproved.Text & "','" & txtGuestComp.Text & "','" & txtGuestName.Text & "',"
            SSQL = SSQL & "'" & txtNoPerson.Text & "','" & lblNetAmt.Text & "','" & iStr1(0) & "','" & iStr2(0) & "')"
            mStatus = InsertDeleteUpdate(SSQL)

            MessageBox.Show("Added Successfully .", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)


            SSQL = "select Auto_Code,TokenType,MealsType,Department,Approved,GuestComp,GuestName,NoofPerson,Amount from MealsDetails where MealsType='" & cmbMealsType.Text & "' and GuestComp='" & txtGuestComp.Text & "' and GuestName='" & txtGuestName.Text & "' and Dates=convert(datetime,'" & txtFromDate.Text & "',103) "
            mDataSet = Nothing
            mDataSet = ReturnMultipleValue(SSQL)

            ' Edited by Narmatha start

            Dim cryRep As New ReportDocument
            Dim cryView As New frmRepView
            If mDataSet Is Nothing OrElse mDataSet.Tables.Count <= 0 Then Exit Sub
            cryRep.Load(Application.StartupPath & "\Reports\" & "GuestMeals.rpt")
            cryRep.SetDataSource(mDataSet.Tables(0))
            cryRep.PrintToPrinter(1, False, 0, 0)
            'cryView.crViewer.ReportSource = cryRep
            'cryView.crViewer.Refresh()
            'cryView.Show()
            ' Edited by Narmatha End

            lblNetAmt.Text = "0.00"
            txtApproved.Text = ""
            txtGuestComp.Text = ""
            txtGuestName.Text = ""
            txtNoPerson.Text = ""

        Else

            'Staff or Others category Entrys
            Dim SdataSet As New DataSet
            Dim iStr1() As String
            Dim iStr2() As String
            Dim EmpNameVal As String

            iStr1 = Split(Trim(cmbCompCode.Text), " | ")
            iStr2 = Split(Trim(cmbLocCode.Text), " | ")

            'lblNetAmt.Text = "10"

            SSQL = "insert into MealsDetails (Auto_Code,TokenType,TknNo,EmpName,WagesType,MealsType,Dates,"
            SSQL = SSQL & "Department,Approved,GuestComp,GuestName,NoofPerson,Amount,Ccode,Lcode) Values("
            SSQL = SSQL & "'" & AutoCode & "','" & cmbTokenType.Text & "','" & cmbTktNo.Text & "','" & txtName.Text & "','" & cmbWages.Text & "','" & cmbMealsType.Text & "',convert(datetime,'" & txtFromDate.Text & "',103),"
            SSQL = SSQL & "'" & cmbDept.Text & "','" & txtApproved.Text & "','Jayalakshmi Textiles (P) Ltd.,','" & txtGuestName.Text & "',"
            SSQL = SSQL & "'" & txtNoPerson.Text & "','" & lblNetAmt.Text & "','" & iStr1(0) & "','" & iStr2(0) & "')"
            mStatus = InsertDeleteUpdate(SSQL)

            MessageBox.Show("Added Successfully .", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)

            SSQL = "select Auto_Code,TokenType,MealsType,TknNo,Department,Approved,GuestComp,EmpName,NoofPerson,Amount from MealsDetails where MealsType='" & cmbMealsType.Text & "' and TknNo='" & cmbTktNo.Text & "'  and Dates=convert(datetime,'" & txtFromDate.Text & "',103) "
            mDataSet = Nothing
            mDataSet = ReturnMultipleValue(SSQL)

            ' Edited by Narmatha start

            Dim cryRep As New ReportDocument
            Dim cryView As New frmRepView
            If mDataSet Is Nothing OrElse mDataSet.Tables.Count <= 0 Then Exit Sub
            cryRep.Load(Application.StartupPath & "\Reports\" & "StaffMeals.rpt")
            cryRep.SetDataSource(mDataSet.Tables(0))
            cryRep.PrintToPrinter(1, False, 0, 0)
            'cryView.crViewer.ReportSource = cryRep
            'cryView.crViewer.Refresh()
            'cryView.Show()

            ' Edited by Narmatha end

            lblNetAmt.Text = "0.00"
            txtApproved.Text = ""
            txtGuestComp.Text = ""
            txtGuestName.Text = ""
            txtNoPerson.Text = ""


        End If

    End Sub
    Private Sub txtName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtName.TextChanged
        
    End Sub
    Private Sub cmbTktNo_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cmbTktNo.KeyPress
        If e.KeyChar = ChrW(Keys.Enter) Then
            If Trim(cmbTktNo.Text) <> "" Then cmbTktNo_LostFocus(sender, e)
        End If
    End Sub

    Private Sub cmbTktNo_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbTktNo.LostFocus
        If Trim(cmbCompCode.Text) = "" Then Exit Sub
        If Trim(cmbLocCode.Text) = "" Then Exit Sub
        If Trim(cmbTktNo.Text) = "" Then Exit Sub

        Dim iStr1() As String
        Dim iStr2() As String
        
        iStr1 = Split(Trim(cmbCompCode.Text), " | ")
        iStr2 = Split(Trim(cmbLocCode.Text), " | ")



        SSQL = ""
        SSQL = "Select ExistingCode,MachineID,(FirstName + '.' + MiddleInitial) as FirstName,DeptName from Employee_Mst Where Compcode='" & Trim(iStr1(0)) & "'"
        SSQL = SSQL & " and LocCode='" & Trim(iStr2(0)) & "'"
        SSQL = SSQL & " and ExistingCode='" & cmbTktNo.Text & "'"
        mDataSet = ReturnMultipleValue(SSQL)

        With mDataSet.Tables(0)
            EMpName = mDataSet.Tables(0).Rows(0)("FirstName")
            txtName.Text = mDataSet.Tables(0).Rows(0)("FirstName")
            cmbDept.Text = mDataSet.Tables(0).Rows(0)("DeptName")
            txtNoPerson.Text = "1"
        End With
    End Sub

    Private Sub btnMaster_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMaster.Click
        Me.Close()
        MealsMst.Show()
    End Sub

   
    Private Sub txtNoPerson_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtNoPerson.KeyDown
     
    End Sub
    Private Sub txtNoPerson_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNoPerson.TextChanged
        If txtNoPerson.Text = "" Then

        Else
            If lblNetAmt.Text = "" Then lblNetAmt.Text = "0"
            lblNetAmt.Text = (Convert.ToDecimal(txtNoPerson.Text) * Convert.ToDecimal(lblNetAmt.Text)).ToString()
        End If

    End Sub

    Private Sub txtNoPerson_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtNoPerson.KeyPress
        Dim iStr1() As String
        Dim iStr2() As String

        iStr1 = Split(Trim(cmbCompCode.Text), " | ")
        iStr2 = Split(Trim(cmbLocCode.Text), " | ")

        lblNetAmt.Text = "0.00"


        SSQL = "select Amt from MealsMst where MealsTypes='" & cmbMealsType.Text & "' and Types='" & cmbTokenType.Text & "' "
        mDataSet = Nothing
        mDataSet = ReturnMultipleValue(SSQL)

        If mDataSet.Tables(0).Rows.Count > 0 Then
            With mDataSet.Tables(0)
                lblNetAmt.Text = mDataSet.Tables(0).Rows(0)("Amt")
            End With

        Else
            lblNetAmt.Text = "0.00"
        End If

        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Return) Then
            Dim Total As String
            If txtNoPerson.Text = "" Then

            Else
                Total = (Convert.ToDecimal(txtNoPerson.Text) * Convert.ToDecimal(lblNetAmt.Text)).ToString()
                lblNetAmt.Text = Total
            End If
            e.Handled = True

        End If
    End Sub

    Private Sub Load_Employee_Type()

        Dim Emp_Category As String
        Dim mDatatSet_TkType As DataSet

        If cmbTokenType.Text = "Staff" Then
            Emp_Category = "1"
        Else
            Emp_Category = "2"
        End If
        SSQL = ""
        SSQL = "Select * from MstEmployeeType Where EmpCategory='" & Emp_Category & "' Order by EmpType Asc"
        mDatatSet_TkType = ReturnMultipleValue(SSQL)

        With mDatatSet_TkType.Tables(0)
            If .Rows.Count > 0 Then
                cmbWages.Items.Clear()
                For iRow = 0 To .Rows.Count - 1
                    cmbWages.Items.Add(.Rows(iRow)("EmpType"))
                Next
            End If
        End With
    End Sub

    Private Sub cmbWages_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbWages.SelectedIndexChanged
        If Trim(cmbCompCode.Text) = "" Then Exit Sub
        If Trim(cmbLocCode.Text) = "" Then Exit Sub
        'If Trim(cmbPrefix.Text) = "" Then Exit Sub

        Dim iStr1() As String
        Dim iStr2() As String

        iStr1 = Split(Trim(cmbCompCode.Text), " | ")
        iStr2 = Split(Trim(cmbLocCode.Text), " | ")

        If mUserLocation <> "" Then
            SSQL = ""
            SSQL = "Select ExistingCode from Employee_Mst Where Compcode='"
            SSQL = SSQL & "" & iStr1(0) & "'"
            SSQL = SSQL & " and LocCode='" & iStr2(0) & "'"
            SSQL = SSQL & " and Wages='" & cmbWages.Text & "'"
            SSQL = SSQL & " Order By convert(numeric,EmpNo)"
        Else
            SSQL = ""
            SSQL = "Select ExistingCode from Employee_Mst Where Compcode='"
            SSQL = SSQL & "" & iStr1(0) & "'"
            SSQL = SSQL & " and LocCode='" & iStr2(0) & "'"
            SSQL = SSQL & " and Wages='" & cmbWages.Text & "'"
            SSQL = SSQL & " Order By convert(numeric,EmpNo)"
        End If

        mDataSet = ReturnMultipleValue(SSQL)

        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                cmbTktNo.Items.Clear()
                For iRow = 0 To .Rows.Count - 1
                    cmbTktNo.Items.Add(.Rows(iRow)(0))
                Next
            End If
        End With
    End Sub

    Private Sub cmbTktNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbTktNo.SelectedIndexChanged
        cmbTktNo_LostFocus(sender, e)
    End Sub

    Private Sub cmbLocCode_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbLocCode.LostFocus
        If Trim(cmbCompCode.Text) = "" Then Exit Sub
        If Trim(cmbLocCode.Text) = "" Then Exit Sub

        Dim iStr1() As String
        Dim iStr2() As String

        iStr1 = Split(Trim(cmbCompCode.Text), " | ")
        iStr2 = Split(Trim(cmbLocCode.Text), " | ")

        SSQL = ""
        SSQL = "Select IPAddress + ' | ' + IPMode as [Name] from IPAddress_Mst Where Compcode='"
        SSQL = SSQL & "" & iStr1(0) & "'"
        SSQL = SSQL & " And LocCode='" & iStr2(0) & "' And IPMode='CANTEEN'"

        mDataSet = ReturnMultipleValue(SSQL)

        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                cmbIPAddress.Items.Clear()
                For iRow = 0 To .Rows.Count - 1
                    cmbIPAddress.Items.Add(.Rows(iRow)(0))
                Next
            End If
        End With
    End Sub

    Private Sub btnConnect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConnect.Click
        If Trim(cmbCompCode.Text) = "" Then
            MessageBox.Show("Please select company code.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbCompCode.Focus()
            Exit Sub
        End If

        If Trim(cmbLocCode.Text) = "" Then
            MessageBox.Show("Please select location code.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbLocCode.Focus()
            Exit Sub
        End If

        If Trim(cmbIPAddress.Text) = "" Then
            MessageBox.Show("Please select IP Address.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbIPAddress.Focus()
            Exit Sub
        End If

        iStr1 = Split(cmbCompCode.Text, " | ")
        iStr2 = Split(cmbLocCode.Text, " | ")
        iStr3 = Split(cmbIPAddress.Text, " | ")

        Connect_Machine_RTEvents()

    End Sub
    Private Sub Connect_Machine_RTEvents()
        Dim idwErrorCode As Integer
        Cursor = Cursors.WaitCursor
        If btnConnect.Text = "Disconnect" Then
            axCZKEM1.Disconnect()

            RemoveHandler axCZKEM1.OnAttTransactionEx, AddressOf AxCZKEM1_OnAttTransactionEx

            bIsConnected = False
            btnConnect.Text = "Connect"
            Cursor = Cursors.Default
            Return
        End If

        bIsConnected = axCZKEM1.Connect_Net(Trim(iStr3(0)), 4370)
        If bIsConnected = True Then
            btnConnect.Text = "Disconnect"
            btnConnect.Refresh()
            iMachineNumber = 1 'In fact,when you are using the tcp/ip communication,this parameter will be ignored,that is any integer will all right.Here we use 1.

            If axCZKEM1.RegEvent(iMachineNumber, 65535) = True Then 'Here you can register the realtime events that you want to be triggered(the parameters 65535 means registering all)

                AddHandler axCZKEM1.OnAttTransactionEx, AddressOf AxCZKEM1_OnAttTransactionEx

            End If
        Else
            axCZKEM1.GetLastError(idwErrorCode)
            MsgBox("Unable to connect the device,ErrorCode=" & idwErrorCode, MsgBoxStyle.Exclamation, "Error")
        End If
        Cursor = Cursors.Default
    End Sub

    Private Sub AxCZKEM1_OnAttTransactionEx(ByVal sEnrollNumber As String, ByVal iIsInValid As Integer, ByVal iAttState As Integer, ByVal iVerifyMethod As Integer, _
                      ByVal iYear As Integer, ByVal iMonth As Integer, ByVal iDay As Integer, ByVal iHour As Integer, ByVal iMinute As Integer, ByVal iSecond As Integer, ByVal iWorkCode As Integer)

        Dim User_Machine_ID As String
        Dim AmountInWords_Str As String
        Dim Class_Check As New clsConversion
        User_Machine_ID = sEnrollNumber
        Dim User_MachineID_Ency As String = Encryption(sEnrollNumber)
        Dim mSaveStatus As Long
        Dim AutoCode As String
        Dim Next_No As String
        Dim Zero_Join As String
        Dim iStr1() As String
        Dim iStr2() As String
        Dim iTime As String
        Dim dt_DateTime As DateTime
        Dim dt_Time As DateTime
        Dim dt_Date As Date
        lblMachineID.Text = sEnrollNumber
        Dim mDataSetEmp As DataSet

        dt_DateTime = Convert.ToDateTime(iYear.ToString() + "-" + iMonth.ToString() + "-" + iDay.ToString() + " " + iHour.ToString() + ":" + iMinute.ToString() + ":" + iSecond.ToString())
        dt_Date = Convert.ToDateTime(iYear.ToString() + "-" + iMonth.ToString() + "-" + iDay.ToString())

        iStr1 = Split(Trim(cmbCompCode.Text), " | ")
        iStr2 = Split(Trim(cmbLocCode.Text), " | ")
        iTime = (iHour & ":" & iMinute)

        'Get Employee Details
        SSQL = ""
        SSQL = "Select * from Employee_Mst Where CompCode='" & iStr1(0) & "'"
        SSQL = SSQL & " And LocCode='" & iStr2(0) & "'"
        'SSQL = SSQL & " And MachineID='" & Val(Remove_Single_Quote(Trim(User_Machine_ID))) & "'"
        SSQL = SSQL & " And MachineID='" & User_Machine_ID & "'"
        mDataSetEmp = ReturnMultipleValue(SSQL)
        If mDataSetEmp.Tables(0).Rows.Count <> 0 Then
            cmbTokenType.Text = IIf(IsDBNull(mDataSetEmp.Tables(0).Rows(0)("CatName")), "", mDataSetEmp.Tables(0).Rows(0)("CatName"))
            cmbWages.Text = IIf(IsDBNull(mDataSetEmp.Tables(0).Rows(0)("Wages")), "", mDataSetEmp.Tables(0).Rows(0)("Wages"))
            cmbTktNo.Text = IIf(IsDBNull(mDataSetEmp.Tables(0).Rows(0)("ExistingCode")), "", mDataSetEmp.Tables(0).Rows(0)("ExistingCode"))
            txtName.Text = IIf(IsDBNull(mDataSetEmp.Tables(0).Rows(0)("FirstName")), "", mDataSetEmp.Tables(0).Rows(0)("FirstName"))
            cmbDept.Text = IIf(IsDBNull(mDataSetEmp.Tables(0).Rows(0)("DeptName")), "", mDataSetEmp.Tables(0).Rows(0)("DeptName"))
            'cmbTokenType.Text = mDataSet.Tables(0).Rows(0)("CatName")
            'cmbWages.Text = mDataSet.Tables(0).Rows(0)("Wages")
            'cmbTktNo.Text = mDataSet.Tables(0).Rows(0)("ExistingCode")
            'txtName.Text = mDataSet.Tables(0).Rows(0)("FirstName")
            'cmbDept.Text = mDataSet.Tables(0).Rows(0)("DeptName")
            txtNoPerson.Text = "1"
            Dim Meal_Token_Not_Match As Boolean = False
            If dt_DateTime >= DateTime.Parse(dt_Date.AddHours(Convert.ToDouble(7))) And dt_DateTime < DateTime.Parse(dt_Date.AddHours(Convert.ToDouble(10))) Then
                cmbMealsType.Text = "Breakfast"
                Meal_Token_Not_Match = True
            ElseIf dt_DateTime >= DateTime.Parse(dt_Date.AddHours(Convert.ToDouble(10))) And dt_DateTime < DateTime.Parse(dt_Date.AddHours(Convert.ToDouble(17))) Then
                cmbMealsType.Text = "Lunch"
                Meal_Token_Not_Match = True
            ElseIf (dt_DateTime >= DateTime.Parse(dt_Date.AddHours(Convert.ToDouble(17))) And dt_DateTime <= DateTime.Parse(dt_Date.AddHours(Convert.ToDouble(23.99)))) Or (dt_DateTime >= DateTime.Parse(dt_Date.AddHours(Convert.ToDouble(0.0))) And dt_DateTime < DateTime.Parse(dt_Date.AddHours(Convert.ToDouble(7)))) Then
                cmbMealsType.Text = "Dinner"
                Meal_Token_Not_Match = True
            Else
                Meal_Token_Not_Match = False
            End If
            If Meal_Token_Not_Match = True Then

                lblNetAmt.Text = "0.00"
                SSQL = "select Amt from MealsMst where MealsTypes='" & cmbMealsType.Text & "' and Types='" & cmbTokenType.Text & "' "
                mDataSet = Nothing
                mDataSet = ReturnMultipleValue(SSQL)
                If mDataSet.Tables(0).Rows.Count > 0 Then
                    With mDataSet.Tables(0)
                        lblNetAmt.Text = mDataSet.Tables(0).Rows(0)("Amt")
                    End With
                Else
                    lblNetAmt.Text = "0.00"
                End If

                'Inser Meals Token
                'AutoID start
                SSQL = "select CAST(RIGHT(Auto_Code,LEN(Auto_Code)-5) as decimal(18,0)) as Auto_Code from MealsDetails "
                SSQL = SSQL & "order by CAST(RIGHT(Auto_Code,LEN(Auto_Code)-5) as decimal(18,0)) desc"
                mDataSet = Nothing
                mDataSet = ReturnMultipleValue(SSQL)
                If mDataSet.Tables(0).Rows.Count > 0 Then
                    With mDataSet.Tables(0)
                        Next_No = mDataSet.Tables(0).Rows(0)("Auto_Code")
                    End With
                Else
                    Next_No = "0"

                End If
                AutoCode = ""
                Next_No = (Convert.ToDecimal(Next_No) + Convert.ToDecimal(1)).ToString()
                If Convert.ToDecimal(Next_No) <= 2 Then
                    For iRow = 0 To Convert.ToDecimal(Next_No) - 3
                        Zero_Join = Zero_Join & "0"
                        AutoCode = "MESS" & "-" & Zero_Join & Next_No
                    Next
                Else
                    AutoCode = "MESS" & "-" & Next_No
                End If
                If AutoCode = Nothing Or AutoCode = "" Then
                    AutoCode = "MESS" & "-" & Next_No
                End If
                'AutoID End

                'Staff or Others category Entrys
                Dim SdataSet As New DataSet
                'lblNetAmt.Text = "10"

                SSQL = ""
                SSQL = "Delete from MealsDetails where TokenType='" & cmbTokenType.Text & "' and MealsType='" & cmbMealsType.Text & "' and TknNo='" & cmbTktNo.Text & "'"
                SSQL = SSQL & " and Convert(datetime,Dates,103)=Convert(datetime,'" & txtFromDate.Text & "',103)"
                ReturnMultipleValue(SSQL)

                SSQL = "insert into MealsDetails (Auto_Code,TokenType,TknNo,EmpName,WagesType,MealsType,Dates,"
                SSQL = SSQL & "Department,Approved,GuestComp,GuestName,NoofPerson,Amount,Ccode,Lcode) Values("
                SSQL = SSQL & "'" & AutoCode & "','" & cmbTokenType.Text & "','" & cmbTktNo.Text & "','" & txtName.Text & "','" & cmbWages.Text & "','" & cmbMealsType.Text & "',convert(datetime,'" & txtFromDate.Text & "',103),"
                SSQL = SSQL & "'" & cmbDept.Text & "','" & txtApproved.Text & "','" & iStr1(1) & "','" & txtGuestName.Text & "',"
                SSQL = SSQL & "'" & txtNoPerson.Text & "','" & lblNetAmt.Text & "','" & iStr1(0) & "','" & iStr2(0) & "')"
                mStatus = InsertDeleteUpdate(SSQL)

                SSQL = "select Auto_Code,TokenType,MealsType,TknNo,Department,Approved,GuestComp,EmpName,NoofPerson,Amount from MealsDetails where MealsType='" & cmbMealsType.Text & "' and Dates=convert(datetime,'" & txtFromDate.Text & "',103) "
                SSQL = SSQL & " and TknNo='" & cmbTktNo.Text & "'"
                mDataSet = Nothing
                mDataSet = ReturnMultipleValue(SSQL)

                ' Edited by Narmatha start

                Dim cryRep As New ReportDocument
                Dim cryView As New frmRepView
                If mDataSet Is Nothing OrElse mDataSet.Tables.Count <= 0 Then Exit Sub
                cryRep.Load(Application.StartupPath & "\Reports\" & "StaffMeals.rpt")
                cryRep.SetDataSource(mDataSet.Tables(0))
                cryRep.PrintToPrinter(1, False, 0, 0)
                'cryView.crViewer.ReportSource = cryRep
                'cryView.crViewer.Refresh()
                'cryView.Show()

                ' Edited by Narmatha end

                'lblNetAmt.Text = "0.00"
                'txtApproved.Text = ""
                'txtGuestComp.Text = ""
                'txtGuestName.Text = ""
                'txtNoPerson.Text = ""
            Else
                MessageBox.Show("Meals Token Timing Not Match...", "Scoto", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Else
            'MessageBox.Show("Employee Not Found in Our DataBase...", "Scoto", MessageBoxButtons.OK, MessageBoxIcon.Information)
            lblNetAmt.Text = "0.00"
            txtApproved.Text = ""
            txtGuestComp.Text = ""
            txtGuestName.Text = ""
            txtNoPerson.Text = ""
        End If


    End Sub
End Class