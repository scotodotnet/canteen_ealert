﻿#Region "Imports"
Imports System
Imports System.Data
Imports System.Data.OleDb
Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.Windows.Forms
Imports CrystalDecisions.CrystalReports.Engine
#End Region
Public Class FrmPermission
    Dim mStatus As Long
    Private mImageFile As Image
    Private mImageFilePath As String
    Dim iStr1() As String
    Dim iStr2() As String
    Dim iStr3() As String
    Dim mStrComp() As String
    Dim mStrLoc() As String
    Dim val As Integer = 1

    Private Sub FrmPermission_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Fill_Company_Details()
        'Fill_Location_Details()

    End Sub
    Public Sub Fill_Company_Details()
        SSQL = ""
        SSQL = "Select CompCode + ' | ' + CompName as [Name] from Company_Mst Order By CompName"

        mDataSet = ReturnMultipleValue(SSQL)
        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                cmbCompCode.Items.Clear()
                For iRow = 0 To .Rows.Count - 1
                    cmbCompCode.Items.Add(.Rows(iRow)(0))
                Next
            End If
        End With
    End Sub
    Public Sub Fill_Location_Details()
        mStrComp = Split(cmbCompCode.Text, " | ")
        SSQL = ""
        SSQL = "Select LocCode + ' - ' + LocName From Location_Mst Where CompCode='" & Trim(mStrComp(0)) & "'"

        mDataSet = Nothing
        mDataSet = ReturnMultipleValue(SSQL)

        cmbLocCode.Items.Clear()

        If mDataSet.Tables(0).Rows.Count <= 0 Then Exit Sub
        Dim iRow As Integer = 0

        Do
            cmbLocCode.Items.Add(Trim(mDataSet.Tables(0).Rows(iRow)(0)))
            cmbLocCode.SelectedIndex = 0
            iRow += 1
            While iRow > mDataSet.Tables(0).Rows.Count - 1 : Exit Sub : End While

        Loop
        'cmbLocCode.Text = ""
    End Sub
    Private Sub User_Login()
        mStrComp = Split(cmbCompCode.Text, " | ")
        mStrLoc = Split(cmbLocCode.Text, " - ")
        SSQL = ""
        SSQL = SSQL & "Select UserName from User_Login where CompCode='" & Trim(mStrComp(0)) & "' and LocCode='" & Trim(mStrLoc(0)) & "'"
        mDataSet = ReturnMultipleValue(SSQL)
        If mDataSet.Tables(0).Rows.Count > 0 Then
            cmbUser.Items.Clear()
            For iRow As Integer = 0 To mDataSet.Tables(0).Rows.Count - 1
                cmbUser.Items.Add(mDataSet.Tables(0).Rows(iRow)(0).ToString())
                cmbUser.SelectedIndex = 0
                'iRow += 1
            Next
            'cmbUser.Text = ""
        End If
    End Sub
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub
    Private Sub clr()
        cbDepartment.Checked = False
        cbDesignation.Checked = False
        cbDownload.Checked = False
        cbEmail.Checked = False
        cbEmployeeDetails.Checked = False
        cbEmployeeType.Checked = False
        cbIP.Checked = False
        cbmachine.Checked = False
        cbmailAdd.Checked = False
        cbManual.Checked = False
        cbReports.Checked = False
        cbSalary.Checked = False
        cbstaff.Checked = False
        cbNoShift.Checked = False
        cbUser.Checked = False
        cbShift.Checked = False
        cbShiftmas.Checked = False
        cbFpDownload.Checked = False

    End Sub

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        clr()
    End Sub

    Private Sub cmbCompCode_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbCompCode.SelectedIndexChanged
        Fill_Location_Details()
        cmbLocCode.Focus()
    End Sub

    Private Sub cmbLocCode_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbLocCode.SelectedIndexChanged    
    End Sub

    Private Sub cmbLocCode_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cmbLocCode.KeyDown
        If e.KeyCode = Keys.Enter Then
            User_Login()
            cmbUser.Focus()
        End If
    End Sub

    Private Sub cmbUser_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cmbUser.KeyUp

    End Sub
    Private Sub btnSave_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim Dept As String = ""
        If cbDepartment.Checked = True Then
            Dept = Dept & "1,"
        Else
            Dept = Dept & "0,"
        End If
        If cbDesignation.Checked = True Then
            Dept = Dept & "1,"
        Else
            Dept = Dept & "0,"
        End If
        If cbDownload.Checked = True Then
            Dept = Dept & "1,"
        Else
            Dept = Dept & "0,"
        End If
        If cbEmail.Checked = True Then
            Dept = Dept & "1,"
        Else
            Dept = Dept & "0,"
        End If
        If cbEmployeeDetails.Checked = True Then
            Dept = Dept & "1,"
        Else
            Dept = Dept & "0,"
        End If
        If cbEmployeeType.Checked = True Then
            Dept = Dept & "1,"
        Else
            Dept = Dept & "0,"
        End If
        If cbIP.Checked = True Then
            Dept = Dept & "1,"
        Else
            Dept = Dept & "0,"
        End If
        If cbmachine.Checked = True Then
            Dept = Dept & "1,"
        Else
            Dept = Dept & "0,"
        End If
        If cbmailAdd.Checked = True Then
            Dept = Dept & "1,"
        Else
            Dept = Dept & "0,"
        End If
        If cbManual.Checked = True Then
            Dept = Dept & "1,"
        Else
            Dept = Dept & "0,"
        End If
        If cbReports.Checked = True Then
            Dept = Dept & "1,"
        Else
            Dept = Dept & "0,"
        End If
        If cbSalary.Checked = True Then
            Dept = Dept & "1,"
        Else
            Dept = Dept & "0,"
        End If
        If cbstaff.Checked = True Then
            Dept = Dept & "1,"
        Else
            Dept = Dept & "0,"
        End If
        If cbUser.Checked = True Then
            Dept = Dept & "1,"
        Else
            Dept = Dept & "0,"
        End If
        If cbNoShift.Checked = True Then
            Dept = Dept & "1,"
        Else
            Dept = Dept & "0,"
        End If
        If cbShift.Checked = True Then
            Dept = Dept & "1,"
        Else
            Dept = Dept & "0,"
        End If
        If cbFpDownload.Checked = True Then
            Dept = Dept & "1,"
        Else
            Dept = Dept & "0,"
        End If
        If cbShiftmas.Checked = True Then
            Dept = Dept & "1,"
        Else
            Dept = Dept & "0,"
        End If

        If Trim(cmbUser.Text) = "" Then
            MessageBox.Show("Plase Select the user.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            'txtFirstName.Focus()
            Exit Sub
        ElseIf Trim(cmbLocCode.Text) = "" Then
            MessageBox.Show("Plase Select the Location.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            'txtFirstName.Focus()
            Exit Sub
        ElseIf Trim(cmbCompCode.Text) = "" Then
            MessageBox.Show("Plase Select the Company.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            'txtFirstName.Focus()
            Exit Sub
        End If
        mStrComp = Split(cmbCompCode.Text, " | ")
        mStrLoc = Split(cmbLocCode.Text, " - ")
        Dim qry As String = ""
        Dim user_Exist As String = ""
        qry = "Select Username from Rights where CompCode = '" & mStrComp(0) & "' and LocCode = '" & mStrLoc(0) & "' and Username = '" & cmbUser.Text & "'"
        user_Exist = ReturnSingleValue(qry)
        If Trim(user_Exist) = "" Then
            SSQL = ""
            SSQL = SSQL & "Insert into Rights (CompCode,LocCode,Username,Data) values ('" & mStrComp(0) & "','" & mStrLoc(0) & "','" & cmbUser.Text & "','" & Dept & "')"
            mStatus = InsertDeleteUpdate(SSQL)
        Else
            SSQL = ""
            SSQL = SSQL & "Update Rights set Data = '" & Dept & "' where CompCode = '" & mStrComp(0) & "' and LocCode = '" & mStrLoc(0) & "' and Username = '" & cmbUser.Text & "'"
            mStatus = InsertDeleteUpdate(SSQL)
        End If

        If mStatus > 0 Then
            MessageBox.Show("Saved Successfully.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            clr()
        End If
    End Sub
    Private Sub Load_det()
        mStrComp = Split(cmbCompCode.Text, " | ")
        mStrLoc = Split(cmbLocCode.Text, " - ")
        Dim qry As String = ""
        qry = "Select Data from Rights where CompCode = '" & mStrComp(0) & "' and LocCode = '" & mStrLoc(0) & "' and Username = '" & cmbUser.Text & "'"
        mDataSet = ReturnMultipleValue(qry)
        If mDataSet.Tables(0).Rows.Count > 0 Then
            Dim Dept() As String
            Dept = Split(mDataSet.Tables(0).Rows(0)("Data").ToString(), ",")
            If Dept(0) = "1" Then
                cbDepartment.Checked = True
            Else
                cbDepartment.Checked = False
            End If
            If Dept(1) = "1" Then
                cbDesignation.Checked = True
            Else
                cbDesignation.Checked = False
            End If
            If Dept(2) = "1" Then
                cbDownload.Checked = True
            Else
                cbDownload.Checked = False
            End If
            If Dept(3) = "1" Then
                cbEmail.Checked = True
            Else
                cbEmail.Checked = False
            End If
            If Dept(4) = "1" Then
                cbEmployeeDetails.Checked = True
            Else
                cbEmployeeDetails.Checked = False
            End If
            If Dept(5) = "1" Then
                cbEmployeeType.Checked = True
            Else
                cbEmployeeType.Checked = False
            End If
            If Dept(6) = "1" Then
                cbIP.Checked = True
            Else
                cbIP.Checked = False
            End If
            If Dept(7) = "1" Then
                cbmachine.Checked = True
            Else
                cbmachine.Checked = False
            End If
            If Dept(8) = "1" Then
                cbmailAdd.Checked = True
            Else
                cbmailAdd.Checked = False
            End If
            If Dept(9) = "1" Then
                cbManual.Checked = True
            Else
                cbManual.Checked = False
            End If
            If Dept(10) = "1" Then
                cbReports.Checked = True
            Else
                cbReports.Checked = False
            End If
            If Dept(11) = "1" Then
                cbSalary.Checked = True
            Else
                cbSalary.Checked = False
            End If
            If Dept(12) = "1" Then
                cbstaff.Checked = True
            Else
                cbstaff.Checked = False
            End If
            If Dept(13) = "1" Then
                cbUser.Checked = True
            Else
                cbUser.Checked = False
            End If
            If Dept(14) = "1" Then
                cbNoShift.Checked = True
            Else
                cbNoShift.Checked = False
            End If
            If Dept(15) = "1" Then
                cbShift.Checked = True
            Else
                cbShift.Checked = False
            End If
            If Dept(16) = "1" Then
                cbFpDownload.Checked = True
            Else
                cbFpDownload.Checked = False
            End If
            If Dept(17) = "1" Then
                cbShiftmas.Checked = True
            Else
                cbShiftmas.Checked = False
            End If
        Else

        End If


    End Sub
    Private Sub cmbUser_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cmbUser.KeyDown        
        If e.KeyCode = Keys.Enter Then
            Load_det()
        End If
    End Sub
End Class