﻿#Region "Imports"
Imports System
Imports System.Data
Imports System.Data.OleDb
Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.Windows.Forms
Imports CrystalDecisions.CrystalReports.Engine
#End Region

Public Class frmEmployee
    Dim mStatus As Long
    Private mImageFile As Image
    Private mImageFilePath As String
    Dim iStr1() As String
    Dim iStr2() As String
    Dim iStr3() As String
    Public selRow As New DataGridViewRow

    Private Sub frmEmployee_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormAlign(Me)
        Fill_Company_Details()
        cmbCompCode.SelectedIndex = 0
        cmbCompCode.Enabled = False
        cmbCompCode_LostFocus(sender, e)
        If Trim(mUserLocation) <> "" Then
            cmbLocCode.SelectedIndex = cmbLocCode.FindString(Trim(mUserLocation))
            cmbLocCode.Enabled = False
            chkNonAdmin.Visible = False
            btnMachine_ID_Update.Visible = False
        Else
            chkNonAdmin.Visible = True
            btnMachine_ID_Update.Visible = True
        End If
        chkNonAdmin.Checked = False
        Fill_Employee_Type_Details()
        Fill_Department_Details()
        Fill_Pay_Period_Details()
        txtAttn_Inc_Eligible.SelectedIndex = 0


    End Sub

    Public Sub Fill_Pay_Period_Details()
        cmbPayPeriod.Items.Clear()
        SSQL = ""
        SSQL = "Select * from PayPeriod_Mst"
        mDataSet = ReturnMultipleValue(SSQL)
        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                For iRow = 0 To .Rows.Count - 1
                    cmbPayPeriod.Items.Add(.Rows(iRow)(0))
                Next
            End If
        End With
    End Sub

    Public Sub Fill_Department_Details()
        cmbDept.Items.Clear()
        SSQL = ""
        SSQL = "Select distinct deptname from Department_Mst"
        mDataSet = ReturnMultipleValue(SSQL)
        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                For iRow = 0 To .Rows.Count - 1
                    cmbDept.Items.Add(.Rows(iRow)(0))
                Next
            End If
        End With
    End Sub

    Public Sub Fill_Employee_Type_Details()
        cmbEmpType.Items.Clear()
        SSQL = ""
        SSQL = "Select * from EmpType_Mst"
        mDataSet = ReturnMultipleValue(SSQL)
        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                For iRow = 0 To .Rows.Count - 1
                    cmbEmpType.Items.Add(.Rows(iRow)(0))
                Next
            End If
        End With
    End Sub

    'Private Sub pbEmployee_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbEmployee.DoubleClick
    '    With dlgOpen
    '        .Title = "Set Image File"
    '        .Filter = "JPEG Files|*.jpg|Gif Files|*.gif|Bitmap Files|*.bmp"
    '        .DefaultExt = "jpg"
    '        .FilterIndex = 1
    '        .FileName = ""
    '    End With
    '    If dlgOpen.ShowDialog = Windows.Forms.DialogResult.Cancel Then
    '        Exit Sub
    '    End If

    '    Dim sFilePath As String
    '    sFilePath = dlgOpen.FileName

    '    If sFilePath = "" Then Exit Sub

    '    If System.IO.File.Exists(sFilePath) = False Then
    '        Exit Sub
    '    Else
    '        mImageFilePath = sFilePath
    '    End If
    '    pbEmployee.BackgroundImage = Image.FromFile(mImageFilePath)
    'End Sub

    Public Sub Fill_Company_Details()
        SSQL = ""
        SSQL = "Select CompCode + ' | ' + CompName as [Name] from Company_Mst Order By CompName"

        mDataSet = ReturnMultipleValue(SSQL)
        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                cmbCompCode.Items.Clear()
                For iRow = 0 To .Rows.Count - 1
                    cmbCompCode.Items.Add(.Rows(iRow)(0))
                Next
            End If
        End With
    End Sub

    Private Sub cmbCompCode_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCompCode.LostFocus
        If Trim(cmbCompCode.Text) = "" Then Exit Sub
        Dim iStr() As String
        iStr = Split(Trim(cmbCompCode.Text), " | ")

        SSQL = ""
        SSQL = "Select LocCode + ' | ' + LocName as [Name] from Location_Mst Where Compcode='"
        SSQL = SSQL & "" & iStr(0) & "'"

        mDataSet = ReturnMultipleValue(SSQL)

        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                cmbLocCode.Items.Clear()
                For iRow = 0 To .Rows.Count - 1
                    cmbLocCode.Items.Add(.Rows(iRow)(0))
                Next
            End If
        End With

    End Sub

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        Clear_Screen_Details()
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Close()
    End Sub

    Public Sub Clear_Screen_Details()
        clr()
        txtRecmob1.Text = ""
        txtParMob1.Text = ""
        txtLL.Text = ""
        txtll1.Text = ""
        txtEmpMob1.Text = ""    
        cmbPrefix.SelectedIndex = 0
        txtEmpNo.Text = ""
        txtExistingCode.Text = ""
        txtMachineID.Text = ""
        txtFirstName.Text = ""
        txtLastName.Text = ""
        txtInitial.Text = ""
        cmbGender.SelectedIndex = 0
        dtpDOB.Value = Now
        dtpDOJ.Value = Now
        txtAge.Text = ""
        cmbMarital.SelectedIndex = 0
        cbPermenant.Checked = False
        cmbDept.Text = ""
        txtDesignation.Text = ""
        cmbPayPeriod.Text = ""
        txtBaseSalary.Text = ""
        txtPFNo.Text = ""
        txtNominee.Text = ""
        txtESINo.Text = ""
        cmbOTEligible.Text = ""
        txtNationality.Text = ""
        txtQualification.Text = ""
        txtCertificate.Text = ""
        DataGridView1.Rows.Clear()
        txtTot_Work_Hours.Text = ""
        txtCalculate_Work_Hours.Text = ""
        txtOT_Hours.Text = ""

        txtFamilyDetails.Text = ""
        txtRecuritmentThrough.Text = ""
        txtIDMark1.Text = ""
        txtIDMark2.Text = ""
        cmbBloodGroup.Text = ""
        cmbHandicapped.Text = ""
        txtHeight.Text = ""
        txtWeight.Text = ""
        txtAddress1.Text = ""
        txtAddress2.Text = ""

        txtBankName.Text = ""
        txtBranchCode.Text = ""
        txtAccountNo.Text = ""

        cmbEmpStatus.SelectedIndex = 0
        cmbIsActive.SelectedIndex = 0
        cmbSubCatName.SelectedIndex = 0
        cmbCatName.SelectedIndex = 0
        cmbShiftType.SelectedIndex = 0

        txtAttn_Inc_Eligible.SelectedIndex = 0

        chkNonAdmin.Checked = False

        If Not (pbEmployee.BackgroundImage Is Nothing) Then
            pbEmployee.BackgroundImage.Dispose()
            pbEmployee.BackgroundImage = Nothing
        Else
            pbEmployee.Image = Nothing
        End If
        cmbCompCode.Focus()
    End Sub

    Private Sub cmbPrefix_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbPrefix.LostFocus
        Fill_Employee_AutoNumber()
    End Sub

    Public Sub Fill_Employee_AutoNumber()
        If Trim(cmbCompCode.Text) = "" Then
            MessageBox.Show("Plase select company.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbCompCode.Focus()
            Exit Sub
        End If

        If Trim(cmbLocCode.Text) = "" Then
            MessageBox.Show("Plase select loaction.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbLocCode.Focus()
            Exit Sub
        End If

        If Trim(cmbPrefix.Text) = "" Then
            MessageBox.Show("Plase select prefix.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbPrefix.Focus()
            Exit Sub
        End If

        Dim iStr1() As String
        Dim iStr2() As String
        iStr1 = Split(Trim(cmbCompCode.Text), " | ")
        iStr2 = Split(Trim(cmbLocCode.Text), " | ")

        SSQL = ""
        SSQL = "Select isnull(max(EmpNo),0)+1 from Employee_Mst Where CompCode='" & iStr1(0) & "'"
        SSQL = SSQL & " And LocCode='" & iStr2(0) & "'"
        SSQL = SSQL & " And EmpPrefix='" & Trim(cmbPrefix.Text) & "'"
        Dim mTxtNo As Double
        mTxtNo = ReturnSingleValue(SSQL)
        txtEmpNo.Text = (mTxtNo)
    End Sub

    'Function EightDigit(ByVal x As Double) As String
    '    Return x.ToString.PadLeft(8, "0"c)
    '    'Return Microsoft.VisualBasic.Right("00" & x.ToString, 8)
    'End Function

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim cbval As Integer
        If Trim(cmbCompCode.Text) = "" Then
            MessageBox.Show("Plase select company.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbCompCode.Focus()
            Exit Sub
        End If
        If Trim(cmbLocCode.Text) = "" Then
            MessageBox.Show("Plase select loaction.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbLocCode.Focus()
            Exit Sub
        End If
        If Trim(cmbEmpType.Text) = "" Then
            MessageBox.Show("Plase select type.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbEmpType.Focus()
            Exit Sub
        End If
        If Trim(cmbPrefix.Text) = "" Then
            MessageBox.Show("Plase select prefix.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbPrefix.Focus()
            Exit Sub
        End If
        If Trim(txtEmpNo.Text) = "" Then
            MessageBox.Show("Plase enter employee Code.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtEmpNo.Focus()
            Exit Sub
        End If
        If Trim(txtFirstName.Text) = "" Then
            MessageBox.Show("Plase enter first name.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtFirstName.Focus()
            Exit Sub
        End If
        If Trim(cmbOTEligible.Text) = "" Then
            MessageBox.Show("Plase select OT eligible.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbOTEligible.Focus()
            Exit Sub
        End If

        'Non Admin Usert Int Value Get
        Dim INTnonAdminUser As Integer
        If chkNonAdmin.Checked = True Then
            INTnonAdminUser = 1
        Else
            INTnonAdminUser = 0
        End If

        Dim iStr1() As String
        Dim iStr2() As String

        iStr1 = Split(Trim(cmbCompCode.Text), " | ")
        iStr2 = Split(Trim(cmbLocCode.Text), " | ")
        If cbPermenant.Checked = True Then
            cbval = 1
        Else
            cbval = 0
        End If
        'Machine ID Encrypt
        Dim StrMachine_ID_Encrypt As String = ""
        StrMachine_ID_Encrypt = Encryption(Trim(txtMachineID.Text))

        'mStatus = InsertDeleteUpdate(SSQL)

        SSQL = ""
        SSQL = "Delete from Employee_Mst Where CompCode='" & iStr1(0) & "'"
        SSQL = SSQL & " And LocCode='" & iStr2(0) & "'"
        'SSQL = SSQL & " And TypeName='" & Trim(cmbEmpType.Text) & "'"
        'SSQL = SSQL & " And EmpPrefix='" & Trim(cmbPrefix.Text) & "'"
        SSQL = SSQL & " And EmpNo='" & Val(Remove_Single_Quote(Trim(txtEmpNo.Text))) & "'"

        mStatus = InsertDeleteUpdate(SSQL)

        SSQL = ""
        SSQL = "Insert into Employee_Mst (CompCode,LocCode,TypeName,EmpPrefix,EmpNo"
        SSQL = SSQL & ",ExistingCode,MachineID,FirstName,LastName,MiddleInitial"
        SSQL = SSQL & ",Gender,BirthDate,Age,MaritalStatus,ShiftType,CatName,SubCatName"

        SSQL = SSQL & " ,DOJ,DeptName,Designation"
        SSQL = SSQL & " ,PayPeriod_Desc,BaseSalary,PFNo"
        SSQL = SSQL & " ,Nominee,ESINo,StdWrkHrs"
        SSQL = SSQL & " ,OTEligible,Nationality,Qualification"
        SSQL = SSQL & " ,Certificate,FamilyDetails,RecuritmentThro"
        SSQL = SSQL & " ,IDMark1,IDMark2,BloodGroup"
        SSQL = SSQL & " ,Handicapped,Height,Weight"
        SSQL = SSQL & " ,Address1,Address2,BankName"
        SSQL = SSQL & " ,BranchCode,AccountNo"

        SSQL = SSQL & " ,EmpStatus,IsActive,Created_By,Created_Date,IsNonAdmin,MachineID_Encrypt,Working_Hours,Calculate_Work_Hours,OT_Hours,Wages,RecutersMob,parentsMobile,ParentsPhone,EmployeeMobile,SamepresentAddress,Religion,WeekOff,AdharNo,VoterID,Driving,Passport,RationCard,PanCard,SmartCard,Others,AttnIncEligible) Values ( "

        SSQL = SSQL & "'" & iStr1(0) & "',"
        SSQL = SSQL & "'" & iStr2(0) & "',"
        SSQL = SSQL & "'" & Trim(cmbEmpType.Text) & "',"
        SSQL = SSQL & "'" & Trim(cmbPrefix.Text) & "',"
        SSQL = SSQL & "'" & Val(Remove_Single_Quote(Trim(txtEmpNo.Text))) & "',"
        SSQL = SSQL & "'" & UCase(Remove_Single_Quote(Trim(txtExistingCode.Text))) & "',"
        SSQL = SSQL & "'" & UCase(Remove_Single_Quote(Trim(txtMachineID.Text))) & "',"
        SSQL = SSQL & "'" & UCase(Remove_Single_Quote(Trim(txtFirstName.Text))) & "',"
        SSQL = SSQL & "'" & UCase(Remove_Single_Quote(Trim(txtLastName.Text))) & "',"
        SSQL = SSQL & "'" & UCase(Remove_Single_Quote(Trim(txtInitial.Text))) & "',"
        SSQL = SSQL & "'" & Trim(cmbGender.Text) & "',"
        SSQL = SSQL & "'" & Format(dtpDOB.Value, "dd-MMM-yyyy") & "',"
        SSQL = SSQL & "'" & Trim(txtAge.Text) & "',"
        SSQL = SSQL & "'" & Trim(cmbMarital.Text) & "',"
        SSQL = SSQL & "'" & Trim(cmbShiftType.Text) & "',"
        SSQL = SSQL & "'" & Trim(cmbCatName.Text) & "',"
        SSQL = SSQL & "'" & Trim(cmbSubCatName.Text) & "',"

        SSQL = SSQL & "'" & Format(dtpDOJ.Value, "dd-MMM-yyyy") & "',"
        SSQL = SSQL & "'" & Trim(cmbDept.Text) & "',"
        SSQL = SSQL & "'" & UCase(Remove_Single_Quote(Trim(txtDesignation.Text))) & "',"
        SSQL = SSQL & "'" & Trim(cmbPayPeriod.Text) & "',"
        SSQL = SSQL & "'" & Val(txtBaseSalary.Text) & "',"
        SSQL = SSQL & "'" & UCase(Remove_Single_Quote(Trim(txtPFNo.Text))) & "',"
        SSQL = SSQL & "'" & UCase(Remove_Single_Quote(Trim(txtNominee.Text))) & "',"
        SSQL = SSQL & "'" & UCase(Remove_Single_Quote(Trim(txtESINo.Text))) & "',"
        SSQL = SSQL & "'" & Val(txtStdWrkHrs.Text) & "',"
        SSQL = SSQL & "'" & Trim(cmbOTEligible.Text) & "',"
        SSQL = SSQL & "'" & UCase(Remove_Single_Quote(Trim(txtNationality.Text))) & "',"
        SSQL = SSQL & "'" & UCase(Remove_Single_Quote(Trim(txtQualification.Text))) & "',"
        SSQL = SSQL & "'" & UCase(Remove_Single_Quote(Trim(txtCertificate.Text))) & "',"
        SSQL = SSQL & "'" & UCase(Remove_Single_Quote(Trim(txtFamilyDetails.Text))) & "',"
        SSQL = SSQL & "'" & UCase(Remove_Single_Quote(Trim(txtRecuritmentThrough.Text))) & "',"
        SSQL = SSQL & "'" & UCase(Remove_Single_Quote(Trim(txtIDMark1.Text))) & "',"
        SSQL = SSQL & "'" & UCase(Remove_Single_Quote(Trim(txtIDMark2.Text))) & "',"
        SSQL = SSQL & "'" & Trim(cmbBloodGroup.Text) & "',"
        SSQL = SSQL & "'" & Trim(cmbHandicapped.Text) & "',"
        SSQL = SSQL & "'" & UCase(Remove_Single_Quote(Trim(txtHeight.Text))) & "',"
        SSQL = SSQL & "'" & UCase(Remove_Single_Quote(Trim(txtWeight.Text))) & "',"
        SSQL = SSQL & "'" & UCase(Remove_Single_Quote(Trim(txtAddress1.Text))) & "',"
        SSQL = SSQL & "'" & UCase(Remove_Single_Quote(Trim(txtAddress2.Text))) & "',"
        SSQL = SSQL & "'" & UCase(Remove_Single_Quote(Trim(txtBankName.Text))) & "',"
        SSQL = SSQL & "'" & UCase(Remove_Single_Quote(Trim(txtBranchCode.Text))) & "',"
        SSQL = SSQL & "'" & UCase(Remove_Single_Quote(Trim(txtAccountNo.Text))) & "',"

        SSQL = SSQL & "'" & Trim(cmbEmpStatus.Text) & "',"
        SSQL = SSQL & "'" & Trim(cmbIsActive.Text) & "',"

        SSQL = SSQL & "'" & Trim(mvarLoginUser) & "',"
        SSQL = SSQL & "'" & Format(CDate(mdiMain.statusServerDate.Text), "dd-MMM-yyyy") & "',"
        SSQL = SSQL & "'" & INTnonAdminUser & "',"
        SSQL = SSQL & "'" & StrMachine_ID_Encrypt & "',"
        SSQL = SSQL & "'" & UCase(Remove_Single_Quote(Trim(txtTot_Work_Hours.Text))) & "',"
        SSQL = SSQL & "'" & UCase(Remove_Single_Quote(Trim(txtCalculate_Work_Hours.Text))) & "',"
        SSQL = SSQL & "'" & UCase(Remove_Single_Quote(Trim(txtOT_Hours.Text))) & "','" & cmbWages.Text & "','" & (Trim(txtRecMob.Text) & "-" & Trim(txtRecmob1.Text)) & "','" & (Trim(txtParMob.Text) & "-" & Trim(txtParMob1.Text)) & "','" & (Trim(txtLL.Text) & "-" & Trim(txtll1.Text)) & "','" & (Trim(txtEmpMob.Text) & "-" & Trim(txtEmpMob1.Text)) & "','" & cbval & "','" & Trim(txtReligion.Text) & "','" & cmbWeakOff.Text & "', "
        SSQL = SSQL & "'" & txtAdharCard.Text & "','" & txtVoterId.Text & "','" & txtDriving.Text & "','" & txtPassport.Text & "','" & txtRationCard.Text & "','" & txtPanCard.Text & "','" & txtSmart.Text & "','" & txtOthers.Text & "','" & txtAttn_Inc_Eligible.Text & "' )"



        mStatus = InsertDeleteUpdate(SSQL)
        Dim mStatus1 As Long
        Dim Ins As String = ""
        If DataGridView1.Rows.Count > 0 Then
            For i As Integer = 0 To DataGridView1.Rows.Count - 1
                Ins = ""
                Ins = Ins & "Insert into ExperienceDetails (CompCode,LocCode,EmpNo,ExistingCode,MachineID,YearsExp,MonthsExp,CompanyName,Designation,Department,PeriodFrom,PeriodTo,Contactname,Mobile,phone) values ('" & iStr1(0) & "','"
                Ins = Ins & iStr2(0) & "','" & Trim(txtEmpNo.Text) & "','" & Trim(txtExistingCode.Text) & "','" & Trim(txtMachineID.Text) & "','" & DataGridView1.Rows(i).Cells(1).Value & "','" & DataGridView1.Rows(i).Cells(2).Value & "','"
                Ins = Ins & DataGridView1.Rows(i).Cells(3).Value & "','" & DataGridView1.Rows(i).Cells(4).Value & "','" & DataGridView1.Rows(i).Cells(5).Value & "','" & DataGridView1.Rows(i).Cells(6).Value & "','"
                Ins = Ins & DataGridView1.Rows(i).Cells(7).Value & "','" & DataGridView1.Rows(i).Cells(8).Value & "','" & DataGridView1.Rows(i).Cells(9).Value & "','" & DataGridView1.Rows(i).Cells(10).Value & "')"
                mStatus1 = InsertDeleteUpdate(Ins)
            Next
        End If
        If mStatus > 0 Then
            MessageBox.Show("Successfully Saved.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Clear_Screen_Details()
            Exit Sub
        End If
    End Sub

    Private Sub txtEmpNo_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEmpNo.LostFocus
        If Trim(txtEmpNo.Text) = "" Then
            txtExistingCode.Text = ""
            txtMachineID.Text = ""
            txtFirstName.Text = ""
            txtLastName.Text = ""
            txtInitial.Text = ""
            cmbGender.SelectedIndex = 0
            dtpDOB.Value = Now
            txtAge.Text = ""
            cmbMarital.SelectedIndex = 0
            cmbEmpStatus.SelectedIndex = 0
            cmbIsActive.SelectedIndex = 0
            txtExistingCode.Focus()
            Exit Sub
        End If


        Dim iStr1() As String
        Dim iStr2() As String

        iStr1 = Split(Trim(cmbCompCode.Text), " | ")
        iStr2 = Split(Trim(cmbLocCode.Text), " | ")

        'Check With Non Admin User
        If mUserLocation <> "" Then
            SSQL = ""
            SSQL = "Select * from Employee_Mst Where CompCode='" & iStr1(0) & "'"
            SSQL = SSQL & " And LocCode='" & iStr2(0) & "'"
            SSQL = SSQL & " And EmpPrefix='" & Trim(cmbPrefix.Text) & "'"
            SSQL = SSQL & " And EmpNo='" & Val(Remove_Single_Quote(Trim(txtEmpNo.Text))) & "' And IsNonAdmin='1'"
            mDataSet = ReturnMultipleValue(SSQL)
            If mDataSet.Tables(0).Rows.Count > 0 Then
                mDataSet = Nothing
                Clear_Screen_Details()
                txtExistingCode.Text = ""
                txtMachineID.Text = ""
                txtFirstName.Text = ""
                txtLastName.Text = ""
                txtInitial.Text = ""
                cmbGender.SelectedIndex = 0
                dtpDOB.Value = Now
                txtAge.Text = ""
                cmbMarital.SelectedIndex = 0
                cmbEmpStatus.SelectedIndex = 0
                cmbIsActive.SelectedIndex = 0
                txtExistingCode.Focus()
                Exit Sub
            Else
                mDataSet = Nothing
            End If
        End If

        SSQL = ""
        SSQL = "Select * from Employee_Mst Where CompCode='" & iStr1(0) & "'"
        SSQL = SSQL & " And LocCode='" & iStr2(0) & "'"
        SSQL = SSQL & " And EmpPrefix='" & Trim(cmbPrefix.Text) & "'"
        SSQL = SSQL & " And EmpNo='" & Val(Remove_Single_Quote(Trim(txtEmpNo.Text))) & "'"

        mDataSet = ReturnMultipleValue(SSQL)


        If mDataSet.Tables(0).Rows.Count > 0 Then
            txtExistingCode.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("ExistingCode")), " ", mDataSet.Tables(0).Rows(0)("ExistingCode"))
            txtMachineID.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("MachineID")), " ", mDataSet.Tables(0).Rows(0)("MachineID"))

            txtFirstName.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("FirstName")), " ", mDataSet.Tables(0).Rows(0)("FirstName"))
            txtLastName.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("LastName")), " ", mDataSet.Tables(0).Rows(0)("LastName"))
            txtInitial.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("MiddleInitial")), " ", mDataSet.Tables(0).Rows(0)("MiddleInitial"))

            cmbGender.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Gender")), cmbGender.SelectedIndex = 0, mDataSet.Tables(0).Rows(0)("Gender"))
            dtpDOB.Value = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("BirthDate")), Now, mDataSet.Tables(0).Rows(0)("BirthDate"))
            txtAge.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Age")), " ", mDataSet.Tables(0).Rows(0)("Age"))
            dtpDOJ.Value = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("DOJ")), Now, mDataSet.Tables(0).Rows(0)("DOJ"))

            cmbMarital.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("MaritalStatus")), cmbMarital.SelectedIndex = 0, mDataSet.Tables(0).Rows(0)("MaritalStatus"))

            cmbDept.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("DeptName")), "", mDataSet.Tables(0).Rows(0)("DeptName"))
            txtDesignation.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Designation")), "", mDataSet.Tables(0).Rows(0)("Designation"))
            cmbPayPeriod.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("PayPeriod_Desc")), "", mDataSet.Tables(0).Rows(0)("PayPeriod_Desc"))
            txtBaseSalary.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("BaseSalary")), "", mDataSet.Tables(0).Rows(0)("BaseSalary"))
            txtPFNo.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("PFNo")), "", mDataSet.Tables(0).Rows(0)("PFNo"))
            txtNominee.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Nominee")), "", mDataSet.Tables(0).Rows(0)("Nominee"))
            txtESINo.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("ESINo")), "", mDataSet.Tables(0).Rows(0)("ESINo"))
            txtStdWrkHrs.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("StdWrkHrs")), "", mDataSet.Tables(0).Rows(0)("StdWrkHrs"))
            cmbOTEligible.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("OTEligible")), "", mDataSet.Tables(0).Rows(0)("OTEligible"))
            txtNationality.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Nationality")), "", mDataSet.Tables(0).Rows(0)("Nationality"))
            txtQualification.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Qualification")), "", mDataSet.Tables(0).Rows(0)("Qualification"))
            txtCertificate.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Certificate")), "", mDataSet.Tables(0).Rows(0)("Certificate"))

            'Working Hours
            txtTot_Work_Hours.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Working_Hours")), "", mDataSet.Tables(0).Rows(0)("Working_Hours"))
            txtCalculate_Work_Hours.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Calculate_Work_Hours")), "", mDataSet.Tables(0).Rows(0)("Calculate_Work_Hours"))
            txtOT_Hours.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("OT_Hours")), "", mDataSet.Tables(0).Rows(0)("OT_Hours"))

            txtFamilyDetails.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("FamilyDetails")), "", mDataSet.Tables(0).Rows(0)("FamilyDetails"))
            txtRecuritmentThrough.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("RecuritmentThro")), "", mDataSet.Tables(0).Rows(0)("RecuritmentThro"))
            txtIDMark1.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("IDMark1")), "", mDataSet.Tables(0).Rows(0)("IDMark1"))
            txtIDMark2.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("IDMark2")), "", mDataSet.Tables(0).Rows(0)("IDMark2"))
            cmbBloodGroup.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("BloodGroup")), "", mDataSet.Tables(0).Rows(0)("BloodGroup"))
            cmbHandicapped.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Handicapped")), "", mDataSet.Tables(0).Rows(0)("Handicapped"))
            txtHeight.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Height")), "", mDataSet.Tables(0).Rows(0)("Height"))
            txtWeight.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Weight")), "", mDataSet.Tables(0).Rows(0)("Weight"))
            txtAddress1.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Address1")), "", mDataSet.Tables(0).Rows(0)("Address1"))
            txtAddress2.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Address2")), "", mDataSet.Tables(0).Rows(0)("Address2"))

            txtBankName.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("BankName")), "", mDataSet.Tables(0).Rows(0)("BankName"))
            txtBranchCode.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("BranchCode")), "", mDataSet.Tables(0).Rows(0)("BranchCode"))
            txtAccountNo.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("AccountNo")), "", mDataSet.Tables(0).Rows(0)("AccountNo"))

            cmbEmpStatus.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("EmpStatus")), "", mDataSet.Tables(0).Rows(0)("EmpStatus"))
            cmbIsActive.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("IsActive")), "", mDataSet.Tables(0).Rows(0)("IsActive"))

            cmbEmpType.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("TypeName")), "", mDataSet.Tables(0).Rows(0)("TypeName"))
            cmbCatName.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("CatName")), "", mDataSet.Tables(0).Rows(0)("CatName"))
            cmbSubCatName.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("SubCatName")), "", mDataSet.Tables(0).Rows(0)("SubCatName"))
            cmbShiftType.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("ShiftType")), "", mDataSet.Tables(0).Rows(0)("ShiftType"))
            cmbWages.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Wages")), "", mDataSet.Tables(0).Rows(0)("Wages"))
            txtReligion.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Religion")), "", mDataSet.Tables(0).Rows(0)("Religion"))


            txtAdharCard.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("AdharNo")), "", mDataSet.Tables(0).Rows(0)("AdharNo"))
            txtVoterId.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("VoterID")), "", mDataSet.Tables(0).Rows(0)("VoterID"))
            txtDriving.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Driving")), "", mDataSet.Tables(0).Rows(0)("Driving"))
            txtPassport.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Passport")), "", mDataSet.Tables(0).Rows(0)("Passport"))
            txtRationCard.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("RationCard")), "", mDataSet.Tables(0).Rows(0)("RationCard"))
            txtPanCard.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("PanCard")), "", mDataSet.Tables(0).Rows(0)("PanCard"))
            txtSmart.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("SmartCard")), "", mDataSet.Tables(0).Rows(0)("SmartCard"))
            txtOthers.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Others")), "", mDataSet.Tables(0).Rows(0)("Others"))

            If mDataSet.Tables(0).Rows(0)("AttnIncEligible") = "Yes" Then
                txtAttn_Inc_Eligible.Text = "Yes"
            Else
                txtAttn_Inc_Eligible.Text = "No"
            End If


            Dim Rec As String = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("RecutersMob")), "-", mDataSet.Tables(0).Rows(0)("RecutersMob"))
            Dim Rec1() As String = Split(Rec, "-")

            If Rec <> "" Then txtRecmob1.Text = Rec1(1)

            Dim ParMob As String = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("parentsMobile")), "-", mDataSet.Tables(0).Rows(0)("parentsMobile"))
            Dim ParMob1() As String = Split(ParMob, "-")
            If ParMob <> "" Then txtParMob1.Text = ParMob1(1)
            Dim ParPh As String = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("ParentsPhone")), "-", mDataSet.Tables(0).Rows(0)("ParentsPhone"))
            Dim Parph1() As String = Split(ParMob, "-")
            If ParPh <> "" Then
                txtLL.Text = Parph1(0)
                txtll1.Text = Parph1(1)
            End If

            If (mDataSet.Tables(0).Rows(0)("SamepresentAddress").ToString() = "1") Then
                cbPermenant.Checked = True
            Else
                cbPermenant.Checked = False
            End If
            Dim qry As String = "Select * from ExperienceDetails Where CompCode='" & iStr1(0) & "'"
            qry = qry & " And LocCode='" & iStr2(0) & "'"
            qry = qry & " And EmpNo='" & Val(Remove_Single_Quote(Trim(txtEmpNo.Text))) & "'"
            Dim gridDataset As New DataSet
            gridDataset = ReturnMultipleValue(qry)
            If (gridDataset.Tables(0).Rows.Count > 0) Then
                For i As Integer = 0 To gridDataset.Tables(0).Rows.Count - 1
                    Dim n As Integer
                    n = DataGridView1.Rows.Add()
                    DataGridView1.Rows(n).Cells(0).Value = n + 1
                    DataGridView1.Rows(n).Cells(1).Value = gridDataset.Tables(0).Rows(i)("YearsExp").ToString()
                    DataGridView1.Rows(n).Cells(2).Value = gridDataset.Tables(0).Rows(i)("MonthsExp").ToString()
                    DataGridView1.Rows(n).Cells(3).Value = gridDataset.Tables(0).Rows(i)("CompanyName").ToString()
                    DataGridView1.Rows(n).Cells(4).Value = gridDataset.Tables(0).Rows(i)("Designation").ToString()
                    DataGridView1.Rows(n).Cells(5).Value = gridDataset.Tables(0).Rows(i)("Department").ToString()
                    DataGridView1.Rows(n).Cells(6).Value = gridDataset.Tables(0).Rows(i)("PeriodFrom").ToString()
                    DataGridView1.Rows(n).Cells(7).Value = gridDataset.Tables(0).Rows(i)("PeriodTo").ToString()
                    DataGridView1.Rows(n).Cells(8).Value = gridDataset.Tables(0).Rows(i)("Contactname").ToString()
                    DataGridView1.Rows(n).Cells(9).Value = gridDataset.Tables(0).Rows(i)("Mobile").ToString()
                    DataGridView1.Rows(n).Cells(10).Value = gridDataset.Tables(0).Rows(i)("phone").ToString()
                Next
            End If
            If mDataSet.Tables(0).Rows(0)("IsNonAdmin") = "0" Then
                chkNonAdmin.Checked = False
            Else
                chkNonAdmin.Checked = True
            End If

            Dim sFilePath As String = ""
            Dim mUnitName As String = ""


            If UCase(Trim(iStr2(0))) = "UNIT I" Then
                mUnitName = "Unit1-Photos"
            ElseIf UCase(Trim(iStr2(0))) = "UNIT II" Then
                mUnitName = "Unit2-Photos"
            Else
                mUnitName = "Tapes-Photos"
            End If

            sFilePath = Application.StartupPath & "\" & Trim(mUnitName) _
                                     & "\" & Trim(txtMachineID.Text) & ".JPG"

            If Not IO.File.Exists(sFilePath) Then
                sFilePath = Application.StartupPath & "\" & Trim(mUnitName) _
                                     & "\No-Iamge.JPG"
            End If

            If sFilePath = "" Then Exit Sub

            pbEmployee.BackgroundImage = Image.FromFile(sFilePath)

            txtExistingCode.Focus()
            Exit Sub
        End If
    End Sub

    Private Sub cmdPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdPrint.Click
        If Trim(cmbCompCode.Text) = "" Then
            MessageBox.Show("Please select company details.", "Altius", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Information)
            cmbCompCode.Focus()
            Exit Sub
        End If

        If Trim(cmbLocCode.Text) = "" Then
            MessageBox.Show("Please select location details.", "Altius", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Information)
            cmbLocCode.Focus()
            Exit Sub
        End If

        If Trim(cmbPrefix.Text) = "" Then
            MessageBox.Show("Please select prefix details.", "Altius", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Information)
            cmbPrefix.Focus()
            Exit Sub
        End If

        If Trim(txtEmpNo.Text) = "" Then
            MessageBox.Show("Please enter employee no.", "Altius", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Information)
            txtEmpNo.Focus()
            Exit Sub
        End If

        Dim iStr1() As String
        Dim iStr2() As String
        Dim strPhotoPath As String
        iStr1 = Split(Trim(cmbCompCode.Text), " | ")
        iStr2 = Split(Trim(cmbLocCode.Text), " | ")

        SSQL = ""
        SSQL = "SELECT CatName,SubCatName,EmpPrefix,EmpNo,MachineID,FirstName"
        SSQL = SSQL & " ,DeptName,Designation,DOJ,Created_By,isnull(Created_Date,'1900-01-01')"
        SSQL = SSQL & " FROM EMPLOYEE_MST"
        SSQL = SSQL & " Where CompCode='" & iStr1(0) & "'"
        SSQL = SSQL & " And LocCode='" & iStr2(0) & "'"
        SSQL = SSQL & " And EmpPrefix='" & Trim(cmbPrefix.Text) & "'"
        SSQL = SSQL & " And EmpNo='" & Trim(txtEmpNo.Text) & "'"

        mDataSet = Nothing
        mDataSet = ReturnMultipleValue(SSQL)
        strPhotoPath = Application.StartupPath & "\" & Trim(iStr2(0)) _
                                     & "\" & Trim(mDataSet.Tables(0).Rows(0)("MachineID")) & ".JPG"

        If Not IO.File.Exists(strPhotoPath) Then
            strPhotoPath = Application.StartupPath & "\" & Trim(iStr2(0)) _
                                 & "\No-Iamge.JPG"
        End If

        'Dim fs As New FileStream(strPhotoPath, FileMode.Open)
        'Dim br As New BinaryReader(fs)
        Dim cryRep As New ReportDocument
        Dim cryView As New frmRepView

        If mDataSet Is Nothing OrElse mDataSet.Tables.Count <= 0 Then Exit Sub

        cryRep.Load(Application.StartupPath & "\Reports\" & "NewEmp.rpt")
        cryRep.SetDataSource(mDataSet.Tables(0))
        'cryRep.Subreports.Item("Name1").SetDataSource(mDataSet.Tables(0))
        cryView.crViewer.ReportSource = cryRep
        cryView.crViewer.Refresh()
        cryView.Show()
    End Sub

    'Private Sub txtExistingCode_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtExistingCode.LostFocus



    '    Dim iStr1() As String
    '    Dim iStr2() As String

    '    iStr1 = Split(Trim(cmbCompCode.Text), " | ")
    '    iStr2 = Split(Trim(cmbLocCode.Text), " | ")

    '    'Check With Non Admin User
    '    If mUserLocation <> "" Then
    '        SSQL = ""
    '        SSQL = "Select * from Employee_Mst Where CompCode='" & iStr1(0) & "'"
    '        SSQL = SSQL & " And LocCode='" & iStr2(0) & "'"
    '        SSQL = SSQL & " And ExisistingCode='" & Val(Remove_Single_Quote(Trim(txtExistingCode.Text))) & "'"
    '        mDataSet = ReturnMultipleValue(SSQL)
    '        If mDataSet.Tables(0).Rows.Count > 0 Then
    '            mDataSet = Nothing
    '            Clear_Screen_Details()
    '            txtExistingCode.Text = ""
    '            txtMachineID.Text = ""
    '            txtFirstName.Text = ""
    '            txtLastName.Text = ""
    '            txtInitial.Text = ""
    '            cmbGender.SelectedIndex = 0
    '            dtpDOB.Value = Now
    '            txtAge.Text = ""
    '            cmbMarital.SelectedIndex = 0
    '            cmbEmpStatus.SelectedIndex = 0
    '            cmbIsActive.SelectedIndex = 0
    '            txtExistingCode.Focus()
    '            Exit Sub
    '        Else
    '            mDataSet = Nothing
    '        End If
    '    End If

    '    SSQL = ""
    '    SSQL = "Select * from Employee_Mst Where CompCode='" & iStr1(0) & "'"
    '    SSQL = SSQL & " And LocCode='" & iStr2(0) & "'"
    '    SSQL = SSQL & " And ExistingCode='" & Val(Remove_Single_Quote(txtExistingCode.Text)) & "'"

    '    mDataSet = ReturnMultipleValue(SSQL)


    '    If mDataSet.Tables(0).Rows.Count > 0 Then
    '        txtEmpNo.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("EmpNo")), " ", mDataSet.Tables(0).Rows(0)("EmpNo"))
    '        txtMachineID.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("MachineID")), " ", mDataSet.Tables(0).Rows(0)("MachineID"))

    '        txtFirstName.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("FirstName")), " ", mDataSet.Tables(0).Rows(0)("FirstName"))
    '        txtLastName.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("LastName")), " ", mDataSet.Tables(0).Rows(0)("LastName"))
    '        txtInitial.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("MiddleInitial")), " ", mDataSet.Tables(0).Rows(0)("MiddleInitial"))

    '        cmbGender.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Gender")), cmbGender.SelectedIndex = 0, mDataSet.Tables(0).Rows(0)("Gender"))
    '        dtpDOB.Value = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("BirthDate")), Now, mDataSet.Tables(0).Rows(0)("BirthDate"))
    '        txtAge.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Age")), " ", mDataSet.Tables(0).Rows(0)("Age"))
    '        dtpDOJ.Value = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("DOJ")), Now, mDataSet.Tables(0).Rows(0)("DOJ"))

    '        cmbMarital.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("MaritalStatus")), cmbMarital.SelectedIndex = 0, mDataSet.Tables(0).Rows(0)("MaritalStatus"))

    '        cmbDept.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("DeptName")), "", mDataSet.Tables(0).Rows(0)("DeptName"))
    '        txtDesignation.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Designation")), "", mDataSet.Tables(0).Rows(0)("Designation"))
    '        cmbPayPeriod.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("PayPeriod_Desc")), "", mDataSet.Tables(0).Rows(0)("PayPeriod_Desc"))
    '        txtBaseSalary.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("BaseSalary")), "", mDataSet.Tables(0).Rows(0)("BaseSalary"))
    '        txtPFNo.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("PFNo")), "", mDataSet.Tables(0).Rows(0)("PFNo"))
    '        txtNominee.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Nominee")), "", mDataSet.Tables(0).Rows(0)("Nominee"))
    '        txtESINo.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("ESINo")), "", mDataSet.Tables(0).Rows(0)("ESINo"))
    '        txtStdWrkHrs.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("StdWrkHrs")), "", mDataSet.Tables(0).Rows(0)("StdWrkHrs"))
    '        cmbOTEligible.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("OTEligible")), "", mDataSet.Tables(0).Rows(0)("OTEligible"))
    '        txtNationality.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Nationality")), "", mDataSet.Tables(0).Rows(0)("Nationality"))
    '        txtQualification.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Qualification")), "", mDataSet.Tables(0).Rows(0)("Qualification"))
    '        txtCertificate.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Certificate")), "", mDataSet.Tables(0).Rows(0)("Certificate"))

    '        'Working Hours
    '        txtTot_Work_Hours.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Working_Hours")), "", mDataSet.Tables(0).Rows(0)("Working_Hours"))
    '        txtCalculate_Work_Hours.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Calculate_Work_Hours")), "", mDataSet.Tables(0).Rows(0)("Calculate_Work_Hours"))
    '        txtOT_Hours.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("OT_Hours")), "", mDataSet.Tables(0).Rows(0)("OT_Hours"))

    '        txtFamilyDetails.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("FamilyDetails")), "", mDataSet.Tables(0).Rows(0)("FamilyDetails"))
    '        txtRecuritmentThrough.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("RecuritmentThro")), "", mDataSet.Tables(0).Rows(0)("RecuritmentThro"))
    '        txtIDMark1.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("IDMark1")), "", mDataSet.Tables(0).Rows(0)("IDMark1"))
    '        txtIDMark2.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("IDMark2")), "", mDataSet.Tables(0).Rows(0)("IDMark2"))
    '        cmbBloodGroup.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("BloodGroup")), "", mDataSet.Tables(0).Rows(0)("BloodGroup"))
    '        cmbHandicapped.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Handicapped")), "", mDataSet.Tables(0).Rows(0)("Handicapped"))
    '        txtHeight.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Height")), "", mDataSet.Tables(0).Rows(0)("Height"))
    '        txtWeight.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Weight")), "", mDataSet.Tables(0).Rows(0)("Weight"))
    '        txtAddress1.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Address1")), "", mDataSet.Tables(0).Rows(0)("Address1"))
    '        txtAddress2.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Address2")), "", mDataSet.Tables(0).Rows(0)("Address2"))

    '        txtBankName.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("BankName")), "", mDataSet.Tables(0).Rows(0)("BankName"))
    '        txtBranchCode.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("BranchCode")), "", mDataSet.Tables(0).Rows(0)("BranchCode"))
    '        txtAccountNo.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("AccountNo")), "", mDataSet.Tables(0).Rows(0)("AccountNo"))

    '        cmbEmpStatus.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("EmpStatus")), "", mDataSet.Tables(0).Rows(0)("EmpStatus"))
    '        cmbIsActive.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("IsActive")), "", mDataSet.Tables(0).Rows(0)("IsActive"))

    '        cmbEmpType.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("TypeName")), "", mDataSet.Tables(0).Rows(0)("TypeName"))
    '        cmbCatName.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("CatName")), "", mDataSet.Tables(0).Rows(0)("CatName"))
    '        cmbSubCatName.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("SubCatName")), "", mDataSet.Tables(0).Rows(0)("SubCatName"))
    '        cmbShiftType.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("ShiftType")), "", mDataSet.Tables(0).Rows(0)("ShiftType"))
    '        cmbWages.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Wages")), "", mDataSet.Tables(0).Rows(0)("Wages"))
    '        txtReligion.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Religion")), "", mDataSet.Tables(0).Rows(0)("Religion"))
    '        cmbWeakOff.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("WeekOff")), "", mDataSet.Tables(0).Rows(0)("WeekOff"))
    '        Dim Rec As String = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("RecutersMob")), "-", mDataSet.Tables(0).Rows(0)("RecutersMob"))
    '        Dim Rec1() As String = Split(Rec, "-")

    '        If Rec <> "" Then txtRecmob1.Text = Rec1(1)

    '        Dim ParMob As String = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("parentsMobile")), "-", mDataSet.Tables(0).Rows(0)("parentsMobile"))
    '        Dim ParMob1() As String = Split(ParMob, "-")
    '        If ParMob <> "" Then txtParMob1.Text = ParMob1(1)
    '        Dim ParPh As String = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("ParentsPhone")), "-", mDataSet.Tables(0).Rows(0)("ParentsPhone"))
    '        Dim Parph1() As String = Split(ParMob, "-")
    '        If ParPh <> "" Then
    '            txtLL.Text = Parph1(0)
    '            txtll1.Text = Parph1(1)
    '        End If

    '        If (mDataSet.Tables(0).Rows(0)("SamepresentAddress").ToString() = "1") Then
    '            cbPermenant.Checked = True
    '        Else
    '            cbPermenant.Checked = False
    '        End If
    '        Dim qry As String = "Select * from ExperienceDetails Where CompCode='" & iStr1(0) & "'"
    '        qry = qry & " And LocCode='" & iStr2(0) & "'"
    '        qry = qry & " And EmpNo='" & Val(Remove_Single_Quote(Trim(txtEmpNo.Text))) & "'"
    '        Dim gridDataset As New DataSet
    '        gridDataset = ReturnMultipleValue(qry)
    '        If (gridDataset.Tables(0).Rows.Count > 0) Then
    '            For i As Integer = 0 To gridDataset.Tables(0).Rows.Count - 1
    '                Dim n As Integer
    '                n = DataGridView1.Rows.Add()
    '                DataGridView1.Rows(n).Cells(0).Value = n + 1
    '                DataGridView1.Rows(n).Cells(1).Value = gridDataset.Tables(0).Rows(i)("YearsExp").ToString()
    '                DataGridView1.Rows(n).Cells(2).Value = gridDataset.Tables(0).Rows(i)("MonthsExp").ToString()
    '                DataGridView1.Rows(n).Cells(3).Value = gridDataset.Tables(0).Rows(i)("CompanyName").ToString()
    '                DataGridView1.Rows(n).Cells(4).Value = gridDataset.Tables(0).Rows(i)("Designation").ToString()
    '                DataGridView1.Rows(n).Cells(5).Value = gridDataset.Tables(0).Rows(i)("Department").ToString()
    '                DataGridView1.Rows(n).Cells(6).Value = gridDataset.Tables(0).Rows(i)("PeriodFrom").ToString()
    '                DataGridView1.Rows(n).Cells(7).Value = gridDataset.Tables(0).Rows(i)("PeriodTo").ToString()
    '                DataGridView1.Rows(n).Cells(8).Value = gridDataset.Tables(0).Rows(i)("Contactname").ToString()
    '                DataGridView1.Rows(n).Cells(9).Value = gridDataset.Tables(0).Rows(i)("Mobile").ToString()
    '                DataGridView1.Rows(n).Cells(10).Value = gridDataset.Tables(0).Rows(i)("phone").ToString()
    '            Next
    '        End If
    '        If mDataSet.Tables(0).Rows(0)("IsNonAdmin") = "0" Then
    '            chkNonAdmin.Checked = False
    '        Else
    '            chkNonAdmin.Checked = True
    '        End If

    '        Dim sFilePath As String = ""
    '        Dim mUnitName As String = ""


    '        If UCase(Trim(iStr2(0))) = "UNIT I" Then
    '            mUnitName = "Unit1-Photos"
    '        ElseIf UCase(Trim(iStr2(0))) = "UNIT II" Then
    '            mUnitName = "Unit2-Photos"
    '        Else
    '            mUnitName = "Tapes-Photos"
    '        End If

    '        sFilePath = Application.StartupPath & "\" & Trim(mUnitName) _
    '                                 & "\" & Trim(txtMachineID.Text) & ".JPG"

    '        If Not IO.File.Exists(sFilePath) Then
    '            sFilePath = Application.StartupPath & "\" & Trim(mUnitName) _
    '                                 & "\No-Iamge.JPG"
    '        End If

    '        If sFilePath = "" Then Exit Sub

    '        pbEmployee.BackgroundImage = Image.FromFile(sFilePath)

    '        txtExistingCode.Focus()
    '        Exit Sub
    '    End If

    'End Sub

  

    Private Sub pbEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbEmployee.Click
        Dim fDialog As OpenFileDialog = New OpenFileDialog()
        fDialog.InitialDirectory = "C:\"
        fDialog.Filter = "Picture Files(*.jpeg;*.jpg;*.png;*.bmp;*.gif;) | *.jpeg;*.jpg;*.png;*.bmp;*.gif;"
        fDialog.FilterIndex = 2
        fDialog.RestoreDirectory = True
        If fDialog.ShowDialog() = DialogResult.OK Then
            pbEmployee.ImageLocation = fDialog.FileName
            Dim sFilePath As String = ""
            Dim mUnitName As String = ""
            Dim iStr1() As String
            Dim iStr2() As String

            iStr1 = Split(Trim(cmbCompCode.Text), " | ")
            iStr2 = Split(Trim(cmbLocCode.Text), " | ")

            If UCase(Trim(iStr2(0))) = "UNIT I" Then
                mUnitName = "Unit1-Photos"
            ElseIf UCase(Trim(iStr2(0))) = "UNIT II" Then
                mUnitName = "Unit2-Photos"
            Else
                mUnitName = "Tapes-Photos"
            End If

            sFilePath = Application.StartupPath & "\" & Trim(mUnitName) _
            & "\" & Trim(txtMachineID.Text) & ".JPG"
            'sFilePath = "\\192.168.1.6\Photos\" & Trim(txtMachineID.Text) & ".JPG"
            If Not IO.File.Exists(sFilePath) Then
                'My.Computer.FileSystem.DeleteFile(sFilePath)
                My.Computer.FileSystem.CopyFile(pbEmployee.ImageLocation, sFilePath)
            Else
                pbEmployee.BackgroundImage.Dispose()
                pbEmployee.BackgroundImage = Nothing
                My.Computer.FileSystem.DeleteFile(sFilePath)
                My.Computer.FileSystem.CopyFile(pbEmployee.ImageLocation, sFilePath)

            End If

        End If
    End Sub

    Private Sub cbPermenant_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbPermenant.CheckedChanged
        If cbPermenant.Checked = True Then
            txtAddress2.Text = txtAddress1.Text
        End If
    End Sub

    Private Sub txtAddress1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtAddress1.TextChanged
        If cbPermenant.Checked = True Then
            txtAddress2.Text = txtAddress1.Text
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Trim(cmbYears.Text) = "" Then
            MessageBox.Show("Please select the Experience in Years.", "Altius", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Information)
            cmbPrefix.Focus()
            Exit Sub
        End If
        If Trim(cmbMonths.Text) = "" Then
            MessageBox.Show("Please select Experience months.", "Altius", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Information)
            cmbPrefix.Focus()
            Exit Sub
        End If
        If Trim(txtCompanyName.Text) = "" Then
            MessageBox.Show("Please Enter Company Name.", "Altius", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Information)
            cmbPrefix.Focus()
            Exit Sub
        End If
        If Trim(txtPreviousDesignation.Text) = "" Then
            MessageBox.Show("Please Enter Designation.", "Altius", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Information)
            cmbPrefix.Focus()
            Exit Sub
        End If
        If Trim(txtPreviousDepartment.Text) = "" Then
            MessageBox.Show("Please Enter the Department.", "Altius", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Information)
            cmbPrefix.Focus()
            Exit Sub
        End If
        If Trim(txtPeriodFrom.Text) = "" Then
            MessageBox.Show("Please Enter Period From.", "Altius", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Information)
            cmbPrefix.Focus()
            Exit Sub
        End If
        If Trim(txtPeriodTo.Text) = "" Then
            MessageBox.Show("Please Enter the Period To.", "Altius", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Information)
            cmbPrefix.Focus()
            Exit Sub
        End If
        If Trim(txtContactname.Text) = "" Then
            MessageBox.Show("Please Enter the Contact Person Name.", "Altius", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Information)
            cmbPrefix.Focus()
            Exit Sub
        End If
        If Trim(txtConll.Text) <> "" Then
            If Trim(txtConll1.Text) = "" Then
                MessageBox.Show("Please Enter the Phone No Properly.", "Altius", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Information)
                cmbPrefix.Focus()
                Exit Sub
            End If

        End If
        Dim n As Integer
        n = DataGridView1.Rows.Add()
        DataGridView1.Rows(n).Cells(0).Value = n + 1
        DataGridView1.Rows(n).Cells(1).Value = cmbYears.Text
        DataGridView1.Rows(n).Cells(2).Value = cmbMonths.Text
        DataGridView1.Rows(n).Cells(3).Value = Trim(txtCompanyName.Text)
        DataGridView1.Rows(n).Cells(4).Value = Trim(txtPreviousDesignation.Text)
        DataGridView1.Rows(n).Cells(5).Value = Trim(txtPreviousDepartment.Text)
        DataGridView1.Rows(n).Cells(6).Value = Trim(txtPeriodFrom.Text)
        DataGridView1.Rows(n).Cells(7).Value = Trim(txtPeriodTo.Text)
        DataGridView1.Rows(n).Cells(8).Value = Trim(txtContactname.Text)
        DataGridView1.Rows(n).Cells(9).Value = (Trim(txtConMob.Text) & "-" & Trim(txtConMob1.Text)).ToString()
        DataGridView1.Rows(n).Cells(10).Value = (Trim(txtConll.Text) & "-" & Trim(txtConll1.Text)).ToString()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        clr()
    End Sub
    Private Sub clr()
        cmbYears.Text = ""
        cmbMonths.Text = "0"
        txtCompanyName.Text = ""
        txtPreviousDepartment.Text = ""
        txtPreviousDesignation.Text = ""
        txtPeriodFrom.Text = ""
        txtPeriodTo.Text = ""
        txtContactname.Text = ""
        txtConMob1.Text = ""
        txtConll.Text = ""
        txtConll1.Text = ""

    End Sub

    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub

    Private Sub DataGridView1_CellDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellDoubleClick
        If (e.RowIndex >= -1) And (e.ColumnIndex >= -1) Then
            Dim mob_con As String
            Dim mob_con1() As String
            Dim Con_ph As String
            Dim Con_ph1() As String
            cmbYears.Text = DataGridView1.Rows(e.RowIndex).Cells(1).Value
            cmbMonths.Text = DataGridView1.Rows(e.RowIndex).Cells(2).Value
            txtCompanyName.Text = DataGridView1.Rows(e.RowIndex).Cells(3).Value
            txtPreviousDesignation.Text = DataGridView1.Rows(e.RowIndex).Cells(4).Value
            txtPreviousDepartment.Text = DataGridView1.Rows(e.RowIndex).Cells(5).Value
            txtPeriodFrom.Text = DataGridView1.Rows(e.RowIndex).Cells(6).Value
            txtPeriodTo.Text = DataGridView1.Rows(e.RowIndex).Cells(7).Value
            txtContactname.Text = DataGridView1.Rows(e.RowIndex).Cells(8).Value
            mob_con = DataGridView1.Rows(e.RowIndex).Cells(9).Value
            mob_con1 = Split(mob_con, "-")
            txtConMob1.Text = mob_con1(1)
            Con_ph = DataGridView1.Rows(e.RowIndex).Cells(10).Value
            Con_ph1 = Split(Con_ph, "-")
            txtConll.Text = Con_ph1(0)
            txtConll1.Text = Con_ph1(1)
            Dim row As Integer
            Dim index As Integer
            index = DataGridView1.SelectedRows.Item(0).Index
            selRow = DataGridView1.Rows.Item(index)
            DataGridView1.Rows.Remove(selRow)
            row = row - 1
        End If
    End Sub

    'Private Sub btnMachine_ID_Update_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMachine_ID_Update.Click

    '    'Dim Query As String
    '    'Dim Emp_Ds As New DataSet
    '    'Dim iStr1() As String
    '    'Dim iStr2() As String
    '    'Dim i As Int32

    '    'If Trim(cmbCompCode.Text) = "" Then
    '    '    MessageBox.Show("Plase select company.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '    '    cmbCompCode.Focus()
    '    '    Exit Sub
    '    'End If
    '    'If Trim(cmbLocCode.Text) = "" Then
    '    '    MessageBox.Show("Plase select loaction.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '    '    cmbLocCode.Focus()
    '    '    Exit Sub
    '    'End If

    '    'iStr1 = Split(Trim(cmbCompCode.Text), " | ")
    '    'iStr2 = Split(Trim(cmbLocCode.Text), " | ")

    '    ''Machine ID Encrypt
    '    'Dim StrMachine_ID_Encrypt As String = ""
    '    'Dim Machine_ID As String
    '    ''StrMachine_ID_Encrypt = Encryption(Trim(txtMachineID.Text))

    '    'SSQL = "Select * from Employee_Mst where CompCode='" & iStr1(0) & "' and LocCode='" & iStr2(0) & "'"
    '    'Emp_Ds = ReturnMultipleValue(SSQL)
    '    'For i = 0 To Emp_Ds.Tables(0).Rows.Count - 1
    '    '    Machine_ID = Emp_Ds.Tables(0).Rows(i)("MachineID").ToString()
    '    '    StrMachine_ID_Encrypt = Encryption(Machine_ID)
    '    '    If Machine_ID <> "" Then
    '    '        Query = "Update Employee_Mst set MachineID_Encrypt='" & StrMachine_ID_Encrypt & "' where CompCode='" & iStr1(0) & "' and LocCode='" & iStr2(0) & "' and MachineID='" & Machine_ID & "'"
    '    '        InsertDeleteUpdate(Query)
    '    '    End If
    '    'Next

    '    'MsgBox("All Machine ID Encrypt Successfully", MsgBoxStyle.Information)
    '    'Exit Sub
    'End Sub

    Private Sub GroupBox3_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupBox3.Enter

    End Sub

    Private Sub txtExistingCode_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtExistingCode.LostFocus

        If Trim(txtExistingCode.Text) = "" Then
            txtExistingCode.Text = ""
            txtMachineID.Text = ""
            txtFirstName.Text = ""
            txtLastName.Text = ""
            txtInitial.Text = ""
            cmbGender.SelectedIndex = 0
            dtpDOB.Value = Now
            txtAge.Text = ""
            cmbMarital.SelectedIndex = 0
            cmbEmpStatus.SelectedIndex = 0
            cmbIsActive.SelectedIndex = 0
            txtExistingCode.Focus()
            Exit Sub
        End If


        Dim iStr1() As String
        Dim iStr2() As String

        iStr1 = Split(Trim(cmbCompCode.Text), " | ")
        iStr2 = Split(Trim(cmbLocCode.Text), " | ")

        'Check With Non Admin User
        If mUserLocation <> "" Then
            SSQL = ""
            SSQL = "Select * from Employee_Mst Where CompCode='" & iStr1(0) & "'"
            SSQL = SSQL & " And LocCode='" & iStr2(0) & "'"
            SSQL = SSQL & " And ExistingCode='" & Val(Remove_Single_Quote(Trim(txtExistingCode.Text))) & "'"
            mDataSet = ReturnMultipleValue(SSQL)
            If mDataSet.Tables(0).Rows.Count > 0 Then
                mDataSet = Nothing
                Clear_Screen_Details()
                txtExistingCode.Text = ""
                txtMachineID.Text = ""
                txtFirstName.Text = ""
                txtLastName.Text = ""
                txtInitial.Text = ""
                cmbGender.SelectedIndex = 0
                dtpDOB.Value = Now
                txtAge.Text = ""
                cmbMarital.SelectedIndex = 0
                cmbEmpStatus.SelectedIndex = 0
                cmbIsActive.SelectedIndex = 0
                txtExistingCode.Focus()
                Exit Sub
            Else
                mDataSet = Nothing
            End If
        End If

        SSQL = ""
        SSQL = "Select * from Employee_Mst Where CompCode='" & iStr1(0) & "'"
        SSQL = SSQL & " And LocCode='" & iStr2(0) & "'"
        SSQL = SSQL & " And ExistingCode='" & txtExistingCode.Text & "'"

        mDataSet = ReturnMultipleValue(SSQL)


        If mDataSet.Tables(0).Rows.Count > 0 Then
            txtEmpNo.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("EmpNo")), " ", mDataSet.Tables(0).Rows(0)("EmpNo"))
            txtMachineID.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("MachineID")), " ", mDataSet.Tables(0).Rows(0)("MachineID"))

            txtFirstName.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("FirstName")), " ", mDataSet.Tables(0).Rows(0)("FirstName"))
            txtLastName.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("LastName")), " ", mDataSet.Tables(0).Rows(0)("LastName"))
            txtInitial.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("MiddleInitial")), " ", mDataSet.Tables(0).Rows(0)("MiddleInitial"))

            cmbGender.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Gender")), cmbGender.SelectedIndex = 0, mDataSet.Tables(0).Rows(0)("Gender"))
            dtpDOB.Value = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("BirthDate")), Now, mDataSet.Tables(0).Rows(0)("BirthDate"))
            txtAge.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Age")), " ", mDataSet.Tables(0).Rows(0)("Age"))
            dtpDOJ.Value = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("DOJ")), Now, mDataSet.Tables(0).Rows(0)("DOJ"))

            cmbMarital.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("MaritalStatus")), cmbMarital.SelectedIndex = 0, mDataSet.Tables(0).Rows(0)("MaritalStatus"))

            cmbDept.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("DeptName")), "", mDataSet.Tables(0).Rows(0)("DeptName"))
            txtDesignation.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Designation")), "", mDataSet.Tables(0).Rows(0)("Designation"))
            cmbPayPeriod.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("PayPeriod_Desc")), "", mDataSet.Tables(0).Rows(0)("PayPeriod_Desc"))
            txtBaseSalary.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("BaseSalary")), "", mDataSet.Tables(0).Rows(0)("BaseSalary"))
            txtPFNo.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("PFNo")), "", mDataSet.Tables(0).Rows(0)("PFNo"))
            txtNominee.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Nominee")), "", mDataSet.Tables(0).Rows(0)("Nominee"))
            txtESINo.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("ESINo")), "", mDataSet.Tables(0).Rows(0)("ESINo"))
            txtStdWrkHrs.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("StdWrkHrs")), "", mDataSet.Tables(0).Rows(0)("StdWrkHrs"))
            cmbOTEligible.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("OTEligible")), "", mDataSet.Tables(0).Rows(0)("OTEligible"))
            txtNationality.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Nationality")), "", mDataSet.Tables(0).Rows(0)("Nationality"))
            txtQualification.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Qualification")), "", mDataSet.Tables(0).Rows(0)("Qualification"))
            txtCertificate.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Certificate")), "", mDataSet.Tables(0).Rows(0)("Certificate"))

            'Working Hours
            txtTot_Work_Hours.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Working_Hours")), "", mDataSet.Tables(0).Rows(0)("Working_Hours"))
            txtCalculate_Work_Hours.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Calculate_Work_Hours")), "", mDataSet.Tables(0).Rows(0)("Calculate_Work_Hours"))
            txtOT_Hours.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("OT_Hours")), "", mDataSet.Tables(0).Rows(0)("OT_Hours"))

            txtFamilyDetails.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("FamilyDetails")), "", mDataSet.Tables(0).Rows(0)("FamilyDetails"))
            txtRecuritmentThrough.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("RecuritmentThro")), "", mDataSet.Tables(0).Rows(0)("RecuritmentThro"))
            txtIDMark1.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("IDMark1")), "", mDataSet.Tables(0).Rows(0)("IDMark1"))
            txtIDMark2.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("IDMark2")), "", mDataSet.Tables(0).Rows(0)("IDMark2"))
            cmbBloodGroup.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("BloodGroup")), "", mDataSet.Tables(0).Rows(0)("BloodGroup"))
            cmbHandicapped.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Handicapped")), "", mDataSet.Tables(0).Rows(0)("Handicapped"))
            txtHeight.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Height")), "", mDataSet.Tables(0).Rows(0)("Height"))
            txtWeight.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Weight")), "", mDataSet.Tables(0).Rows(0)("Weight"))
            txtAddress1.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Address1")), "", mDataSet.Tables(0).Rows(0)("Address1"))
            txtAddress2.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Address2")), "", mDataSet.Tables(0).Rows(0)("Address2"))

            txtBankName.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("BankName")), "", mDataSet.Tables(0).Rows(0)("BankName"))
            txtBranchCode.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("BranchCode")), "", mDataSet.Tables(0).Rows(0)("BranchCode"))
            txtAccountNo.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("AccountNo")), "", mDataSet.Tables(0).Rows(0)("AccountNo"))

            cmbEmpStatus.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("EmpStatus")), "", mDataSet.Tables(0).Rows(0)("EmpStatus"))
            cmbIsActive.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("IsActive")), "", mDataSet.Tables(0).Rows(0)("IsActive"))

            cmbEmpType.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("TypeName")), "", mDataSet.Tables(0).Rows(0)("TypeName"))
            cmbCatName.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("CatName")), "", mDataSet.Tables(0).Rows(0)("CatName"))
            cmbSubCatName.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("SubCatName")), "", mDataSet.Tables(0).Rows(0)("SubCatName"))
            cmbShiftType.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("ShiftType")), "", mDataSet.Tables(0).Rows(0)("ShiftType"))
            cmbWages.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Wages")), "", mDataSet.Tables(0).Rows(0)("Wages"))
            txtReligion.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Religion")), "", mDataSet.Tables(0).Rows(0)("Religion"))
            cmbWeakOff.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("WeekOff")), "", mDataSet.Tables(0).Rows(0)("WeekOff"))




            txtAdharCard.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("AdharNo")), "", mDataSet.Tables(0).Rows(0)("AdharNo"))
            txtVoterId.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("VoterID")), "", mDataSet.Tables(0).Rows(0)("VoterID"))
            txtDriving.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Driving")), "", mDataSet.Tables(0).Rows(0)("Driving"))
            txtPassport.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Passport")), "", mDataSet.Tables(0).Rows(0)("Passport"))
            txtRationCard.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("RationCard")), "", mDataSet.Tables(0).Rows(0)("RationCard"))
            txtPanCard.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("PanCard")), "", mDataSet.Tables(0).Rows(0)("PanCard"))
            txtSmart.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("SmartCard")), "", mDataSet.Tables(0).Rows(0)("SmartCard"))
            txtOthers.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Others")), "", mDataSet.Tables(0).Rows(0)("Others"))

            If mDataSet.Tables(0).Rows(0)("AttnIncEligible") = "Yes" Then
                txtAttn_Inc_Eligible.Text = "Yes"
            Else
                txtAttn_Inc_Eligible.Text = "No"
            End If

            Dim Rec As String = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("RecutersMob")), "-", mDataSet.Tables(0).Rows(0)("RecutersMob"))
            Dim Rec1() As String = Split(Rec, "-")

            If Rec <> "" Then txtRecmob1.Text = Rec1(1)

            Dim ParMob As String = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("parentsMobile")), "-", mDataSet.Tables(0).Rows(0)("parentsMobile"))
            Dim ParMob1() As String = Split(ParMob, "-")
            If ParMob <> "" Then txtParMob1.Text = ParMob1(1)
            Dim ParPh As String = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("ParentsPhone")), "-", mDataSet.Tables(0).Rows(0)("ParentsPhone"))
            Dim Parph1() As String = Split(ParMob, "-")
            If ParPh <> "" Then
                txtLL.Text = Parph1(0)
                txtll1.Text = Parph1(1)
            End If

            If (mDataSet.Tables(0).Rows(0)("SamepresentAddress").ToString() = "1") Then
                cbPermenant.Checked = True
            Else
                cbPermenant.Checked = False
            End If
            Dim qry As String = "Select * from ExperienceDetails Where CompCode='" & iStr1(0) & "'"
            qry = qry & " And LocCode='" & iStr2(0) & "'"
            qry = qry & " And EmpNo='" & Val(Remove_Single_Quote(Trim(txtEmpNo.Text))) & "'"
            Dim gridDataset As New DataSet
            gridDataset = ReturnMultipleValue(qry)
            If (gridDataset.Tables(0).Rows.Count > 0) Then
                For i As Integer = 0 To gridDataset.Tables(0).Rows.Count - 1
                    Dim n As Integer
                    n = DataGridView1.Rows.Add()
                    DataGridView1.Rows(n).Cells(0).Value = n + 1
                    DataGridView1.Rows(n).Cells(1).Value = gridDataset.Tables(0).Rows(i)("YearsExp").ToString()
                    DataGridView1.Rows(n).Cells(2).Value = gridDataset.Tables(0).Rows(i)("MonthsExp").ToString()
                    DataGridView1.Rows(n).Cells(3).Value = gridDataset.Tables(0).Rows(i)("CompanyName").ToString()
                    DataGridView1.Rows(n).Cells(4).Value = gridDataset.Tables(0).Rows(i)("Designation").ToString()
                    DataGridView1.Rows(n).Cells(5).Value = gridDataset.Tables(0).Rows(i)("Department").ToString()
                    DataGridView1.Rows(n).Cells(6).Value = gridDataset.Tables(0).Rows(i)("PeriodFrom").ToString()
                    DataGridView1.Rows(n).Cells(7).Value = gridDataset.Tables(0).Rows(i)("PeriodTo").ToString()
                    DataGridView1.Rows(n).Cells(8).Value = gridDataset.Tables(0).Rows(i)("Contactname").ToString()
                    DataGridView1.Rows(n).Cells(9).Value = gridDataset.Tables(0).Rows(i)("Mobile").ToString()
                    DataGridView1.Rows(n).Cells(10).Value = gridDataset.Tables(0).Rows(i)("phone").ToString()
                Next
            End If
            If mDataSet.Tables(0).Rows(0)("IsNonAdmin") = "0" Then
                chkNonAdmin.Checked = False
            Else
                chkNonAdmin.Checked = True
            End If

            Dim sFilePath As String = ""
            Dim mUnitName As String = ""


            If UCase(Trim(iStr2(0))) = "UNIT I" Then
                mUnitName = "Unit1-Photos"
            ElseIf UCase(Trim(iStr2(0))) = "UNIT II" Then
                mUnitName = "Unit2-Photos"
            Else
                mUnitName = "Tapes-Photos"
            End If

            sFilePath = Application.StartupPath & "\" & Trim(mUnitName) _
                                     & "\" & Trim(txtMachineID.Text) & ".JPG"

            If Not IO.File.Exists(sFilePath) Then
                sFilePath = Application.StartupPath & "\" & Trim(mUnitName) _
                                     & "\No-Iamge.JPG"
            End If

            If sFilePath = "" Then Exit Sub

            pbEmployee.BackgroundImage = Image.FromFile(sFilePath)

            txtMachineID.Focus()
            Exit Sub
        End If
    End Sub

    Private Sub txtExistingCode_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtExistingCode.TextChanged

    End Sub

    Private Sub txtEmpNo_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtEmpNo.TextChanged

    End Sub

    Private Sub BtnEPay_Save_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnEPay_Save.Click

        'SSQL = ""
        'SSQL = "Select ExistingCode from Employee_Mst where Wages='NEW COMMERS'"
        'Dim Ds_Loop As New DataSet()
        'Ds_Loop = ReturnMultipleValue(SSQL)

        'With Ds_Loop.Tables(0)
        '    If .Rows.Count > 0 Then
        '        For iRow = 0 To .Rows.Count - 1
        '            txtExistingCode.Text = .Rows(iRow)("ExistingCode")
        '            txtExistingCode_LostFocus(sender, e)
        '            Call InsertE_Pay_EmployeeDetails()
        '            Clear_Screen_Details()
        '        Next
        '    End If

        'End With

        If Trim(cmbCompCode.Text) = "" Then
            MessageBox.Show("Plase select company.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbCompCode.Focus()
            Exit Sub
        End If
        If Trim(cmbLocCode.Text) = "" Then
            MessageBox.Show("Plase select loaction.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbLocCode.Focus()
            Exit Sub
        End If
        If Trim(cmbEmpType.Text) = "" Then
            MessageBox.Show("Plase select type.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbEmpType.Focus()
            Exit Sub
        End If
        If Trim(txtEmpNo.Text) = "" Then
            MessageBox.Show("Plase enter employee Code.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtEmpNo.Focus()
            Exit Sub
        End If
        If Trim(txtMachineID.Text) = "" Then
            MessageBox.Show("Plase enter MachineID.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtMachineID.Focus()
            Exit Sub
        End If
        If Trim(txtFirstName.Text) = "" Then
            MessageBox.Show("Plase enter first name.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtFirstName.Focus()
            Exit Sub
        End If
        If Trim(cmbOTEligible.Text) = "" Then
            MessageBox.Show("Plase select OT eligible.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbOTEligible.Focus()
            Exit Sub
        End If

        If Trim(cmbGender.Text) = "" Then MsgBox("Select the GENDER", MsgBoxStyle.Critical) : cmbGender.Focus() : Exit Sub
        'If Trim(txtAge.Text) = "" Then MsgBox("Enter the AGE", MsgBoxStyle.Critical) : txtAge.Focus() : Exit Sub
        If Trim(cmbMarital.Text) = "" Then MsgBox("Select the MARITAL STATUS", MsgBoxStyle.Critical) : cmbMarital.Focus() : Exit Sub
        If Trim(txtExistingCode.Text) = "" Then MsgBox("Enter the TOKEN NO", MsgBoxStyle.Critical) : txtExistingCode.Focus() : Exit Sub
        If Trim(txtMachineID.Text) = "" Then MsgBox("Enter the MACHINE NO", MsgBoxStyle.Critical) : txtMachineID.Focus() : Exit Sub
        If Trim(txtDesignation.Text) = "" Then MsgBox("Select the DESIGNATION", MsgBoxStyle.Critical) : txtDesignation.Focus() : Exit Sub
        If Trim(cmbShiftType.Text) = "" Then MsgBox("Select the SHIFT TYPE", MsgBoxStyle.Critical) : cmbShiftType.Focus() : Exit Sub
        If Trim(cmbSubCatName.Text) = "" Then MsgBox("Select the Sub.Category", MsgBoxStyle.Critical) : cmbSubCatName.Focus() : Exit Sub

        If UCase(Trim(cmbWages.Text)) = UCase("MONTHLY STAFF-I") Or UCase(Trim(cmbWages.Text)) = UCase("MONTHLY STAFF-II") Or UCase(Trim(cmbWages.Text)) = UCase("MONTHLY TEMP WORKER") Or UCase(Trim(cmbWages.Text)) = UCase("MONTHLY TEN DAYS") Or UCase(Trim(cmbWages.Text)) = UCase("MONTHLY C-WORKER") Or UCase(Trim(cmbWages.Text)) = UCase("MONTHLY M-WORKER") Or UCase(Trim(cmbWages.Text)) = UCase("WEEKLY") Or UCase(Trim(cmbWages.Text)) = UCase("NEW COMMERS") Then
            'Skip
        Else
            MsgBox("Wages Type Not Match in E-Pay...", MsgBoxStyle.Critical) : cmbWages.Focus() : Exit Sub
        End If

        Dim iStr1() As String
        Dim iStr2() As String

        iStr1 = Split(Trim(cmbCompCode.Text), " | ")
        iStr2 = Split(Trim(cmbLocCode.Text), " | ")

        'Check Machine ID
        SSQL = "Select * from [JLM-Epay]..EmployeeDetails where "
        SSQL = SSQL & " BiometricID = '" & Val(Remove_Single_Quote(Trim(txtMachineID.Text))) & "'"
        'SSQL = SSQL & " And ExisistingCode <> '" & Trim(txtExistingCode.Text) & "'"
        mDataSet = ReturnMultipleValue(SSQL)
        If mDataSet.Tables(0).Rows.Count <> 0 Then
            MsgBox("This Machine NO Already Assign to Another Person...", MsgBoxStyle.Critical)
            txtMachineID.Focus() : Exit Sub
        End If

        'Department Get
        SSQL = "Select * from [JLM-Epay]..MstDepartment where DepartmentNm='" & cmbDept.Text & "'"
        mDataSet = ReturnMultipleValue(SSQL)
        If mDataSet.Tables(0).Rows.Count <> 0 Then
            'Skip
        Else
            'Insert Department Name
            SSQL = "Select * from [JLM-Epay]..MstDepartment order by DepartmentCd desc"
            mDataSet = ReturnMultipleValue(SSQL)
            If mDataSet.Tables(0).Rows.Count <> 0 Then
                Dim Dept_Incr As Long = 0
                Dept_Incr = mDataSet.Tables(0).Rows(0)("DepartmentCd")
                Dept_Incr = Dept_Incr + 1
                SSQL = "Insert Into [JLM-Epay]..MstDepartment(DepartmentCd,DepartmentNm) Values('" & Dept_Incr & "','" & UCase(Trim(cmbDept.Text)) & "')"
                InsertDeleteUpdate(SSQL)
            End If
        End If


        Call InsertE_Pay_EmployeeDetails()

        MsgBox("Employee Details Saved in E-Pay Software Successfully", MsgBoxStyle.Information)
        Clear_Screen_Details()
    End Sub
    Private Sub InsertE_Pay_EmployeeDetails()

        Dim sqlCon As New OleDbConnection
        Dim sqlComm As New OleDbCommand

        iStr1 = Split(Trim(cmbCompCode.Text), " | ")
        iStr2 = Split(Trim(cmbLocCode.Text), " | ")

        'Variable
        Dim Employee_Type As String = ""
        Dim Update_Or_Insert As String = ""
        Dim GenderCode As String = "1"
        Dim Edu_code As String = "1"
        Dim NonPfGradeCode As String = "1"
        Dim MartialStatus As String = "U"
        Dim Hostel As String = "0"
        Dim Department_No_Get As String = "1"
        Dim Staff_Labour As String = "L"
        Dim Eligibleforovertime As String
        Dim WagesType As String = "2"
        Dim FinancialYear As String


        'Assign OT
        If UCase(Trim(cmbOTEligible.Text)) = UCase("Yes") Then
            Eligibleforovertime = "1"
        Else
            Eligibleforovertime = "2"
        End If

        'Assign Wages Weekly
        If UCase(Trim(cmbWages.Text)) = UCase("WEEKLY") Then
            WagesType = "2"
        Else
            WagesType = "2"
        End If

        'Financial Year
        Dim CurrentYear As Int32
        Dim i As Int32
        'Financial Year Add
        CurrentYear = Year(Now)
        If (Now.Month >= 1 And Now.Month <= 3) Then
            CurrentYear = CurrentYear - 1
        Else
            CurrentYear = CurrentYear
        End If
        FinancialYear = Convert.ToString(CurrentYear) & "-" & Convert.ToString(CurrentYear + 1)


        'Check Employee Already assing to employee details or not
        SSQL = "Select * from [JLM-Epay]..EmployeeDetails where BiometricID='" & Trim(txtMachineID.Text) & "' And Ccode='" & iStr1(0) & "' And Lcode='" & iStr2(1) & "'"
        mDataSet = ReturnMultipleValue(SSQL)
        If mDataSet.Tables(0).Rows.Count <> 0 Then
            Update_Or_Insert = "Update"
            mvarE_Pay_EmpNo = mDataSet.Tables(0).Rows(0)("EmpNo").ToString()
        Else
            Update_Or_Insert = "Insert"
            E_Pay_EmpNo_Generate()
        End If

        'Get Gender
        If UCase(cmbGender.Text) = UCase("Female") Then
            GenderCode = "2"
        Else
            GenderCode = "1"
        End If

        'Get Married Status
        If UCase(cmbMarital.Text) = UCase("Married") Then
            MartialStatus = "M"
        Else
            MartialStatus = "U"
        End If

        'Employee Type Get
        If UCase(Trim(cmbWages.Text)) = UCase("MONTHLY STAFF-I") Then Employee_Type = "1" : Staff_Labour = "S"
        If UCase(Trim(cmbWages.Text)) = UCase("MONTHLY STAFF-II") Then Employee_Type = "2" : Staff_Labour = "S"
        If UCase(Trim(cmbWages.Text)) = UCase("MONTHLY TEMP WORKER") Then Employee_Type = "3" : Staff_Labour = "L"
        If UCase(Trim(cmbWages.Text)) = UCase("MONTHLY C-WORKER") Then Employee_Type = "5" : Staff_Labour = "L"
        If UCase(Trim(cmbWages.Text)) = UCase("MONTHLY M-WORKER") Then Employee_Type = "6" : Staff_Labour = "L"
        If UCase(Trim(cmbWages.Text)) = UCase("MONTHLY TEN DAYS") Then Employee_Type = "4" : Staff_Labour = "L"
        If UCase(Trim(cmbWages.Text)) = UCase("WEEKLY") Then Employee_Type = "7" : Staff_Labour = "L"
        If UCase(Trim(cmbWages.Text)) = UCase("WEEKLY II") Then Employee_Type = "8" : Staff_Labour = "L"
        If UCase(Trim(cmbWages.Text)) = UCase("NEW COMMERS") Then Employee_Type = "9" : Staff_Labour = "L"


        'Department Get
        SSQL = "Select * from [JLM-Epay]..MstDepartment where DepartmentNm='" & cmbDept.Text & "'"
        mDataSet = ReturnMultipleValue(SSQL)
        If mDataSet.Tables(0).Rows.Count <> 0 Then
            Department_No_Get = mDataSet.Tables(0).Rows(0)("DepartmentCd").ToString()
        End If


        sqlCon = New OleDbConnection(GetConnection("eAlert"))
        sqlComm.Connection = sqlCon

        'Employee Details Insert Or Update
        If Update_Or_Insert = "Insert" Then
            sqlComm.CommandText = "[JLM-Epay]..EmployeeDetails_SP"
            sqlComm.CommandType = CommandType.StoredProcedure
            Dim DOB_Date As Date = dtpDOB.Value
            sqlComm.Parameters.Clear()

            sqlComm.Parameters.AddWithValue("EmpNo", SqlDbType.NVarChar).Value = mvarE_Pay_EmpNo
            sqlComm.Parameters.AddWithValue("MachineNo", SqlDbType.NVarChar).Value = mvarE_Pay_EmpNo
            sqlComm.Parameters.AddWithValue("ExisistingCode", SqlDbType.NVarChar).Value = Trim(txtExistingCode.Text)
            sqlComm.Parameters.AddWithValue("EmpName", SqlDbType.NVarChar).Value = Trim(txtFirstName.Text)
            sqlComm.Parameters.AddWithValue("FatherName", SqlDbType.NVarChar).Value = Trim(txtLastName.Text)
            sqlComm.Parameters.AddWithValue("Gender", SqlDbType.NVarChar).Value = GenderCode
            sqlComm.Parameters.AddWithValue("Dob", SqlDbType.NVarChar).Value = dtpDOB.Text
            sqlComm.Parameters.AddWithValue("PSAdd1", SqlDbType.NVarChar).Value = Trim(txtAddress1.Text)
            sqlComm.Parameters.AddWithValue("PSAdd2", SqlDbType.NVarChar).Value = ""
            sqlComm.Parameters.AddWithValue("PSAdd3", SqlDbType.NVarChar).Value = ""
            sqlComm.Parameters.AddWithValue("PSTaluk", SqlDbType.NVarChar).Value = ""
            sqlComm.Parameters.AddWithValue("PSDistrict", SqlDbType.NVarChar).Value = ""
            sqlComm.Parameters.AddWithValue("PSState", SqlDbType.NVarChar).Value = "Tamilnadu"
            sqlComm.Parameters.AddWithValue("Phone", SqlDbType.NVarChar).Value = "+91-" & Trim(txtll1.Text)
            sqlComm.Parameters.AddWithValue("Mobile", SqlDbType.NVarChar).Value = "+91-" & Trim(txtEmpMob1.Text)
            sqlComm.Parameters.AddWithValue("Passportno", SqlDbType.NVarChar).Value = ""

            sqlComm.Parameters.AddWithValue("UnEducated", SqlDbType.NVarChar).Value = Edu_code
            sqlComm.Parameters.AddWithValue("SchoolCollege", SqlDbType.NVarChar).Value = ""
            sqlComm.Parameters.AddWithValue("DrivinglicenceNo", SqlDbType.NVarChar).Value = ""
            sqlComm.Parameters.AddWithValue("Qualification", SqlDbType.Int).Value = Convert.ToInt32(6)
            sqlComm.Parameters.AddWithValue("University", SqlDbType.NVarChar).Value = Trim(txtQualification.Text)
            sqlComm.Parameters.AddWithValue("CompanyName", SqlDbType.Int).Value = 0
            sqlComm.Parameters.AddWithValue("Department", SqlDbType.Int).Value = Convert.ToInt32(Department_No_Get)
            sqlComm.Parameters.AddWithValue("Designation", SqlDbType.NVarChar).Value = Trim(txtDesignation.Text)

            sqlComm.Parameters.AddWithValue("UnEduDept", SqlDbType.NVarChar).Value = ""
            sqlComm.Parameters.AddWithValue("UnEduDesig", SqlDbType.NVarChar).Value = ""

            sqlComm.Parameters.AddWithValue("Shift", SqlDbType.NVarChar).Value = ""
            sqlComm.Parameters.AddWithValue("EmployeeType", SqlDbType.Int).Value = Convert.ToInt32(Employee_Type)
            sqlComm.Parameters.AddWithValue("CreatedBy", SqlDbType.DateTime).Value = DateTime.Parse(Now.ToShortDateString)
            sqlComm.Parameters.AddWithValue("ModifiedBy", SqlDbType.DateTime).Value = DateTime.Parse(Now.ToShortDateString)
            sqlComm.Parameters.AddWithValue("MaxNo", SqlDbType.Int).Value = Convert.ToInt32(mvarE_Pay_RegNo)
            sqlComm.Parameters.AddWithValue("StaffLabour", SqlDbType.NVarChar).Value = Staff_Labour
            sqlComm.Parameters.AddWithValue("NonPFGrade", SqlDbType.NVarChar).Value = NonPfGradeCode
            sqlComm.Parameters.AddWithValue("RoleCode", SqlDbType.Int).Value = Convert.ToInt32(1)
            sqlComm.Parameters.AddWithValue("Ccode", SqlDbType.NVarChar).Value = iStr1(0)
            sqlComm.Parameters.AddWithValue("Lcode", SqlDbType.NVarChar).Value = iStr2(0)
            sqlComm.Parameters.AddWithValue("Martial", SqlDbType.NVarChar).Value = MartialStatus
            sqlComm.Parameters.AddWithValue("Initial", SqlDbType.NVarChar).Value = Trim(txtInitial.Text)
            sqlComm.Parameters.AddWithValue("Contract", SqlDbType.Int).Value = Convert.ToInt32("0")
            sqlComm.Parameters.AddWithValue("bio", SqlDbType.NVarChar).Value = Trim(txtMachineID.Text)

            sqlCon.Open()
            sqlComm.ExecuteNonQuery()
            sqlCon.Close()

            '//Update Attendance Insentive Eligible
            SSQL = "Update [JLM-Epay]..EmployeeDetails set AttnIncEligible='" & txtAttn_Inc_Eligible.Text & "' where EmpNo='" & mvarE_Pay_EmpNo & "' And Ccode='" & iStr1(0) & "' And Lcode='" & iStr2(0) & "'"
            InsertDeleteUpdate(SSQL)

        ElseIf Update_Or_Insert = "Update" Then

            sqlComm.CommandText = "[JLM-Epay]..updateEmployeeRegistration_sp"
            sqlComm.CommandType = CommandType.StoredProcedure
            sqlComm.Parameters.Clear()



            sqlComm.Parameters.AddWithValue("EmpNo", SqlDbType.NVarChar).Value = mvarE_Pay_EmpNo
            sqlComm.Parameters.AddWithValue("MachineNo", SqlDbType.NVarChar).Value = mvarE_Pay_EmpNo




            sqlComm.Parameters.AddWithValue("EmpName", SqlDbType.NVarChar).Value = Trim(txtFirstName.Text)
            sqlComm.Parameters.AddWithValue("FatherName", SqlDbType.NVarChar).Value = Trim(txtLastName.Text)
            sqlComm.Parameters.AddWithValue("Gender", SqlDbType.NVarChar).Value = GenderCode
            sqlComm.Parameters.AddWithValue("Dob", SqlDbType.NVarChar).Value = dtpDOB.Text
            sqlComm.Parameters.AddWithValue("PSAdd1", SqlDbType.NVarChar).Value = Trim(txtAddress1.Text)
            sqlComm.Parameters.AddWithValue("PSAdd2", SqlDbType.NVarChar).Value = ""
            sqlComm.Parameters.AddWithValue("PSAdd3", SqlDbType.NVarChar).Value = ""
            sqlComm.Parameters.AddWithValue("PSTaluk", SqlDbType.NVarChar).Value = ""
            sqlComm.Parameters.AddWithValue("PSDistrict", SqlDbType.NVarChar).Value = ""
            sqlComm.Parameters.AddWithValue("PSState", SqlDbType.NVarChar).Value = "Tamilnadu"






            sqlComm.Parameters.AddWithValue("Phone", SqlDbType.NVarChar).Value = "+91-" & Trim(txtll1.Text)
            sqlComm.Parameters.AddWithValue("Mobile", SqlDbType.NVarChar).Value = "+91-" & Trim(txtEmpMob1.Text)
            sqlComm.Parameters.AddWithValue("Passportno", SqlDbType.NVarChar).Value = ""
            sqlComm.Parameters.AddWithValue("DrivinglicenceNo", SqlDbType.NVarChar).Value = ""
            sqlComm.Parameters.AddWithValue("Qualification", SqlDbType.Int).Value = Convert.ToInt32(6)
            sqlComm.Parameters.AddWithValue("SchoolCollege", SqlDbType.NVarChar).Value = ""
            sqlComm.Parameters.AddWithValue("UnEducated", SqlDbType.NVarChar).Value = Edu_code
            sqlComm.Parameters.AddWithValue("University", SqlDbType.NVarChar).Value = Trim(txtQualification.Text)
            sqlComm.Parameters.AddWithValue("Department", SqlDbType.Int).Value = Convert.ToInt32(Department_No_Get)
            sqlComm.Parameters.AddWithValue("Designation", SqlDbType.NVarChar).Value = Trim(txtDesignation.Text)

            sqlComm.Parameters.AddWithValue("UnEduDept", SqlDbType.NVarChar).Value = ""
            sqlComm.Parameters.AddWithValue("UnEduDesig", SqlDbType.NVarChar).Value = ""
            sqlComm.Parameters.AddWithValue("Shift", SqlDbType.NVarChar).Value = ""
            sqlComm.Parameters.AddWithValue("EmployeeType", SqlDbType.Int).Value = Convert.ToInt32(Employee_Type)
            sqlComm.Parameters.AddWithValue("StaffLabour", SqlDbType.NVarChar).Value = Staff_Labour
            sqlComm.Parameters.AddWithValue("NonPFGrade", SqlDbType.NVarChar).Value = NonPfGradeCode
            sqlComm.Parameters.AddWithValue("Ccode", SqlDbType.NVarChar).Value = iStr1(0)
            sqlComm.Parameters.AddWithValue("Lcode", SqlDbType.NVarChar).Value = iStr2(0)
            sqlComm.Parameters.AddWithValue("Martial", SqlDbType.NVarChar).Value = MartialStatus
            sqlComm.Parameters.AddWithValue("Initial", SqlDbType.NVarChar).Value = Trim(txtInitial.Text)
            sqlComm.Parameters.AddWithValue("bio", SqlDbType.NVarChar).Value = Trim(txtMachineID.Text)
            sqlComm.Parameters.AddWithValue("InsideOutside", SqlDbType.NVarChar).Value = ""
            sqlComm.Parameters.AddWithValue("BRName", SqlDbType.NVarChar).Value = ""

            ' sqlComm.Parameters.AddWithValue("CompanyName", SqlDbType.Int).Value = 0
            'sqlComm.Parameters.AddWithValue("CreatedBy", SqlDbType.DateTime).Value = DateTime.Parse(Now.ToShortDateString)
            'sqlComm.Parameters.AddWithValue("ModifiedBy", SqlDbType.DateTime).Value = DateTime.Parse(Now.ToShortDateString)
            'sqlComm.Parameters.AddWithValue("MaxNo", SqlDbType.Int).Value = Convert.ToInt32(mvarE_Pay_RegNo)

            'sqlComm.Parameters.AddWithValue("InsideOutside", SqlDbType.Int).Value = ""
            'sqlComm.Parameters.AddWithValue("BRName", SqlDbType.Int).Value = Conv


            sqlCon.Open()
            sqlComm.ExecuteNonQuery()
            sqlCon.Close()

            '//Update Attendance Insentive Eligible
            SSQL = "Update [JLM-Epay]..EmployeeDetails set AttnIncEligible='" & txtAttn_Inc_Eligible.Text & "' where EmpNo='" & mvarE_Pay_EmpNo & "' And Ccode='" & iStr1(0) & "' And Lcode='" & iStr2(0) & "'"
            InsertDeleteUpdate(SSQL)

        End If

    End Sub
    Private Sub E_Pay_EmpNo_Generate()
        Dim MonthYear As String
        Dim RegNo As String = 1001

        Dim EmpCode As String = ""

        If cmbWages.Text = "MONTHLY STAFF-I" Or cmbWages.Text = "MONTHLY STAFF-II" Then
            EmpCode = "Emp"

        ElseIf cmbWages.Text = "MONTHLY TEMP WORKER" Then
            EmpCode = "TEM"
        ElseIf cmbWages.Text = "MONTHLY C-WORKER" Then
            EmpCode = "C-WO"
        ElseIf cmbWages.Text = "MONTHLY M-WORKER" Then
            EmpCode = "M-WO"
        ElseIf cmbWages.Text = "MONTHLY TEN DAYS" Then
            EmpCode = "TDAYS"
        ElseIf cmbWages.Text = "WEEKLY" Then
            EmpCode = "WEE"
        ElseIf cmbWages.Text = "WEEKLY II" Then
            EmpCode = "WEE"
        ElseIf cmbWages.Text = "NEW COMMERS" Then
            EmpCode = "NC"
        End If

        'Dim EmpCode As String = UCase(Left_Val(cmbWages.Text, 3))

        MonthYear = System.DateTime.Now.ToString("MMyyyy")

        SSQL = "Select max(MaxNo+1) from [JLM-Epay]..MaxNo Where Maxid=1"
        mDataSet = ReturnMultipleValue(SSQL)
        If mDataSet.Tables(0).Rows.Count <> 0 Then RegNo = mDataSet.Tables(0).Rows(0)(0)

        'RegNo = UCase(Trim(txtExistingCode.Text))

        mvarE_Pay_EmpNo = EmpCode + MonthYear + RegNo
        mvarE_Pay_RegNo = RegNo


    End Sub

    Private Sub GroupBox7_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupBox7.Enter

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        
        Dim iStr1() As String
        Dim iStr2() As String

        iStr1 = Split(Trim(cmbCompCode.Text), " | ")
        iStr2 = Split(Trim(cmbLocCode.Text), " | ")
        'Machine ID Encrypt
        Dim StrMachine_ID_Encrypt As String = ""



        'mStatus = InsertDeleteUpdate(SSQL)

        SSQL = ""
        SSQL = "Select * from Employee_Mst Where CompCode='" & iStr1(0) & "'"
        SSQL = SSQL & " And LocCode='" & iStr2(0) & "'"
        mDataSet = ReturnMultipleValue(SSQL)
        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                For iRow = 0 To .Rows.Count - 1
                    cmbPayPeriod.Items.Add(.Rows(iRow)(0))
                    StrMachine_ID_Encrypt = Encryption(Trim(.Rows(iRow)("MachineID")))

                    Dim EmpNo As String
                    EmpNo = .Rows(iRow)("MachineID")
                    SSQL = "Update Employee_Mst set MachineID_Encrypt='" & StrMachine_ID_Encrypt & "' where CompCode='" & iStr1(0) & "' And LocCode='" & iStr2(0) & "' and EmpNo='" & EmpNo & "'"
                    mStatus = InsertDeleteUpdate(SSQL)
                Next
                MessageBox.Show("Encrypted Updated.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        End With


    End Sub
End Class