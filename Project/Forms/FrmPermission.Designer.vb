﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmPermission
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label11 = New System.Windows.Forms.Label
        Me.cmbUser = New System.Windows.Forms.ComboBox
        Me.Label57 = New System.Windows.Forms.Label
        Me.cbIP = New System.Windows.Forms.CheckBox
        Me.cbmailAdd = New System.Windows.Forms.CheckBox
        Me.cbEmail = New System.Windows.Forms.CheckBox
        Me.cbDownload = New System.Windows.Forms.CheckBox
        Me.cbDepartment = New System.Windows.Forms.CheckBox
        Me.cbEmployeeType = New System.Windows.Forms.CheckBox
        Me.cbDesignation = New System.Windows.Forms.CheckBox
        Me.cbEmployeeDetails = New System.Windows.Forms.CheckBox
        Me.cbManual = New System.Windows.Forms.CheckBox
        Me.cbSalary = New System.Windows.Forms.CheckBox
        Me.cbReports = New System.Windows.Forms.CheckBox
        Me.cbmachine = New System.Windows.Forms.CheckBox
        Me.cbstaff = New System.Windows.Forms.CheckBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.cbFpDownload = New System.Windows.Forms.CheckBox
        Me.cbShiftmas = New System.Windows.Forms.CheckBox
        Me.cbShift = New System.Windows.Forms.CheckBox
        Me.cbNoShift = New System.Windows.Forms.CheckBox
        Me.cbUser = New System.Windows.Forms.CheckBox
        Me.cmbLocCode = New System.Windows.Forms.ComboBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.cmbCompCode = New System.Windows.Forms.ComboBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.btnSave = New System.Windows.Forms.Button
        Me.btnClear = New System.Windows.Forms.Button
        Me.btnExit = New System.Windows.Forms.Button
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label11
        '
        Me.Label11.BackColor = System.Drawing.Color.Transparent
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label11.Location = New System.Drawing.Point(9, 15)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(556, 19)
        Me.Label11.TabIndex = 5
        Me.Label11.Text = "User Rights"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'cmbUser
        '
        Me.cmbUser.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbUser.Location = New System.Drawing.Point(116, 105)
        Me.cmbUser.Name = "cmbUser"
        Me.cmbUser.Size = New System.Drawing.Size(429, 21)
        Me.cmbUser.TabIndex = 35
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label57.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label57.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label57.Location = New System.Drawing.Point(30, 106)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(86, 16)
        Me.Label57.TabIndex = 20
        Me.Label57.Text = "User Name"
        '
        'cbIP
        '
        Me.cbIP.AutoSize = True
        Me.cbIP.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.cbIP.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbIP.Location = New System.Drawing.Point(40, 135)
        Me.cbIP.Name = "cbIP"
        Me.cbIP.Size = New System.Drawing.Size(94, 20)
        Me.cbIP.TabIndex = 22
        Me.cbIP.Text = "IP Details"
        Me.cbIP.UseVisualStyleBackColor = False
        '
        'cbmailAdd
        '
        Me.cbmailAdd.AutoSize = True
        Me.cbmailAdd.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.cbmailAdd.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbmailAdd.Location = New System.Drawing.Point(40, 230)
        Me.cbmailAdd.Name = "cbmailAdd"
        Me.cbmailAdd.Size = New System.Drawing.Size(139, 20)
        Me.cbmailAdd.TabIndex = 23
        Me.cbmailAdd.Text = "Mailing Address"
        Me.cbmailAdd.UseVisualStyleBackColor = False
        '
        'cbEmail
        '
        Me.cbEmail.AutoSize = True
        Me.cbEmail.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.cbEmail.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbEmail.Location = New System.Drawing.Point(415, 135)
        Me.cbEmail.Name = "cbEmail"
        Me.cbEmail.Size = New System.Drawing.Size(110, 20)
        Me.cbEmail.TabIndex = 24
        Me.cbEmail.Text = "Email Setup"
        Me.cbEmail.UseVisualStyleBackColor = False
        '
        'cbDownload
        '
        Me.cbDownload.AutoSize = True
        Me.cbDownload.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.cbDownload.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbDownload.Location = New System.Drawing.Point(40, 158)
        Me.cbDownload.Name = "cbDownload"
        Me.cbDownload.Size = New System.Drawing.Size(154, 20)
        Me.cbDownload.TabIndex = 25
        Me.cbDownload.Text = "Down Load / Clear"
        Me.cbDownload.UseVisualStyleBackColor = False
        '
        'cbDepartment
        '
        Me.cbDepartment.AutoSize = True
        Me.cbDepartment.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.cbDepartment.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbDepartment.Location = New System.Drawing.Point(248, 157)
        Me.cbDepartment.Name = "cbDepartment"
        Me.cbDepartment.Size = New System.Drawing.Size(107, 20)
        Me.cbDepartment.TabIndex = 26
        Me.cbDepartment.Text = "Department"
        Me.cbDepartment.UseVisualStyleBackColor = False
        '
        'cbEmployeeType
        '
        Me.cbEmployeeType.AutoSize = True
        Me.cbEmployeeType.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.cbEmployeeType.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbEmployeeType.Location = New System.Drawing.Point(414, 158)
        Me.cbEmployeeType.Name = "cbEmployeeType"
        Me.cbEmployeeType.Size = New System.Drawing.Size(137, 20)
        Me.cbEmployeeType.TabIndex = 27
        Me.cbEmployeeType.Text = "Employee Type"
        Me.cbEmployeeType.UseVisualStyleBackColor = False
        '
        'cbDesignation
        '
        Me.cbDesignation.AutoSize = True
        Me.cbDesignation.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.cbDesignation.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbDesignation.Location = New System.Drawing.Point(40, 253)
        Me.cbDesignation.Name = "cbDesignation"
        Me.cbDesignation.Size = New System.Drawing.Size(163, 20)
        Me.cbDesignation.TabIndex = 28
        Me.cbDesignation.Text = "Designation Details"
        Me.cbDesignation.UseVisualStyleBackColor = False
        '
        'cbEmployeeDetails
        '
        Me.cbEmployeeDetails.AutoSize = True
        Me.cbEmployeeDetails.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.cbEmployeeDetails.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbEmployeeDetails.Location = New System.Drawing.Point(248, 181)
        Me.cbEmployeeDetails.Name = "cbEmployeeDetails"
        Me.cbEmployeeDetails.Size = New System.Drawing.Size(150, 20)
        Me.cbEmployeeDetails.TabIndex = 29
        Me.cbEmployeeDetails.Text = "Employee Details"
        Me.cbEmployeeDetails.UseVisualStyleBackColor = False
        '
        'cbManual
        '
        Me.cbManual.AutoSize = True
        Me.cbManual.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.cbManual.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbManual.Location = New System.Drawing.Point(415, 181)
        Me.cbManual.Name = "cbManual"
        Me.cbManual.Size = New System.Drawing.Size(150, 20)
        Me.cbManual.TabIndex = 30
        Me.cbManual.Text = "Manual Attenance"
        Me.cbManual.UseVisualStyleBackColor = False
        '
        'cbSalary
        '
        Me.cbSalary.AutoSize = True
        Me.cbSalary.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.cbSalary.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbSalary.Location = New System.Drawing.Point(40, 204)
        Me.cbSalary.Name = "cbSalary"
        Me.cbSalary.Size = New System.Drawing.Size(171, 20)
        Me.cbSalary.TabIndex = 31
        Me.cbSalary.Text = "Salary Disbursement"
        Me.cbSalary.UseVisualStyleBackColor = False
        '
        'cbReports
        '
        Me.cbReports.AutoSize = True
        Me.cbReports.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.cbReports.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbReports.Location = New System.Drawing.Point(248, 204)
        Me.cbReports.Name = "cbReports"
        Me.cbReports.Size = New System.Drawing.Size(82, 20)
        Me.cbReports.TabIndex = 32
        Me.cbReports.Text = "Reports"
        Me.cbReports.UseVisualStyleBackColor = False
        '
        'cbmachine
        '
        Me.cbmachine.AutoSize = True
        Me.cbmachine.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.cbmachine.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbmachine.Location = New System.Drawing.Point(415, 204)
        Me.cbmachine.Name = "cbmachine"
        Me.cbmachine.Size = New System.Drawing.Size(122, 20)
        Me.cbmachine.TabIndex = 33
        Me.cbmachine.Text = "Machine Data"
        Me.cbmachine.UseVisualStyleBackColor = False
        '
        'cbstaff
        '
        Me.cbstaff.AutoSize = True
        Me.cbstaff.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.cbstaff.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbstaff.Location = New System.Drawing.Point(456, 255)
        Me.cbstaff.Name = "cbstaff"
        Me.cbstaff.Size = New System.Drawing.Size(58, 20)
        Me.cbstaff.TabIndex = 34
        Me.cbstaff.Text = "Staff"
        Me.cbstaff.UseVisualStyleBackColor = False
        Me.cbstaff.Visible = False
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.GroupBox1.Controls.Add(Me.cbFpDownload)
        Me.GroupBox1.Controls.Add(Me.cbShiftmas)
        Me.GroupBox1.Controls.Add(Me.cbShift)
        Me.GroupBox1.Controls.Add(Me.cbNoShift)
        Me.GroupBox1.Controls.Add(Me.cbUser)
        Me.GroupBox1.Controls.Add(Me.cmbLocCode)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.cmbCompCode)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label57)
        Me.GroupBox1.Controls.Add(Me.cbstaff)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.cbmachine)
        Me.GroupBox1.Controls.Add(Me.btnSave)
        Me.GroupBox1.Controls.Add(Me.cbReports)
        Me.GroupBox1.Controls.Add(Me.btnClear)
        Me.GroupBox1.Controls.Add(Me.cbSalary)
        Me.GroupBox1.Controls.Add(Me.btnExit)
        Me.GroupBox1.Controls.Add(Me.cbManual)
        Me.GroupBox1.Controls.Add(Me.cmbUser)
        Me.GroupBox1.Controls.Add(Me.cbEmployeeDetails)
        Me.GroupBox1.Controls.Add(Me.cbIP)
        Me.GroupBox1.Controls.Add(Me.cbDesignation)
        Me.GroupBox1.Controls.Add(Me.cbmailAdd)
        Me.GroupBox1.Controls.Add(Me.cbEmployeeType)
        Me.GroupBox1.Controls.Add(Me.cbEmail)
        Me.GroupBox1.Controls.Add(Me.cbDepartment)
        Me.GroupBox1.Controls.Add(Me.cbDownload)
        Me.GroupBox1.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(571, 319)
        Me.GroupBox1.TabIndex = 38
        Me.GroupBox1.TabStop = False
        '
        'cbFpDownload
        '
        Me.cbFpDownload.AutoSize = True
        Me.cbFpDownload.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.cbFpDownload.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbFpDownload.Location = New System.Drawing.Point(415, 230)
        Me.cbFpDownload.Name = "cbFpDownload"
        Me.cbFpDownload.Size = New System.Drawing.Size(123, 20)
        Me.cbFpDownload.TabIndex = 49
        Me.cbFpDownload.Text = "FP DownLoad"
        Me.cbFpDownload.UseVisualStyleBackColor = False
        '
        'cbShiftmas
        '
        Me.cbShiftmas.AutoSize = True
        Me.cbShiftmas.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.cbShiftmas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbShiftmas.Location = New System.Drawing.Point(247, 230)
        Me.cbShiftmas.Name = "cbShiftmas"
        Me.cbShiftmas.Size = New System.Drawing.Size(108, 20)
        Me.cbShiftmas.TabIndex = 48
        Me.cbShiftmas.Text = "Shift Master"
        Me.cbShiftmas.UseVisualStyleBackColor = False
        '
        'cbShift
        '
        Me.cbShift.AutoSize = True
        Me.cbShift.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.cbShift.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbShift.Location = New System.Drawing.Point(248, 132)
        Me.cbShift.Name = "cbShift"
        Me.cbShift.Size = New System.Drawing.Size(150, 20)
        Me.cbShift.TabIndex = 47
        Me.cbShift.Text = "Shift Type Details"
        Me.cbShift.UseVisualStyleBackColor = False
        '
        'cbNoShift
        '
        Me.cbNoShift.AutoSize = True
        Me.cbNoShift.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.cbNoShift.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbNoShift.Location = New System.Drawing.Point(456, 285)
        Me.cbNoShift.Name = "cbNoShift"
        Me.cbNoShift.Size = New System.Drawing.Size(81, 20)
        Me.cbNoShift.TabIndex = 46
        Me.cbNoShift.Text = "No Shift"
        Me.cbNoShift.UseVisualStyleBackColor = False
        Me.cbNoShift.Visible = False
        '
        'cbUser
        '
        Me.cbUser.AutoSize = True
        Me.cbUser.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.cbUser.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbUser.Location = New System.Drawing.Point(40, 181)
        Me.cbUser.Name = "cbUser"
        Me.cbUser.Size = New System.Drawing.Size(143, 20)
        Me.cbUser.TabIndex = 45
        Me.cbUser.Text = "Company Master"
        Me.cbUser.UseVisualStyleBackColor = False
        '
        'cmbLocCode
        '
        Me.cmbLocCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbLocCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbLocCode.FormattingEnabled = True
        Me.cmbLocCode.Location = New System.Drawing.Point(116, 75)
        Me.cmbLocCode.Name = "cmbLocCode"
        Me.cmbLocCode.Size = New System.Drawing.Size(429, 24)
        Me.cmbLocCode.TabIndex = 44
        '
        'Label6
        '
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(9, 75)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(101, 23)
        Me.Label6.TabIndex = 43
        Me.Label6.Text = "Location"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cmbCompCode
        '
        Me.cmbCompCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbCompCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbCompCode.FormattingEnabled = True
        Me.cmbCompCode.Location = New System.Drawing.Point(116, 45)
        Me.cmbCompCode.Name = "cmbCompCode"
        Me.cmbCompCode.Size = New System.Drawing.Size(429, 24)
        Me.cmbCompCode.TabIndex = 42
        '
        'Label5
        '
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(9, 46)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(101, 23)
        Me.Label5.TabIndex = 41
        Me.Label5.Text = "Company"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnSave
        '
        Me.btnSave.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Location = New System.Drawing.Point(174, 279)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 30)
        Me.btnSave.TabIndex = 38
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClear
        '
        Me.btnClear.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClear.Location = New System.Drawing.Point(255, 279)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(75, 30)
        Me.btnClear.TabIndex = 39
        Me.btnClear.Text = "C&lear"
        Me.btnClear.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExit.Location = New System.Drawing.Point(336, 279)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(75, 30)
        Me.btnExit.TabIndex = 40
        Me.btnExit.Text = "E&xit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'FrmPermission
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(574, 323)
        Me.Controls.Add(Me.GroupBox1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmPermission"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "User Rights"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents cmbUser As System.Windows.Forms.ComboBox
    Friend WithEvents Label57 As System.Windows.Forms.Label
    Friend WithEvents cbIP As System.Windows.Forms.CheckBox
    Friend WithEvents cbmailAdd As System.Windows.Forms.CheckBox
    Friend WithEvents cbEmail As System.Windows.Forms.CheckBox
    Friend WithEvents cbDownload As System.Windows.Forms.CheckBox
    Friend WithEvents cbDepartment As System.Windows.Forms.CheckBox
    Friend WithEvents cbEmployeeType As System.Windows.Forms.CheckBox
    Friend WithEvents cbDesignation As System.Windows.Forms.CheckBox
    Friend WithEvents cbEmployeeDetails As System.Windows.Forms.CheckBox
    Friend WithEvents cbManual As System.Windows.Forms.CheckBox
    Friend WithEvents cbSalary As System.Windows.Forms.CheckBox
    Friend WithEvents cbReports As System.Windows.Forms.CheckBox
    Friend WithEvents cbmachine As System.Windows.Forms.CheckBox
    Friend WithEvents cbstaff As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnClear As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents cmbCompCode As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cmbLocCode As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents cbUser As System.Windows.Forms.CheckBox
    Friend WithEvents cbNoShift As System.Windows.Forms.CheckBox
    Friend WithEvents cbShift As System.Windows.Forms.CheckBox
    Friend WithEvents cbShiftmas As System.Windows.Forms.CheckBox
    Friend WithEvents cbFpDownload As System.Windows.Forms.CheckBox
End Class
