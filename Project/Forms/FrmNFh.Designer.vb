﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmNFh
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.lblSum = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.btnDelete = New System.Windows.Forms.Button
        Me.PanelReport = New System.Windows.Forms.Panel
        Me.GVLeave = New System.Windows.Forms.DataGridView
        Me.Months = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Days = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.TotalDays = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.PanelGrid = New System.Windows.Forms.Panel
        Me.gvHoliday = New System.Windows.Forms.DataGridView
        Me.ID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Date_1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Button1 = New System.Windows.Forms.Button
        Me.btnEdit = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.LeaveDate = New System.Windows.Forms.DateTimePicker
        Me.label6 = New System.Windows.Forms.Label
        Me.Panel1.SuspendLayout()
        Me.PanelReport.SuspendLayout()
        CType(Me.GVLeave, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelGrid.SuspendLayout()
        CType(Me.gvHoliday, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(3, 2)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(576, 27)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "HOLIDAY MASTER"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Panel1.Controls.Add(Me.lblSum)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.btnDelete)
        Me.Panel1.Controls.Add(Me.PanelReport)
        Me.Panel1.Controls.Add(Me.PanelGrid)
        Me.Panel1.Controls.Add(Me.Button1)
        Me.Panel1.Controls.Add(Me.btnEdit)
        Me.Panel1.Controls.Add(Me.btnSave)
        Me.Panel1.Controls.Add(Me.LeaveDate)
        Me.Panel1.Controls.Add(Me.label6)
        Me.Panel1.Location = New System.Drawing.Point(4, 33)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(575, 455)
        Me.Panel1.TabIndex = 1
        '
        'lblSum
        '
        Me.lblSum.BackColor = System.Drawing.Color.White
        Me.lblSum.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSum.ForeColor = System.Drawing.Color.Black
        Me.lblSum.Location = New System.Drawing.Point(202, 422)
        Me.lblSum.Name = "lblSum"
        Me.lblSum.Size = New System.Drawing.Size(87, 27)
        Me.lblSum.TabIndex = 33
        Me.lblSum.Text = "0"
        Me.lblSum.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(89, 422)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(99, 27)
        Me.Label2.TabIndex = 32
        Me.Label2.Text = "Total Days"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnDelete
        '
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.Location = New System.Drawing.Point(170, 79)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(100, 28)
        Me.btnDelete.TabIndex = 31
        Me.btnDelete.Text = "Delete"
        Me.btnDelete.UseVisualStyleBackColor = False
        '
        'PanelReport
        '
        Me.PanelReport.Controls.Add(Me.GVLeave)
        Me.PanelReport.Location = New System.Drawing.Point(2, 113)
        Me.PanelReport.Name = "PanelReport"
        Me.PanelReport.Size = New System.Drawing.Size(316, 305)
        Me.PanelReport.TabIndex = 30
        '
        'GVLeave
        '
        Me.GVLeave.AllowUserToAddRows = False
        Me.GVLeave.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GVLeave.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Months, Me.Days, Me.TotalDays})
        Me.GVLeave.Location = New System.Drawing.Point(4, 4)
        Me.GVLeave.Name = "GVLeave"
        Me.GVLeave.ReadOnly = True
        Me.GVLeave.RowHeadersVisible = False
        Me.GVLeave.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.GVLeave.Size = New System.Drawing.Size(310, 298)
        Me.GVLeave.TabIndex = 0
        '
        'Months
        '
        Me.Months.HeaderText = "Months"
        Me.Months.Name = "Months"
        Me.Months.ReadOnly = True
        '
        'Days
        '
        Me.Days.HeaderText = "Days"
        Me.Days.Name = "Days"
        Me.Days.ReadOnly = True
        '
        'TotalDays
        '
        Me.TotalDays.HeaderText = "Total Days"
        Me.TotalDays.Name = "TotalDays"
        Me.TotalDays.ReadOnly = True
        Me.TotalDays.Width = 90
        '
        'PanelGrid
        '
        Me.PanelGrid.Controls.Add(Me.gvHoliday)
        Me.PanelGrid.Location = New System.Drawing.Point(319, 3)
        Me.PanelGrid.Name = "PanelGrid"
        Me.PanelGrid.Size = New System.Drawing.Size(248, 449)
        Me.PanelGrid.TabIndex = 29
        '
        'gvHoliday
        '
        Me.gvHoliday.AllowUserToAddRows = False
        Me.gvHoliday.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gvHoliday.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ID, Me.Date_1})
        Me.gvHoliday.Location = New System.Drawing.Point(3, 0)
        Me.gvHoliday.Name = "gvHoliday"
        Me.gvHoliday.ReadOnly = True
        Me.gvHoliday.RowHeadersVisible = False
        Me.gvHoliday.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.gvHoliday.Size = New System.Drawing.Size(243, 447)
        Me.gvHoliday.TabIndex = 0
        '
        'ID
        '
        Me.ID.HeaderText = "ID"
        Me.ID.Name = "ID"
        Me.ID.ReadOnly = True
        Me.ID.Width = 75
        '
        'Date_1
        '
        Me.Date_1.HeaderText = "Date"
        Me.Date_1.Name = "Date_1"
        Me.Date_1.ReadOnly = True
        Me.Date_1.Width = 150
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.White
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(170, 45)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(100, 28)
        Me.Button1.TabIndex = 28
        Me.Button1.Text = "Exit"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'btnEdit
        '
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.Location = New System.Drawing.Point(68, 79)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(100, 28)
        Me.btnEdit.TabIndex = 27
        Me.btnEdit.Text = "Edit"
        Me.btnEdit.UseVisualStyleBackColor = False
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Location = New System.Drawing.Point(68, 45)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(100, 28)
        Me.btnSave.TabIndex = 26
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'LeaveDate
        '
        Me.LeaveDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LeaveDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.LeaveDate.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.LeaveDate.Location = New System.Drawing.Point(71, 9)
        Me.LeaveDate.Name = "LeaveDate"
        Me.LeaveDate.Size = New System.Drawing.Size(196, 22)
        Me.LeaveDate.TabIndex = 5
        '
        'label6
        '
        Me.label6.AutoSize = True
        Me.label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label6.ForeColor = System.Drawing.Color.White
        Me.label6.Location = New System.Drawing.Point(8, 9)
        Me.label6.Name = "label6"
        Me.label6.Size = New System.Drawing.Size(41, 16)
        Me.label6.TabIndex = 4
        Me.label6.Text = "Date"
        '
        'FrmNFh
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(582, 491)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Label1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmNFh"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "National Festival Holiday Master"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.PanelReport.ResumeLayout(False)
        CType(Me.GVLeave, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelGrid.ResumeLayout(False)
        CType(Me.gvHoliday, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Private WithEvents label6 As System.Windows.Forms.Label
    Private WithEvents btnSave As System.Windows.Forms.Button
    Private WithEvents Button1 As System.Windows.Forms.Button
    Private WithEvents btnEdit As System.Windows.Forms.Button
    Friend WithEvents PanelGrid As System.Windows.Forms.Panel
    Friend WithEvents PanelReport As System.Windows.Forms.Panel
    Friend WithEvents gvHoliday As System.Windows.Forms.DataGridView
    Private WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents LeaveDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Date_1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GVLeave As System.Windows.Forms.DataGridView
    Friend WithEvents Months As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Days As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TotalDays As System.Windows.Forms.DataGridViewTextBoxColumn
    Private WithEvents Label2 As System.Windows.Forms.Label
    Private WithEvents lblSum As System.Windows.Forms.Label
End Class
