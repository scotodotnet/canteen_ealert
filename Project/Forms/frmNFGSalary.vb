﻿#Region "Imports"
Imports System
Imports System.Data
Imports System.Data.OleDb
Imports CrystalDecisions.CrystalReports.Engine

#End Region
Public Class frmNFGSalary
    Private iStr1() As String
    Private iStr2() As String
    Private iStr3() As String
    Private Check_Download_Clear_Error As Boolean = False
    Public axCZKEM1 As New zkemkeeper.CZKEM
    Private bIsConnected = False 'the boolean value identifies whether the device is connected
    Private iMachineNumber As Integer
    Private Sub frmNFGSalary_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormAlign(Me)
        Fill_Company_Details()
        cmbCompCode.SelectedIndex = 0
        cmbCompCode.Enabled = False
        cmbCompCode_LostFocus(sender, e)

        If Trim(mUserLocation) <> "" Then
            cmbLocCode.SelectedIndex = cmbLocCode.FindString(Trim(mUserLocation))
            cmbLocCode.Enabled = False

            Dim iStr1() As String
            Dim iStr2() As String

            iStr1 = Split(Trim(cmbCompCode.Text), " | ")
            iStr2 = Split(Trim(cmbLocCode.Text), " | ")
        Else
            cmbLocCode.SelectedIndex = 0
        End If
        cmbLocCode_LostFocus(sender, e)
        Check_Download_Clear_Error = False

        Call Fin_Year_Add()
        'LoadCover()
    End Sub
    Private Sub cmbLocCode_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbLocCode.LostFocus
        If Trim(cmbCompCode.Text) = "" Then Exit Sub
        If Trim(cmbLocCode.Text) = "" Then Exit Sub

        Dim iStr1() As String
        Dim iStr2() As String

        iStr1 = Split(Trim(cmbCompCode.Text), " | ")
        iStr2 = Split(Trim(cmbLocCode.Text), " | ")

        SSQL = ""
        SSQL = "Select IPAddress + ' | ' + IPMode as [Name] from IPAddress_Mst Where Compcode='"
        SSQL = SSQL & "" & iStr1(0) & "'"
        SSQL = SSQL & " And LocCode='" & iStr2(0) & "'"

        mDataSet = ReturnMultipleValue(SSQL)

        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                cmbIPAddress.Items.Clear()
                For iRow = 0 To .Rows.Count - 1
                    cmbIPAddress.Items.Add(.Rows(iRow)(0))
                Next
            End If
        End With
    End Sub
    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Close()
    End Sub
    Public Sub Fill_Company_Details()
        SSQL = ""
        SSQL = "Select CompCode + ' | ' + CompName as [Name] from Company_Mst Order By CompName"

        mDataSet = ReturnMultipleValue(SSQL)
        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                cmbCompCode.Items.Clear()
                For iRow = 0 To .Rows.Count - 1
                    cmbCompCode.Items.Add(.Rows(iRow)(0))
                Next
            End If
        End With
    End Sub

    Private Sub Fin_Year_Add()

        Dim CurrentYear As Int32
        Dim i As Int32
        'Financial Year Add
        CurrentYear = Year(Now)
        cmbFinYear.Items.Clear()
        For i = 0 To 11
            cmbFinYear.Items.Add(Convert.ToString(CurrentYear) & "-" & Convert.ToString(CurrentYear + 1))
            CurrentYear = CurrentYear - 1
        Next i
        cmbFinYear.SelectedIndex = 0
    End Sub
    Private Sub cmbCompCode_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCompCode.LostFocus
        If Trim(cmbCompCode.Text) = "" Then Exit Sub
        Dim iStr() As String
        iStr = Split(Trim(cmbCompCode.Text), " | ")

        SSQL = ""
        SSQL = "Select LocCode + ' | ' + LocName as [Name] from Location_Mst Where Compcode='"
        SSQL = SSQL & "" & iStr(0) & "'"

        mDataSet = ReturnMultipleValue(SSQL)

        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                cmbLocCode.Items.Clear()
                For iRow = 0 To .Rows.Count - 1
                    cmbLocCode.Items.Add(.Rows(iRow)(0))
                Next
            End If
        End With
    End Sub
    Private Sub btnConnect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConnect.Click

        If Trim(cmbCompCode.Text) = "" Then
            MessageBox.Show("Please select company code.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbCompCode.Focus()
            Exit Sub
        End If

        If Trim(cmbLocCode.Text) = "" Then
            MessageBox.Show("Please select location code.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbLocCode.Focus()
            Exit Sub
        End If

        If Trim(cmbIPAddress.Text) = "" Then
            MessageBox.Show("Please select IP Address.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbIPAddress.Focus()
            Exit Sub
        End If

        iStr1 = Split(cmbCompCode.Text, " | ")
        iStr2 = Split(cmbLocCode.Text, " | ")
        iStr3 = Split(cmbIPAddress.Text, " | ")

        Connect_Machine_RTEvents()
        'LoadCover()

    End Sub
    Private Sub Connect_Machine_RTEvents()
        Dim idwErrorCode As Integer
        Cursor = Cursors.WaitCursor
        If btnConnect.Text = "Disconnect" Then
            axCZKEM1.Disconnect()

            RemoveHandler axCZKEM1.OnAttTransactionEx, AddressOf AxCZKEM1_OnAttTransactionEx

            bIsConnected = False
            btnConnect.Text = "Connect"
            Cursor = Cursors.Default
            Return
        End If

        bIsConnected = axCZKEM1.Connect_Net(Trim(iStr3(0)), 4370)
        If bIsConnected = True Then
            btnConnect.Text = "Disconnect"
            btnConnect.Refresh()
            iMachineNumber = 1 'In fact,when you are using the tcp/ip communication,this parameter will be ignored,that is any integer will all right.Here we use 1.

            If axCZKEM1.RegEvent(iMachineNumber, 65535) = True Then 'Here you can register the realtime events that you want to be triggered(the parameters 65535 means registering all)

                AddHandler axCZKEM1.OnAttTransactionEx, AddressOf AxCZKEM1_OnAttTransactionEx

            End If
        Else
            axCZKEM1.GetLastError(idwErrorCode)
            MsgBox("Unable to connect the device,ErrorCode=" & idwErrorCode, MsgBoxStyle.Exclamation, "Error")
        End If
        Cursor = Cursors.Default
    End Sub
    Private Sub AxCZKEM1_OnAttTransactionEx(ByVal sEnrollNumber As String, ByVal iIsInValid As Integer, ByVal iAttState As Integer, ByVal iVerifyMethod As Integer, _
                      ByVal iYear As Integer, ByVal iMonth As Integer, ByVal iDay As Integer, ByVal iHour As Integer, ByVal iMinute As Integer, ByVal iSecond As Integer, ByVal iWorkCode As Integer)

        Dim User_Machine_ID As String
        Dim AmountInWords_Str As String
        Dim Class_Check As New clsConversion
        User_Machine_ID = sEnrollNumber
        Dim User_MachineID_Ency As String = Encryption(sEnrollNumber)
        Dim mSaveStatus As Long

        'Get Employee Details
        SSQL = ""
        SSQL = "Select * from Employee_Mst Where CompCode='" & iStr1(0) & "'"
        SSQL = SSQL & " And LocCode='" & iStr2(0) & "'"
        SSQL = SSQL & " And MachineID='" & Val(Remove_Single_Quote(Trim(User_Machine_ID))) & "'"
        mDataSet = ReturnMultipleValue(SSQL)
        If mDataSet.Tables(0).Rows.Count <> 0 Then
            lblTokenNo.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("ExistingCode")), " ", mDataSet.Tables(0).Rows(0)("ExistingCode"))
            lblMachineID.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("MachineID")), " ", mDataSet.Tables(0).Rows(0)("MachineID"))
            lblEmpName.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("FirstName")), " ", mDataSet.Tables(0).Rows(0)("FirstName"))
            lblDept.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("DeptName")), "", mDataSet.Tables(0).Rows(0)("DeptName"))
            lblDesg.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Designation")), "", mDataSet.Tables(0).Rows(0)("Designation"))
            lblWagesType.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Designation")), "", mDataSet.Tables(0).Rows(0)("Wages"))

            'Get Photo
            Dim sFilePath As String = ""
            Dim mUnitName As String = ""


            If UCase(Trim(iStr2(0))) = "UNIT I" Then
                mUnitName = "Unit1-Photos"
            ElseIf UCase(Trim(iStr2(0))) = "UNIT II" Then
                mUnitName = "Unit2-Photos"
            ElseIf UCase(Trim(iStr2(0))) = "UNIT III" Then
                mUnitName = "Unit3-Photos"
            ElseIf UCase(Trim(iStr2(0))) = "UNIT IV" Then
                mUnitName = "Unit4-Photos"
            Else
                mUnitName = "Tapes-Photos"
            End If

            sFilePath = mvarPhotoFilePath & "\" & Trim(mUnitName) _
                                     & "\" & Trim(lblMachineID.Text) & ".JPG"

            If Not IO.File.Exists(sFilePath) Then
                sFilePath = Application.StartupPath & "\Tapes-Photos\No-Iamge.JPG"
            End If

            If sFilePath = "" Then Exit Sub

            pbEmployee.BackgroundImage = Image.FromFile(sFilePath)

            'Get Payroll EmpNo
            Dim Payroll_EmpNo As String = ""
            Dim EmpCheck As New DataSet
            Dim CheckDate As String
            Dim EdataSet As DataSet
            Dim Fin_Year_Split() As String = Split(Trim(cmbFinYear.Text), "-")

            'Payroll_EmpNo = mDataSet.Tables(0).Rows(0)("EmpNo")

            SSQL = "Select Status from MstNFHsalary where Ccode='" & iStr1(0) & "' And Lcode='" & iStr2(0) & "'"
            SSQL = SSQL & " And MachineID='" & mDataSet.Tables(0).Rows(0)("MachineID") & "' And Month='" & cmbMonth.Text & "' And FinacialYear='" & Fin_Year_Split(0) & "'"
            SSQL = SSQL & " And FromDate=convert(datetime,'" & txtFromDate.Text & "',103)"
            SSQL = SSQL & " And ToDate=convert(datetime,'" & txtFromDate.Text & "',103)"
            EdataSet = ReturnMultipleValue(SSQL)

            If EdataSet.Tables(0).Rows(0)("Status") = "1" Then

                MessageBox.Show("Already Gave.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)

            Else
                SSQL = "Select Amount from MstNFHsalary where Ccode='" & iStr1(0) & "' And Lcode='" & iStr2(0) & "'"
                SSQL = SSQL & " And MachineID='" & mDataSet.Tables(0).Rows(0)("MachineID") & "' And Month='" & cmbMonth.Text & "' And FinacialYear='" & Fin_Year_Split(0) & "'"
                SSQL = SSQL & " And FromDate=convert(datetime,'" & txtFromDate.Text & "',103)"
                SSQL = SSQL & " And ToDate=convert(datetime,'" & txtFromDate.Text & "',103)"
                EdataSet = ReturnMultipleValue(SSQL)
                If EdataSet.Tables(0).Rows.Count <> 0 Then
                    lblNetAmt.Text = "NET AMOUNT : " & IIf(IsDBNull(EdataSet.Tables(0).Rows(0)("Amount")), "", EdataSet.Tables(0).Rows(0)("Amount"))
                    AmountInWords_Str = Class_Check.ConvertNumberToWords(IIf(IsDBNull(EdataSet.Tables(0).Rows(0)("Amount")), "0.0", EdataSet.Tables(0).Rows(0)("Amount")))

                    SSQL = "Update MstNFHsalary set Status='1' where Ccode='" & iStr1(0) & "' And Lcode='" & iStr2(0) & "' "
                    SSQL = SSQL & " And MachineID='" & mDataSet.Tables(0).Rows(0)("MachineID") & "' And Month='" & cmbMonth.Text & "' And FinacialYear='" & Fin_Year_Split(0) & "'"
                    SSQL = SSQL & " And FromDate=convert(datetime,'" & txtFromDate.Text & "',103)"
                    SSQL = SSQL & " And ToDate=convert(datetime,'" & txtFromDate.Text & "',103)"
                    mSaveStatus = InsertDeleteUpdate(SSQL)
                Else
                    MessageBox.Show("This Employee NFH not upload.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)

                    lblNetAmt.Text = "NET AMOUNT : 0.00"
                    AmountInWords_Str = Class_Check.ConvertNumberToWords("0.0")
                End If
            End If

        Else
            lblTokenNo.Text = ""
            lblMachineID.Text = ""
            lblEmpName.Text = ""
            lblDept.Text = ""
            lblDesg.Text = ""
            lblWagesType.Text = ""
            lblNetAmt.Text = "NET AMOUNT : 0.00"
            AmountInWords_Str = Class_Check.ConvertNumberToWords("0.0")
            Dim sFilePath_Clear As String = ""

            sFilePath_Clear = Application.StartupPath & "\Tapes-Photos\No-Iamge.JPG"
            pbEmployee.BackgroundImage = Image.FromFile(sFilePath_Clear)
        End If

        lblRupees.Text = "RUPEES : " & UCase(AmountInWords_Str)

        'lbRTShow.Items.Add("...UserID:" & sEnrollNumber)
        'lbRTShow.Items.Add("...isInvalid:" & iIsInValid.ToString())
        'lbRTShow.Items.Add("...attState:" & iAttState.ToString())
        'lbRTShow.Items.Add("...VerifyMethod:" & iVerifyMethod.ToString())
        'lbRTShow.Items.Add("...Workcode:" & iWorkCode.ToString()) 'the difference between the event OnAttTransaction and OnAttTransactionEx
        'lbRTShow.Items.Add("...Time:" & iYear.ToString() & "-" & iMonth.ToString() & "-" & iDay.ToString() & " " & iHour.ToString() & ":" & iMinute.ToString() & ":" & iSecond.ToString())
    End Sub
    Private Sub BtnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnClear.Click

        Dim AmountInWords_Str As String
        Dim Class_Check As New clsConversion

        lblTotalCoverResult.Text = ""
        lblTotalAmtResult.Text = ""
        lblBalCoverResult.Text = ""
        lblBalAmtResult.Text = ""


        lblTokenNo.Text = ""
        lblMachineID.Text = ""
        lblEmpName.Text = ""
        lblDept.Text = ""
        lblDesg.Text = ""
        lblWagesType.Text = ""
        lblNetAmt.Text = "NET AMOUNT : 0.00"
        Dim sFilePath_Clear As String = ""

        sFilePath_Clear = Application.StartupPath & "\Tapes-Photos\No-Iamge.JPG"
        pbEmployee.BackgroundImage = Image.FromFile(sFilePath_Clear)

        AmountInWords_Str = Class_Check.ConvertNumberToWords("0.0")
        lblRupees.Text = "RUPEES : " & UCase(AmountInWords_Str)

    End Sub
    Public Sub LoadCover()
        Dim year() As String

        If Trim(cmbMonth.Text) = "" Then
            MessageBox.Show("Please select Month.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbCompCode.Focus()
            Exit Sub
        End If

        If Trim(txtFromDate.Text) = "" Then
            MessageBox.Show("Please Enter From Date.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbCompCode.Focus()
            Exit Sub
        End If

        iStr1 = Split(cmbCompCode.Text, " | ")
        iStr2 = Split(cmbLocCode.Text, " | ")
        year = Split(Trim(cmbFinYear.Text), "-")

        SSQL = "Select count(*) as Cnt from MstNFHsalary where Ccode='" & iStr1(0) & "' And Lcode='" & iStr2(0) & "'"
        SSQL = SSQL & " And Month='" & cmbMonth.Text & "' And FinacialYear='" & year(0) & "'"
        SSQL = SSQL & " And FromDate=convert(datetime,'" & txtFromDate.Text & "',103)"
        SSQL = SSQL & " And ToDate=convert(datetime,'" & txtFromDate.Text & "',103)"
        mDataSet = ReturnMultipleValue(SSQL)

        lblTotalCoverResult.Text = mDataSet.Tables(0).Rows(0)("Cnt").ToString()


        SSQL = "Select sum(Amount) as Amt from MstNFHsalary where Ccode='" & iStr1(0) & "' And Lcode='" & iStr2(0) & "'"
        SSQL = SSQL & " And Month='" & cmbMonth.Text & "' And FinacialYear='" & year(0) & "'"
        SSQL = SSQL & " And FromDate=convert(datetime,'" & txtFromDate.Text & "',103)"
        SSQL = SSQL & " And ToDate=convert(datetime,'" & txtFromDate.Text & "',103)"
        mDataSet = ReturnMultipleValue(SSQL)

        lblTotalAmtResult.Text = mDataSet.Tables(0).Rows(0)("Amt").ToString()

        SSQL = "Select count(*) as Cover from MstNFHsalary where Status='2' and Ccode='" & iStr1(0) & "' And Lcode='" & iStr2(0) & "'"
        SSQL = SSQL & " And Month='" & cmbMonth.Text & "' And FinacialYear='" & year(0) & "'"
        SSQL = SSQL & " And FromDate=convert(datetime,'" & txtFromDate.Text & "',103)"
        SSQL = SSQL & " And ToDate=convert(datetime,'" & txtFromDate.Text & "',103)"
        mDataSet = ReturnMultipleValue(SSQL)

        lblBalCoverResult.Text = mDataSet.Tables(0).Rows(0)("Cover").ToString()



        SSQL = "Select sum(Amount) as BalAmt from MstNFHsalary where Status='2' and  Ccode='" & iStr1(0) & "' And Lcode='" & iStr2(0) & "'"
        SSQL = SSQL & " And Month='" & cmbMonth.Text & "' And FinacialYear='" & year(0) & "'"
        SSQL = SSQL & " And FromDate=convert(datetime,'" & txtFromDate.Text & "',103)"
        SSQL = SSQL & " And ToDate=convert(datetime,'" & txtFromDate.Text & "',103)"
        mDataSet = ReturnMultipleValue(SSQL)

        lblBalAmtResult.Text = mDataSet.Tables(0).Rows(0)("BalAmt").ToString()




    End Sub

    Private Sub btnupload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupload.Click

        If Trim(cmbCompCode.Text) = "" Then
            MessageBox.Show("Please select company code.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbCompCode.Focus()
            Exit Sub
        End If
        If Trim(cmbLocCode.Text) = "" Then
            MessageBox.Show("Please select Location code.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbCompCode.Focus()
            Exit Sub
        End If
        If Trim(cmbMonth.Text) = "" Then
            MessageBox.Show("Please select Month.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbCompCode.Focus()
            Exit Sub
        End If

        If opdOT.ShowDialog = Windows.Forms.DialogResult.OK Then
            Dim ErrFlag As Boolean = False
            Dim iStr() As String
            Dim intRow As Integer
            Dim dsEmployee As New DataSet
            Dim TokenNo As String
            Dim EmpNo As String = ""
            Dim DeptName As String = ""
            Dim EmpName As String = ""
            Dim Days As String
            Dim Amount As String


            If ErrFlag = False Then

                Dim xls As Object = CreateObject("Excel.Application")
                With xls
                    Dim fullpath As String = opdOT.FileName
                    Dim MyConnection As System.Data.OleDb.OleDbConnection
                    Dim DSet As New DataSet
                    Dim DT As New DataTable
                    Dim MyCommand As System.Data.OleDb.OleDbDataAdapter
                    Dim year() As String
                    Dim SSQL As String = ""
                    Dim mdSet As New DataSet
                    Dim mSaveStatus As Long

                    MyConnection = New System.Data.OleDb.OleDbConnection("provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + fullpath + ";Extended Properties=Excel 8.0;")
                    MyCommand = New System.Data.OleDb.OleDbDataAdapter("select * from [Sheet1$]", MyConnection)
                    MyCommand.TableMappings.Add("Table", "Net-informations.com")
                    DSet = New System.Data.DataSet
                    DT = New System.Data.DataTable

                    DSet.Tables.Add(DT)
                    dgvOT.DataSource = Val(0)
                    MyCommand.Fill(DT)

                    iStr1 = Split(cmbCompCode.Text, " | ")
                    iStr2 = Split(cmbLocCode.Text, " | ")
                    year = Split(Trim(cmbFinYear.Text), "-")

                    If DSet.Tables(0).Rows.Count > 0 Then
                        For intRow = 0 To DSet.Tables(0).Rows.Count - 1

                            Dim ID As String = DSet.Tables(0).Rows(intRow)(0)

                            SSQL = ""
                            SSQL = SSQL & " select * from MstNFHsalary where MachineID='" & ID & "' and Month='" & cmbMonth.Text & "' and FinacialYear='" & year(0) & "' and  FromDate=convert(datetime,'" & txtFromDate.Text & "',103) and ToDate=convert(datetime,'" & txtFromDate.Text & "',103)"
                            dsEmployee = ReturnMultipleValue(SSQL)

                            If dsEmployee.Tables(0).Rows.Count <> 0 Then

                            Else
                                EmpNo = DSet.Tables(0).Rows(intRow)(0)
                                TokenNo = DSet.Tables(0).Rows(intRow)(1)
                                EmpName = DSet.Tables(0).Rows(intRow)(2)
                                DeptName = DSet.Tables(0).Rows(intRow)(3)
                                'Days = DSet.Tables(0).Rows(intRow)(4)
                                Amount = DSet.Tables(0).Rows(intRow)(4)

                                SSQL = ""
                                SSQL = SSQL & " insert into MstNFHsalary (TokenNo,MachineID,FirstName,Dept,Amount,Month,FinacialYear,FromDate,ToDate,Status,Ccode,Lcode) "
                                SSQL = SSQL & "values('" & TokenNo & "','" & EmpNo & "','" & EmpName & "','" & DeptName & "', "
                                SSQL = SSQL & "'" & Amount & "','" & cmbMonth.Text.Trim() & "','" & year(0) & "',convert(datetime,'" & txtFromDate.Text & "',103), "
                                SSQL = SSQL & "convert(datetime,'" & txtFromDate.Text & "',103),'" & 2 & "','" & iStr1(0) & "','" & iStr2(0) & "')"

                                mSaveStatus = InsertDeleteUpdate(SSQL)

                            End If
                        Next
                        MessageBox.Show("Successfully Upload..,", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)

                    End If

                End With
            End If
        End If
    End Sub

    Private Sub btnReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReport.Click
        If Trim(cmbCompCode.Text) = "" Then
            MessageBox.Show("Please select company code.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbCompCode.Focus()
            Exit Sub
        End If
        If Trim(cmbLocCode.Text) = "" Then
            MessageBox.Show("Please select Location code.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbCompCode.Focus()
            Exit Sub
        End If
        If Trim(cmbMonth.Text) = "" Then
            MessageBox.Show("Please select Month.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbCompCode.Focus()
            Exit Sub
        End If

        If Trim(txtFromDate.Text) = "" Then
            MessageBox.Show("Please Enter From Date.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbCompCode.Focus()
            Exit Sub
        End If

        Dim SdataSet As New DataSet
        Dim iStr1() As String
        Dim iStr2() As String
        Dim strPhotoPath As String

        iStr1 = Split(Trim(cmbCompCode.Text), " | ")
        iStr2 = Split(Trim(cmbLocCode.Text), " | ")


        SSQL = "select (ED.ExistingCode) as BiometricID,(NH.FirstName) as EmpName,(NH.Dept) as DepartmentNm,NH.Amount,NH.Status from MstNFHSalary NH "
        SSQL = SSQL & "inner join Employee_Mst ED on NH.MachineID=ED.MachineID "
        SSQL = SSQL & "where NH.Month='" & cmbMonth.Text & "' and FromDate=convert(datetime,'" & txtFromDate.Text & "',103) and ToDate=convert(datetime,'" & txtFromDate.Text & "',103) order by BiometricID asc"
        mDataSet = Nothing
        mDataSet = ReturnMultipleValue(SSQL)

        Dim cryRep As New ReportDocument
        Dim cryView As New frmRepView

        If mDataSet Is Nothing OrElse mDataSet.Tables.Count <= 0 Then Exit Sub
        cryRep.Load(Application.StartupPath & "\Reports\" & "NFH.rpt")
        cryRep.SetDataSource(mDataSet.Tables(0))
        cryRep.DataDefinition.FormulaFields("Company").Text = "'" & iStr1(1) & "'"
        cryRep.DataDefinition.FormulaFields("Dates").Text = "'" & txtFromDate.Value.ToShortDateString() & "'"
        cryView.crViewer.ReportSource = cryRep
        cryView.crViewer.Refresh()
        cryView.Show()


    End Sub

    Private Sub btnGet_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGet.Click
        LoadCover()
    End Sub
End Class