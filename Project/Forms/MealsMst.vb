﻿#Region "Imports"
Imports System
Imports System.Data
Imports System.Data.OleDb
Imports CrystalDecisions.CrystalReports.Engine
#End Region
Public Class MealsMst

    Private iStr1() As String
    Private iStr2() As String
    Private iStr3() As String
    Private EMpName As String
    Dim mStatus As Long
    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        txtAmount.Text = ""
    End Sub

    Private Sub MealsMst_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormAlign(Me)
        Fill_Company_Details()
        cmbCompCode.SelectedIndex = 0
        cmbCompCode.Enabled = False
        cmbCompCode_LostFocus(sender, e)

        If Trim(mUserLocation) <> "" Then
            cmbLocCode.SelectedIndex = cmbLocCode.FindString(Trim(mUserLocation))
            cmbLocCode.Enabled = False

            Dim iStr1() As String
            Dim iStr2() As String
            Dim iYear() As String

            iStr1 = Split(Trim(cmbCompCode.Text), " | ")
            iStr2 = Split(Trim(cmbLocCode.Text), " | ")

        Else
            cmbLocCode.SelectedIndex = 0
        End If
        Fill_Grid_Details()
    End Sub
   
    Public Sub Fill_Company_Details()
        SSQL = ""
        SSQL = "Select CompCode + ' | ' + CompName as [Name] from Company_Mst Order By CompName"

        mDataSet = ReturnMultipleValue(SSQL)
        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                cmbCompCode.Items.Clear()
                For iRow = 0 To .Rows.Count - 1
                    cmbCompCode.Items.Add(.Rows(iRow)(0))
                Next
            End If
        End With
    End Sub
    Private Sub cmbCompCode_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCompCode.LostFocus
        If Trim(cmbCompCode.Text) = "" Then Exit Sub
        Dim iStr() As String
        iStr = Split(Trim(cmbCompCode.Text), " | ")

        SSQL = ""
        SSQL = "Select LocCode + ' | ' + LocName as [Name] from Location_Mst Where Compcode='"
        SSQL = SSQL & "" & iStr(0) & "'"

        mDataSet = ReturnMultipleValue(SSQL)

        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                cmbLocCode.Items.Clear()
                For iRow = 0 To .Rows.Count - 1
                    cmbLocCode.Items.Add(.Rows(iRow)(0))
                Next
            End If
        End With
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Trim(txtAmount.Text) = "" Then
            MessageBox.Show("Enter the Amount By.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtAmount.Focus()
            Exit Sub
        End If

        Dim SdataSet As New DataSet
        Dim iStr1() As String
        Dim iStr2() As String

        iStr1 = Split(Trim(cmbCompCode.Text), " | ")
        iStr2 = Split(Trim(cmbLocCode.Text), " | ")

        SSQL = ""
        SSQL = "Delete from MealsMst Where Types='" & cmbTokenType.Text & "'"
        SSQL = SSQL & " And MealsTypes='" & cmbMealsType.Text & "'"
        mStatus = InsertDeleteUpdate(SSQL)


        SSQL = "insert into MealsMst (Types,MealsTypes,Amt,Ccode,Lcode) Values("
        SSQL = SSQL & "'" & cmbTokenType.Text & "','" & cmbMealsType.Text & "','" & txtAmount.Text & "','" & iStr1(0) & "','" & iStr2(0) & "')"
        mStatus = InsertDeleteUpdate(SSQL)
        txtAmount.Text = ""
        MessageBox.Show("Added Successfully .", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Fill_Grid_Details()

    End Sub
    Public Sub Fill_Grid_Details()
        With fgEmpType
            .Clear()
            .Cols.Fixed = 0
            .Rows.Fixed = 0
            .Cols.Count = 3
            .Rows.Count = 1
            .Cols(0).Width = 150
            .Cols(1).Width = 100
            .Cols(1).Width = 100
        End With
        mDataSet = New DataSet
        SSQL = ""
        SSQL = "Select Types,MealsTypes,Amt from MealsMst  Order By Types"

        mDataSet = ReturnMultipleValue(SSQL)

        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                fgEmpType.Rows.Count = .Rows.Count + 1
                For irow = 0 To .Rows.Count - 1
                    fgEmpType(irow, 0) = .Rows(irow)(0)
                    fgEmpType(irow, 1) = .Rows(irow)(1)
                    fgEmpType(irow, 2) = .Rows(irow)(2)
                Next
            End If
        End With

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Close()

    End Sub
End Class