﻿Public Class FrmNFh
    Dim ServerDate As String
    Dim ServerYr As Integer
    Dim yr As Integer
    Dim MonthID As String
    Dim Month As String
    Dim mStatus As Long
    Dim date_val As String
    Dim Date_count As Integer


    Private Sub FrmNFh_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Get_Server_Date()
        Load_grid()
        Leave_load()
    End Sub
    Public Sub Get_Server_Date()
        SSQL = ""
        SSQL = "Select Convert(Varchar(10),GetDate(),103)"
        ServerDate = ReturnSingleValue(SSQL)
    End Sub
    Public Sub Load_grid()
        gvHoliday.Rows.Clear()
        ServerYr = Convert.ToDateTime(ServerDate).Year
        SSQL = ""
        SSQL = "Select ID,convert(varchar, NFHDate, 105) as NFHDate from NFH_Mst where Year='" & ServerYr & "' order by NFHDate Asc"
        mDataSet = ReturnMultipleValue(SSQL)
        If mDataSet.Tables(0).Rows.Count > 0 Then
            For i As Integer = 0 To mDataSet.Tables(0).Rows.Count - 1
                Dim n As Integer = gvHoliday.Rows.Add()
                gvHoliday.Rows(n).Cells(0).Value = mDataSet.Tables(0).Rows(i)("ID").ToString()
                gvHoliday.Rows(n).Cells(1).Value = mDataSet.Tables(0).Rows(i)("NFHDate").ToString()
            Next
        

        End If
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        ServerYr = Convert.ToDateTime(ServerDate).Year
        yr = Convert.ToDateTime(LeaveDate.Text).Year

        yr = Convert.ToDateTime(LeaveDate.Text).Year
        MonthID = Convert.ToDateTime(LeaveDate.Text).Month.ToString()
        If MonthID = "1" Then
            Month = "January"
        ElseIf MonthID = "2" Then
            Month = "February"
        ElseIf MonthID = "3" Then
            Month = "March"
        ElseIf MonthID = "4" Then
            Month = "April"
        ElseIf MonthID = "5" Then
            Month = "May"
        ElseIf MonthID = "6" Then
            Month = "June"
        ElseIf MonthID = "7" Then
            Month = "July"
        ElseIf MonthID = "8" Then
            Month = "August"
        ElseIf MonthID = "9" Then
            Month = "September"
        ElseIf MonthID = "10" Then
            Month = "October"
        ElseIf MonthID = "11" Then
            Month = "November"
        ElseIf MonthID = "12" Then
            Month = "December"
        End If
        If btnSave.Text = "Save" Then
            SSQL = "Select Convert(varchar,NFHDate,105) as NFHDate from NFH_Mst where NFHDate=convert(datetime,'" & LeaveDate.Text & "',105)"
            Dim NFh As String = ReturnSingleValue(SSQL)
            If Convert.ToDateTime(NFh) = Convert.ToDateTime(LeaveDate.Text) Then

                MessageBox.Show("Date was Already Registered", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else

                Dim qry As String = "Insert into NFH_Mst (NFHDate,Months,Year) values (convert(datetime,'" & LeaveDate.Text & "',103),'" & Month & "','" & yr & "')"
                mStatus = InsertDeleteUpdate(qry)
                If mStatus > 0 Then
                    MessageBox.Show("Saved Successfully...!", "Altius InfoSystems", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
                    Load_grid()
                    Leave_load()
                End If


            End If
        ElseIf btnSave.Text = "Update" Then
            Dim y As Integer
            y = Convert.ToInt32(gvHoliday.SelectedRows(0).Cells(0).Value)
            SSQL = "Update NFH_Mst set NFHDate=convert(datetime,'" & LeaveDate.Text & "',103), Months='" & Month & "',Year='" & yr & "' where ID='" & y & "'"
            mStatus = InsertDeleteUpdate(SSQL)
            Load_grid()
            Leave_load()
            If mStatus > 0 Then
                MessageBox.Show("Updated Successfully...!", "Altius InfoSystems", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
            End If
        End If


        'If ServerYr > yr Then
        '    MessageBox.Show("Please Apply Leave Properly.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
        'Else
        '    yr = Convert.ToDateTime(LeaveDate.Text).Year
        '    MonthID = Convert.ToDateTime(LeaveDate.Text).Month.ToString()
        '    If MonthID = "1" Then
        '        Month = "January"
        '    ElseIf MonthID = "2" Then
        '        Month = "February"
        '    ElseIf MonthID = "3" Then
        '        Month = "March"
        '    ElseIf MonthID = "4" Then
        '        Month = "April"
        '    ElseIf MonthID = "5" Then
        '        Month = "May"
        '    ElseIf MonthID = "6" Then
        '        Month = "June"
        '    ElseIf MonthID = "7" Then
        '        Month = "July"
        '    ElseIf MonthID = "8" Then
        '        Month = "August"
        '    ElseIf MonthID = "9" Then
        '        Month = "September"
        '    ElseIf MonthID = "10" Then
        '        Month = "October"
        '    ElseIf MonthID = "11" Then
        '        Month = "November"
        '    ElseIf MonthID = "12" Then
        '        Month = "December"
        '    End If
        '    If btnSave.Text = "Save" Then
        '        SSQL = "Select Convert(varchar,NFHDate,105) as NFHDate from NFH_Mst where NFHDate=convert(datetime,'" & LeaveDate.Text & "',105)"
        '        Dim NFh As String = ReturnSingleValue(SSQL)
        '        If NFh = LeaveDate.Text Then

        '            MessageBox.Show("Date was Already Registered", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
        '        Else

        '            Dim qry As String = "Insert into NFH_Mst (NFHDate,Months,Year) values (convert(datetime,'" & LeaveDate.Text & "',103),'" & Month & "','" & yr & "')"
        '            mStatus = InsertDeleteUpdate(qry)
        '            If mStatus > 0 Then
        '                MessageBox.Show("Saved Successfully...!", "Altius InfoSystems", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
        '                Load_grid()
        '                Leave_load()
        '            End If


        '        End If
        '    ElseIf btnSave.Text = "Update" Then
        '        Dim y As Integer
        '        y = Convert.ToInt32(gvHoliday.SelectedRows(0).Cells(0).Value)
        '        SSQL = "Update NFH_Mst set NFHDate=convert(datetime,'" & LeaveDate.Text & "',103), Months='" & Month & "',Year='" & yr & "' where ID='" & y & "'"
        '        mStatus = InsertDeleteUpdate(SSQL)
        '        Load_grid()
        '        Leave_load()
        '        If mStatus > 0 Then
        '            MessageBox.Show("Updated Successfully...!", "Altius InfoSystems", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
        '        End If
        '    End If
        'End If

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        Me.Close()
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If btnEdit.Text = "Edit" Then
            PanelGrid.Visible = True
            btnSave.Text = "Update"
            btnEdit.Text = "Back"

        ElseIf btnEdit.Text = "Back" Then

            btnSave.Text = "Save"
            btnEdit.Text = "Edit"

        End If
    End Sub
    Public Sub Leave_load()
        ServerYr = Convert.ToDateTime(ServerDate).Year
        GVLeave.Rows.Clear()
        For i As Integer = 0 To 11
            date_val = ""
            Date_count = 0
            If i = "0" Then
                Month = "January"
            ElseIf i = "1" Then
                Month = "February"
            ElseIf i = "2" Then
                Month = "March"
            ElseIf i = "3" Then
                Month = "April"
            ElseIf i = "4" Then
                Month = "May"
            ElseIf i = "5" Then
                Month = "June"
            ElseIf i = "6" Then
                Month = "July"
            ElseIf i = "7" Then
                Month = "August"
            ElseIf i = "8" Then
                Month = "September"
            ElseIf i = "9" Then
                Month = "October"
            ElseIf i = "10" Then
                Month = "November"
            ElseIf i = "11" Then
                Month = "December"
            End If
            SSQL = "Select convert(varchar(2),NFHDate,103) as Day from NFH_Mst where Months='" & Month & "' and Year='" & ServerYr & "' order by NFHDate Asc"
            mDataSet = ReturnMultipleValue(SSQL)
            If mDataSet.Tables(0).Rows.Count > 0 Then
                For j As Integer = 0 To mDataSet.Tables(0).Rows.Count - 1
                    date_val = mDataSet.Tables(0).Rows(j)("Day").ToString() & "," & date_val

                Next
            Else
                date_val = "0"

            End If
            Dim qry As String = "Select count (*) as Day from NFH_Mst where Months='" & Month & "' and Year='" & ServerYr & "'"
            mDataSet = ReturnMultipleValue(qry)
            If mDataSet.Tables(0).Rows.Count > 0 Then
                Date_count = Convert.ToInt32(mDataSet.Tables(0).Rows(0)("Day").ToString())
            Else
                Date_count = 0
            End If

            Dim n As Integer = GVLeave.Rows.Add
            GVLeave.Rows(n).Cells(0).Value = Month.ToString()
            GVLeave.Rows(n).Cells(1).Value = date_val
            GVLeave.Rows(n).Cells(2).Value = Date_count
        Next
        lblSum.Text = "0"
        For k As Integer = 0 To GVLeave.Rows.Count - 1
            lblSum.Text = (Convert.ToInt32(GVLeave.Rows(k).Cells(2).Value) + Convert.ToInt32(lblSum.Text)).ToString()

        Next

    End Sub

    Private Sub gvHoliday_CellMouseClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles gvHoliday.CellMouseClick
        LeaveDate.Text = gvHoliday.SelectedRows(0).Cells(1).Value.ToString()
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If gvHoliday.Visible = True Then
                Dim y As Integer
                Dim res As DialogResult
                res = MessageBox.Show("Do you want to Delete ?", "Altius InfoSystems", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                If res.ToString() = "Yes" Then
                    y = Convert.ToInt32(gvHoliday.SelectedRows(0).Cells(0).Value)
                    Dim qry As String = "Delete from NFH_Mst where ID='" & y & "'"
                    mStatus = InsertDeleteUpdate(qry)
                    Load_grid()
                    Leave_load()
                    MessageBox.Show("Deleted Successfully...!", "Altius InfoSystems", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
                Else

                End If
                

            Else
                MessageBox.Show("No Data....!", "Altius InfoSystems", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
            End If

        Catch ex As Exception

        End Try
        

    End Sub

    Private Sub Panel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel1.Paint

    End Sub

    Private Sub Label3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblSum.Click

    End Sub

    Private Sub GVLeave_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles GVLeave.CellContentClick

    End Sub

    Private Sub gvHoliday_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles gvHoliday.CellContentClick

    End Sub
End Class