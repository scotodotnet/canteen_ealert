﻿Public Class frmTimeDelete
    Dim Time_Minus_Value_Check() As String
   

    Public Sub Fill_Company_Details()
        SSQL = ""
        SSQL = "Select CompCode + ' | ' + CompName as [Name] from Company_Mst Order By CompName"

        mDataSet = ReturnMultipleValue(SSQL)
        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                cmbCompCode.Items.Clear()
                For iRow = 0 To .Rows.Count - 1
                    cmbCompCode.Items.Add(.Rows(iRow)(0))
                Next
            End If
        End With
    End Sub

    Private Sub IN_Grid_Col_Align()

        With flxGrid_IN
            .Columns(0).Width = 5 'DummySno
            .Columns(1).Width = 53 'Select
            .Columns(2).Width = 47 'SNo
            .Columns(3).Width = 82 'MachineID
            .Columns(4).Width = 95 'TokenNo
            .Columns(5).Width = 180 'EmpName
            .Columns(6).Width = 187 'IN Time

            .RowHeadersVisible = False
            .ColumnHeadersVisible = True
        End With
    End Sub

    Private Sub OUT_Grid_Col_Align()

        With flxGrid_Out
            .Columns(0).Width = 5 'DummySno
            .Columns(1).Width = 53 'Select
            .Columns(2).Width = 47 'SNo
            .Columns(3).Width = 82 'MachineID
            .Columns(4).Width = 95 'TokenNo
            .Columns(5).Width = 180 'EmpName
            .Columns(6).Width = 187 'OUT Time

            .RowHeadersVisible = False
            .ColumnHeadersVisible = True
        End With
    End Sub

    Private Sub cmbCompCode_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCompCode.LostFocus
        If Trim(cmbCompCode.Text) = "" Then Exit Sub
        Dim iStr() As String
        iStr = Split(Trim(cmbCompCode.Text), " | ")

        SSQL = ""
        SSQL = "Select LocCode + ' | ' + LocName as [Name] from Location_Mst Where Compcode='"
        SSQL = SSQL & "" & iStr(0) & "'"

        mDataSet = ReturnMultipleValue(SSQL)

        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                cmbLocCode.Items.Clear()
                For iRow = 0 To .Rows.Count - 1
                    cmbLocCode.Items.Add(.Rows(iRow)(0))
                Next
            End If
        End With
    End Sub

    Private Sub cmbCompCode_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbCompCode.SelectedIndexChanged

    End Sub

    Private Sub cmbLocCode_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbLocCode.LostFocus
        Call Load_Token_No_WagesWise()
        Call Load_MachineID_WagesWise()
    End Sub

    Private Sub cmbLocCode_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbLocCode.SelectedIndexChanged

    End Sub

    Private Sub Load_Token_No_WagesWise()
        Dim i As Int32

        Dim iStr1() As String
        Dim iStr2() As String
        iStr1 = Split(cmbCompCode.Text, " | ")
        iStr2 = Split(cmbLocCode.Text, " | ")

        SSQL = "Select * from Employee_Mst where CompCode='" & iStr1(0) & "' and LocCode='" & iStr2(0) & "' And IsActive='YES' Order by ExistingCode Asc"
        mDataSet = ReturnMultipleValue(SSQL)
        cmbTokenNo.Items.Clear()
        For i = 0 To mDataSet.Tables(0).Rows.Count - 1
            With cmbTokenNo
                .Items.Add(mDataSet.Tables(0).Rows(i)("ExistingCode").ToString())
            End With
        Next i

    End Sub

    Private Sub Load_MachineID_WagesWise()
        Dim i As Int32

        Dim iStr1() As String
        Dim iStr2() As String
        iStr1 = Split(cmbCompCode.Text, " | ")
        iStr2 = Split(cmbLocCode.Text, " | ")

        SSQL = "Select * from Employee_Mst where CompCode='" & iStr1(0) & "' and LocCode='" & iStr2(0) & "' And IsActive='YES' Order by MachineID Asc"
        mDataSet = ReturnMultipleValue(SSQL)
        cmbMachineID.Items.Clear()
        For i = 0 To mDataSet.Tables(0).Rows.Count - 1
            With cmbMachineID
                .Items.Add(mDataSet.Tables(0).Rows(i)("MachineID").ToString())
            End With
        Next i

    End Sub

    Private Sub BtnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnExit.Click
        'MsgBox(flxGrid_IN.Columns(0).Width())
        'MsgBox(flxGrid_IN.Columns(1).Width())
        'MsgBox(flxGrid_IN.Columns(2).Width())
        'MsgBox(flxGrid_IN.Columns(3).Width())
        'MsgBox(flxGrid_IN.Columns(4).Width())
        'MsgBox(flxGrid_IN.Columns(5).Width())
        'MsgBox(flxGrid_IN.Columns(6).Width())

        'MsgBox(flxGrid_Out.Columns(0).Width())
        'MsgBox(flxGrid_Out.Columns(1).Width())
        'MsgBox(flxGrid_Out.Columns(2).Width())
        'MsgBox(flxGrid_Out.Columns(3).Width())
        'MsgBox(flxGrid_Out.Columns(4).Width())
        'MsgBox(flxGrid_Out.Columns(5).Width())
        'MsgBox(flxGrid_Out.Columns(6).Width())

        Close()
    End Sub

    Private Sub cmbMachineID_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbMachineID.SelectedIndexChanged
        If Trim(cmbMachineID.Text) <> "" Then
            Dim iStr1() As String
            Dim iStr2() As String
            iStr1 = Split(cmbCompCode.Text, " | ")
            iStr2 = Split(cmbLocCode.Text, " | ")
            SSQL = "Select * from Employee_Mst where CompCode='" & iStr1(0) & "' and LocCode='" & iStr2(0) & "' And MachineID='" & Trim(cmbMachineID.Text) & "'"
            mDataSet = ReturnMultipleValue(SSQL)
            If mDataSet.Tables(0).Rows.Count <> 0 Then
                txtEmpName.Text = mDataSet.Tables(0).Rows(0)("FirstName")
                cmbTokenNo.Text = mDataSet.Tables(0).Rows(0)("ExistingCode")
            End If
        End If
    End Sub

    Private Sub cmbTokenNo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbTokenNo.SelectedIndexChanged
        If Trim(cmbTokenNo.Text) <> "" Then
            Dim iStr1() As String
            Dim iStr2() As String
            iStr1 = Split(cmbCompCode.Text, " | ")
            iStr2 = Split(cmbLocCode.Text, " | ")
            SSQL = "Select * from Employee_Mst where CompCode='" & iStr1(0) & "' and LocCode='" & iStr2(0) & "' And ExistingCode='" & Trim(cmbTokenNo.Text) & "'"
            mDataSet = ReturnMultipleValue(SSQL)
            If mDataSet.Tables(0).Rows.Count <> 0 Then
                txtEmpName.Text = mDataSet.Tables(0).Rows(0)("FirstName")
                cmbMachineID.Text = mDataSet.Tables(0).Rows(0)("MachineID")
            End If
        End If
    End Sub

    Private Sub BtnLoad_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnLoad.Click

        If Trim(cmbCompCode.Text) = "" Then MsgBox("You have to Select Company Code", MsgBoxStyle.Critical) : cmbCompCode.Focus() : Exit Sub
        If Trim(cmbLocCode.Text) = "" Then MsgBox("You have to Select Location Code", MsgBoxStyle.Critical) : cmbLocCode.Focus() : Exit Sub
        If Trim(cmbMachineID.Text) = "" And Trim(cmbTokenNo.Text) = "" Then MsgBox("You have to Select Machine ID Or Token No", MsgBoxStyle.Critical) : cmbMachineID.Focus() : Exit Sub

        Dim iStr1() As String
        Dim iStr2() As String
        Dim Machine_ID_Str As String
        Dim Machine_ID_Encrypt As String
        Dim i As Int32
        iStr1 = Split(cmbCompCode.Text, " | ")
        iStr2 = Split(cmbLocCode.Text, " | ")

        Machine_ID_Str = Trim(cmbMachineID.Text)
        Machine_ID_Encrypt = Encryption(Trim(cmbMachineID.Text))

        Call Load_IN_Time()
        Call Load_Out_Time()

        If flxGrid_IN.Rows.Count = 0 And flxGrid_Out.Rows.Count = 0 Then
            MsgBox("No Record Found", MsgBoxStyle.Critical) : txtAttnDate.Focus() : Exit Sub
        Else
            MsgBox("Time Load Successfully", MsgBoxStyle.Information) : Exit Sub
        End If

    End Sub

    Private Sub flxGrid_IN_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles flxGrid_IN.CellContentClick
        If e.ColumnIndex = 1 Then
            With flxGrid_IN
                If .Rows(e.RowIndex).Cells(e.ColumnIndex).Value = True Then
                    .Rows(e.RowIndex).Cells(e.ColumnIndex).Value = False
                Else
                    .Rows(e.RowIndex).Cells(e.ColumnIndex).Value = True
                End If
            End With
        End If
    End Sub

    Private Sub flxGrid_Out_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles flxGrid_Out.CellContentClick
        If e.ColumnIndex = 1 Then
            With flxGrid_Out
                If .Rows(e.RowIndex).Cells(e.ColumnIndex).Value = True Then
                    .Rows(e.RowIndex).Cells(e.ColumnIndex).Value = False
                Else
                    .Rows(e.RowIndex).Cells(e.ColumnIndex).Value = True
                End If
            End With
        End If
    End Sub

    Private Sub btnINDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnINDelete.Click

        If flxGrid_IN.Rows.Count = 0 Then MsgBox("No Record Found", MsgBoxStyle.Critical) : Exit Sub

        Dim i As Int32
        Dim Delete_Column_Check As Boolean = False
        Dim iStr1() As String
        Dim iStr2() As String
        Dim Machine_ID_Str As String
        Dim Machine_ID_Encrypt As String
        iStr1 = Split(cmbCompCode.Text, " | ")
        iStr2 = Split(cmbLocCode.Text, " | ")
        Delete_Column_Check = False
        For i = 0 To flxGrid_IN.Rows.Count - 1
            With flxGrid_IN
                If .Rows(i).Cells(1).Value = True Then
                    Machine_ID_Str = Trim(.Rows(i).Cells(3).Value)
                    Machine_ID_Encrypt = Encryption(Trim(.Rows(i).Cells(3).Value))

                    'Delete TIME IN Record
                    SSQL = "Delete from LogTime_IN where MachineID='" & Machine_ID_Encrypt & "'"
                    SSQL = SSQL & " And Compcode='" & iStr1(0) & "' And LocCode='" & iStr2(0) & "'"
                    SSQL = SSQL & " And TimeIN = '" & Format(.Rows(i).Cells(6).Value, "yyyy/MM/dd H:mm:ss.000") & "'"


                    InsertDeleteUpdate(SSQL)
                    Delete_Column_Check = True
                End If
            End With
        Next i

        Dim Attn_Date_Str As String = txtAttnDate.Text
        Get_Working_Days_Save_DB(cmbMachineID.Text, Attn_Date_Str)

        If Delete_Column_Check = True Then
            Call Load_IN_Time()
            MsgBox("Selected Row Deleted Successfully", MsgBoxStyle.Information)
        Else
            MsgBox("You have to Select atleast one Row", MsgBoxStyle.Critical) : flxGrid_IN.Focus() : Exit Sub
        End If
    End Sub

    Private Sub Load_IN_Time()

        Dim iStr1() As String
        Dim iStr2() As String
        Dim Machine_ID_Str As String
        Dim Machine_ID_Encrypt As String
        Dim i As Int32
        iStr1 = Split(cmbCompCode.Text, " | ")
        iStr2 = Split(cmbLocCode.Text, " | ")

        Machine_ID_Str = Trim(cmbMachineID.Text)
        Machine_ID_Encrypt = Encryption(Trim(cmbMachineID.Text))
        'Time IN Load
        SSQL = "Select TimeIN from LogTime_IN where MachineID='" & Machine_ID_Encrypt & "'"
        SSQL = SSQL & " And Compcode='" & iStr1(0) & "' And LocCode='" & iStr2(0) & "'"
        SSQL = SSQL & " And TimeIN >='" & txtAttnDate.Value.AddDays(0).ToString("yyyy/MM/dd") & " " & "00:01" & "'"
        SSQL = SSQL & " And TimeIN <='" & txtAttnDate.Value.AddDays(0).ToString("yyyy/MM/dd") & " " & "23:59" & "'"
        SSQL = SSQL & " Order by TimeIN ASC"
        mDataSet = ReturnMultipleValue(SSQL)

        'IN_Grid_Load
        flxGrid_IN.Rows.Clear()
        For i = 0 To mDataSet.Tables(0).Rows.Count - 1
            With flxGrid_IN
                .Rows.Add()
                .Rows(.Rows.Count - 1).Cells(0).Value = .Rows.Count
                .Rows(.Rows.Count - 1).Cells(1).Value = False
                .Rows(.Rows.Count - 1).Cells(2).Value = .Rows.Count
                .Rows(.Rows.Count - 1).Cells(3).Value = Trim(cmbMachineID.Text)
                .Rows(.Rows.Count - 1).Cells(4).Value = Trim(cmbTokenNo.Text)
                .Rows(.Rows.Count - 1).Cells(5).Value = Trim(txtEmpName.Text)
                .Rows(.Rows.Count - 1).Cells(6).Value = mDataSet.Tables(0).Rows(i)("TimeIN")
            End With
        Next i

    End Sub

    Private Sub Load_Out_Time()

        Dim iStr1() As String
        Dim iStr2() As String
        Dim Machine_ID_Str As String
        Dim Machine_ID_Encrypt As String
        Dim i As Int32
        iStr1 = Split(cmbCompCode.Text, " | ")
        iStr2 = Split(cmbLocCode.Text, " | ")

        Machine_ID_Str = Trim(cmbMachineID.Text)
        Machine_ID_Encrypt = Encryption(Trim(cmbMachineID.Text))

        'Time OUT Load
        SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" & Machine_ID_Encrypt & "'"
        SSQL = SSQL & " And Compcode='" & iStr1(0) & "' And LocCode='" & iStr2(0) & "'"
        SSQL = SSQL & " And TimeOUT >='" & txtAttnDate.Value.AddDays(0).ToString("yyyy/MM/dd") & " " & "00:01" & "'"
        SSQL = SSQL & " And TimeOUT <='" & txtAttnDate.Value.AddDays(0).ToString("yyyy/MM/dd") & " " & "23:59" & "'"
        SSQL = SSQL & " Order by TimeOUT ASC"
        mDataSet = ReturnMultipleValue(SSQL)

        'OUT_Grid_Load
        flxGrid_Out.Rows.Clear()
        For i = 0 To mDataSet.Tables(0).Rows.Count - 1
            With flxGrid_Out
                .Rows.Add()
                .Rows(.Rows.Count - 1).Cells(0).Value = .Rows.Count
                .Rows(.Rows.Count - 1).Cells(1).Value = False
                .Rows(.Rows.Count - 1).Cells(2).Value = .Rows.Count
                .Rows(.Rows.Count - 1).Cells(3).Value = Trim(cmbMachineID.Text)
                .Rows(.Rows.Count - 1).Cells(4).Value = Trim(cmbTokenNo.Text)
                .Rows(.Rows.Count - 1).Cells(5).Value = Trim(txtEmpName.Text)
                .Rows(.Rows.Count - 1).Cells(6).Value = mDataSet.Tables(0).Rows(i)("TimeOUT")
            End With
        Next i

    End Sub

    Private Sub BtnOutDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnOutDelete.Click

        If flxGrid_Out.Rows.Count = 0 Then MsgBox("No Record Found", MsgBoxStyle.Critical) : Exit Sub

        Dim i As Int32
        Dim Delete_Column_Check As Boolean = False
        Dim iStr1() As String
        Dim iStr2() As String
        Dim Machine_ID_Str As String
        Dim Machine_ID_Encrypt As String
        iStr1 = Split(cmbCompCode.Text, " | ")
        iStr2 = Split(cmbLocCode.Text, " | ")
        Delete_Column_Check = False
        For i = 0 To flxGrid_Out.Rows.Count - 1
            With flxGrid_Out
                If .Rows(i).Cells(1).Value = True Then
                    Machine_ID_Str = Trim(.Rows(i).Cells(3).Value)
                    Machine_ID_Encrypt = Encryption(Trim(.Rows(i).Cells(3).Value))

                    'Delete TIME OUT Record
                    SSQL = "Delete from LogTime_OUT where MachineID='" & Machine_ID_Encrypt & "'"
                    SSQL = SSQL & " And Compcode='" & iStr1(0) & "' And LocCode='" & iStr2(0) & "'"
                    SSQL = SSQL & " And TimeOut = '" & Format(.Rows(i).Cells(6).Value, "yyyy/MM/dd H:mm:ss.000") & "'"
                    InsertDeleteUpdate(SSQL)
                    Delete_Column_Check = True
                End If
            End With
        Next i

        Dim Attn_Date_Str As String = txtAttnDate.Text
        Get_Working_Days_Save_DB(cmbMachineID.Text, Attn_Date_Str)

        If Delete_Column_Check = True Then
            Call Load_Out_Time()
            MsgBox("Selected Row Deleted Successfully", MsgBoxStyle.Information)
        Else
            MsgBox("You have to Select atleast one Row", MsgBoxStyle.Critical) : flxGrid_Out.Focus() : Exit Sub
        End If
    End Sub

    Private Sub Get_Working_Days_Save_DB(ByVal Get_Machine_ID As String, ByVal Get_Punching_Date As String)
        Try

            Dim iStr1() As String
            Dim iStr2() As String

            Dim intI As Integer = 1
            Dim intK As Integer = 1
            Dim intCol As Integer
            'Dim SQl_Query As String

            'Spinning Incentive Var
            Dim Fin_Year As String = ""
            Dim Months_Full_Str As String = ""
            Dim Date_Col_Str As String = ""
            Dim Month_Int As Int32 = 1
            Dim Spin_Machine_ID_Str As String = ""


            'Attn_Flex Col Add Var
            Dim Att_Date_Check() As String
            Dim Att_Already_Date_Check As String
            Dim Att_Year_Check As String
            Dim Month_Name_Change As Integer
            Dim halfPresent As String = "0"
            Att_Year_Check = ""
            Dim EPay_Total_Days_Get As Int32 = 0

            intCol = 4 : Month_Name_Change = 1
            Dim dayCount As Integer = 1
            Dim daysAdded As Integer = 1
            Att_Already_Date_Check = ""


            EPay_Total_Days_Get = 1

            intI = 2
            intK = 1

            iStr1 = Split(cmbCompCode.Text, " | ")
            iStr2 = Split(cmbLocCode.Text, " | ")
            'get Employee Details
            Dim Emp_DS As New DataSet
            SSQL = "Select * from Employee_MST where Compcode='" & iStr1(0) & "' And LocCode='" & iStr2(0) & "'"
            SSQL = SSQL & " And MachineID='" & Get_Machine_ID & "'"

            Emp_DS = ReturnMultipleValue(SSQL)
            If Emp_DS.Tables(0).Rows.Count = 0 Then
                SSQL = "Select * from Employee_Mst_New_Emp where Compcode='" & iStr1(0) & "' And LocCode='" & iStr2(0) & "'"
                SSQL = SSQL & " And MachineID='" & Get_Machine_ID & "'"
                Emp_DS = ReturnMultipleValue(SSQL)
                If Emp_DS.Tables(0).Rows.Count = 0 Then
                    Exit Sub
                End If
            End If


            Dim Transfer_Check_blb As Boolean = False

            For intRow = 0 To Emp_DS.Tables(0).Rows.Count - 1
                intK = 1

                'Get Employee Week OF DAY
                Dim DS_WH As New DataSet
                Dim Emp_WH_Day As String = ""
                Dim DOJ_Date_Str As String = ""
                SSQL = "Select * from Employee_MST where MachineID='" & Emp_DS.Tables(0).Rows(intRow)("MachineID") & "' And Compcode='" & iStr1(0) & "' And LocCode='" & iStr2(0) & "'"
                DS_WH = ReturnMultipleValue(SSQL)
                If DS_WH.Tables(0).Rows.Count <> 0 Then
                    Emp_WH_Day = DS_WH.Tables(0).Rows(0)("WeekOff").ToString()
                    DOJ_Date_Str = IIf(IsDBNull(DS_WH.Tables(0).Rows(0)("DOJ")), "", DS_WH.Tables(0).Rows(0)("DOJ"))
                Else
                    Emp_WH_Day = ""
                    DOJ_Date_Str = ""
                End If

                Dim colIndex As Integer = intK
                Dim isPresent As Boolean = False
                Dim Present_Count As Decimal = 0
                Dim Month_WH_Count As Decimal = 0
                Dim Present_WH_Count As Decimal = 0
                Dim Appsent_Count As Int32 = 0
                Dim Final_Count As Decimal = 0
                Dim Total_Days_Count As Decimal = 0
                Dim Month_Mid_Total_Days_Count As Decimal = 0

                Dim Already_Date_Check As String
                Dim Year_Check As String
                Dim Days_Insert_RowVal As Int32 = 0

                Dim FullNight_Shift_Count As Decimal = 0
                Dim NFH_Days_Count As Int32 = 0
                Dim NFH_Days_Present_Count As Decimal = 0
                Already_Date_Check = "" : Year_Check = ""

                For intCol = 0 To daysAdded - 1

                    isPresent = False
                    Dim Emp_Total_Work_Time_1 As String = "00:00"
                    Dim Machine_ID_Str As String = ""
                    Dim OT_Week_OFF_Machine_No As String
                    Dim Date_Value_Str As String = ""
                    Dim Total_Time_get As String = ""
                    Dim Final_OT_Work_Time_1 As String = "00:00"
                    Dim mLocalDS As New DataSet
                    Dim Time_IN_Str As String = ""
                    Dim Time_Out_Str As String = ""
                    'Dim Total_Time_get As String = ""
                    Dim j As Int32 = 0
                    Dim time_Check_dbl As Double = 0
                    Dim Date_Value_Str1 As String = ""
                    Dim Employee_Shift_Name_DB As String = "No Shift"
                    isPresent = False

                    'Shift Change Check Variable Declaration Start
                    Dim InTime_Check As DateTime
                    Dim InToTime_Check As DateTime
                    Dim InTime_TimeSpan As TimeSpan
                    Dim From_Time_Str As String = ""
                    Dim To_Time_Str As String = ""
                    Dim DS_Time As DataSet
                    Dim DS_InTime As DataSet
                    Dim Final_InTime As String = ""
                    Dim Final_OutTime As String = ""
                    Dim Final_Shift As String = ""
                    Dim Shift_DS_Change As DataSet
                    Dim K As Int32 = 0
                    Dim Shift_Check_blb As Boolean = False
                    Dim Shift_Start_Time_Change As String
                    Dim Shift_End_Time_Change As String
                    Dim Employee_Time_Change As String = ""
                    Dim ShiftdateStartIN_Change As DateTime
                    Dim ShiftdateEndIN_Change As DateTime
                    Dim EmpdateIN_Change As DateTime
                    'Shift Change Check Variable Declaration End

                    Dim Employee_Punch As String = ""


                    Spin_Machine_ID_Str = Emp_DS.Tables(0).Rows(intRow)("MachineID")

                    Machine_ID_Str = Encryption(Emp_DS.Tables(0).Rows(intRow)("MachineID"))
                    OT_Week_OFF_Machine_No = Emp_DS.Tables(0).Rows(intRow)("MachineID")
                    Date_Value_Str = Format((Convert.ToDateTime(Get_Punching_Date).AddDays(0)), "yyyy/MM/dd")
                    Date_Value_Str1 = Format((Convert.ToDateTime(Date_Value_Str).AddDays(1)), "yyyy/MM/dd")

                    'TimeIN Get
                    Dim mLocalDS1 As DataSet
                    SSQL = "Select TimeIN from LogTime_IN where MachineID='" & Machine_ID_Str & "'"
                    SSQL = SSQL & " And Compcode='" & iStr1(0) & "' And LocCode='" & iStr2(0) & "'"
                    SSQL = SSQL & " And TimeIN >='" & Date_Value_Str & " " & "02:00' And TimeIN <='" & Date_Value_Str1 & " " & "02:00' Order by TimeIN ASC"
                    mLocalDS = ReturnMultipleValue(SSQL)
                    If mLocalDS.Tables(0).Rows.Count <= 0 Then
                        Time_IN_Str = ""
                    Else
                        'Time_IN_Str = mLocalDS.Tables(0).Rows(0)(0)
                    End If

                    'TimeOUT Get
                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" & Machine_ID_Str & "'"
                    SSQL = SSQL & " And Compcode='" & iStr1(0) & "' And LocCode='" & iStr2(0) & "'"
                    SSQL = SSQL & " And TimeOUT >='" & Date_Value_Str & " " & "10:00' And TimeOUT <='" & Date_Value_Str1 & " " & "10:00' Order by TimeOUT Asc"
                    mLocalDS1 = ReturnMultipleValue(SSQL)
                    If mLocalDS.Tables(0).Rows.Count <= 0 Then
                        Time_Out_Str = ""
                    Else
                        'Time_Out_Str = mLocalDS.Tables(0).Rows(0)(0)
                    End If

                    'Shift Change Check Start
                    If mLocalDS.Tables(0).Rows.Count <> 0 Then
                        InTime_Check = mLocalDS.Tables(0).Rows(0)("TimeIN").ToString()
                        InToTime_Check = InTime_Check.AddHours(2)
                        InTime_Check = Format(InTime_Check, "HH:mm:ss") : InToTime_Check = Format(InToTime_Check, "HH:mm:ss")
                        InTime_TimeSpan = TimeSpan.Parse(InTime_Check) : From_Time_Str = InTime_TimeSpan.Hours & ":" & InTime_TimeSpan.Minutes
                        InTime_TimeSpan = TimeSpan.Parse(InToTime_Check) : To_Time_Str = InTime_TimeSpan.Hours & ":" & InTime_TimeSpan.Minutes

                        'Two Hours OutTime Check
                        SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" & Machine_ID_Str & "'"
                        SSQL = SSQL & " And Compcode='" & iStr1(0) & "' And LocCode='" & iStr2(0) & "'"
                        SSQL = SSQL & " And TimeOUT >='" & Date_Value_Str & " " & From_Time_Str & "' And TimeOUT <='" & Date_Value_Str & " " & To_Time_Str & "' Order by TimeOUT Asc"
                        DS_Time = ReturnMultipleValue(SSQL)
                        If DS_Time.Tables(0).Rows.Count <> 0 Then
                            SSQL = "Select TimeIN from LogTime_IN where MachineID='" & Machine_ID_Str & "'"
                            SSQL = SSQL & " And Compcode='" & iStr1(0) & "' And LocCode='" & iStr2(0) & "'"
                            SSQL = SSQL & " And TimeIN >='" & Date_Value_Str & " " & To_Time_Str & "' And TimeIN <='" & Date_Value_Str1 & " " & "02:00' Order by TimeIN ASC"
                            DS_InTime = ReturnMultipleValue(SSQL)
                            If DS_InTime.Tables(0).Rows.Count <> 0 Then
                                Final_InTime = DS_InTime.Tables(0).Rows(0)(0)
                                'Check With IN Time Shift
                                SSQL = "Select * from Shift_Mst Where CompCode='" & iStr1(0) & "' And LocCode='" & iStr2(0) & "' And ShiftDesc like '%SHIFT%'"
                                Shift_DS_Change = ReturnMultipleValue(SSQL)
                                Shift_Check_blb = False
                                For K = 0 To Shift_DS_Change.Tables(0).Rows.Count - 1
                                    Shift_Start_Time_Change = Date_Value_Str & " " & Shift_DS_Change.Tables(0).Rows(K)("StartIN").ToString()
                                    If Shift_DS_Change.Tables(0).Rows(K)("EndIN_Days").ToString() = "1" Then
                                        Shift_End_Time_Change = Date_Value_Str1 & " " & Shift_DS_Change.Tables(0).Rows(K)("EndIN").ToString()
                                    Else
                                        Shift_End_Time_Change = Date_Value_Str & " " & Shift_DS_Change.Tables(0).Rows(K)("EndIN").ToString()
                                    End If

                                    ShiftdateStartIN_Change = System.Convert.ToDateTime(Shift_Start_Time_Change)
                                    ShiftdateEndIN_Change = System.Convert.ToDateTime(Shift_End_Time_Change)
                                    EmpdateIN_Change = System.Convert.ToDateTime(Final_InTime)
                                    If EmpdateIN_Change >= ShiftdateStartIN_Change And EmpdateIN_Change <= ShiftdateEndIN_Change Then
                                        Final_Shift = Shift_DS_Change.Tables(0).Rows(K)("ShiftDesc").ToString()
                                        Shift_Check_blb = True : Exit For
                                    End If
                                Next K
                                If Shift_Check_blb = True Then
                                    'IN Time Query Update
                                    SSQL = "Select TimeIN from LogTime_IN where MachineID='" & Machine_ID_Str & "'"
                                    SSQL = SSQL & " And Compcode='" & iStr1(0) & "' And LocCode='" & iStr2(0) & "'"
                                    SSQL = SSQL & " And TimeIN >='" & Date_Value_Str & " " & To_Time_Str & "' And TimeIN <='" & Date_Value_Str1 & " " & "02:00' Order by TimeIN ASC"
                                    mLocalDS = ReturnMultipleValue(SSQL)
                                    If UCase(Final_Shift) = UCase("SHIFT2") Then
                                        SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" & Machine_ID_Str & "'"
                                        SSQL = SSQL & " And Compcode='" & iStr1(0) & "' And LocCode='" & iStr2(0) & "'"
                                        SSQL = SSQL & " And TimeOUT >='" & Date_Value_Str & " " & "13:00' And TimeOUT <='" & Date_Value_Str1 & " " & "03:00' Order by TimeOUT Asc"
                                    Else
                                        SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" & Machine_ID_Str & "'"
                                        SSQL = SSQL & " And Compcode='" & iStr1(0) & "' And LocCode='" & iStr2(0) & "'"
                                        SSQL = SSQL & " And TimeOUT >='" & Date_Value_Str & " " & "17:40' And TimeOUT <='" & Date_Value_Str1 & " " & "10:00' Order by TimeOUT Asc"
                                    End If
                                    mLocalDS1 = ReturnMultipleValue(SSQL)
                                Else
                                    'Get Employee In Time
                                    SSQL = "Select TimeIN from LogTime_IN where MachineID='" & Machine_ID_Str & "'"
                                    SSQL = SSQL & " And Compcode='" & iStr1(0) & "' And LocCode='" & iStr2(0) & "'"
                                    SSQL = SSQL & " And TimeIN >='" & Date_Value_Str & " " & "02:00' And TimeIN <='" & Date_Value_Str1 & " " & "02:00' Order by TimeIN ASC"
                                    mLocalDS = ReturnMultipleValue(SSQL)
                                    'TimeOUT Get
                                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" & Machine_ID_Str & "'"
                                    SSQL = SSQL & " And Compcode='" & iStr1(0) & "' And LocCode='" & iStr2(0) & "'"
                                    SSQL = SSQL & " And TimeOUT >='" & Date_Value_Str & " " & "10:00' And TimeOUT <='" & Date_Value_Str1 & " " & "10:00' Order by TimeOUT Asc"
                                    mLocalDS1 = ReturnMultipleValue(SSQL)
                                End If
                            Else
                                'Get Employee In Time
                                SSQL = "Select TimeIN from LogTime_IN where MachineID='" & Machine_ID_Str & "'"
                                SSQL = SSQL & " And Compcode='" & iStr1(0) & "' And LocCode='" & iStr2(0) & "'"
                                SSQL = SSQL & " And TimeIN >='" & Date_Value_Str & " " & "02:00' And TimeIN <='" & Date_Value_Str1 & " " & "02:00' Order by TimeIN ASC"
                                mLocalDS = ReturnMultipleValue(SSQL)

                                'TimeOUT Get
                                SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" & Machine_ID_Str & "'"
                                SSQL = SSQL & " And Compcode='" & iStr1(0) & "' And LocCode='" & iStr2(0) & "'"
                                SSQL = SSQL & " And TimeOUT >='" & Date_Value_Str & " " & "10:00' And TimeOUT <='" & Date_Value_Str1 & " " & "10:00' Order by TimeOUT Asc"
                                mLocalDS1 = ReturnMultipleValue(SSQL)
                            End If
                        Else
                            'Get Employee In Time
                            SSQL = "Select TimeIN from LogTime_IN where MachineID='" & Machine_ID_Str & "'"
                            SSQL = SSQL & " And Compcode='" & iStr1(0) & "' And LocCode='" & iStr2(0) & "'"
                            SSQL = SSQL & " And TimeIN >='" & Date_Value_Str & " " & "02:00' And TimeIN <='" & Date_Value_Str1 & " " & "02:00' Order by TimeIN ASC"
                            mLocalDS = ReturnMultipleValue(SSQL)

                            'TimeOUT Get
                            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" & Machine_ID_Str & "'"
                            SSQL = SSQL & " And Compcode='" & iStr1(0) & "' And LocCode='" & iStr2(0) & "'"
                            SSQL = SSQL & " And TimeOUT >='" & Date_Value_Str & " " & "10:00' And TimeOUT <='" & Date_Value_Str1 & " " & "10:00' Order by TimeOUT Asc"
                            mLocalDS1 = ReturnMultipleValue(SSQL)
                        End If
                    End If
                    'Shift Change Check End
                    If mLocalDS.Tables(0).Rows.Count = 0 Then
                        Employee_Punch = ""
                    Else
                        Employee_Punch = mLocalDS.Tables(0).Rows(0)(0)
                    End If

                    If mLocalDS.Tables(0).Rows.Count > 1 Then
                        For tin = 0 To mLocalDS.Tables(0).Rows.Count - 1
                            Time_IN_Str = mLocalDS.Tables(0).Rows(tin)(0)

                            If mLocalDS1.Tables(0).Rows.Count > tin Then
                                Time_Out_Str = mLocalDS1.Tables(0).Rows(tin)(0)
                            ElseIf mLocalDS1.Tables(0).Rows.Count > mLocalDS.Tables(0).Rows.Count Then
                                Time_Out_Str = mLocalDS1.Tables(0).Rows(mLocalDS1.Tables(0).Rows.Count - 1)(0)
                            Else
                                Time_Out_Str = ""
                            End If
                            Dim ts4 As New TimeSpan()
                            ts4 = Convert.ToDateTime(String.Format("{0:hh\:mm}", Emp_Total_Work_Time_1)).TimeOfDay()
                            If mLocalDS.Tables(0).Rows.Count <= 0 Then
                                Time_IN_Str = ""
                            Else
                                Time_IN_Str = mLocalDS.Tables(0).Rows(tin)(0)
                            End If
                            If Time_IN_Str = "" Or Time_Out_Str = "" Then
                                time_Check_dbl = time_Check_dbl
                            Else
                                Dim date1 As DateTime = System.Convert.ToDateTime(Time_IN_Str)
                                Dim date2 As DateTime = System.Convert.ToDateTime(Time_Out_Str)
                                'If date1 > date2 Then

                                '    date2 = System.Convert.ToDateTime(mLocalDS1.Tables(0).Rows(tin + 1)(0))
                                'End If
                                Dim ts As New TimeSpan()
                                ts = date2.Subtract(date1)
                                ts = date2.Subtract(date1)
                                Total_Time_get = Trim(ts.Hours) '& ":" & Trim(ts.Minutes)
                                ts4 = ts4.Add(ts)
                                'OT Time Get
                                Emp_Total_Work_Time_1 = Trim(ts4.Hours) & ":" & Trim(ts4.Minutes)
                                If Left_Val(Trim(ts4.Minutes), 1) = "-" Or Left_Val(Trim(ts4.Hours), 1) = "-" Or Emp_Total_Work_Time_1 = "0:-1" Then
                                    Emp_Total_Work_Time_1 = "00:00"
                                End If
                                If Left_Val(Total_Time_get, 1) = "-" Then
                                    date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1)
                                    ts = date2.Subtract(date1)
                                    ts = date2.Subtract(date1)
                                    Total_Time_get = Trim(ts.Hours) '& ":" & Trim(ts.Minutes)
                                    time_Check_dbl = Total_Time_get
                                    Emp_Total_Work_Time_1 = Trim(ts.Hours) & ":" & Trim(ts.Minutes)
                                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(":")
                                    If Left_Val(Time_Minus_Value_Check(0), 1) = "-" Or Left_Val(Time_Minus_Value_Check(1), 1) = "-" Then Emp_Total_Work_Time_1 = "00:00"
                                    If Left_Val(Emp_Total_Work_Time_1, 1) = "0:" Or Emp_Total_Work_Time_1 = "0:-1" Or Emp_Total_Work_Time_1 = "0:-3" Then Emp_Total_Work_Time_1 = "00:00"
                                Else
                                    time_Check_dbl = time_Check_dbl + Total_Time_get
                                End If
                            End If
                        Next
                    Else
                        Dim ts4 As New TimeSpan()
                        ts4 = Convert.ToDateTime(String.Format("{0:hh\:mm}", Emp_Total_Work_Time_1)).TimeOfDay()
                        If mLocalDS.Tables(0).Rows.Count <= 0 Then
                            Time_IN_Str = ""
                        Else
                            Time_IN_Str = mLocalDS.Tables(0).Rows(0)(0)
                        End If
                        For tout = 0 To mLocalDS1.Tables(0).Rows.Count - 1
                            If mLocalDS1.Tables(0).Rows.Count <= 0 Then
                                Time_Out_Str = ""
                            Else
                                Time_Out_Str = mLocalDS1.Tables(0).Rows(0)(0)
                            End If

                        Next
                        'Emp_Total_Work_Time
                        If Time_IN_Str = "" Or Time_Out_Str = "" Then
                            time_Check_dbl = 0
                        Else
                            Dim date1 As DateTime = System.Convert.ToDateTime(Time_IN_Str)
                            Dim date2 As DateTime = System.Convert.ToDateTime(Time_Out_Str)
                            Dim ts As New TimeSpan()
                            ts = date2.Subtract(date1)
                            ts = date2.Subtract(date1)
                            Total_Time_get = Trim(ts.Hours) '& ":" & Trim(ts.Minutes)
                            ts4 = ts4.Add(ts)
                            'OT Time Get
                            Emp_Total_Work_Time_1 = Trim(ts4.Hours) & ":" & Trim(ts4.Minutes)
                            Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(":")
                            If Left_Val(Time_Minus_Value_Check(0), 1) = "-" Or Left_Val(Time_Minus_Value_Check(1), 1) = "-" Then Emp_Total_Work_Time_1 = "00:00"
                            If Left_Val(Emp_Total_Work_Time_1, 1) = "0:" Or Emp_Total_Work_Time_1 = "0:-1" Or Emp_Total_Work_Time_1 = "0:-3" Then Emp_Total_Work_Time_1 = "00:00"
                            'MessageBox.Show(Emp_Total_Work_Time_1.ToString() + "")
                            If Left_Val(Total_Time_get, 1) = "-" Then
                                date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1)
                                ts = date2.Subtract(date1)
                                ts = date2.Subtract(date1)
                                Total_Time_get = Trim(ts.Hours) '& ":" & Trim(ts.Minutes)
                                time_Check_dbl = Total_Time_get
                                'Total_Time_get = Right_Val(Total_Time_get, Len(Total_Time_get) - 1)
                                Emp_Total_Work_Time_1 = Trim(ts.Hours) & ":" & Trim(ts.Minutes)
                                Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(":")
                                If Left_Val(Time_Minus_Value_Check(0), 1) = "-" Or Left_Val(Time_Minus_Value_Check(1), 1) = "-" Then Emp_Total_Work_Time_1 = "00:00"
                                If Left_Val(Emp_Total_Work_Time_1, 1) = "0:" Or Emp_Total_Work_Time_1 = "0:-1" Or Emp_Total_Work_Time_1 = "0:-3" Then Emp_Total_Work_Time_1 = "00:00"
                            Else
                                time_Check_dbl = Total_Time_get
                            End If
                        End If
                    End If


                    'Emp_Total_Work_Time
                    Dim Emp_Total_Work_Time As String = ""
                    Dim Final_OT_Work_Time As String = "00:00"
                    Dim Final_OT_Work_Time_Val As String = "00:00"
                    Emp_Total_Work_Time = Emp_Total_Work_Time_1
                    'Find Week OFF
                    Dim Employee_Week_Name As String = ""
                    Dim Assign_Week_Name As String = ""
                    mLocalDS = Nothing
                    Employee_Week_Name = Convert.ToDateTime(Date_Value_Str).DayOfWeek.ToString
                    SSQL = "Select WeekOff from Shiftrrecording where MachineNo='" & OT_Week_OFF_Machine_No & "'"
                    mLocalDS = ReturnMultipleValue(SSQL)
                    If mLocalDS.Tables(0).Rows.Count <= 0 Then
                        Assign_Week_Name = ""
                    Else
                        'Assign_Week_Name = mLocalDS.Tables(0).Rows(0)(0)
                        Assign_Week_Name = ""
                    End If

                    'NFH Days Check Start
                    Dim NFH_Day_Check As Boolean = False
                    Dim NFH_Date As Date
                    Dim DOJ_Date_Format As Date
                    Dim qry_nfh As String = "Select NFHDate from NFH_Mst where NFHDate=convert(varchar,'" & Date_Value_Str & "',103)"
                    mLocalDS = ReturnMultipleValue(qry_nfh)
                    'If mLocalDS.Tables(0).Rows.Count > 0 Then
                    '    'Date OF Joining Check Start
                    '    If DOJ_Date_Str <> "" Then
                    '        NFH_Date = mLocalDS.Tables(0).Rows(0)("NFHDate")
                    '        DOJ_Date_Format = DOJ_Date_Str
                    '        If DOJ_Date_Format.Date < NFH_Date.Date Then
                    '            'isPresent = True
                    '            NFH_Day_Check = True
                    '            NFH_Days_Count = NFH_Days_Count + 1
                    '        Else
                    '            'Skip
                    '        End If
                    '    Else
                    '        'isPresent = True
                    '        NFH_Day_Check = True
                    '        NFH_Days_Count = NFH_Days_Count + 1
                    '    End If
                    '    'Date OF Joining Check End
                    'End If
                    'NFH Days Check End


                    If UCase(Employee_Week_Name) = UCase(Assign_Week_Name) Then
                        'Skip
                    Else
                        'Get Employee Work Time
                        Dim Calculate_Attd_Work_Time As Double = 0
                        Dim Calculate_Attd_Work_Time_half As Double = 0
                        SSQL = "Select * from Employee_MST where MachineID='" & OT_Week_OFF_Machine_No & "' and Calculate_Work_Hours <> '' And CompCode='" & iStr1(0) & "' And LocCode='" & iStr2(0) & "' And IsActive='Yes'"
                        mLocalDS = ReturnMultipleValue(SSQL)
                        If mLocalDS.Tables(0).Rows.Count <= 0 Then
                            Calculate_Attd_Work_Time = 7
                        Else
                            Calculate_Attd_Work_Time = IIf(mLocalDS.Tables(0).Rows(0)("Calculate_Work_Hours") <> "", mLocalDS.Tables(0).Rows(0)("Calculate_Work_Hours"), 0)
                            If Calculate_Attd_Work_Time = 0 Then
                                Calculate_Attd_Work_Time = 7
                            Else
                                Calculate_Attd_Work_Time = 7
                            End If
                        End If
                        Calculate_Attd_Work_Time_half = 4.0
                        halfPresent = "0" 'Half Days Calculate Start
                        If time_Check_dbl >= Calculate_Attd_Work_Time_half And time_Check_dbl < Calculate_Attd_Work_Time Then
                            isPresent = True
                            halfPresent = "1"
                        End If
                        'Half Days Calculate End
                        If time_Check_dbl >= Calculate_Attd_Work_Time Then
                            isPresent = True
                            halfPresent = "0"
                        End If
                    End If

                    'Shift Check Code Start
                    Dim Shift_Ds As New DataSet
                    Dim Start_IN As String = ""
                    Dim End_In As String = ""
                    Dim Shift_Name_Store As String = ""
                    Dim Shift_Check_blb_Check As Boolean = False

                    Dim ShiftdateStartIN_Check As DateTime
                    Dim ShiftdateEndIN_Check As DateTime
                    Dim EmpdateIN_Check As DateTime
                    If isPresent = True Then
                        Shift_Check_blb_Check = False
                        If UCase(Emp_DS.Tables(0).Rows(intRow)("Wages").ToString()) = UCase("STAFF") Or UCase(Emp_DS.Tables(0).Rows(intRow)("Wages").ToString()) = UCase("Watch & Ward") Then
                            Employee_Shift_Name_DB = "GENERAL"
                        Else

                            'Shift Master Check Code Start
                            SSQL = "Select * from Shift_Mst where CompCode='" & iStr1(0) & "' And LocCode='" & iStr2(0) & "' And ShiftDesc <> 'GENERAL' Order by ShiftDesc Asc"
                            Shift_Ds = ReturnMultipleValue(SSQL)
                            If Shift_Ds.Tables(0).Rows.Count <> 0 Then
                                SSQL = "Select * from Shift_Mst Where CompCode='" & iStr1(0) & "' And LocCode='" & iStr2(0) & "' And ShiftDesc like '%SHIFT%'"
                                Shift_Ds = ReturnMultipleValue(SSQL)
                                Shift_Check_blb = False
                                For K = 0 To Shift_Ds.Tables(0).Rows.Count - 1
                                    Start_IN = Convert.ToDateTime(Date_Value_Str).AddDays(Shift_Ds.Tables(0).Rows(K)("StartIN_Days")).ToString("dd/MM/yyyy") & " " & Shift_Ds.Tables(0).Rows(K)("StartIN").ToString()
                                    End_In = Convert.ToDateTime(Date_Value_Str).AddDays(Shift_Ds.Tables(0).Rows(K)("EndIN_Days")).ToString("dd/MM/yyyy") & " " & Shift_Ds.Tables(0).Rows(K)("EndIN").ToString()

                                    ShiftdateStartIN_Check = System.Convert.ToDateTime(Start_IN)
                                    ShiftdateEndIN_Check = System.Convert.ToDateTime(End_In)
                                    EmpdateIN_Check = System.Convert.ToDateTime(Employee_Punch)
                                    If EmpdateIN_Check >= ShiftdateStartIN_Check And EmpdateIN_Check <= ShiftdateEndIN_Check Then
                                        Employee_Shift_Name_DB = Shift_Ds.Tables(0).Rows(K)("ShiftDesc").ToString()
                                        Shift_Check_blb_Check = True : Exit For
                                    End If
                                Next K
                                If Shift_Check_blb_Check = False Then
                                    Employee_Shift_Name_DB = "No Shift"
                                End If
                            Else
                                Employee_Shift_Name_DB = "No Shift"
                            End If
                            'Shift Master Check Code End

                        End If

                    Else
                        Shift_Name_Store = "No Shift"
                    End If

                    'Shift Check Code End

                    'Week OF Day Check
                    Dim Check_Week_Off_DOJ As Boolean = False
                    Dim Attn_Date_Day As String = DateTime.Parse(Date_Value_Str).DayOfWeek.ToString()
                    If UCase(Emp_WH_Day) = UCase(Attn_Date_Day) Then

                        'Check DOJ Date to Report Date
                        Dim Week_Off_Date As Date
                        Dim WH_DOJ_Date_Format As Date

                        If UCase(Trim(Emp_DS.Tables(0).Rows(intRow)("Wages"))) = UCase("STAFF") Or UCase(Trim(Emp_DS.Tables(0).Rows(intRow)("Wages"))) = UCase("Watch & Ward") Then
                            If DOJ_Date_Str <> "" Then
                                Week_Off_Date = Date_Value_Str 'Report Date
                                WH_DOJ_Date_Format = DOJ_Date_Str
                                If WH_DOJ_Date_Format.Date <= Week_Off_Date.Date Then
                                    Check_Week_Off_DOJ = True
                                Else
                                    Check_Week_Off_DOJ = False
                                End If
                            Else
                                Check_Week_Off_DOJ = True
                            End If
                        Else
                            Check_Week_Off_DOJ = True
                        End If

                        Month_WH_Count = Month_WH_Count + 1
                        If isPresent = True Then
                            If NFH_Day_Check = False Then
                                If halfPresent = "1" Then
                                    Present_WH_Count = Present_WH_Count + 0.5
                                ElseIf halfPresent = "0" Then
                                    Present_WH_Count = Present_WH_Count + 1
                                End If
                            End If
                        End If


                    End If

                    If isPresent = True Then
                        If halfPresent = "1" Then
                            Present_Count = Present_Count + 0.5
                        ElseIf halfPresent = "0" Then
                            Present_Count = Present_Count + 1
                        End If
                    Else
                        Appsent_Count = Appsent_Count + 1
                    End If
                    'colIndex += shiftCount
                    intK += 1

                    'Update Employee Worked_Days
                    SSQL = "Select * from LogTime_Days where MachineID='" & Get_Machine_ID & "' And CompCode='" & iStr1(0) & "' And LocCode='" & iStr2(0) & "' And Attn_Date_Str='" & Date_Value_Str & "'"
                    mDataSet = ReturnMultipleValue(SSQL)
                    If mDataSet.Tables(0).Rows.Count <> 0 Then
                        SSQL = "Delete from LogTime_Days where MachineID='" & Get_Machine_ID & "' And CompCode='" & iStr1(0) & "' And LocCode='" & iStr2(0) & "' And Attn_Date_Str='" & Date_Value_Str & "'"
                        InsertDeleteUpdate(SSQL)
                    End If
                    SSQL = "Insert Into LogTime_Days(CompCode,LocCode,MachineID,ExistingCode,FirstName,LastName,DeptName,"
                    SSQL = SSQL & "Designation,DOJ,Wages,Present,Wh_Check,Wh_Count,Wh_Present_Count,Shift,Total_Hrs,Attn_Date_Str,Attn_Date)"
                    SSQL = SSQL & " Values('" & iStr1(0) & "','" & iStr2(0) & "','" & Get_Machine_ID & "','" & Emp_DS.Tables(0).Rows(intRow)("ExistingCode") & "',"
                    SSQL = SSQL & "'" & Emp_DS.Tables(0).Rows(intRow)("FirstName") & "','" & Emp_DS.Tables(0).Rows(intRow)("LastName") & "',"
                    SSQL = SSQL & "'" & Emp_DS.Tables(0).Rows(intRow)("DeptName") & "','" & Emp_DS.Tables(0).Rows(intRow)("Designation") & "',"
                    SSQL = SSQL & "'" & Emp_DS.Tables(0).Rows(intRow)("DOJ") & "','" & Emp_DS.Tables(0).Rows(intRow)("Wages") & "','" & Present_Count & "','" & Check_Week_Off_DOJ & "',"
                    SSQL = SSQL & "'" & Month_WH_Count & "','" & Present_WH_Count & "','" & Employee_Shift_Name_DB & "','" & time_Check_dbl & "','" & Date_Value_Str & "','" & Date_Value_Str & "')"
                    InsertDeleteUpdate(SSQL)
                    Present_Count = 0
                    Check_Week_Off_DOJ = False
                    Month_WH_Count = 0
                    Present_WH_Count = 0
                    Employee_Shift_Name_DB = "No Shift"
                    'Employee_Shift_Name_DB
                    'Final_Count = Present_Count
                    'Month_WH_Count
                    'Present_WH_Count
                    'time_Check_dbl
                Next
                'Final_Count = Present_Count
                'Month_WH_Count
                'Present_WH_Count

                intI += 1
                Total_Days_Count = 0
                Final_Count = 0
                Appsent_Count = 0
                Present_Count = 0
                Month_WH_Count = 0
                Present_WH_Count = 0
                NFH_Days_Count = 0
                NFH_Days_Present_Count = 0
            Next

            Exit Sub

        Catch ex As Exception
            Show_Message(ex.Message)
        End Try

    End Sub

    Private Sub GroupBox1_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupBox1.Enter

    End Sub

    Private Sub frmTimeDelete_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormAlign(Me)
        Fill_Company_Details()
        cmbCompCode.SelectedIndex = 0
        cmbCompCode.Enabled = False
        cmbCompCode_LostFocus(sender, e)
        If Trim(mUserLocation) <> "" Then
            cmbLocCode.SelectedIndex = cmbLocCode.FindString(Trim(mUserLocation))
            cmbLocCode.Enabled = False
            Call cmbLocCode_LostFocus(sender, e)
        End If

        Call IN_Grid_Col_Align()
        Call OUT_Grid_Col_Align()
    End Sub
End Class