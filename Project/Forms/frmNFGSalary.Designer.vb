﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmNFGSalary
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.lblBalAmtResult = New System.Windows.Forms.Label
        Me.lblBalCoverResult = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.lblBalAmt = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.lblBalCover = New System.Windows.Forms.Label
        Me.lblTotalAmtResult = New System.Windows.Forms.Label
        Me.lblTotalPay = New System.Windows.Forms.Label
        Me.lblTotalCoverResult = New System.Windows.Forms.Label
        Me.lblTotalCover = New System.Windows.Forms.Label
        Me.lblRupees = New System.Windows.Forms.Label
        Me.lblNetAmt = New System.Windows.Forms.Label
        Me.lblWagesType = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtToDate = New System.Windows.Forms.DateTimePicker
        Me.txtFromDate = New System.Windows.Forms.DateTimePicker
        Me.Label23 = New System.Windows.Forms.Label
        Me.Label24 = New System.Windows.Forms.Label
        Me.cmbFinYear = New System.Windows.Forms.ComboBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.cmbMonth = New System.Windows.Forms.ComboBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.lblDesg = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.lblDept = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.lblEmpName = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.lblMachineID = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.lblTokenNo = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.pbEmployee = New System.Windows.Forms.PictureBox
        Me.cmbIPAddress = New System.Windows.Forms.ComboBox
        Me.cmbLocCode = New System.Windows.Forms.ComboBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.cmbCompCode = New System.Windows.Forms.ComboBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.btnReport = New System.Windows.Forms.Button
        Me.btnupload = New System.Windows.Forms.Button
        Me.BtnClear = New System.Windows.Forms.Button
        Me.btnExit = New System.Windows.Forms.Button
        Me.btnConnect = New System.Windows.Forms.Button
        Me.dgvOT = New System.Windows.Forms.DataGridView
        Me.opdOT = New System.Windows.Forms.OpenFileDialog
        Me.btnGet = New System.Windows.Forms.Button
        Me.GroupBox1.SuspendLayout()
        CType(Me.pbEmployee, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvOT, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.GroupBox1.Controls.Add(Me.lblBalAmtResult)
        Me.GroupBox1.Controls.Add(Me.lblBalCoverResult)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.lblBalAmt)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.lblBalCover)
        Me.GroupBox1.Controls.Add(Me.lblTotalAmtResult)
        Me.GroupBox1.Controls.Add(Me.lblTotalPay)
        Me.GroupBox1.Controls.Add(Me.lblTotalCoverResult)
        Me.GroupBox1.Controls.Add(Me.lblTotalCover)
        Me.GroupBox1.Controls.Add(Me.lblRupees)
        Me.GroupBox1.Controls.Add(Me.lblNetAmt)
        Me.GroupBox1.Controls.Add(Me.lblWagesType)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.txtToDate)
        Me.GroupBox1.Controls.Add(Me.txtFromDate)
        Me.GroupBox1.Controls.Add(Me.Label23)
        Me.GroupBox1.Controls.Add(Me.Label24)
        Me.GroupBox1.Controls.Add(Me.cmbFinYear)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.cmbMonth)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.lblDesg)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.lblDept)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.lblEmpName)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.lblMachineID)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.lblTokenNo)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.pbEmployee)
        Me.GroupBox1.Controls.Add(Me.cmbIPAddress)
        Me.GroupBox1.Controls.Add(Me.cmbLocCode)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.cmbCompCode)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Location = New System.Drawing.Point(-1, -1)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(814, 405)
        Me.GroupBox1.TabIndex = 28
        Me.GroupBox1.TabStop = False
        '
        'lblBalAmtResult
        '
        Me.lblBalAmtResult.AutoSize = True
        Me.lblBalAmtResult.BackColor = System.Drawing.Color.Transparent
        Me.lblBalAmtResult.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBalAmtResult.ForeColor = System.Drawing.Color.White
        Me.lblBalAmtResult.Location = New System.Drawing.Point(421, 369)
        Me.lblBalAmtResult.Name = "lblBalAmtResult"
        Me.lblBalAmtResult.Size = New System.Drawing.Size(0, 16)
        Me.lblBalAmtResult.TabIndex = 97
        '
        'lblBalCoverResult
        '
        Me.lblBalCoverResult.AutoSize = True
        Me.lblBalCoverResult.BackColor = System.Drawing.Color.Transparent
        Me.lblBalCoverResult.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBalCoverResult.ForeColor = System.Drawing.Color.White
        Me.lblBalCoverResult.Location = New System.Drawing.Point(421, 338)
        Me.lblBalCoverResult.Name = "lblBalCoverResult"
        Me.lblBalCoverResult.Size = New System.Drawing.Size(0, 16)
        Me.lblBalCoverResult.TabIndex = 96
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.BackColor = System.Drawing.Color.Transparent
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.White
        Me.Label9.Location = New System.Drawing.Point(415, 369)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(0, 16)
        Me.Label9.TabIndex = 95
        '
        'lblBalAmt
        '
        Me.lblBalAmt.AutoSize = True
        Me.lblBalAmt.BackColor = System.Drawing.Color.Transparent
        Me.lblBalAmt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBalAmt.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.lblBalAmt.Location = New System.Drawing.Point(291, 369)
        Me.lblBalAmt.Name = "lblBalAmt"
        Me.lblBalAmt.Size = New System.Drawing.Size(120, 16)
        Me.lblBalAmt.TabIndex = 94
        Me.lblBalAmt.Text = "Balance Amout :"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.BackColor = System.Drawing.Color.Transparent
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.White
        Me.Label14.Location = New System.Drawing.Point(415, 338)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(0, 16)
        Me.Label14.TabIndex = 93
        '
        'lblBalCover
        '
        Me.lblBalCover.AutoSize = True
        Me.lblBalCover.BackColor = System.Drawing.Color.Transparent
        Me.lblBalCover.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBalCover.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.lblBalCover.Location = New System.Drawing.Point(291, 338)
        Me.lblBalCover.Name = "lblBalCover"
        Me.lblBalCover.Size = New System.Drawing.Size(134, 16)
        Me.lblBalCover.TabIndex = 92
        Me.lblBalCover.Text = "Balance Persons :"
        '
        'lblTotalAmtResult
        '
        Me.lblTotalAmtResult.AutoSize = True
        Me.lblTotalAmtResult.BackColor = System.Drawing.Color.Transparent
        Me.lblTotalAmtResult.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalAmtResult.ForeColor = System.Drawing.Color.White
        Me.lblTotalAmtResult.Location = New System.Drawing.Point(119, 369)
        Me.lblTotalAmtResult.Name = "lblTotalAmtResult"
        Me.lblTotalAmtResult.Size = New System.Drawing.Size(0, 16)
        Me.lblTotalAmtResult.TabIndex = 87
        '
        'lblTotalPay
        '
        Me.lblTotalPay.AutoSize = True
        Me.lblTotalPay.BackColor = System.Drawing.Color.Transparent
        Me.lblTotalPay.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalPay.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.lblTotalPay.Location = New System.Drawing.Point(6, 369)
        Me.lblTotalPay.Name = "lblTotalPay"
        Me.lblTotalPay.Size = New System.Drawing.Size(107, 16)
        Me.lblTotalPay.TabIndex = 86
        Me.lblTotalPay.Text = "Total Amount :"
        '
        'lblTotalCoverResult
        '
        Me.lblTotalCoverResult.AutoSize = True
        Me.lblTotalCoverResult.BackColor = System.Drawing.Color.Transparent
        Me.lblTotalCoverResult.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalCoverResult.ForeColor = System.Drawing.Color.White
        Me.lblTotalCoverResult.Location = New System.Drawing.Point(119, 338)
        Me.lblTotalCoverResult.Name = "lblTotalCoverResult"
        Me.lblTotalCoverResult.Size = New System.Drawing.Size(0, 16)
        Me.lblTotalCoverResult.TabIndex = 85
        '
        'lblTotalCover
        '
        Me.lblTotalCover.AutoSize = True
        Me.lblTotalCover.BackColor = System.Drawing.Color.Transparent
        Me.lblTotalCover.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalCover.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.lblTotalCover.Location = New System.Drawing.Point(6, 338)
        Me.lblTotalCover.Name = "lblTotalCover"
        Me.lblTotalCover.Size = New System.Drawing.Size(113, 16)
        Me.lblTotalCover.TabIndex = 84
        Me.lblTotalCover.Text = "Total Persons :"
        '
        'lblRupees
        '
        Me.lblRupees.BackColor = System.Drawing.Color.Transparent
        Me.lblRupees.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblRupees.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRupees.ForeColor = System.Drawing.Color.Red
        Me.lblRupees.Location = New System.Drawing.Point(-250, 406)
        Me.lblRupees.Name = "lblRupees"
        Me.lblRupees.Size = New System.Drawing.Size(802, 10)
        Me.lblRupees.TabIndex = 83
        Me.lblRupees.Text = "RUPEES :"
        Me.lblRupees.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblNetAmt
        '
        Me.lblNetAmt.BackColor = System.Drawing.Color.Transparent
        Me.lblNetAmt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNetAmt.Font = New System.Drawing.Font("Arial", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNetAmt.ForeColor = System.Drawing.Color.Red
        Me.lblNetAmt.Location = New System.Drawing.Point(9, 241)
        Me.lblNetAmt.Name = "lblNetAmt"
        Me.lblNetAmt.Size = New System.Drawing.Size(496, 75)
        Me.lblNetAmt.TabIndex = 82
        Me.lblNetAmt.Text = "NFH AMOUNT : 0.00"
        Me.lblNetAmt.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblWagesType
        '
        Me.lblWagesType.BackColor = System.Drawing.Color.Transparent
        Me.lblWagesType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblWagesType.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWagesType.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.lblWagesType.Location = New System.Drawing.Point(126, 216)
        Me.lblWagesType.Name = "lblWagesType"
        Me.lblWagesType.Size = New System.Drawing.Size(104, 22)
        Me.lblWagesType.TabIndex = 80
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label7.Location = New System.Drawing.Point(9, 217)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(111, 21)
        Me.Label7.TabIndex = 79
        Me.Label7.Text = "Wages Type  :"
        '
        'txtToDate
        '
        Me.txtToDate.CustomFormat = "dd/MM/yyyy"
        Me.txtToDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.txtToDate.Location = New System.Drawing.Point(365, 134)
        Me.txtToDate.Name = "txtToDate"
        Me.txtToDate.Size = New System.Drawing.Size(140, 22)
        Me.txtToDate.TabIndex = 76
        Me.txtToDate.Visible = False
        '
        'txtFromDate
        '
        Me.txtFromDate.CustomFormat = "dd/MM/yyyy"
        Me.txtFromDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.txtFromDate.Location = New System.Drawing.Point(126, 134)
        Me.txtFromDate.Name = "txtFromDate"
        Me.txtFromDate.Size = New System.Drawing.Size(159, 22)
        Me.txtFromDate.TabIndex = 75
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.BackColor = System.Drawing.Color.Transparent
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label23.Location = New System.Drawing.Point(291, 136)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(64, 16)
        Me.Label23.TabIndex = 78
        Me.Label23.Text = "To Date"
        Me.Label23.Visible = False
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.BackColor = System.Drawing.Color.Transparent
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label24.Location = New System.Drawing.Point(6, 136)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(76, 16)
        Me.Label24.TabIndex = 77
        Me.Label24.Text = "NFH Date"
        '
        'cmbFinYear
        '
        Me.cmbFinYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbFinYear.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbFinYear.FormattingEnabled = True
        Me.cmbFinYear.Location = New System.Drawing.Point(365, 104)
        Me.cmbFinYear.Name = "cmbFinYear"
        Me.cmbFinYear.Size = New System.Drawing.Size(140, 24)
        Me.cmbFinYear.TabIndex = 42
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label4.Location = New System.Drawing.Point(291, 107)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(70, 16)
        Me.Label4.TabIndex = 44
        Me.Label4.Text = "Fin. Year"
        '
        'cmbMonth
        '
        Me.cmbMonth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbMonth.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbMonth.FormattingEnabled = True
        Me.cmbMonth.Items.AddRange(New Object() {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"})
        Me.cmbMonth.Location = New System.Drawing.Point(126, 102)
        Me.cmbMonth.Name = "cmbMonth"
        Me.cmbMonth.Size = New System.Drawing.Size(159, 24)
        Me.cmbMonth.TabIndex = 41
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label6.Location = New System.Drawing.Point(6, 105)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(49, 16)
        Me.Label6.TabIndex = 43
        Me.Label6.Text = "Month"
        '
        'lblDesg
        '
        Me.lblDesg.BackColor = System.Drawing.Color.Transparent
        Me.lblDesg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblDesg.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDesg.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.lblDesg.Location = New System.Drawing.Point(340, 216)
        Me.lblDesg.Name = "lblDesg"
        Me.lblDesg.Size = New System.Drawing.Size(165, 22)
        Me.lblDesg.TabIndex = 25
        '
        'Label13
        '
        Me.Label13.BackColor = System.Drawing.Color.Transparent
        Me.Label13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label13.Location = New System.Drawing.Point(234, 216)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(102, 22)
        Me.Label13.TabIndex = 24
        Me.Label13.Text = "Desg     :"
        '
        'lblDept
        '
        Me.lblDept.BackColor = System.Drawing.Color.Transparent
        Me.lblDept.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblDept.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDept.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.lblDept.Location = New System.Drawing.Point(340, 192)
        Me.lblDept.Name = "lblDept"
        Me.lblDept.Size = New System.Drawing.Size(165, 21)
        Me.lblDept.TabIndex = 23
        '
        'Label8
        '
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        Me.Label8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label8.Location = New System.Drawing.Point(234, 192)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(102, 21)
        Me.Label8.TabIndex = 22
        Me.Label8.Text = "Dept      :"
        '
        'lblEmpName
        '
        Me.lblEmpName.BackColor = System.Drawing.Color.Transparent
        Me.lblEmpName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblEmpName.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpName.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.lblEmpName.Location = New System.Drawing.Point(340, 165)
        Me.lblEmpName.Name = "lblEmpName"
        Me.lblEmpName.Size = New System.Drawing.Size(165, 22)
        Me.lblEmpName.TabIndex = 21
        '
        'Label10
        '
        Me.Label10.BackColor = System.Drawing.Color.Transparent
        Me.Label10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label10.Location = New System.Drawing.Point(234, 165)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(102, 22)
        Me.Label10.TabIndex = 20
        Me.Label10.Text = "Emp. Name  : "
        '
        'lblMachineID
        '
        Me.lblMachineID.BackColor = System.Drawing.Color.Transparent
        Me.lblMachineID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblMachineID.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMachineID.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.lblMachineID.Location = New System.Drawing.Point(126, 192)
        Me.lblMachineID.Name = "lblMachineID"
        Me.lblMachineID.Size = New System.Drawing.Size(104, 21)
        Me.lblMachineID.TabIndex = 19
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label5.Location = New System.Drawing.Point(9, 192)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(111, 21)
        Me.Label5.TabIndex = 18
        Me.Label5.Text = "MG. No : "
        '
        'lblTokenNo
        '
        Me.lblTokenNo.BackColor = System.Drawing.Color.Transparent
        Me.lblTokenNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblTokenNo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTokenNo.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.lblTokenNo.Location = New System.Drawing.Point(126, 165)
        Me.lblTokenNo.Name = "lblTokenNo"
        Me.lblTokenNo.Size = New System.Drawing.Size(104, 22)
        Me.lblTokenNo.TabIndex = 17
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label3.Location = New System.Drawing.Point(9, 165)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(111, 22)
        Me.Label3.TabIndex = 16
        Me.Label3.Text = "Token No       : "
        '
        'pbEmployee
        '
        Me.pbEmployee.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.pbEmployee.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pbEmployee.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pbEmployee.Location = New System.Drawing.Point(508, 11)
        Me.pbEmployee.Name = "pbEmployee"
        Me.pbEmployee.Size = New System.Drawing.Size(296, 305)
        Me.pbEmployee.TabIndex = 15
        Me.pbEmployee.TabStop = False
        '
        'cmbIPAddress
        '
        Me.cmbIPAddress.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbIPAddress.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbIPAddress.FormattingEnabled = True
        Me.cmbIPAddress.Location = New System.Drawing.Point(126, 71)
        Me.cmbIPAddress.Name = "cmbIPAddress"
        Me.cmbIPAddress.Size = New System.Drawing.Size(379, 24)
        Me.cmbIPAddress.TabIndex = 2
        '
        'cmbLocCode
        '
        Me.cmbLocCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbLocCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbLocCode.FormattingEnabled = True
        Me.cmbLocCode.Location = New System.Drawing.Point(126, 41)
        Me.cmbLocCode.Name = "cmbLocCode"
        Me.cmbLocCode.Size = New System.Drawing.Size(379, 24)
        Me.cmbLocCode.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label2.Location = New System.Drawing.Point(6, 75)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(84, 16)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "IP Address"
        '
        'cmbCompCode
        '
        Me.cmbCompCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbCompCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbCompCode.FormattingEnabled = True
        Me.cmbCompCode.Location = New System.Drawing.Point(126, 11)
        Me.cmbCompCode.Name = "cmbCompCode"
        Me.cmbCompCode.Size = New System.Drawing.Size(379, 24)
        Me.cmbCompCode.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label1.Location = New System.Drawing.Point(6, 45)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(108, 16)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Location Code"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.BackColor = System.Drawing.Color.Transparent
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label11.Location = New System.Drawing.Point(6, 15)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(114, 16)
        Me.Label11.TabIndex = 4
        Me.Label11.Text = "Company Code"
        '
        'btnReport
        '
        Me.btnReport.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReport.Location = New System.Drawing.Point(538, 410)
        Me.btnReport.Name = "btnReport"
        Me.btnReport.Size = New System.Drawing.Size(102, 30)
        Me.btnReport.TabIndex = 99
        Me.btnReport.Text = "R&eport"
        Me.btnReport.UseVisualStyleBackColor = True
        '
        'btnupload
        '
        Me.btnupload.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnupload.Location = New System.Drawing.Point(430, 410)
        Me.btnupload.Name = "btnupload"
        Me.btnupload.Size = New System.Drawing.Size(102, 30)
        Me.btnupload.TabIndex = 98
        Me.btnupload.Text = "&Upload "
        Me.btnupload.UseVisualStyleBackColor = True
        '
        'BtnClear
        '
        Me.BtnClear.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnClear.Location = New System.Drawing.Point(217, 410)
        Me.BtnClear.Name = "BtnClear"
        Me.BtnClear.Size = New System.Drawing.Size(102, 30)
        Me.BtnClear.TabIndex = 97
        Me.BtnClear.Text = "&Clear"
        Me.BtnClear.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExit.Location = New System.Drawing.Point(322, 410)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(102, 30)
        Me.btnExit.TabIndex = 96
        Me.btnExit.Text = "E&xit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnConnect
        '
        Me.btnConnect.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnConnect.Location = New System.Drawing.Point(103, 410)
        Me.btnConnect.Name = "btnConnect"
        Me.btnConnect.Size = New System.Drawing.Size(112, 30)
        Me.btnConnect.TabIndex = 95
        Me.btnConnect.Text = "&Connect"
        Me.btnConnect.UseVisualStyleBackColor = True
        '
        'dgvOT
        '
        Me.dgvOT.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvOT.Location = New System.Drawing.Point(205, 457)
        Me.dgvOT.Name = "dgvOT"
        Me.dgvOT.Size = New System.Drawing.Size(240, 150)
        Me.dgvOT.TabIndex = 100
        '
        'opdOT
        '
        Me.opdOT.FileName = "OpenFileDialog1"
        '
        'btnGet
        '
        Me.btnGet.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGet.Location = New System.Drawing.Point(646, 410)
        Me.btnGet.Name = "btnGet"
        Me.btnGet.Size = New System.Drawing.Size(102, 30)
        Me.btnGet.TabIndex = 101
        Me.btnGet.Text = "Get Details"
        Me.btnGet.UseVisualStyleBackColor = True
        '
        'frmNFGSalary
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(805, 441)
        Me.Controls.Add(Me.btnGet)
        Me.Controls.Add(Me.dgvOT)
        Me.Controls.Add(Me.btnReport)
        Me.Controls.Add(Me.btnupload)
        Me.Controls.Add(Me.BtnClear)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnConnect)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "frmNFGSalary"
        Me.Text = "frmNFGSalary"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.pbEmployee, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvOT, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lblBalAmtResult As System.Windows.Forms.Label
    Friend WithEvents lblBalCoverResult As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents lblBalAmt As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents lblBalCover As System.Windows.Forms.Label
    Friend WithEvents lblTotalAmtResult As System.Windows.Forms.Label
    Friend WithEvents lblTotalPay As System.Windows.Forms.Label
    Friend WithEvents lblTotalCoverResult As System.Windows.Forms.Label
    Friend WithEvents lblTotalCover As System.Windows.Forms.Label
    Friend WithEvents lblRupees As System.Windows.Forms.Label
    Friend WithEvents lblNetAmt As System.Windows.Forms.Label
    Friend WithEvents lblWagesType As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtToDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtFromDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents cmbFinYear As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cmbMonth As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents lblDesg As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents lblDept As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents lblEmpName As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents lblMachineID As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lblTokenNo As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents pbEmployee As System.Windows.Forms.PictureBox
    Friend WithEvents cmbIPAddress As System.Windows.Forms.ComboBox
    Friend WithEvents cmbLocCode As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cmbCompCode As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents btnReport As System.Windows.Forms.Button
    Friend WithEvents btnupload As System.Windows.Forms.Button
    Friend WithEvents BtnClear As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnConnect As System.Windows.Forms.Button
    Friend WithEvents dgvOT As System.Windows.Forms.DataGridView
    Friend WithEvents opdOT As System.Windows.Forms.OpenFileDialog
    Friend WithEvents btnGet As System.Windows.Forms.Button
End Class
