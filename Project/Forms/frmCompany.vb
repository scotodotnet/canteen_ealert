﻿#Region "Imports"
Imports System
Imports System.Data
Imports System.Data.OleDb
#End Region
Public Class frmCompany
    Dim mStatus As Long
    Dim mDataSet As New DataSet
    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        Call Clear_Screen_Details()
    End Sub
    Public Sub Clear_Screen_Details()
        txtCompCode.Text = ""
        txtCompName.Text = ""
        txtAdd1.Text = ""
        txtAdd2.Text = ""
        txtCity.Text = ""
        txtPincode.Text = ""
        txtState.Text = ""
        txtCountry.Text = ""
        txtUserDef1.Text = ""
        txtUserDef2.Text = ""
        txtUserDef3.Text = ""
        txtCompCode.Focus()
    End Sub
    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Close()
    End Sub
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If Trim(txtCompCode.Text) = "" Then
                MessageBox.Show("Please Enter Company Code.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
                txtCompCode.Focus()
                Exit Sub
            End If
            If Trim(txtCompName.Text) = "" Then
                MessageBox.Show("Please Enter Company Name.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
                txtCompName.Focus()
                Exit Sub
            End If

            SSQL = ""
            SSQL = "Delete from Company_Mst Where CompCode='" & UCase(Remove_Single_Quote(Trim(txtCompCode.Text))) & "'"
            mStatus = InsertDeleteUpdate(SSQL)

            If mStatus >= 0 Then
                SSQL = ""
                SSQL = "Insert into Company_Mst values( "
                SSQL = SSQL & "'" & UCase(Remove_Single_Quote(Trim(txtCompCode.Text))) & "',"
                SSQL = SSQL & "'" & UCase(Remove_Single_Quote(Trim(txtCompName.Text))) & "',"
                SSQL = SSQL & "'" & UCase(Remove_Single_Quote(Trim(txtAdd1.Text))) & "',"
                SSQL = SSQL & "'" & UCase(Remove_Single_Quote(Trim(txtAdd2.Text))) & "',"
                SSQL = SSQL & "'" & UCase(Remove_Single_Quote(Trim(txtCity.Text))) & "',"
                SSQL = SSQL & "'" & UCase(Remove_Single_Quote(Trim(txtPincode.Text))) & "',"
                SSQL = SSQL & "'" & UCase(Remove_Single_Quote(Trim(txtState.Text))) & "',"
                SSQL = SSQL & "'" & UCase(Remove_Single_Quote(Trim(txtCountry.Text))) & "',"
                SSQL = SSQL & "'" & UCase(Remove_Single_Quote(Trim(txtUserDef1.Text))) & "',"
                SSQL = SSQL & "'" & UCase(Remove_Single_Quote(Trim(txtUserDef2.Text))) & "',"
                SSQL = SSQL & "'" & UCase(Remove_Single_Quote(Trim(txtUserDef3.Text))) & "')"
                mStatus = InsertDeleteUpdate(SSQL)
                If mStatus > 0 Then
                    MessageBox.Show("Successfully Saved.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Call Clear_Screen_Details()
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Altius")
        End Try
    End Sub
    Private Sub txtCompCode_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCompCode.LostFocus
        If Trim(txtCompCode.Text) = "" Then Exit Sub
        SSQL = "Select * from Company_Mst Where CompCode='" & UCase(Remove_Single_Quote(Trim(txtCompCode.Text))) & "'"
        mDataSet = New DataSet
        mDataSet = ReturnMultipleValue(SSQL)
        With mDataSet.Tables(0)
            If (.Rows.Count) > 0 Then
                txtCompName.Text = .Rows(0)("CompName")
                txtAdd1.Text = .Rows(0)("Add1")
                txtAdd2.Text = .Rows(0)("Add2")
                txtCity.Text = .Rows(0)("City")
                txtPincode.Text = .Rows(0)("PinCode")
                txtState.Text = .Rows(0)("State")
                txtCountry.Text = .Rows(0)("country")
                txtUserDef1.Text = .Rows(0)("UserDef1")
                txtUserDef2.Text = .Rows(0)("UserDef2")
                txtUserDef3.Text = .Rows(0)("UserDef3")
                txtCompName.Focus()
                Exit Sub
            End If
        End With
    End Sub
    Private Sub frmCompany_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormAlign(Me)
    End Sub
End Class