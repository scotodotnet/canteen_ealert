﻿Imports System
Imports System.Data
Imports System.Data.OleDb

Public Class frmFingerPrintDownload

    Dim Download_Machine_IP() As String
    Dim Upload_Machine_IP() As String
    Private bIsConnected = False
    Private iMachineNumber As Integer
    Public axCZKEM1 As New zkemkeeper.CZKEM

    Private Sub frmFingerPrintDownload_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormAlign(Me)
        Fill_Company_Details()
        cmbCompCode.SelectedIndex = 0
        cmbCompCode.Enabled = False
        cmbCompCode_LostFocus(sender, e)
    End Sub
    Public Sub Fill_Company_Details()
        SSQL = ""
        SSQL = "Select CompCode + ' | ' + CompName as [Name] from Company_Mst Order By CompName"

        mDataSet = ReturnMultipleValue(SSQL)
        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                cmbCompCode.Items.Clear()
                For iRow = 0 To .Rows.Count - 1
                    cmbCompCode.Items.Add(.Rows(iRow)(0))
                Next
            End If
        End With
    End Sub

    Private Sub cmbCompCode_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCompCode.LostFocus
        If Trim(cmbCompCode.Text) = "" Then Exit Sub
        Dim iStr() As String
        iStr = Split(Trim(cmbCompCode.Text), " | ")

        SSQL = ""
        SSQL = "Select LocCode + ' | ' + LocName as [Name] from Location_Mst Where Compcode='"
        SSQL = SSQL & "" & iStr(0) & "'"

        mDataSet = ReturnMultipleValue(SSQL)

        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                cmbLocCode.Items.Clear()
                For iRow = 0 To .Rows.Count - 1
                    cmbLocCode.Items.Add(.Rows(iRow)(0))
                Next
            End If
        End With
    End Sub

    Private Sub cmbCompCode_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbCompCode.SelectedIndexChanged

    End Sub

    Private Sub cmbLocCode_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbLocCode.LostFocus
        If Trim(cmbCompCode.Text) = "" Then Exit Sub
        If Trim(cmbLocCode.Text) = "" Then Exit Sub

        Dim iStr1() As String
        Dim iStr2() As String

        iStr1 = Split(Trim(cmbCompCode.Text), " | ")
        iStr2 = Split(Trim(cmbLocCode.Text), " | ")

        SSQL = ""
        SSQL = "Select IPAddress + ' | ' + IPMode as [Name] from IPAddress_Mst Where Compcode='"
        SSQL = SSQL & "" & iStr1(0) & "'"
        SSQL = SSQL & " And LocCode='" & iStr2(0) & "'"

        mDataSet = ReturnMultipleValue(SSQL)

        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                txtDownload_Machine.Items.Clear()
                txtUpload_Machine.Items.Clear()
                For iRow = 0 To .Rows.Count - 1
                    txtDownload_Machine.Items.Add(.Rows(iRow)(0))
                    txtUpload_Machine.Items.Add(.Rows(iRow)(0))
                Next
            End If
        End With
    End Sub

    Private Sub cmbLocCode_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbLocCode.SelectedIndexChanged

    End Sub

    Private Sub btnDownloadTmp9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDownloadTmp9.Click
        Dim idwErrorCode As Integer

        If Trim(txtDownload_Machine.Text) = "" Then
            MsgBox("Please Select Download Machine IP Address", MsgBoxStyle.Critical)
            txtDownload_Machine.Focus()
            Exit Sub
        End If
        'Check IP SAME Or Not
        If UCase(Trim(txtDownload_Machine.Text)) = UCase(Trim(txtUpload_Machine.Text)) Then
            MsgBox("Download Machine And Upload Machine IP SAME. You have to Select Different IP Address", MsgBoxStyle.Critical)
            txtUpload_Machine.Focus()
            Exit Sub
        End If

        Cursor = Cursors.WaitCursor

        'Connect Download Machine
        Download_Machine_IP = Split(txtDownload_Machine.Text, " | ")
        bIsConnected = axCZKEM1.Connect_Net(Trim(Download_Machine_IP(0)), Convert.ToInt32(4370))
        If bIsConnected = True Then
            iMachineNumber = 1 'In fact,when you are using the tcp/ip communication,this parameter will be ignored,that is any integer will all right.Here we use 1.
            axCZKEM1.RegEvent(iMachineNumber, 65535) 'Here you can register the realtime events that you want to be triggered(the parameters 65535 means registering all)
        Else
            axCZKEM1.GetLastError(idwErrorCode)
            MsgBox("Unable to connect the device,ErrorCode=" & idwErrorCode, MsgBoxStyle.Exclamation, "Error")
        End If
        Cursor = Cursors.Default
        'Download
        If bIsConnected = False Then
            MsgBox("Please connect the device first", MsgBoxStyle.Exclamation, "Error")
            Return
        End If

        Cursor = Cursors.WaitCursor

        Dim sdwEnrollNumber As String = ""
        Dim sName As String = ""
        Dim sPassword As String = ""
        Dim iPrivilege As Integer
        Dim bEnabled As Boolean = False
        Dim idwFingerIndex As Integer
        Dim sTmpData As String = ""
        Dim iTmpLength As Integer
        Dim iFlag As Integer

        Dim lvItem As New ListViewItem("Items", 0)

        lvDownload.Items.Clear()

        gvdisplay.Rows.Clear() 'Clear Gird

        lvDownload.BeginUpdate()
        axCZKEM1.EnableDevice(iMachineNumber, False)

        Cursor = Cursors.WaitCursor
        axCZKEM1.ReadAllUserID(iMachineNumber) 'read all the user information to the memory
        axCZKEM1.ReadAllTemplate(iMachineNumber) 'read all the users' fingerprint templates to the memory

        With gvdisplay
            While axCZKEM1.SSR_GetAllUserInfo(iMachineNumber, sdwEnrollNumber, sName, sPassword, iPrivilege, bEnabled) = True  'get all the users' information from the memory
                For idwFingerIndex = 0 To 9


                    If axCZKEM1.GetUserTmpExStr(iMachineNumber, sdwEnrollNumber, idwFingerIndex, iFlag, sTmpData, iTmpLength) Then 'get the corresponding templates string and length from the memory
                        lvItem = lvDownload.Items.Add(sdwEnrollNumber.ToString())
                        lvItem.SubItems.Add(sName)
                        lvItem.SubItems.Add(idwFingerIndex.ToString())
                        lvItem.SubItems.Add(sTmpData)
                        lvItem.SubItems.Add(iPrivilege.ToString())
                        lvItem.SubItems.Add(sPassword)
                        If bEnabled = True Then
                            lvItem.SubItems.Add("true")
                        Else
                            lvItem.SubItems.Add("false")
                        End If
                        lvItem.SubItems.Add(iFlag.ToString())
                        .Rows.Add()
                        .Rows(.Rows.Count - 1).Cells(0).Value = sdwEnrollNumber
                        .Rows(.Rows.Count - 1).Cells(1).Value = False
                        '.Rows(.Rows.Count - 1).Cells(2).Value = False


                    End If
                    'End With
                Next
            End While
        End With

        lvDownload.EndUpdate()
        axCZKEM1.EnableDevice(iMachineNumber, True)

        'Disconnected Server
        axCZKEM1.Disconnect()
        bIsConnected = False
        Cursor = Cursors.Default
        MsgBox("Successfully Download FINGER PRINT USER", MsgBoxStyle.Information, "Success")
    End Sub

    Private Sub btnBatchUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBatchUpdate.Click
        Dim idwErrorCode As Integer

        If Trim(txtUpload_Machine.Text) = "" Then
            MsgBox("Please Select Upload Machine IP Address", MsgBoxStyle.Critical)
            txtUpload_Machine.Focus()
            Exit Sub
        End If
        'Check IP SAME Or Not
        If UCase(Trim(txtDownload_Machine.Text)) = UCase(Trim(txtUpload_Machine.Text)) Then
            MsgBox("Download Machine And Upload Machine IP SAME. You have to Select Different IP Address", MsgBoxStyle.Critical)
            txtUpload_Machine.Focus()
            Exit Sub
        End If

        'Connect Download Machine
        Cursor = Cursors.WaitCursor
        Upload_Machine_IP = Split(txtUpload_Machine.Text, " | ")
        bIsConnected = axCZKEM1.Connect_Net(Trim(Upload_Machine_IP(0)), Convert.ToInt32(4370))
        If bIsConnected = True Then
            iMachineNumber = 1 'In fact,when you are using the tcp/ip communication,this parameter will be ignored,that is any integer will all right.Here we use 1.
            axCZKEM1.RegEvent(iMachineNumber, 65535) 'Here you can register the realtime events that you want to be triggered(the parameters 65535 means registering all)
        Else
            axCZKEM1.GetLastError(idwErrorCode)
            MsgBox("Unable to connect the device,ErrorCode=" & idwErrorCode, MsgBoxStyle.Exclamation, "Error")
        End If
        Cursor = Cursors.Default

        'Upload 
        If bIsConnected = False Then
            MsgBox("Please connect the device first", MsgBoxStyle.Exclamation, "Error")
            Return
        End If
        If lvDownload.Items.Count = 0 Then
            MsgBox("There is no data to upload!", MsgBoxStyle.Exclamation, "Error")
            Return
        End If

        Dim sdwEnrollNumber As String = ""
        Dim sName As String = ""
        Dim sPassword As String = ""
        Dim iPrivilege As Integer
        Dim idwFingerIndex As Integer
        Dim sTmpData As String = ""
        Dim sEnabled As String = ""
        Dim bEnabled As Boolean = False
        Dim iflag As Integer

        Dim iUpdateFlag As Integer = 1
        Dim lvItem As New ListViewItem

        Cursor = Cursors.WaitCursor
        axCZKEM1.EnableDevice(iMachineNumber, False)

        If axCZKEM1.BeginBatchUpdate(iMachineNumber, iUpdateFlag) Then 'create memory space for batching data
            Dim iLastEnrollNumber As Integer = 0 'the former enrollnumber you have upload(define original value as 0)
            For i = 0 To gvdisplay.Rows.Count - 1

                With gvdisplay
                    If .Rows(i).Cells(1).Value = True Then

                        For j = 0 To lvDownload.Items.Count - 1




                            sdwEnrollNumber = Convert.ToInt32(lvDownload.Items(j).SubItems(0).Text.Trim()) 'Convert.ToInt32(lvItem.SubItems(0).Text.Trim())

                            If .Rows(i).Cells(0).Value = sdwEnrollNumber Then

                                sName = lvDownload.Items(j).SubItems(1).Text.Trim()
                                idwFingerIndex = Convert.ToInt32(lvDownload.Items(j).SubItems(2).Text.Trim())
                                sTmpData = lvDownload.Items(j).SubItems(3).Text.Trim()
                                iPrivilege = Convert.ToInt32(lvDownload.Items(j).SubItems(4).Text.Trim())
                                sPassword = lvDownload.Items(j).SubItems(5).Text.Trim()
                                sEnabled = lvDownload.Items(j).SubItems(6).Text.Trim()
                                iflag = Convert.ToInt32(lvDownload.Items(j).SubItems(7).Text.Trim())

                                If sEnabled = "true" Then
                                    bEnabled = True
                                Else
                                    bEnabled = False
                                End If

                                If sdwEnrollNumber <> iLastEnrollNumber Then 'identify whether the user information(except fingerprint templates) has been uploaded
                                    If axCZKEM1.SSR_SetUserInfo(iMachineNumber, sdwEnrollNumber, sName, sPassword, iPrivilege, bEnabled) Then 'upload user information to the device
                                        axCZKEM1.SetUserTmpExStr(iMachineNumber, sdwEnrollNumber, idwFingerIndex, iflag, sTmpData) 'upload templates information to the device
                                    Else
                                        axCZKEM1.GetLastError(idwErrorCode)
                                        MsgBox("Operation failed,ErrorCode=" & idwErrorCode.ToString(), MsgBoxStyle.Exclamation, "Error")
                                        Cursor = Cursors.Default
                                        axCZKEM1.EnableDevice(iMachineNumber, True)
                                        Return
                                    End If
                                Else 'the current fingerprint and the former one belongs the same user,that is ,one user has more than one template
                                    axCZKEM1.SetUserTmpExStr(iMachineNumber, sdwEnrollNumber, idwFingerIndex, iflag, sTmpData) 'upload tempates information to the memory
                                End If
                                iLastEnrollNumber = sdwEnrollNumber 'change the value of iLastEnrollNumber dynamicly

                            End If


                        Next


                    End If
                End With
            Next i
        End If

        axCZKEM1.BatchUpdate(iMachineNumber) 'upload all the information in the memory
        axCZKEM1.RefreshData(iMachineNumber) 'the data in the device should be refreshed
        axCZKEM1.EnableDevice(iMachineNumber, True)

        'Disconnected Server
        axCZKEM1.Disconnect()
        bIsConnected = False
        Cursor = Cursors.Default
        MsgBox("Successfully upload FINGER PRINT USER, " + "total:" + lvDownload.Items.Count.ToString(), MsgBoxStyle.Information, "Success")
    End Sub

    Private Sub gvdisplay_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles gvdisplay.CellContentClick
        If e.ColumnIndex = 1 Then
            With gvdisplay
                If .Rows(e.RowIndex).Cells(e.ColumnIndex).Value = True Then
                    .Rows(e.RowIndex).Cells(e.ColumnIndex).Value = False
                Else
                    .Rows(e.RowIndex).Cells(e.ColumnIndex).Value = True
                End If
            End With
        End If
    End Sub
End Class