﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmployee
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEmployee))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.cmbLocCode = New System.Windows.Forms.ComboBox
        Me.cmbCompCode = New System.Windows.Forms.ComboBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.Experience = New System.Windows.Forms.TabControl
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.txtAttn_Inc_Eligible = New System.Windows.Forms.ComboBox
        Me.Label91 = New System.Windows.Forms.Label
        Me.cmbWages = New System.Windows.Forms.ComboBox
        Me.Label74 = New System.Windows.Forms.Label
        Me.chkNonAdmin = New System.Windows.Forms.CheckBox
        Me.pbEmployee = New System.Windows.Forms.PictureBox
        Me.dtpDOJ = New System.Windows.Forms.DateTimePicker
        Me.dtpDOB = New System.Windows.Forms.DateTimePicker
        Me.txtMachineID = New System.Windows.Forms.TextBox
        Me.txtFirstName = New System.Windows.Forms.TextBox
        Me.txtLastName = New System.Windows.Forms.TextBox
        Me.cmbSubCatName = New System.Windows.Forms.ComboBox
        Me.Label54 = New System.Windows.Forms.Label
        Me.cmbCatName = New System.Windows.Forms.ComboBox
        Me.Label53 = New System.Windows.Forms.Label
        Me.cmbEmpType = New System.Windows.Forms.ComboBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtAge = New System.Windows.Forms.TextBox
        Me.txtInitial = New System.Windows.Forms.TextBox
        Me.txtExistingCode = New System.Windows.Forms.TextBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.txtEmpNo = New System.Windows.Forms.TextBox
        Me.cmbIsActive = New System.Windows.Forms.ComboBox
        Me.cmbEmpStatus = New System.Windows.Forms.ComboBox
        Me.cmbMarital = New System.Windows.Forms.ComboBox
        Me.Label16 = New System.Windows.Forms.Label
        Me.cmbShiftType = New System.Windows.Forms.ComboBox
        Me.cmbGender = New System.Windows.Forms.ComboBox
        Me.Label15 = New System.Windows.Forms.Label
        Me.cmbPrefix = New System.Windows.Forms.ComboBox
        Me.Label26 = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.Label55 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.TabPage2 = New System.Windows.Forms.TabPage
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.cmbWeakOff = New System.Windows.Forms.ComboBox
        Me.Label81 = New System.Windows.Forms.Label
        Me.txtCalculate_Work_Hours = New System.Windows.Forms.TextBox
        Me.Label73 = New System.Windows.Forms.Label
        Me.txtOT_Hours = New System.Windows.Forms.TextBox
        Me.Label58 = New System.Windows.Forms.Label
        Me.txtTot_Work_Hours = New System.Windows.Forms.TextBox
        Me.Label57 = New System.Windows.Forms.Label
        Me.txtNationality = New System.Windows.Forms.TextBox
        Me.Label22 = New System.Windows.Forms.Label
        Me.txtStdWrkHrs = New System.Windows.Forms.TextBox
        Me.Label18 = New System.Windows.Forms.Label
        Me.txtESINo = New System.Windows.Forms.TextBox
        Me.txtNominee = New System.Windows.Forms.TextBox
        Me.txtCertificate = New System.Windows.Forms.TextBox
        Me.txtQualification = New System.Windows.Forms.TextBox
        Me.txtPFNo = New System.Windows.Forms.TextBox
        Me.txtBaseSalary = New System.Windows.Forms.TextBox
        Me.txtDesignation = New System.Windows.Forms.TextBox
        Me.cmbOTEligible = New System.Windows.Forms.ComboBox
        Me.cmbPayPeriod = New System.Windows.Forms.ComboBox
        Me.Label24 = New System.Windows.Forms.Label
        Me.Label20 = New System.Windows.Forms.Label
        Me.Label23 = New System.Windows.Forms.Label
        Me.Label21 = New System.Windows.Forms.Label
        Me.Label19 = New System.Windows.Forms.Label
        Me.Label25 = New System.Windows.Forms.Label
        Me.Label17 = New System.Windows.Forms.Label
        Me.Label27 = New System.Windows.Forms.Label
        Me.Label28 = New System.Windows.Forms.Label
        Me.cmbDept = New System.Windows.Forms.ComboBox
        Me.Label30 = New System.Windows.Forms.Label
        Me.TabPage3 = New System.Windows.Forms.TabPage
        Me.GroupBox4 = New System.Windows.Forms.GroupBox
        Me.txtEmpMob1 = New System.Windows.Forms.TextBox
        Me.txtEmpMob = New System.Windows.Forms.TextBox
        Me.Label77 = New System.Windows.Forms.Label
        Me.txtll1 = New System.Windows.Forms.TextBox
        Me.txtLL = New System.Windows.Forms.TextBox
        Me.Label79 = New System.Windows.Forms.Label
        Me.txtParMob1 = New System.Windows.Forms.TextBox
        Me.txtParMob = New System.Windows.Forms.TextBox
        Me.Label80 = New System.Windows.Forms.Label
        Me.txtReligion = New System.Windows.Forms.TextBox
        Me.Label76 = New System.Windows.Forms.Label
        Me.txtRecmob1 = New System.Windows.Forms.TextBox
        Me.txtRecMob = New System.Windows.Forms.TextBox
        Me.Label75 = New System.Windows.Forms.Label
        Me.cbPermenant = New System.Windows.Forms.CheckBox
        Me.cmbHandicapped = New System.Windows.Forms.ComboBox
        Me.Label47 = New System.Windows.Forms.Label
        Me.cmbBloodGroup = New System.Windows.Forms.ComboBox
        Me.Label29 = New System.Windows.Forms.Label
        Me.txtWeight = New System.Windows.Forms.TextBox
        Me.txtHeight = New System.Windows.Forms.TextBox
        Me.Label49 = New System.Windows.Forms.Label
        Me.txtIDMark2 = New System.Windows.Forms.TextBox
        Me.Label48 = New System.Windows.Forms.Label
        Me.txtIDMark1 = New System.Windows.Forms.TextBox
        Me.Label46 = New System.Windows.Forms.Label
        Me.Label45 = New System.Windows.Forms.Label
        Me.txtRecuritmentThrough = New System.Windows.Forms.TextBox
        Me.Label50 = New System.Windows.Forms.Label
        Me.txtAddress2 = New System.Windows.Forms.TextBox
        Me.txtAddress1 = New System.Windows.Forms.TextBox
        Me.Label52 = New System.Windows.Forms.Label
        Me.txtFamilyDetails = New System.Windows.Forms.TextBox
        Me.Label51 = New System.Windows.Forms.Label
        Me.Label56 = New System.Windows.Forms.Label
        Me.TabPage5 = New System.Windows.Forms.TabPage
        Me.GroupBox6 = New System.Windows.Forms.GroupBox
        Me.Button2 = New System.Windows.Forms.Button
        Me.Button1 = New System.Windows.Forms.Button
        Me.DataGridView1 = New System.Windows.Forms.DataGridView
        Me.SiNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Years = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Months = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CompanyName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Designation = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Department = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.PeriodFrom = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.PeriodTo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ContactName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Mobile = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Phone = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.txtConll1 = New System.Windows.Forms.TextBox
        Me.txtConll = New System.Windows.Forms.TextBox
        Me.Label106 = New System.Windows.Forms.Label
        Me.txtConMob1 = New System.Windows.Forms.TextBox
        Me.txtConMob = New System.Windows.Forms.TextBox
        Me.Label105 = New System.Windows.Forms.Label
        Me.txtContactname = New System.Windows.Forms.TextBox
        Me.Label104 = New System.Windows.Forms.Label
        Me.txtPeriodTo = New System.Windows.Forms.TextBox
        Me.Label102 = New System.Windows.Forms.Label
        Me.txtPeriodFrom = New System.Windows.Forms.TextBox
        Me.Label103 = New System.Windows.Forms.Label
        Me.txtPreviousDepartment = New System.Windows.Forms.TextBox
        Me.Label101 = New System.Windows.Forms.Label
        Me.txtPreviousDesignation = New System.Windows.Forms.TextBox
        Me.Label100 = New System.Windows.Forms.Label
        Me.txtCompanyName = New System.Windows.Forms.TextBox
        Me.Label98 = New System.Windows.Forms.Label
        Me.cmbMonths = New System.Windows.Forms.ComboBox
        Me.Label97 = New System.Windows.Forms.Label
        Me.cmbYears = New System.Windows.Forms.ComboBox
        Me.Label96 = New System.Windows.Forms.Label
        Me.TabPage4 = New System.Windows.Forms.TabPage
        Me.GroupBox5 = New System.Windows.Forms.GroupBox
        Me.txtAccountNo = New System.Windows.Forms.TextBox
        Me.Label78 = New System.Windows.Forms.Label
        Me.txtBankName = New System.Windows.Forms.TextBox
        Me.txtBranchCode = New System.Windows.Forms.TextBox
        Me.Label84 = New System.Windows.Forms.Label
        Me.Label85 = New System.Windows.Forms.Label
        Me.TabPage6 = New System.Windows.Forms.TabPage
        Me.GroupBox7 = New System.Windows.Forms.GroupBox
        Me.txtOthers = New System.Windows.Forms.TextBox
        Me.Label90 = New System.Windows.Forms.Label
        Me.txtSmart = New System.Windows.Forms.TextBox
        Me.Label89 = New System.Windows.Forms.Label
        Me.txtPanCard = New System.Windows.Forms.TextBox
        Me.Label88 = New System.Windows.Forms.Label
        Me.txtRationCard = New System.Windows.Forms.TextBox
        Me.Label87 = New System.Windows.Forms.Label
        Me.txtPassport = New System.Windows.Forms.TextBox
        Me.Label86 = New System.Windows.Forms.Label
        Me.txtDriving = New System.Windows.Forms.TextBox
        Me.Label83 = New System.Windows.Forms.Label
        Me.txtVoterId = New System.Windows.Forms.TextBox
        Me.Label82 = New System.Windows.Forms.Label
        Me.txtAdharCard = New System.Windows.Forms.TextBox
        Me.lblAdharCard = New System.Windows.Forms.Label
        Me.btnSave = New System.Windows.Forms.Button
        Me.btnClear = New System.Windows.Forms.Button
        Me.btnExit = New System.Windows.Forms.Button
        Me.dlgOpen = New System.Windows.Forms.OpenFileDialog
        Me.PictureBox3 = New System.Windows.Forms.PictureBox
        Me.DateTimePicker2 = New System.Windows.Forms.DateTimePicker
        Me.TextBox8 = New System.Windows.Forms.TextBox
        Me.TextBox9 = New System.Windows.Forms.TextBox
        Me.TextBox10 = New System.Windows.Forms.TextBox
        Me.TextBox11 = New System.Windows.Forms.TextBox
        Me.TextBox12 = New System.Windows.Forms.TextBox
        Me.TextBox13 = New System.Windows.Forms.TextBox
        Me.Label31 = New System.Windows.Forms.Label
        Me.Label32 = New System.Windows.Forms.Label
        Me.Label33 = New System.Windows.Forms.Label
        Me.Label34 = New System.Windows.Forms.Label
        Me.Label35 = New System.Windows.Forms.Label
        Me.Label36 = New System.Windows.Forms.Label
        Me.TextBox14 = New System.Windows.Forms.TextBox
        Me.ComboBox7 = New System.Windows.Forms.ComboBox
        Me.ComboBox8 = New System.Windows.Forms.ComboBox
        Me.ComboBox9 = New System.Windows.Forms.ComboBox
        Me.Label37 = New System.Windows.Forms.Label
        Me.ComboBox10 = New System.Windows.Forms.ComboBox
        Me.Label38 = New System.Windows.Forms.Label
        Me.ComboBox11 = New System.Windows.Forms.ComboBox
        Me.Label39 = New System.Windows.Forms.Label
        Me.Label40 = New System.Windows.Forms.Label
        Me.Label41 = New System.Windows.Forms.Label
        Me.Label42 = New System.Windows.Forms.Label
        Me.Label43 = New System.Windows.Forms.Label
        Me.ComboBox12 = New System.Windows.Forms.ComboBox
        Me.Label44 = New System.Windows.Forms.Label
        Me.PictureBox5 = New System.Windows.Forms.PictureBox
        Me.DateTimePicker4 = New System.Windows.Forms.DateTimePicker
        Me.TextBox22 = New System.Windows.Forms.TextBox
        Me.TextBox23 = New System.Windows.Forms.TextBox
        Me.TextBox24 = New System.Windows.Forms.TextBox
        Me.TextBox25 = New System.Windows.Forms.TextBox
        Me.TextBox26 = New System.Windows.Forms.TextBox
        Me.TextBox27 = New System.Windows.Forms.TextBox
        Me.Label59 = New System.Windows.Forms.Label
        Me.Label60 = New System.Windows.Forms.Label
        Me.Label61 = New System.Windows.Forms.Label
        Me.Label62 = New System.Windows.Forms.Label
        Me.Label63 = New System.Windows.Forms.Label
        Me.Label64 = New System.Windows.Forms.Label
        Me.TextBox28 = New System.Windows.Forms.TextBox
        Me.ComboBox19 = New System.Windows.Forms.ComboBox
        Me.ComboBox20 = New System.Windows.Forms.ComboBox
        Me.ComboBox21 = New System.Windows.Forms.ComboBox
        Me.Label65 = New System.Windows.Forms.Label
        Me.ComboBox22 = New System.Windows.Forms.ComboBox
        Me.Label66 = New System.Windows.Forms.Label
        Me.ComboBox23 = New System.Windows.Forms.ComboBox
        Me.Label67 = New System.Windows.Forms.Label
        Me.Label68 = New System.Windows.Forms.Label
        Me.Label69 = New System.Windows.Forms.Label
        Me.Label70 = New System.Windows.Forms.Label
        Me.Label71 = New System.Windows.Forms.Label
        Me.ComboBox24 = New System.Windows.Forms.ComboBox
        Me.Label72 = New System.Windows.Forms.Label
        Me.cmdPrint = New System.Windows.Forms.Button
        Me.btnMachine_ID_Update = New System.Windows.Forms.Button
        Me.PictureBox2 = New System.Windows.Forms.PictureBox
        Me.BtnEPay_Save = New System.Windows.Forms.Button
        Me.Button3 = New System.Windows.Forms.Button
        Me.GroupBox1.SuspendLayout()
        Me.Experience.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.pbEmployee, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.TabPage5.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage4.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.TabPage6.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.GroupBox1.Controls.Add(Me.cmbLocCode)
        Me.GroupBox1.Controls.Add(Me.cmbCompCode)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Location = New System.Drawing.Point(1, -4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(784, 45)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'cmbLocCode
        '
        Me.cmbLocCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbLocCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbLocCode.FormattingEnabled = True
        Me.cmbLocCode.Location = New System.Drawing.Point(512, 11)
        Me.cmbLocCode.Name = "cmbLocCode"
        Me.cmbLocCode.Size = New System.Drawing.Size(266, 24)
        Me.cmbLocCode.TabIndex = 1
        '
        'cmbCompCode
        '
        Me.cmbCompCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbCompCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbCompCode.FormattingEnabled = True
        Me.cmbCompCode.Location = New System.Drawing.Point(126, 11)
        Me.cmbCompCode.Name = "cmbCompCode"
        Me.cmbCompCode.Size = New System.Drawing.Size(266, 24)
        Me.cmbCompCode.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label1.Location = New System.Drawing.Point(398, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(108, 16)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Location Code"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.BackColor = System.Drawing.Color.Transparent
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label11.Location = New System.Drawing.Point(6, 15)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(114, 16)
        Me.Label11.TabIndex = 4
        Me.Label11.Text = "Company Code"
        '
        'Experience
        '
        Me.Experience.Controls.Add(Me.TabPage1)
        Me.Experience.Controls.Add(Me.TabPage2)
        Me.Experience.Controls.Add(Me.TabPage3)
        Me.Experience.Controls.Add(Me.TabPage5)
        Me.Experience.Controls.Add(Me.TabPage4)
        Me.Experience.Controls.Add(Me.TabPage6)
        Me.Experience.Location = New System.Drawing.Point(1, 43)
        Me.Experience.Name = "Experience"
        Me.Experience.SelectedIndex = 0
        Me.Experience.Size = New System.Drawing.Size(785, 400)
        Me.Experience.TabIndex = 1
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.GroupBox3)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(777, 374)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Main"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.GroupBox3.Controls.Add(Me.txtAttn_Inc_Eligible)
        Me.GroupBox3.Controls.Add(Me.Label91)
        Me.GroupBox3.Controls.Add(Me.cmbWages)
        Me.GroupBox3.Controls.Add(Me.Label74)
        Me.GroupBox3.Controls.Add(Me.chkNonAdmin)
        Me.GroupBox3.Controls.Add(Me.pbEmployee)
        Me.GroupBox3.Controls.Add(Me.dtpDOJ)
        Me.GroupBox3.Controls.Add(Me.dtpDOB)
        Me.GroupBox3.Controls.Add(Me.txtMachineID)
        Me.GroupBox3.Controls.Add(Me.txtFirstName)
        Me.GroupBox3.Controls.Add(Me.txtLastName)
        Me.GroupBox3.Controls.Add(Me.cmbSubCatName)
        Me.GroupBox3.Controls.Add(Me.Label54)
        Me.GroupBox3.Controls.Add(Me.cmbCatName)
        Me.GroupBox3.Controls.Add(Me.Label53)
        Me.GroupBox3.Controls.Add(Me.cmbEmpType)
        Me.GroupBox3.Controls.Add(Me.Label2)
        Me.GroupBox3.Controls.Add(Me.txtAge)
        Me.GroupBox3.Controls.Add(Me.txtInitial)
        Me.GroupBox3.Controls.Add(Me.txtExistingCode)
        Me.GroupBox3.Controls.Add(Me.Label9)
        Me.GroupBox3.Controls.Add(Me.Label13)
        Me.GroupBox3.Controls.Add(Me.Label7)
        Me.GroupBox3.Controls.Add(Me.Label8)
        Me.GroupBox3.Controls.Add(Me.Label6)
        Me.GroupBox3.Controls.Add(Me.Label5)
        Me.GroupBox3.Controls.Add(Me.txtEmpNo)
        Me.GroupBox3.Controls.Add(Me.cmbIsActive)
        Me.GroupBox3.Controls.Add(Me.cmbEmpStatus)
        Me.GroupBox3.Controls.Add(Me.cmbMarital)
        Me.GroupBox3.Controls.Add(Me.Label16)
        Me.GroupBox3.Controls.Add(Me.cmbShiftType)
        Me.GroupBox3.Controls.Add(Me.cmbGender)
        Me.GroupBox3.Controls.Add(Me.Label15)
        Me.GroupBox3.Controls.Add(Me.cmbPrefix)
        Me.GroupBox3.Controls.Add(Me.Label26)
        Me.GroupBox3.Controls.Add(Me.Label14)
        Me.GroupBox3.Controls.Add(Me.Label55)
        Me.GroupBox3.Controls.Add(Me.Label12)
        Me.GroupBox3.Controls.Add(Me.Label10)
        Me.GroupBox3.Controls.Add(Me.Label4)
        Me.GroupBox3.Controls.Add(Me.Label3)
        Me.GroupBox3.Location = New System.Drawing.Point(1, -2)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(775, 386)
        Me.GroupBox3.TabIndex = 3
        Me.GroupBox3.TabStop = False
        '
        'txtAttn_Inc_Eligible
        '
        Me.txtAttn_Inc_Eligible.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.txtAttn_Inc_Eligible.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAttn_Inc_Eligible.FormattingEnabled = True
        Me.txtAttn_Inc_Eligible.Items.AddRange(New Object() {"No", "Yes"})
        Me.txtAttn_Inc_Eligible.Location = New System.Drawing.Point(436, 303)
        Me.txtAttn_Inc_Eligible.Name = "txtAttn_Inc_Eligible"
        Me.txtAttn_Inc_Eligible.Size = New System.Drawing.Size(123, 24)
        Me.txtAttn_Inc_Eligible.TabIndex = 25
        '
        'Label91
        '
        Me.Label91.AutoSize = True
        Me.Label91.BackColor = System.Drawing.Color.Transparent
        Me.Label91.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label91.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label91.Location = New System.Drawing.Point(301, 307)
        Me.Label91.Name = "Label91"
        Me.Label91.Size = New System.Drawing.Size(123, 16)
        Me.Label91.TabIndex = 24
        Me.Label91.Text = "Attn. Inc. Eligible"
        '
        'cmbWages
        '
        Me.cmbWages.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbWages.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbWages.FormattingEnabled = True
        Me.cmbWages.Items.AddRange(New Object() {"MONTHLY TEMP WORKER", "MONTHLY C-WORKER", "MONTHLY STAFF-II", "MONTHLY M-WORKER", "MONTHLY STAFF-I", "MONTHLY TEN DAYS", "WEEKLY", "WEEKLY II", "NEW COMMERS"})
        Me.cmbWages.Location = New System.Drawing.Point(134, 275)
        Me.cmbWages.Name = "cmbWages"
        Me.cmbWages.Size = New System.Drawing.Size(201, 24)
        Me.cmbWages.TabIndex = 21
        '
        'Label74
        '
        Me.Label74.AutoSize = True
        Me.Label74.BackColor = System.Drawing.Color.Transparent
        Me.Label74.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label74.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label74.Location = New System.Drawing.Point(26, 278)
        Me.Label74.Name = "Label74"
        Me.Label74.Size = New System.Drawing.Size(97, 16)
        Me.Label74.TabIndex = 20
        Me.Label74.Text = "Wages Type"
        '
        'chkNonAdmin
        '
        Me.chkNonAdmin.AutoSize = True
        Me.chkNonAdmin.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkNonAdmin.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.chkNonAdmin.Location = New System.Drawing.Point(134, 305)
        Me.chkNonAdmin.Name = "chkNonAdmin"
        Me.chkNonAdmin.Size = New System.Drawing.Size(155, 20)
        Me.chkNonAdmin.TabIndex = 18
        Me.chkNonAdmin.Text = "Is Non Admin User"
        Me.chkNonAdmin.UseVisualStyleBackColor = True
        '
        'pbEmployee
        '
        Me.pbEmployee.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.pbEmployee.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pbEmployee.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pbEmployee.Location = New System.Drawing.Point(584, 53)
        Me.pbEmployee.Name = "pbEmployee"
        Me.pbEmployee.Size = New System.Drawing.Size(183, 201)
        Me.pbEmployee.TabIndex = 14
        Me.pbEmployee.TabStop = False
        '
        'dtpDOJ
        '
        Me.dtpDOJ.CustomFormat = "dd/MM/yyyy"
        Me.dtpDOJ.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDOJ.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpDOJ.Location = New System.Drawing.Point(134, 185)
        Me.dtpDOJ.Name = "dtpDOJ"
        Me.dtpDOJ.Size = New System.Drawing.Size(112, 22)
        Me.dtpDOJ.TabIndex = 11
        '
        'dtpDOB
        '
        Me.dtpDOB.CustomFormat = "dd/MM/yyyy"
        Me.dtpDOB.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDOB.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpDOB.Location = New System.Drawing.Point(134, 157)
        Me.dtpDOB.Name = "dtpDOB"
        Me.dtpDOB.Size = New System.Drawing.Size(112, 22)
        Me.dtpDOB.TabIndex = 9
        '
        'txtMachineID
        '
        Me.txtMachineID.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMachineID.Location = New System.Drawing.Point(436, 43)
        Me.txtMachineID.Name = "txtMachineID"
        Me.txtMachineID.Size = New System.Drawing.Size(123, 22)
        Me.txtMachineID.TabIndex = 3
        '
        'txtFirstName
        '
        Me.txtFirstName.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFirstName.Location = New System.Drawing.Point(134, 71)
        Me.txtFirstName.Name = "txtFirstName"
        Me.txtFirstName.Size = New System.Drawing.Size(201, 22)
        Me.txtFirstName.TabIndex = 4
        '
        'txtLastName
        '
        Me.txtLastName.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLastName.Location = New System.Drawing.Point(134, 99)
        Me.txtLastName.Name = "txtLastName"
        Me.txtLastName.Size = New System.Drawing.Size(201, 22)
        Me.txtLastName.TabIndex = 6
        '
        'cmbSubCatName
        '
        Me.cmbSubCatName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbSubCatName.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbSubCatName.FormattingEnabled = True
        Me.cmbSubCatName.Items.AddRange(New Object() {"INSIDER", "OUTSIDER"})
        Me.cmbSubCatName.Location = New System.Drawing.Point(436, 213)
        Me.cmbSubCatName.Name = "cmbSubCatName"
        Me.cmbSubCatName.Size = New System.Drawing.Size(123, 24)
        Me.cmbSubCatName.TabIndex = 14
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.BackColor = System.Drawing.Color.Transparent
        Me.Label54.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label54.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label54.Location = New System.Drawing.Point(326, 217)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(98, 16)
        Me.Label54.TabIndex = 6
        Me.Label54.Text = "SubCategory"
        '
        'cmbCatName
        '
        Me.cmbCatName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbCatName.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbCatName.FormattingEnabled = True
        Me.cmbCatName.Items.AddRange(New Object() {"LABOUR", "STAFF"})
        Me.cmbCatName.Location = New System.Drawing.Point(134, 213)
        Me.cmbCatName.Name = "cmbCatName"
        Me.cmbCatName.Size = New System.Drawing.Size(123, 24)
        Me.cmbCatName.TabIndex = 13
        '
        'Label53
        '
        Me.Label53.AutoSize = True
        Me.Label53.BackColor = System.Drawing.Color.Transparent
        Me.Label53.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label53.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label53.Location = New System.Drawing.Point(52, 217)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(71, 16)
        Me.Label53.TabIndex = 6
        Me.Label53.Text = "Category"
        '
        'cmbEmpType
        '
        Me.cmbEmpType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbEmpType.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbEmpType.FormattingEnabled = True
        Me.cmbEmpType.Location = New System.Drawing.Point(134, 243)
        Me.cmbEmpType.Name = "cmbEmpType"
        Me.cmbEmpType.Size = New System.Drawing.Size(201, 24)
        Me.cmbEmpType.TabIndex = 15
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label2.Location = New System.Drawing.Point(5, 247)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(118, 16)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Employee Type"
        '
        'txtAge
        '
        Me.txtAge.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAge.Location = New System.Drawing.Point(512, 157)
        Me.txtAge.MaxLength = 2
        Me.txtAge.Name = "txtAge"
        Me.txtAge.Size = New System.Drawing.Size(47, 22)
        Me.txtAge.TabIndex = 10
        '
        'txtInitial
        '
        Me.txtInitial.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInitial.Location = New System.Drawing.Point(512, 71)
        Me.txtInitial.Name = "txtInitial"
        Me.txtInitial.Size = New System.Drawing.Size(47, 22)
        Me.txtInitial.TabIndex = 5
        '
        'txtExistingCode
        '
        Me.txtExistingCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtExistingCode.Location = New System.Drawing.Point(134, 43)
        Me.txtExistingCode.Name = "txtExistingCode"
        Me.txtExistingCode.Size = New System.Drawing.Size(123, 22)
        Me.txtExistingCode.TabIndex = 2
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.BackColor = System.Drawing.Color.Transparent
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label9.Location = New System.Drawing.Point(41, 102)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(82, 16)
        Me.Label9.TabIndex = 11
        Me.Label9.Text = "Last Name"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.BackColor = System.Drawing.Color.Transparent
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label13.Location = New System.Drawing.Point(470, 160)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(36, 16)
        Me.Label13.TabIndex = 11
        Me.Label13.Text = "Age"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label7.Location = New System.Drawing.Point(40, 74)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(83, 16)
        Me.Label7.TabIndex = 11
        Me.Label7.Text = "First Name"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label8.Location = New System.Drawing.Point(465, 74)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(41, 16)
        Me.Label8.TabIndex = 11
        Me.Label8.Text = "Intial"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label6.Location = New System.Drawing.Point(339, 46)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(85, 16)
        Me.Label6.TabIndex = 11
        Me.Label6.Text = "Machine ID"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label5.Location = New System.Drawing.Point(20, 46)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(103, 16)
        Me.Label5.TabIndex = 11
        Me.Label5.Text = "Existing Code"
        '
        'txtEmpNo
        '
        Me.txtEmpNo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmpNo.Location = New System.Drawing.Point(436, 14)
        Me.txtEmpNo.Name = "txtEmpNo"
        Me.txtEmpNo.Size = New System.Drawing.Size(123, 22)
        Me.txtEmpNo.TabIndex = 1
        '
        'cmbIsActive
        '
        Me.cmbIsActive.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbIsActive.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbIsActive.FormattingEnabled = True
        Me.cmbIsActive.Items.AddRange(New Object() {"Yes", "No"})
        Me.cmbIsActive.Location = New System.Drawing.Point(436, 275)
        Me.cmbIsActive.Name = "cmbIsActive"
        Me.cmbIsActive.Size = New System.Drawing.Size(123, 24)
        Me.cmbIsActive.TabIndex = 17
        '
        'cmbEmpStatus
        '
        Me.cmbEmpStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbEmpStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbEmpStatus.FormattingEnabled = True
        Me.cmbEmpStatus.Items.AddRange(New Object() {"On Roll", "Short Leave", "Long Leave", "Transfer"})
        Me.cmbEmpStatus.Location = New System.Drawing.Point(436, 243)
        Me.cmbEmpStatus.Name = "cmbEmpStatus"
        Me.cmbEmpStatus.Size = New System.Drawing.Size(123, 24)
        Me.cmbEmpStatus.TabIndex = 16
        '
        'cmbMarital
        '
        Me.cmbMarital.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbMarital.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbMarital.FormattingEnabled = True
        Me.cmbMarital.Items.AddRange(New Object() {"None", "Single", "Married", "Separated", "Divorced", "Widowed", "Prefer not to answer"})
        Me.cmbMarital.Location = New System.Drawing.Point(436, 184)
        Me.cmbMarital.Name = "cmbMarital"
        Me.cmbMarital.Size = New System.Drawing.Size(123, 24)
        Me.cmbMarital.TabIndex = 12
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.BackColor = System.Drawing.Color.Transparent
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label16.Location = New System.Drawing.Point(357, 279)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(67, 16)
        Me.Label16.TabIndex = 8
        Me.Label16.Text = "Is Active"
        '
        'cmbShiftType
        '
        Me.cmbShiftType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbShiftType.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbShiftType.FormattingEnabled = True
        Me.cmbShiftType.Items.AddRange(New Object() {"GENERAL", "SHIFT", "ANY TIME"})
        Me.cmbShiftType.Location = New System.Drawing.Point(134, 127)
        Me.cmbShiftType.Name = "cmbShiftType"
        Me.cmbShiftType.Size = New System.Drawing.Size(112, 24)
        Me.cmbShiftType.TabIndex = 7
        '
        'cmbGender
        '
        Me.cmbGender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbGender.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbGender.FormattingEnabled = True
        Me.cmbGender.Items.AddRange(New Object() {"Male", "Female"})
        Me.cmbGender.Location = New System.Drawing.Point(447, 127)
        Me.cmbGender.Name = "cmbGender"
        Me.cmbGender.Size = New System.Drawing.Size(112, 24)
        Me.cmbGender.TabIndex = 8
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.BackColor = System.Drawing.Color.Transparent
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label15.Location = New System.Drawing.Point(338, 247)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(86, 16)
        Me.Label15.TabIndex = 8
        Me.Label15.Text = "Emp Status"
        '
        'cmbPrefix
        '
        Me.cmbPrefix.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbPrefix.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbPrefix.FormattingEnabled = True
        Me.cmbPrefix.Items.AddRange(New Object() {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"})
        Me.cmbPrefix.Location = New System.Drawing.Point(134, 13)
        Me.cmbPrefix.Name = "cmbPrefix"
        Me.cmbPrefix.Size = New System.Drawing.Size(123, 24)
        Me.cmbPrefix.TabIndex = 0
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.BackColor = System.Drawing.Color.Transparent
        Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label26.Location = New System.Drawing.Point(73, 188)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(50, 16)
        Me.Label26.TabIndex = 8
        Me.Label26.Text = "D.O.J."
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.BackColor = System.Drawing.Color.Transparent
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label14.Location = New System.Drawing.Point(322, 188)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(102, 16)
        Me.Label14.TabIndex = 8
        Me.Label14.Text = "Marital Status"
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.BackColor = System.Drawing.Color.Transparent
        Me.Label55.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label55.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label55.Location = New System.Drawing.Point(45, 131)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(78, 16)
        Me.Label55.TabIndex = 8
        Me.Label55.Text = "Shift Type"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.BackColor = System.Drawing.Color.Transparent
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label12.Location = New System.Drawing.Point(71, 160)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(52, 16)
        Me.Label12.TabIndex = 8
        Me.Label12.Text = "D.O.B."
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.Color.Transparent
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label10.Location = New System.Drawing.Point(365, 131)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(59, 16)
        Me.Label10.TabIndex = 8
        Me.Label10.Text = "Gender"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label4.Location = New System.Drawing.Point(370, 17)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(54, 16)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Tkt No"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label3.Location = New System.Drawing.Point(76, 17)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(47, 16)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Prefix"
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.GroupBox2)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(777, 374)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Others"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.GroupBox2.Controls.Add(Me.cmbWeakOff)
        Me.GroupBox2.Controls.Add(Me.Label81)
        Me.GroupBox2.Controls.Add(Me.txtCalculate_Work_Hours)
        Me.GroupBox2.Controls.Add(Me.Label73)
        Me.GroupBox2.Controls.Add(Me.txtOT_Hours)
        Me.GroupBox2.Controls.Add(Me.Label58)
        Me.GroupBox2.Controls.Add(Me.txtTot_Work_Hours)
        Me.GroupBox2.Controls.Add(Me.Label57)
        Me.GroupBox2.Controls.Add(Me.txtNationality)
        Me.GroupBox2.Controls.Add(Me.Label22)
        Me.GroupBox2.Controls.Add(Me.txtStdWrkHrs)
        Me.GroupBox2.Controls.Add(Me.Label18)
        Me.GroupBox2.Controls.Add(Me.txtESINo)
        Me.GroupBox2.Controls.Add(Me.txtNominee)
        Me.GroupBox2.Controls.Add(Me.txtCertificate)
        Me.GroupBox2.Controls.Add(Me.txtQualification)
        Me.GroupBox2.Controls.Add(Me.txtPFNo)
        Me.GroupBox2.Controls.Add(Me.txtBaseSalary)
        Me.GroupBox2.Controls.Add(Me.txtDesignation)
        Me.GroupBox2.Controls.Add(Me.cmbOTEligible)
        Me.GroupBox2.Controls.Add(Me.cmbPayPeriod)
        Me.GroupBox2.Controls.Add(Me.Label24)
        Me.GroupBox2.Controls.Add(Me.Label20)
        Me.GroupBox2.Controls.Add(Me.Label23)
        Me.GroupBox2.Controls.Add(Me.Label21)
        Me.GroupBox2.Controls.Add(Me.Label19)
        Me.GroupBox2.Controls.Add(Me.Label25)
        Me.GroupBox2.Controls.Add(Me.Label17)
        Me.GroupBox2.Controls.Add(Me.Label27)
        Me.GroupBox2.Controls.Add(Me.Label28)
        Me.GroupBox2.Controls.Add(Me.cmbDept)
        Me.GroupBox2.Controls.Add(Me.Label30)
        Me.GroupBox2.Location = New System.Drawing.Point(1, -2)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(775, 373)
        Me.GroupBox2.TabIndex = 4
        Me.GroupBox2.TabStop = False
        '
        'cmbWeakOff
        '
        Me.cmbWeakOff.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbWeakOff.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbWeakOff.FormattingEnabled = True
        Me.cmbWeakOff.Items.AddRange(New Object() {"None", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"})
        Me.cmbWeakOff.Location = New System.Drawing.Point(522, 252)
        Me.cmbWeakOff.Name = "cmbWeakOff"
        Me.cmbWeakOff.Size = New System.Drawing.Size(240, 24)
        Me.cmbWeakOff.TabIndex = 19
        '
        'Label81
        '
        Me.Label81.AutoSize = True
        Me.Label81.BackColor = System.Drawing.Color.Transparent
        Me.Label81.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label81.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label81.Location = New System.Drawing.Point(428, 256)
        Me.Label81.Name = "Label81"
        Me.Label81.Size = New System.Drawing.Size(71, 16)
        Me.Label81.TabIndex = 18
        Me.Label81.Text = "Weak Off"
        '
        'txtCalculate_Work_Hours
        '
        Me.txtCalculate_Work_Hours.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCalculate_Work_Hours.Location = New System.Drawing.Point(573, 221)
        Me.txtCalculate_Work_Hours.MaxLength = 6
        Me.txtCalculate_Work_Hours.Name = "txtCalculate_Work_Hours"
        Me.txtCalculate_Work_Hours.Size = New System.Drawing.Size(189, 22)
        Me.txtCalculate_Work_Hours.TabIndex = 16
        '
        'Label73
        '
        Me.Label73.AutoSize = True
        Me.Label73.BackColor = System.Drawing.Color.Transparent
        Me.Label73.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label73.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label73.Location = New System.Drawing.Point(393, 224)
        Me.Label73.Name = "Label73"
        Me.Label73.Size = New System.Drawing.Size(179, 16)
        Me.Label73.TabIndex = 17
        Me.Label73.Text = "Calculate Working Hours"
        '
        'txtOT_Hours
        '
        Me.txtOT_Hours.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOT_Hours.Location = New System.Drawing.Point(142, 249)
        Me.txtOT_Hours.MaxLength = 6
        Me.txtOT_Hours.Name = "txtOT_Hours"
        Me.txtOT_Hours.Size = New System.Drawing.Size(240, 22)
        Me.txtOT_Hours.TabIndex = 14
        '
        'Label58
        '
        Me.Label58.AutoSize = True
        Me.Label58.BackColor = System.Drawing.Color.Transparent
        Me.Label58.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label58.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label58.Location = New System.Drawing.Point(60, 252)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(74, 16)
        Me.Label58.TabIndex = 15
        Me.Label58.Text = "OT Hours"
        '
        'txtTot_Work_Hours
        '
        Me.txtTot_Work_Hours.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTot_Work_Hours.Location = New System.Drawing.Point(142, 221)
        Me.txtTot_Work_Hours.MaxLength = 6
        Me.txtTot_Work_Hours.Name = "txtTot_Work_Hours"
        Me.txtTot_Work_Hours.Size = New System.Drawing.Size(240, 22)
        Me.txtTot_Work_Hours.TabIndex = 12
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.BackColor = System.Drawing.Color.Transparent
        Me.Label57.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label57.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label57.Location = New System.Drawing.Point(24, 224)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(110, 16)
        Me.Label57.TabIndex = 13
        Me.Label57.Text = "Working Hours"
        '
        'txtNationality
        '
        Me.txtNationality.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNationality.Location = New System.Drawing.Point(142, 193)
        Me.txtNationality.MaxLength = 2
        Me.txtNationality.Name = "txtNationality"
        Me.txtNationality.Size = New System.Drawing.Size(240, 22)
        Me.txtNationality.TabIndex = 10
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.BackColor = System.Drawing.Color.Transparent
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label22.Location = New System.Drawing.Point(52, 196)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(82, 16)
        Me.Label22.TabIndex = 11
        Me.Label22.Text = "Nationality"
        '
        'txtStdWrkHrs
        '
        Me.txtStdWrkHrs.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtStdWrkHrs.Location = New System.Drawing.Point(142, 135)
        Me.txtStdWrkHrs.MaxLength = 2
        Me.txtStdWrkHrs.Name = "txtStdWrkHrs"
        Me.txtStdWrkHrs.Size = New System.Drawing.Size(240, 22)
        Me.txtStdWrkHrs.TabIndex = 10
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.BackColor = System.Drawing.Color.Transparent
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label18.Location = New System.Drawing.Point(10, 138)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(124, 16)
        Me.Label18.TabIndex = 11
        Me.Label18.Text = "Std Working Hrs."
        '
        'txtESINo
        '
        Me.txtESINo.Enabled = False
        Me.txtESINo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtESINo.Location = New System.Drawing.Point(142, 107)
        Me.txtESINo.Name = "txtESINo"
        Me.txtESINo.Size = New System.Drawing.Size(240, 22)
        Me.txtESINo.TabIndex = 2
        '
        'txtNominee
        '
        Me.txtNominee.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNominee.Location = New System.Drawing.Point(522, 79)
        Me.txtNominee.Name = "txtNominee"
        Me.txtNominee.Size = New System.Drawing.Size(240, 22)
        Me.txtNominee.TabIndex = 2
        '
        'txtCertificate
        '
        Me.txtCertificate.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCertificate.Location = New System.Drawing.Point(522, 135)
        Me.txtCertificate.Multiline = True
        Me.txtCertificate.Name = "txtCertificate"
        Me.txtCertificate.Size = New System.Drawing.Size(240, 76)
        Me.txtCertificate.TabIndex = 2
        '
        'txtQualification
        '
        Me.txtQualification.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtQualification.Location = New System.Drawing.Point(522, 107)
        Me.txtQualification.Name = "txtQualification"
        Me.txtQualification.Size = New System.Drawing.Size(240, 22)
        Me.txtQualification.TabIndex = 2
        '
        'txtPFNo
        '
        Me.txtPFNo.Enabled = False
        Me.txtPFNo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPFNo.Location = New System.Drawing.Point(142, 79)
        Me.txtPFNo.Name = "txtPFNo"
        Me.txtPFNo.Size = New System.Drawing.Size(240, 22)
        Me.txtPFNo.TabIndex = 2
        '
        'txtBaseSalary
        '
        Me.txtBaseSalary.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBaseSalary.Location = New System.Drawing.Point(522, 50)
        Me.txtBaseSalary.Name = "txtBaseSalary"
        Me.txtBaseSalary.Size = New System.Drawing.Size(240, 22)
        Me.txtBaseSalary.TabIndex = 2
        '
        'txtDesignation
        '
        Me.txtDesignation.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDesignation.Location = New System.Drawing.Point(522, 21)
        Me.txtDesignation.Name = "txtDesignation"
        Me.txtDesignation.Size = New System.Drawing.Size(240, 22)
        Me.txtDesignation.TabIndex = 2
        '
        'cmbOTEligible
        '
        Me.cmbOTEligible.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbOTEligible.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbOTEligible.FormattingEnabled = True
        Me.cmbOTEligible.Items.AddRange(New Object() {"Yes", "No"})
        Me.cmbOTEligible.Location = New System.Drawing.Point(142, 163)
        Me.cmbOTEligible.Name = "cmbOTEligible"
        Me.cmbOTEligible.Size = New System.Drawing.Size(240, 24)
        Me.cmbOTEligible.TabIndex = 11
        '
        'cmbPayPeriod
        '
        Me.cmbPayPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbPayPeriod.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbPayPeriod.FormattingEnabled = True
        Me.cmbPayPeriod.Items.AddRange(New Object() {"Monthly", "Weekly", "Daily"})
        Me.cmbPayPeriod.Location = New System.Drawing.Point(142, 49)
        Me.cmbPayPeriod.Name = "cmbPayPeriod"
        Me.cmbPayPeriod.Size = New System.Drawing.Size(240, 24)
        Me.cmbPayPeriod.TabIndex = 8
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.BackColor = System.Drawing.Color.Transparent
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label24.Location = New System.Drawing.Point(437, 137)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(78, 16)
        Me.Label24.TabIndex = 8
        Me.Label24.Text = "Certificate"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.BackColor = System.Drawing.Color.Transparent
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label20.Location = New System.Drawing.Point(74, 110)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(60, 16)
        Me.Label20.TabIndex = 8
        Me.Label20.Text = "ESI No."
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.BackColor = System.Drawing.Color.Transparent
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label23.Location = New System.Drawing.Point(421, 110)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(94, 16)
        Me.Label23.TabIndex = 8
        Me.Label23.Text = "Qualification"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.BackColor = System.Drawing.Color.Transparent
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label21.Location = New System.Drawing.Point(445, 82)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(70, 16)
        Me.Label21.TabIndex = 8
        Me.Label21.Text = "Nominee"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.BackColor = System.Drawing.Color.Transparent
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label19.Location = New System.Drawing.Point(79, 81)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(55, 16)
        Me.Label19.TabIndex = 8
        Me.Label19.Text = "PF No."
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.BackColor = System.Drawing.Color.Transparent
        Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label25.Location = New System.Drawing.Point(48, 167)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(86, 16)
        Me.Label25.TabIndex = 8
        Me.Label25.Text = "OT Eligible"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.BackColor = System.Drawing.Color.Transparent
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label17.Location = New System.Drawing.Point(421, 53)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(93, 16)
        Me.Label17.TabIndex = 8
        Me.Label17.Text = "Base Salary"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.BackColor = System.Drawing.Color.Transparent
        Me.Label27.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label27.Location = New System.Drawing.Point(49, 53)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(85, 16)
        Me.Label27.TabIndex = 8
        Me.Label27.Text = "Pay Period"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.BackColor = System.Drawing.Color.Transparent
        Me.Label28.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label28.Location = New System.Drawing.Point(423, 24)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(91, 16)
        Me.Label28.TabIndex = 8
        Me.Label28.Text = "Designation"
        '
        'cmbDept
        '
        Me.cmbDept.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbDept.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbDept.FormattingEnabled = True
        Me.cmbDept.Location = New System.Drawing.Point(142, 19)
        Me.cmbDept.Name = "cmbDept"
        Me.cmbDept.Size = New System.Drawing.Size(240, 24)
        Me.cmbDept.TabIndex = 0
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.BackColor = System.Drawing.Color.Transparent
        Me.Label30.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label30.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label30.Location = New System.Drawing.Point(46, 23)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(88, 16)
        Me.Label30.TabIndex = 6
        Me.Label30.Text = "Department"
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.GroupBox4)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(777, 374)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Personal"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.GroupBox4.Controls.Add(Me.txtEmpMob1)
        Me.GroupBox4.Controls.Add(Me.txtEmpMob)
        Me.GroupBox4.Controls.Add(Me.Label77)
        Me.GroupBox4.Controls.Add(Me.txtll1)
        Me.GroupBox4.Controls.Add(Me.txtLL)
        Me.GroupBox4.Controls.Add(Me.Label79)
        Me.GroupBox4.Controls.Add(Me.txtParMob1)
        Me.GroupBox4.Controls.Add(Me.txtParMob)
        Me.GroupBox4.Controls.Add(Me.Label80)
        Me.GroupBox4.Controls.Add(Me.txtReligion)
        Me.GroupBox4.Controls.Add(Me.Label76)
        Me.GroupBox4.Controls.Add(Me.txtRecmob1)
        Me.GroupBox4.Controls.Add(Me.txtRecMob)
        Me.GroupBox4.Controls.Add(Me.Label75)
        Me.GroupBox4.Controls.Add(Me.cbPermenant)
        Me.GroupBox4.Controls.Add(Me.cmbHandicapped)
        Me.GroupBox4.Controls.Add(Me.Label47)
        Me.GroupBox4.Controls.Add(Me.cmbBloodGroup)
        Me.GroupBox4.Controls.Add(Me.Label29)
        Me.GroupBox4.Controls.Add(Me.txtWeight)
        Me.GroupBox4.Controls.Add(Me.txtHeight)
        Me.GroupBox4.Controls.Add(Me.Label49)
        Me.GroupBox4.Controls.Add(Me.txtIDMark2)
        Me.GroupBox4.Controls.Add(Me.Label48)
        Me.GroupBox4.Controls.Add(Me.txtIDMark1)
        Me.GroupBox4.Controls.Add(Me.Label46)
        Me.GroupBox4.Controls.Add(Me.Label45)
        Me.GroupBox4.Controls.Add(Me.txtRecuritmentThrough)
        Me.GroupBox4.Controls.Add(Me.Label50)
        Me.GroupBox4.Controls.Add(Me.txtAddress2)
        Me.GroupBox4.Controls.Add(Me.txtAddress1)
        Me.GroupBox4.Controls.Add(Me.Label52)
        Me.GroupBox4.Controls.Add(Me.txtFamilyDetails)
        Me.GroupBox4.Controls.Add(Me.Label51)
        Me.GroupBox4.Controls.Add(Me.Label56)
        Me.GroupBox4.Location = New System.Drawing.Point(1, -2)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(775, 375)
        Me.GroupBox4.TabIndex = 5
        Me.GroupBox4.TabStop = False
        '
        'txtEmpMob1
        '
        Me.txtEmpMob1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmpMob1.Location = New System.Drawing.Point(206, 340)
        Me.txtEmpMob1.Name = "txtEmpMob1"
        Me.txtEmpMob1.Size = New System.Drawing.Size(285, 22)
        Me.txtEmpMob1.TabIndex = 43
        '
        'txtEmpMob
        '
        Me.txtEmpMob.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmpMob.Location = New System.Drawing.Point(155, 340)
        Me.txtEmpMob.Name = "txtEmpMob"
        Me.txtEmpMob.ReadOnly = True
        Me.txtEmpMob.Size = New System.Drawing.Size(48, 22)
        Me.txtEmpMob.TabIndex = 42
        Me.txtEmpMob.Text = "+91"
        '
        'Label77
        '
        Me.Label77.AutoSize = True
        Me.Label77.BackColor = System.Drawing.Color.Transparent
        Me.Label77.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label77.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label77.Location = New System.Drawing.Point(20, 343)
        Me.Label77.Name = "Label77"
        Me.Label77.Size = New System.Drawing.Size(129, 16)
        Me.Label77.TabIndex = 41
        Me.Label77.Text = "Employee Mobile"
        '
        'txtll1
        '
        Me.txtll1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtll1.Location = New System.Drawing.Point(205, 315)
        Me.txtll1.Name = "txtll1"
        Me.txtll1.Size = New System.Drawing.Size(285, 22)
        Me.txtll1.TabIndex = 40
        '
        'txtLL
        '
        Me.txtLL.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLL.Location = New System.Drawing.Point(154, 315)
        Me.txtLL.Name = "txtLL"
        Me.txtLL.Size = New System.Drawing.Size(48, 22)
        Me.txtLL.TabIndex = 39
        '
        'Label79
        '
        Me.Label79.AutoSize = True
        Me.Label79.BackColor = System.Drawing.Color.Transparent
        Me.Label79.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label79.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label79.Location = New System.Drawing.Point(36, 318)
        Me.Label79.Name = "Label79"
        Me.Label79.Size = New System.Drawing.Size(109, 16)
        Me.Label79.TabIndex = 38
        Me.Label79.Text = "Parents Phone"
        '
        'txtParMob1
        '
        Me.txtParMob1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtParMob1.Location = New System.Drawing.Point(205, 290)
        Me.txtParMob1.Name = "txtParMob1"
        Me.txtParMob1.Size = New System.Drawing.Size(285, 22)
        Me.txtParMob1.TabIndex = 37
        '
        'txtParMob
        '
        Me.txtParMob.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtParMob.Location = New System.Drawing.Point(154, 290)
        Me.txtParMob.Name = "txtParMob"
        Me.txtParMob.ReadOnly = True
        Me.txtParMob.Size = New System.Drawing.Size(48, 22)
        Me.txtParMob.TabIndex = 36
        Me.txtParMob.Text = "+91"
        '
        'Label80
        '
        Me.Label80.AutoSize = True
        Me.Label80.BackColor = System.Drawing.Color.Transparent
        Me.Label80.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label80.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label80.Location = New System.Drawing.Point(36, 293)
        Me.Label80.Name = "Label80"
        Me.Label80.Size = New System.Drawing.Size(112, 16)
        Me.Label80.TabIndex = 35
        Me.Label80.Text = "Parents Mobile"
        '
        'txtReligion
        '
        Me.txtReligion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtReligion.Location = New System.Drawing.Point(154, 150)
        Me.txtReligion.Name = "txtReligion"
        Me.txtReligion.Size = New System.Drawing.Size(336, 22)
        Me.txtReligion.TabIndex = 34
        '
        'Label76
        '
        Me.Label76.AutoSize = True
        Me.Label76.BackColor = System.Drawing.Color.Transparent
        Me.Label76.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label76.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label76.Location = New System.Drawing.Point(82, 152)
        Me.Label76.Name = "Label76"
        Me.Label76.Size = New System.Drawing.Size(66, 16)
        Me.Label76.TabIndex = 33
        Me.Label76.Text = "Religion"
        '
        'txtRecmob1
        '
        Me.txtRecmob1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRecmob1.Location = New System.Drawing.Point(205, 123)
        Me.txtRecmob1.Name = "txtRecmob1"
        Me.txtRecmob1.Size = New System.Drawing.Size(285, 22)
        Me.txtRecmob1.TabIndex = 32
        '
        'txtRecMob
        '
        Me.txtRecMob.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRecMob.Location = New System.Drawing.Point(154, 123)
        Me.txtRecMob.Name = "txtRecMob"
        Me.txtRecMob.ReadOnly = True
        Me.txtRecMob.Size = New System.Drawing.Size(48, 22)
        Me.txtRecMob.TabIndex = 31
        Me.txtRecMob.Text = "+91"
        '
        'Label75
        '
        Me.Label75.AutoSize = True
        Me.Label75.BackColor = System.Drawing.Color.Transparent
        Me.Label75.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label75.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label75.Location = New System.Drawing.Point(19, 126)
        Me.Label75.Name = "Label75"
        Me.Label75.Size = New System.Drawing.Size(134, 16)
        Me.Label75.TabIndex = 30
        Me.Label75.Text = "Recuriter's Mobile"
        '
        'cbPermenant
        '
        Me.cbPermenant.AutoSize = True
        Me.cbPermenant.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbPermenant.Location = New System.Drawing.Point(501, 181)
        Me.cbPermenant.Name = "cbPermenant"
        Me.cbPermenant.Size = New System.Drawing.Size(166, 20)
        Me.cbPermenant.TabIndex = 29
        Me.cbPermenant.Text = "Same as Permenant"
        Me.cbPermenant.UseVisualStyleBackColor = True
        '
        'cmbHandicapped
        '
        Me.cmbHandicapped.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbHandicapped.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbHandicapped.FormattingEnabled = True
        Me.cmbHandicapped.Items.AddRange(New Object() {"Yes", "No"})
        Me.cmbHandicapped.Location = New System.Drawing.Point(379, 232)
        Me.cmbHandicapped.Name = "cmbHandicapped"
        Me.cmbHandicapped.Size = New System.Drawing.Size(111, 24)
        Me.cmbHandicapped.TabIndex = 13
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.BackColor = System.Drawing.Color.Transparent
        Me.Label47.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label47.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label47.Location = New System.Drawing.Point(271, 236)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(102, 16)
        Me.Label47.TabIndex = 12
        Me.Label47.Text = "Handicapped"
        '
        'cmbBloodGroup
        '
        Me.cmbBloodGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbBloodGroup.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbBloodGroup.FormattingEnabled = True
        Me.cmbBloodGroup.Items.AddRange(New Object() {"NONE", "O− ", "O+ ", "A−", "A+", "B− ", "B+ ", "AB− ", "AB+"})
        Me.cmbBloodGroup.Location = New System.Drawing.Point(154, 232)
        Me.cmbBloodGroup.Name = "cmbBloodGroup"
        Me.cmbBloodGroup.Size = New System.Drawing.Size(111, 24)
        Me.cmbBloodGroup.TabIndex = 13
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.BackColor = System.Drawing.Color.Transparent
        Me.Label29.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label29.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label29.Location = New System.Drawing.Point(58, 236)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(95, 16)
        Me.Label29.TabIndex = 12
        Me.Label29.Text = "Blood Group"
        '
        'txtWeight
        '
        Me.txtWeight.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWeight.Location = New System.Drawing.Point(379, 262)
        Me.txtWeight.Name = "txtWeight"
        Me.txtWeight.Size = New System.Drawing.Size(111, 22)
        Me.txtWeight.TabIndex = 3
        '
        'txtHeight
        '
        Me.txtHeight.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHeight.Location = New System.Drawing.Point(154, 262)
        Me.txtHeight.Name = "txtHeight"
        Me.txtHeight.Size = New System.Drawing.Size(111, 22)
        Me.txtHeight.TabIndex = 3
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.BackColor = System.Drawing.Color.Transparent
        Me.Label49.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label49.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label49.Location = New System.Drawing.Point(325, 265)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(56, 16)
        Me.Label49.TabIndex = 11
        Me.Label49.Text = "Weight"
        '
        'txtIDMark2
        '
        Me.txtIDMark2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIDMark2.Location = New System.Drawing.Point(154, 204)
        Me.txtIDMark2.Name = "txtIDMark2"
        Me.txtIDMark2.Size = New System.Drawing.Size(336, 22)
        Me.txtIDMark2.TabIndex = 3
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.BackColor = System.Drawing.Color.Transparent
        Me.Label48.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label48.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label48.Location = New System.Drawing.Point(100, 265)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(53, 16)
        Me.Label48.TabIndex = 11
        Me.Label48.Text = "Height"
        '
        'txtIDMark1
        '
        Me.txtIDMark1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIDMark1.Location = New System.Drawing.Point(154, 176)
        Me.txtIDMark1.Name = "txtIDMark1"
        Me.txtIDMark1.Size = New System.Drawing.Size(336, 22)
        Me.txtIDMark1.TabIndex = 3
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.BackColor = System.Drawing.Color.Transparent
        Me.Label46.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label46.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label46.Location = New System.Drawing.Point(7, 207)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(146, 16)
        Me.Label46.TabIndex = 11
        Me.Label46.Text = "Identification Mark 2"
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.BackColor = System.Drawing.Color.Transparent
        Me.Label45.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label45.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label45.Location = New System.Drawing.Point(7, 179)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(146, 16)
        Me.Label45.TabIndex = 11
        Me.Label45.Text = "Identification Mark 1"
        '
        'txtRecuritmentThrough
        '
        Me.txtRecuritmentThrough.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRecuritmentThrough.Location = New System.Drawing.Point(154, 98)
        Me.txtRecuritmentThrough.Name = "txtRecuritmentThrough"
        Me.txtRecuritmentThrough.Size = New System.Drawing.Size(336, 22)
        Me.txtRecuritmentThrough.TabIndex = 3
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.BackColor = System.Drawing.Color.Transparent
        Me.Label50.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label50.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label50.Location = New System.Drawing.Point(7, 101)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(151, 16)
        Me.Label50.TabIndex = 11
        Me.Label50.Text = "Recuritment Through"
        '
        'txtAddress2
        '
        Me.txtAddress2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddress2.Location = New System.Drawing.Point(501, 232)
        Me.txtAddress2.Multiline = True
        Me.txtAddress2.Name = "txtAddress2"
        Me.txtAddress2.Size = New System.Drawing.Size(266, 130)
        Me.txtAddress2.TabIndex = 2
        '
        'txtAddress1
        '
        Me.txtAddress1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddress1.Location = New System.Drawing.Point(501, 43)
        Me.txtAddress1.Multiline = True
        Me.txtAddress1.Name = "txtAddress1"
        Me.txtAddress1.Size = New System.Drawing.Size(266, 129)
        Me.txtAddress1.TabIndex = 2
        '
        'Label52
        '
        Me.Label52.AutoSize = True
        Me.Label52.BackColor = System.Drawing.Color.Transparent
        Me.Label52.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label52.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label52.Location = New System.Drawing.Point(504, 204)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(146, 16)
        Me.Label52.TabIndex = 8
        Me.Label52.Text = "Temporary Address"
        '
        'txtFamilyDetails
        '
        Me.txtFamilyDetails.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFamilyDetails.Location = New System.Drawing.Point(155, 23)
        Me.txtFamilyDetails.Multiline = True
        Me.txtFamilyDetails.Name = "txtFamilyDetails"
        Me.txtFamilyDetails.Size = New System.Drawing.Size(336, 72)
        Me.txtFamilyDetails.TabIndex = 2
        '
        'Label51
        '
        Me.Label51.AutoSize = True
        Me.Label51.BackColor = System.Drawing.Color.Transparent
        Me.Label51.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label51.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label51.Location = New System.Drawing.Point(498, 23)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(144, 16)
        Me.Label51.TabIndex = 8
        Me.Label51.Text = "Permenant Address"
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.BackColor = System.Drawing.Color.Transparent
        Me.Label56.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label56.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label56.Location = New System.Drawing.Point(43, 23)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(107, 16)
        Me.Label56.TabIndex = 8
        Me.Label56.Text = "Family Details"
        '
        'TabPage5
        '
        Me.TabPage5.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TabPage5.Controls.Add(Me.GroupBox6)
        Me.TabPage5.Location = New System.Drawing.Point(4, 22)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage5.Size = New System.Drawing.Size(777, 374)
        Me.TabPage5.TabIndex = 4
        Me.TabPage5.Text = "Experience"
        Me.TabPage5.UseVisualStyleBackColor = True
        '
        'GroupBox6
        '
        Me.GroupBox6.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.GroupBox6.Controls.Add(Me.Button2)
        Me.GroupBox6.Controls.Add(Me.Button1)
        Me.GroupBox6.Controls.Add(Me.DataGridView1)
        Me.GroupBox6.Controls.Add(Me.txtConll1)
        Me.GroupBox6.Controls.Add(Me.txtConll)
        Me.GroupBox6.Controls.Add(Me.Label106)
        Me.GroupBox6.Controls.Add(Me.txtConMob1)
        Me.GroupBox6.Controls.Add(Me.txtConMob)
        Me.GroupBox6.Controls.Add(Me.Label105)
        Me.GroupBox6.Controls.Add(Me.txtContactname)
        Me.GroupBox6.Controls.Add(Me.Label104)
        Me.GroupBox6.Controls.Add(Me.txtPeriodTo)
        Me.GroupBox6.Controls.Add(Me.Label102)
        Me.GroupBox6.Controls.Add(Me.txtPeriodFrom)
        Me.GroupBox6.Controls.Add(Me.Label103)
        Me.GroupBox6.Controls.Add(Me.txtPreviousDepartment)
        Me.GroupBox6.Controls.Add(Me.Label101)
        Me.GroupBox6.Controls.Add(Me.txtPreviousDesignation)
        Me.GroupBox6.Controls.Add(Me.Label100)
        Me.GroupBox6.Controls.Add(Me.txtCompanyName)
        Me.GroupBox6.Controls.Add(Me.Label98)
        Me.GroupBox6.Controls.Add(Me.cmbMonths)
        Me.GroupBox6.Controls.Add(Me.Label97)
        Me.GroupBox6.Controls.Add(Me.cmbYears)
        Me.GroupBox6.Controls.Add(Me.Label96)
        Me.GroupBox6.Location = New System.Drawing.Point(3, 0)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(774, 383)
        Me.GroupBox6.TabIndex = 1
        Me.GroupBox6.TabStop = False
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(410, 169)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 30)
        Me.Button2.TabIndex = 37
        Me.Button2.Text = "Clear"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(329, 169)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 30)
        Me.Button1.TabIndex = 16
        Me.Button1.Text = "Add"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.SiNo, Me.Years, Me.Months, Me.CompanyName, Me.Designation, Me.Department, Me.PeriodFrom, Me.PeriodTo, Me.ContactName, Me.Mobile, Me.Phone})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridView1.DefaultCellStyle = DataGridViewCellStyle2
        Me.DataGridView1.Location = New System.Drawing.Point(5, 201)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.RowHeadersVisible = False
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(764, 170)
        Me.DataGridView1.TabIndex = 36
        '
        'SiNo
        '
        Me.SiNo.HeaderText = "Si No"
        Me.SiNo.Name = "SiNo"
        Me.SiNo.ReadOnly = True
        Me.SiNo.Visible = False
        Me.SiNo.Width = 40
        '
        'Years
        '
        Me.Years.HeaderText = "Years"
        Me.Years.Name = "Years"
        Me.Years.ReadOnly = True
        Me.Years.Visible = False
        '
        'Months
        '
        Me.Months.HeaderText = "Months"
        Me.Months.Name = "Months"
        Me.Months.ReadOnly = True
        Me.Months.Visible = False
        '
        'CompanyName
        '
        Me.CompanyName.HeaderText = "Company Name"
        Me.CompanyName.Name = "CompanyName"
        Me.CompanyName.ReadOnly = True
        Me.CompanyName.Width = 200
        '
        'Designation
        '
        Me.Designation.HeaderText = "Designation"
        Me.Designation.Name = "Designation"
        Me.Designation.ReadOnly = True
        '
        'Department
        '
        Me.Department.HeaderText = "Department"
        Me.Department.Name = "Department"
        Me.Department.ReadOnly = True
        Me.Department.Visible = False
        '
        'PeriodFrom
        '
        Me.PeriodFrom.HeaderText = "From"
        Me.PeriodFrom.Name = "PeriodFrom"
        Me.PeriodFrom.ReadOnly = True
        Me.PeriodFrom.Width = 80
        '
        'PeriodTo
        '
        Me.PeriodTo.HeaderText = "To"
        Me.PeriodTo.Name = "PeriodTo"
        Me.PeriodTo.ReadOnly = True
        Me.PeriodTo.Width = 80
        '
        'ContactName
        '
        Me.ContactName.HeaderText = "Contact Name"
        Me.ContactName.Name = "ContactName"
        Me.ContactName.ReadOnly = True
        '
        'Mobile
        '
        Me.Mobile.HeaderText = "Mobile"
        Me.Mobile.Name = "Mobile"
        Me.Mobile.ReadOnly = True
        Me.Mobile.Width = 90
        '
        'Phone
        '
        Me.Phone.HeaderText = "Phone"
        Me.Phone.Name = "Phone"
        Me.Phone.ReadOnly = True
        Me.Phone.Width = 90
        '
        'txtConll1
        '
        Me.txtConll1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtConll1.Location = New System.Drawing.Point(553, 141)
        Me.txtConll1.Name = "txtConll1"
        Me.txtConll1.Size = New System.Drawing.Size(178, 22)
        Me.txtConll1.TabIndex = 35
        '
        'txtConll
        '
        Me.txtConll.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtConll.Location = New System.Drawing.Point(508, 141)
        Me.txtConll.Name = "txtConll"
        Me.txtConll.Size = New System.Drawing.Size(42, 22)
        Me.txtConll.TabIndex = 34
        '
        'Label106
        '
        Me.Label106.AutoSize = True
        Me.Label106.BackColor = System.Drawing.Color.Transparent
        Me.Label106.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label106.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label106.Location = New System.Drawing.Point(448, 146)
        Me.Label106.Name = "Label106"
        Me.Label106.Size = New System.Drawing.Size(52, 16)
        Me.Label106.TabIndex = 33
        Me.Label106.Text = "Phone"
        '
        'txtConMob1
        '
        Me.txtConMob1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtConMob1.Location = New System.Drawing.Point(203, 141)
        Me.txtConMob1.Name = "txtConMob1"
        Me.txtConMob1.Size = New System.Drawing.Size(147, 22)
        Me.txtConMob1.TabIndex = 32
        '
        'txtConMob
        '
        Me.txtConMob.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtConMob.Location = New System.Drawing.Point(160, 141)
        Me.txtConMob.Name = "txtConMob"
        Me.txtConMob.ReadOnly = True
        Me.txtConMob.Size = New System.Drawing.Size(39, 22)
        Me.txtConMob.TabIndex = 31
        Me.txtConMob.Text = "+91"
        '
        'Label105
        '
        Me.Label105.AutoSize = True
        Me.Label105.BackColor = System.Drawing.Color.Transparent
        Me.Label105.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label105.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label105.Location = New System.Drawing.Point(99, 144)
        Me.Label105.Name = "Label105"
        Me.Label105.Size = New System.Drawing.Size(55, 16)
        Me.Label105.TabIndex = 30
        Me.Label105.Text = "Mobile"
        '
        'txtContactname
        '
        Me.txtContactname.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtContactname.Location = New System.Drawing.Point(160, 117)
        Me.txtContactname.Name = "txtContactname"
        Me.txtContactname.Size = New System.Drawing.Size(571, 22)
        Me.txtContactname.TabIndex = 29
        '
        'Label104
        '
        Me.Label104.AutoSize = True
        Me.Label104.BackColor = System.Drawing.Color.Transparent
        Me.Label104.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label104.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label104.Location = New System.Drawing.Point(51, 119)
        Me.Label104.Name = "Label104"
        Me.Label104.Size = New System.Drawing.Size(105, 16)
        Me.Label104.TabIndex = 28
        Me.Label104.Text = "Contact Name"
        '
        'txtPeriodTo
        '
        Me.txtPeriodTo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPeriodTo.Location = New System.Drawing.Point(508, 92)
        Me.txtPeriodTo.Name = "txtPeriodTo"
        Me.txtPeriodTo.Size = New System.Drawing.Size(223, 22)
        Me.txtPeriodTo.TabIndex = 27
        '
        'Label102
        '
        Me.Label102.AutoSize = True
        Me.Label102.BackColor = System.Drawing.Color.Transparent
        Me.Label102.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label102.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label102.Location = New System.Drawing.Point(425, 95)
        Me.Label102.Name = "Label102"
        Me.Label102.Size = New System.Drawing.Size(77, 16)
        Me.Label102.TabIndex = 26
        Me.Label102.Text = "Period To"
        '
        'txtPeriodFrom
        '
        Me.txtPeriodFrom.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPeriodFrom.Location = New System.Drawing.Point(160, 92)
        Me.txtPeriodFrom.Name = "txtPeriodFrom"
        Me.txtPeriodFrom.Size = New System.Drawing.Size(189, 22)
        Me.txtPeriodFrom.TabIndex = 25
        '
        'Label103
        '
        Me.Label103.AutoSize = True
        Me.Label103.BackColor = System.Drawing.Color.Transparent
        Me.Label103.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label103.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label103.Location = New System.Drawing.Point(63, 92)
        Me.Label103.Name = "Label103"
        Me.Label103.Size = New System.Drawing.Size(93, 16)
        Me.Label103.TabIndex = 24
        Me.Label103.Text = "Period From"
        '
        'txtPreviousDepartment
        '
        Me.txtPreviousDepartment.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPreviousDepartment.Location = New System.Drawing.Point(508, 66)
        Me.txtPreviousDepartment.Name = "txtPreviousDepartment"
        Me.txtPreviousDepartment.Size = New System.Drawing.Size(223, 22)
        Me.txtPreviousDepartment.TabIndex = 23
        '
        'Label101
        '
        Me.Label101.AutoSize = True
        Me.Label101.BackColor = System.Drawing.Color.Transparent
        Me.Label101.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label101.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label101.Location = New System.Drawing.Point(414, 69)
        Me.Label101.Name = "Label101"
        Me.Label101.Size = New System.Drawing.Size(88, 16)
        Me.Label101.TabIndex = 22
        Me.Label101.Text = "Department"
        '
        'txtPreviousDesignation
        '
        Me.txtPreviousDesignation.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPreviousDesignation.Location = New System.Drawing.Point(160, 66)
        Me.txtPreviousDesignation.Name = "txtPreviousDesignation"
        Me.txtPreviousDesignation.Size = New System.Drawing.Size(189, 22)
        Me.txtPreviousDesignation.TabIndex = 21
        '
        'Label100
        '
        Me.Label100.AutoSize = True
        Me.Label100.BackColor = System.Drawing.Color.Transparent
        Me.Label100.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label100.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label100.Location = New System.Drawing.Point(63, 66)
        Me.Label100.Name = "Label100"
        Me.Label100.Size = New System.Drawing.Size(91, 16)
        Me.Label100.TabIndex = 20
        Me.Label100.Text = "Designation"
        '
        'txtCompanyName
        '
        Me.txtCompanyName.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCompanyName.Location = New System.Drawing.Point(160, 40)
        Me.txtCompanyName.Name = "txtCompanyName"
        Me.txtCompanyName.Size = New System.Drawing.Size(571, 22)
        Me.txtCompanyName.TabIndex = 17
        '
        'Label98
        '
        Me.Label98.AutoSize = True
        Me.Label98.BackColor = System.Drawing.Color.Transparent
        Me.Label98.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label98.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label98.Location = New System.Drawing.Point(38, 43)
        Me.Label98.Name = "Label98"
        Me.Label98.Size = New System.Drawing.Size(118, 16)
        Me.Label98.TabIndex = 16
        Me.Label98.Text = "Company Name"
        '
        'cmbMonths
        '
        Me.cmbMonths.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbMonths.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbMonths.FormattingEnabled = True
        Me.cmbMonths.Items.AddRange(New Object() {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11"})
        Me.cmbMonths.Location = New System.Drawing.Point(286, 13)
        Me.cmbMonths.Name = "cmbMonths"
        Me.cmbMonths.Size = New System.Drawing.Size(63, 24)
        Me.cmbMonths.TabIndex = 15
        '
        'Label97
        '
        Me.Label97.AutoSize = True
        Me.Label97.BackColor = System.Drawing.Color.Transparent
        Me.Label97.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label97.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label97.Location = New System.Drawing.Point(229, 16)
        Me.Label97.Name = "Label97"
        Me.Label97.Size = New System.Drawing.Size(57, 16)
        Me.Label97.TabIndex = 14
        Me.Label97.Text = "Months"
        '
        'cmbYears
        '
        Me.cmbYears.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cmbYears.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbYears.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbYears.FormattingEnabled = True
        Me.cmbYears.Items.AddRange(New Object() {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "16", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50"})
        Me.cmbYears.Location = New System.Drawing.Point(160, 13)
        Me.cmbYears.Name = "cmbYears"
        Me.cmbYears.Size = New System.Drawing.Size(63, 24)
        Me.cmbYears.TabIndex = 13
        '
        'Label96
        '
        Me.Label96.AutoSize = True
        Me.Label96.BackColor = System.Drawing.Color.Transparent
        Me.Label96.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label96.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label96.Location = New System.Drawing.Point(16, 17)
        Me.Label96.Name = "Label96"
        Me.Label96.Size = New System.Drawing.Size(138, 16)
        Me.Label96.TabIndex = 12
        Me.Label96.Text = "Experince in Years"
        '
        'TabPage4
        '
        Me.TabPage4.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TabPage4.Controls.Add(Me.GroupBox5)
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Size = New System.Drawing.Size(777, 374)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "Bank"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'GroupBox5
        '
        Me.GroupBox5.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.GroupBox5.Controls.Add(Me.txtAccountNo)
        Me.GroupBox5.Controls.Add(Me.Label78)
        Me.GroupBox5.Controls.Add(Me.txtBankName)
        Me.GroupBox5.Controls.Add(Me.txtBranchCode)
        Me.GroupBox5.Controls.Add(Me.Label84)
        Me.GroupBox5.Controls.Add(Me.Label85)
        Me.GroupBox5.Location = New System.Drawing.Point(1, -2)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(775, 373)
        Me.GroupBox5.TabIndex = 4
        Me.GroupBox5.TabStop = False
        '
        'txtAccountNo
        '
        Me.txtAccountNo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAccountNo.Location = New System.Drawing.Point(118, 85)
        Me.txtAccountNo.Name = "txtAccountNo"
        Me.txtAccountNo.Size = New System.Drawing.Size(376, 22)
        Me.txtAccountNo.TabIndex = 3
        '
        'Label78
        '
        Me.Label78.AutoSize = True
        Me.Label78.BackColor = System.Drawing.Color.Transparent
        Me.Label78.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label78.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label78.Location = New System.Drawing.Point(20, 88)
        Me.Label78.Name = "Label78"
        Me.Label78.Size = New System.Drawing.Size(91, 16)
        Me.Label78.TabIndex = 11
        Me.Label78.Text = "Account No."
        '
        'txtBankName
        '
        Me.txtBankName.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBankName.Location = New System.Drawing.Point(118, 29)
        Me.txtBankName.Name = "txtBankName"
        Me.txtBankName.Size = New System.Drawing.Size(376, 22)
        Me.txtBankName.TabIndex = 2
        '
        'txtBranchCode
        '
        Me.txtBranchCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBranchCode.Location = New System.Drawing.Point(118, 57)
        Me.txtBranchCode.Name = "txtBranchCode"
        Me.txtBranchCode.Size = New System.Drawing.Size(376, 22)
        Me.txtBranchCode.TabIndex = 2
        '
        'Label84
        '
        Me.Label84.AutoSize = True
        Me.Label84.BackColor = System.Drawing.Color.Transparent
        Me.Label84.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label84.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label84.Location = New System.Drawing.Point(14, 60)
        Me.Label84.Name = "Label84"
        Me.Label84.Size = New System.Drawing.Size(97, 16)
        Me.Label84.TabIndex = 8
        Me.Label84.Text = "Branch Code"
        '
        'Label85
        '
        Me.Label85.AutoSize = True
        Me.Label85.BackColor = System.Drawing.Color.Transparent
        Me.Label85.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label85.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label85.Location = New System.Drawing.Point(23, 32)
        Me.Label85.Name = "Label85"
        Me.Label85.Size = New System.Drawing.Size(88, 16)
        Me.Label85.TabIndex = 8
        Me.Label85.Text = "Bank Name"
        '
        'TabPage6
        '
        Me.TabPage6.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TabPage6.Controls.Add(Me.GroupBox7)
        Me.TabPage6.Location = New System.Drawing.Point(4, 22)
        Me.TabPage6.Name = "TabPage6"
        Me.TabPage6.Size = New System.Drawing.Size(777, 374)
        Me.TabPage6.TabIndex = 5
        Me.TabPage6.Text = "ID Proof"
        Me.TabPage6.UseVisualStyleBackColor = True
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.txtOthers)
        Me.GroupBox7.Controls.Add(Me.Label90)
        Me.GroupBox7.Controls.Add(Me.txtSmart)
        Me.GroupBox7.Controls.Add(Me.Label89)
        Me.GroupBox7.Controls.Add(Me.txtPanCard)
        Me.GroupBox7.Controls.Add(Me.Label88)
        Me.GroupBox7.Controls.Add(Me.txtRationCard)
        Me.GroupBox7.Controls.Add(Me.Label87)
        Me.GroupBox7.Controls.Add(Me.txtPassport)
        Me.GroupBox7.Controls.Add(Me.Label86)
        Me.GroupBox7.Controls.Add(Me.txtDriving)
        Me.GroupBox7.Controls.Add(Me.Label83)
        Me.GroupBox7.Controls.Add(Me.txtVoterId)
        Me.GroupBox7.Controls.Add(Me.Label82)
        Me.GroupBox7.Controls.Add(Me.txtAdharCard)
        Me.GroupBox7.Controls.Add(Me.lblAdharCard)
        Me.GroupBox7.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(774, 374)
        Me.GroupBox7.TabIndex = 0
        Me.GroupBox7.TabStop = False
        '
        'txtOthers
        '
        Me.txtOthers.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOthers.Location = New System.Drawing.Point(224, 301)
        Me.txtOthers.Name = "txtOthers"
        Me.txtOthers.Size = New System.Drawing.Size(336, 22)
        Me.txtOthers.TabIndex = 26
        Me.txtOthers.Text = "0"
        '
        'Label90
        '
        Me.Label90.AutoSize = True
        Me.Label90.BackColor = System.Drawing.Color.Transparent
        Me.Label90.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label90.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label90.Location = New System.Drawing.Point(77, 304)
        Me.Label90.Name = "Label90"
        Me.Label90.Size = New System.Drawing.Size(53, 16)
        Me.Label90.TabIndex = 27
        Me.Label90.Text = "Others"
        '
        'txtSmart
        '
        Me.txtSmart.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSmart.Location = New System.Drawing.Point(224, 261)
        Me.txtSmart.Name = "txtSmart"
        Me.txtSmart.Size = New System.Drawing.Size(336, 22)
        Me.txtSmart.TabIndex = 24
        Me.txtSmart.Text = "0"
        '
        'Label89
        '
        Me.Label89.AutoSize = True
        Me.Label89.BackColor = System.Drawing.Color.Transparent
        Me.Label89.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label89.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label89.Location = New System.Drawing.Point(77, 264)
        Me.Label89.Name = "Label89"
        Me.Label89.Size = New System.Drawing.Size(85, 16)
        Me.Label89.TabIndex = 25
        Me.Label89.Text = "Smart Card"
        '
        'txtPanCard
        '
        Me.txtPanCard.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPanCard.Location = New System.Drawing.Point(224, 222)
        Me.txtPanCard.Name = "txtPanCard"
        Me.txtPanCard.Size = New System.Drawing.Size(336, 22)
        Me.txtPanCard.TabIndex = 22
        Me.txtPanCard.Text = "0"
        '
        'Label88
        '
        Me.Label88.AutoSize = True
        Me.Label88.BackColor = System.Drawing.Color.Transparent
        Me.Label88.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label88.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label88.Location = New System.Drawing.Point(77, 225)
        Me.Label88.Name = "Label88"
        Me.Label88.Size = New System.Drawing.Size(68, 16)
        Me.Label88.TabIndex = 23
        Me.Label88.Text = "PanCard"
        '
        'txtRationCard
        '
        Me.txtRationCard.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRationCard.Location = New System.Drawing.Point(224, 184)
        Me.txtRationCard.Name = "txtRationCard"
        Me.txtRationCard.Size = New System.Drawing.Size(336, 22)
        Me.txtRationCard.TabIndex = 20
        Me.txtRationCard.Text = "0"
        '
        'Label87
        '
        Me.Label87.AutoSize = True
        Me.Label87.BackColor = System.Drawing.Color.Transparent
        Me.Label87.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label87.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label87.Location = New System.Drawing.Point(77, 187)
        Me.Label87.Name = "Label87"
        Me.Label87.Size = New System.Drawing.Size(90, 16)
        Me.Label87.TabIndex = 21
        Me.Label87.Text = "Ration Card"
        '
        'txtPassport
        '
        Me.txtPassport.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPassport.Location = New System.Drawing.Point(224, 146)
        Me.txtPassport.Name = "txtPassport"
        Me.txtPassport.Size = New System.Drawing.Size(336, 22)
        Me.txtPassport.TabIndex = 18
        Me.txtPassport.Text = "0"
        '
        'Label86
        '
        Me.Label86.AutoSize = True
        Me.Label86.BackColor = System.Drawing.Color.Transparent
        Me.Label86.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label86.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label86.Location = New System.Drawing.Point(77, 149)
        Me.Label86.Name = "Label86"
        Me.Label86.Size = New System.Drawing.Size(94, 16)
        Me.Label86.TabIndex = 19
        Me.Label86.Text = "Passport No"
        '
        'txtDriving
        '
        Me.txtDriving.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDriving.Location = New System.Drawing.Point(224, 109)
        Me.txtDriving.Name = "txtDriving"
        Me.txtDriving.Size = New System.Drawing.Size(336, 22)
        Me.txtDriving.TabIndex = 16
        Me.txtDriving.Text = "0"
        '
        'Label83
        '
        Me.Label83.AutoSize = True
        Me.Label83.BackColor = System.Drawing.Color.Transparent
        Me.Label83.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label83.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label83.Location = New System.Drawing.Point(77, 112)
        Me.Label83.Name = "Label83"
        Me.Label83.Size = New System.Drawing.Size(98, 16)
        Me.Label83.TabIndex = 17
        Me.Label83.Text = "Driving Lince"
        '
        'txtVoterId
        '
        Me.txtVoterId.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtVoterId.Location = New System.Drawing.Point(224, 70)
        Me.txtVoterId.Name = "txtVoterId"
        Me.txtVoterId.Size = New System.Drawing.Size(336, 22)
        Me.txtVoterId.TabIndex = 14
        Me.txtVoterId.Text = "0"
        '
        'Label82
        '
        Me.Label82.AutoSize = True
        Me.Label82.BackColor = System.Drawing.Color.Transparent
        Me.Label82.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label82.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label82.Location = New System.Drawing.Point(77, 73)
        Me.Label82.Name = "Label82"
        Me.Label82.Size = New System.Drawing.Size(64, 16)
        Me.Label82.TabIndex = 15
        Me.Label82.Text = "Voter ID"
        '
        'txtAdharCard
        '
        Me.txtAdharCard.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAdharCard.Location = New System.Drawing.Point(224, 33)
        Me.txtAdharCard.Name = "txtAdharCard"
        Me.txtAdharCard.Size = New System.Drawing.Size(336, 22)
        Me.txtAdharCard.TabIndex = 12
        Me.txtAdharCard.Text = "0"
        '
        'lblAdharCard
        '
        Me.lblAdharCard.AutoSize = True
        Me.lblAdharCard.BackColor = System.Drawing.Color.Transparent
        Me.lblAdharCard.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAdharCard.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.lblAdharCard.Location = New System.Drawing.Point(77, 36)
        Me.lblAdharCard.Name = "lblAdharCard"
        Me.lblAdharCard.Size = New System.Drawing.Size(86, 16)
        Me.lblAdharCard.TabIndex = 13
        Me.lblAdharCard.Text = "Adhar Card"
        '
        'btnSave
        '
        Me.btnSave.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Location = New System.Drawing.Point(229, 449)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 30)
        Me.btnSave.TabIndex = 4
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClear
        '
        Me.btnClear.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClear.Location = New System.Drawing.Point(310, 448)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(75, 30)
        Me.btnClear.TabIndex = 5
        Me.btnClear.Text = "C&lear"
        Me.btnClear.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExit.Location = New System.Drawing.Point(394, 448)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(75, 30)
        Me.btnExit.TabIndex = 6
        Me.btnExit.Text = "E&xit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'dlgOpen
        '
        Me.dlgOpen.FileName = "OpenFileDialog1"
        '
        'PictureBox3
        '
        Me.PictureBox3.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.PictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox3.Location = New System.Drawing.Point(451, 19)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(304, 245)
        Me.PictureBox3.TabIndex = 14
        Me.PictureBox3.TabStop = False
        '
        'DateTimePicker2
        '
        Me.DateTimePicker2.CustomFormat = "dd/MM/yyyy"
        Me.DateTimePicker2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DateTimePicker2.Location = New System.Drawing.Point(129, 200)
        Me.DateTimePicker2.Name = "DateTimePicker2"
        Me.DateTimePicker2.Size = New System.Drawing.Size(96, 22)
        Me.DateTimePicker2.TabIndex = 9
        '
        'TextBox8
        '
        Me.TextBox8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox8.Location = New System.Drawing.Point(325, 98)
        Me.TextBox8.Name = "TextBox8"
        Me.TextBox8.Size = New System.Drawing.Size(108, 22)
        Me.TextBox8.TabIndex = 4
        '
        'TextBox9
        '
        Me.TextBox9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox9.Location = New System.Drawing.Point(127, 123)
        Me.TextBox9.Name = "TextBox9"
        Me.TextBox9.Size = New System.Drawing.Size(306, 22)
        Me.TextBox9.TabIndex = 5
        '
        'TextBox10
        '
        Me.TextBox10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox10.Location = New System.Drawing.Point(268, 148)
        Me.TextBox10.Name = "TextBox10"
        Me.TextBox10.Size = New System.Drawing.Size(165, 22)
        Me.TextBox10.TabIndex = 7
        '
        'TextBox11
        '
        Me.TextBox11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox11.Location = New System.Drawing.Point(268, 200)
        Me.TextBox11.MaxLength = 2
        Me.TextBox11.Name = "TextBox11"
        Me.TextBox11.Size = New System.Drawing.Size(165, 22)
        Me.TextBox11.TabIndex = 10
        '
        'TextBox12
        '
        Me.TextBox12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox12.Location = New System.Drawing.Point(129, 148)
        Me.TextBox12.Name = "TextBox12"
        Me.TextBox12.Size = New System.Drawing.Size(47, 22)
        Me.TextBox12.TabIndex = 6
        '
        'TextBox13
        '
        Me.TextBox13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox13.Location = New System.Drawing.Point(129, 98)
        Me.TextBox13.Name = "TextBox13"
        Me.TextBox13.Size = New System.Drawing.Size(108, 22)
        Me.TextBox13.TabIndex = 3
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.BackColor = System.Drawing.Color.Transparent
        Me.Label31.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label31.Location = New System.Drawing.Point(185, 151)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(82, 16)
        Me.Label31.TabIndex = 11
        Me.Label31.Text = "Last Name"
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.BackColor = System.Drawing.Color.Transparent
        Me.Label32.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label32.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label32.Location = New System.Drawing.Point(231, 203)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(36, 16)
        Me.Label32.TabIndex = 11
        Me.Label32.Text = "Age"
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.BackColor = System.Drawing.Color.Transparent
        Me.Label33.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label33.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label33.Location = New System.Drawing.Point(40, 126)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(83, 16)
        Me.Label33.TabIndex = 11
        Me.Label33.Text = "First Name"
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.BackColor = System.Drawing.Color.Transparent
        Me.Label34.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label34.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label34.Location = New System.Drawing.Point(82, 151)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(41, 16)
        Me.Label34.TabIndex = 11
        Me.Label34.Text = "Intial"
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.BackColor = System.Drawing.Color.Transparent
        Me.Label35.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label35.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label35.Location = New System.Drawing.Point(236, 101)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(85, 16)
        Me.Label35.TabIndex = 11
        Me.Label35.Text = "Machine ID"
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.BackColor = System.Drawing.Color.Transparent
        Me.Label36.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label36.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label36.Location = New System.Drawing.Point(20, 101)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(103, 16)
        Me.Label36.TabIndex = 11
        Me.Label36.Text = "Existing Code"
        '
        'TextBox14
        '
        Me.TextBox14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox14.Location = New System.Drawing.Point(129, 73)
        Me.TextBox14.Name = "TextBox14"
        Me.TextBox14.Size = New System.Drawing.Size(108, 22)
        Me.TextBox14.TabIndex = 2
        '
        'ComboBox7
        '
        Me.ComboBox7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox7.FormattingEnabled = True
        Me.ComboBox7.Items.AddRange(New Object() {"Yes", "No"})
        Me.ComboBox7.Location = New System.Drawing.Point(642, 270)
        Me.ComboBox7.Name = "ComboBox7"
        Me.ComboBox7.Size = New System.Drawing.Size(113, 24)
        Me.ComboBox7.TabIndex = 13
        '
        'ComboBox8
        '
        Me.ComboBox8.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox8.FormattingEnabled = True
        Me.ComboBox8.Items.AddRange(New Object() {"On Roll", "Short Leave", "Long Leave", "Transfer"})
        Me.ComboBox8.Location = New System.Drawing.Point(129, 252)
        Me.ComboBox8.Name = "ComboBox8"
        Me.ComboBox8.Size = New System.Drawing.Size(304, 24)
        Me.ComboBox8.TabIndex = 12
        '
        'ComboBox9
        '
        Me.ComboBox9.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox9.FormattingEnabled = True
        Me.ComboBox9.Items.AddRange(New Object() {"None", "Single", "Married", "Separated", "Divorced", "Widowed", "Prefer not to answer"})
        Me.ComboBox9.Location = New System.Drawing.Point(129, 225)
        Me.ComboBox9.Name = "ComboBox9"
        Me.ComboBox9.Size = New System.Drawing.Size(304, 24)
        Me.ComboBox9.TabIndex = 11
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.BackColor = System.Drawing.Color.Transparent
        Me.Label37.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label37.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label37.Location = New System.Drawing.Point(569, 274)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(67, 16)
        Me.Label37.TabIndex = 8
        Me.Label37.Text = "Is Active"
        '
        'ComboBox10
        '
        Me.ComboBox10.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox10.FormattingEnabled = True
        Me.ComboBox10.Items.AddRange(New Object() {"Male", "Female"})
        Me.ComboBox10.Location = New System.Drawing.Point(129, 173)
        Me.ComboBox10.Name = "ComboBox10"
        Me.ComboBox10.Size = New System.Drawing.Size(143, 24)
        Me.ComboBox10.TabIndex = 8
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.BackColor = System.Drawing.Color.Transparent
        Me.Label38.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label38.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label38.Location = New System.Drawing.Point(37, 256)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(86, 16)
        Me.Label38.TabIndex = 8
        Me.Label38.Text = "Emp Status"
        '
        'ComboBox11
        '
        Me.ComboBox11.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox11.FormattingEnabled = True
        Me.ComboBox11.Items.AddRange(New Object() {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"})
        Me.ComboBox11.Location = New System.Drawing.Point(129, 46)
        Me.ComboBox11.Name = "ComboBox11"
        Me.ComboBox11.Size = New System.Drawing.Size(309, 24)
        Me.ComboBox11.TabIndex = 1
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.BackColor = System.Drawing.Color.Transparent
        Me.Label39.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label39.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label39.Location = New System.Drawing.Point(21, 229)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(102, 16)
        Me.Label39.TabIndex = 8
        Me.Label39.Text = "Marital Status"
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.BackColor = System.Drawing.Color.Transparent
        Me.Label40.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label40.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label40.Location = New System.Drawing.Point(71, 203)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(52, 16)
        Me.Label40.TabIndex = 8
        Me.Label40.Text = "D.O.B."
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.BackColor = System.Drawing.Color.Transparent
        Me.Label41.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label41.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label41.Location = New System.Drawing.Point(64, 177)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(59, 16)
        Me.Label41.TabIndex = 8
        Me.Label41.Text = "Gender"
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.BackColor = System.Drawing.Color.Transparent
        Me.Label42.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label42.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label42.Location = New System.Drawing.Point(69, 76)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(54, 16)
        Me.Label42.TabIndex = 8
        Me.Label42.Text = "Tkt No"
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.BackColor = System.Drawing.Color.Transparent
        Me.Label43.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label43.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label43.Location = New System.Drawing.Point(76, 50)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(47, 16)
        Me.Label43.TabIndex = 8
        Me.Label43.Text = "Prefix"
        '
        'ComboBox12
        '
        Me.ComboBox12.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox12.FormattingEnabled = True
        Me.ComboBox12.Location = New System.Drawing.Point(129, 19)
        Me.ComboBox12.Name = "ComboBox12"
        Me.ComboBox12.Size = New System.Drawing.Size(309, 24)
        Me.ComboBox12.TabIndex = 0
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.BackColor = System.Drawing.Color.Transparent
        Me.Label44.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label44.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label44.Location = New System.Drawing.Point(5, 23)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(118, 16)
        Me.Label44.TabIndex = 6
        Me.Label44.Text = "Employee Type"
        '
        'PictureBox5
        '
        Me.PictureBox5.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.PictureBox5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox5.Location = New System.Drawing.Point(451, 19)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(304, 245)
        Me.PictureBox5.TabIndex = 14
        Me.PictureBox5.TabStop = False
        '
        'DateTimePicker4
        '
        Me.DateTimePicker4.CustomFormat = "dd/MM/yyyy"
        Me.DateTimePicker4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateTimePicker4.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DateTimePicker4.Location = New System.Drawing.Point(129, 200)
        Me.DateTimePicker4.Name = "DateTimePicker4"
        Me.DateTimePicker4.Size = New System.Drawing.Size(96, 22)
        Me.DateTimePicker4.TabIndex = 9
        '
        'TextBox22
        '
        Me.TextBox22.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox22.Location = New System.Drawing.Point(325, 98)
        Me.TextBox22.Name = "TextBox22"
        Me.TextBox22.Size = New System.Drawing.Size(108, 22)
        Me.TextBox22.TabIndex = 4
        '
        'TextBox23
        '
        Me.TextBox23.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox23.Location = New System.Drawing.Point(127, 123)
        Me.TextBox23.Name = "TextBox23"
        Me.TextBox23.Size = New System.Drawing.Size(306, 22)
        Me.TextBox23.TabIndex = 5
        '
        'TextBox24
        '
        Me.TextBox24.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox24.Location = New System.Drawing.Point(268, 148)
        Me.TextBox24.Name = "TextBox24"
        Me.TextBox24.Size = New System.Drawing.Size(165, 22)
        Me.TextBox24.TabIndex = 7
        '
        'TextBox25
        '
        Me.TextBox25.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox25.Location = New System.Drawing.Point(268, 200)
        Me.TextBox25.MaxLength = 2
        Me.TextBox25.Name = "TextBox25"
        Me.TextBox25.Size = New System.Drawing.Size(165, 22)
        Me.TextBox25.TabIndex = 10
        '
        'TextBox26
        '
        Me.TextBox26.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox26.Location = New System.Drawing.Point(129, 148)
        Me.TextBox26.Name = "TextBox26"
        Me.TextBox26.Size = New System.Drawing.Size(47, 22)
        Me.TextBox26.TabIndex = 6
        '
        'TextBox27
        '
        Me.TextBox27.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox27.Location = New System.Drawing.Point(129, 98)
        Me.TextBox27.Name = "TextBox27"
        Me.TextBox27.Size = New System.Drawing.Size(108, 22)
        Me.TextBox27.TabIndex = 3
        '
        'Label59
        '
        Me.Label59.AutoSize = True
        Me.Label59.BackColor = System.Drawing.Color.Transparent
        Me.Label59.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label59.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label59.Location = New System.Drawing.Point(185, 151)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(82, 16)
        Me.Label59.TabIndex = 11
        Me.Label59.Text = "Last Name"
        '
        'Label60
        '
        Me.Label60.AutoSize = True
        Me.Label60.BackColor = System.Drawing.Color.Transparent
        Me.Label60.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label60.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label60.Location = New System.Drawing.Point(231, 203)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(36, 16)
        Me.Label60.TabIndex = 11
        Me.Label60.Text = "Age"
        '
        'Label61
        '
        Me.Label61.AutoSize = True
        Me.Label61.BackColor = System.Drawing.Color.Transparent
        Me.Label61.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label61.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label61.Location = New System.Drawing.Point(40, 126)
        Me.Label61.Name = "Label61"
        Me.Label61.Size = New System.Drawing.Size(83, 16)
        Me.Label61.TabIndex = 11
        Me.Label61.Text = "First Name"
        '
        'Label62
        '
        Me.Label62.AutoSize = True
        Me.Label62.BackColor = System.Drawing.Color.Transparent
        Me.Label62.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label62.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label62.Location = New System.Drawing.Point(82, 151)
        Me.Label62.Name = "Label62"
        Me.Label62.Size = New System.Drawing.Size(41, 16)
        Me.Label62.TabIndex = 11
        Me.Label62.Text = "Intial"
        '
        'Label63
        '
        Me.Label63.AutoSize = True
        Me.Label63.BackColor = System.Drawing.Color.Transparent
        Me.Label63.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label63.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label63.Location = New System.Drawing.Point(236, 101)
        Me.Label63.Name = "Label63"
        Me.Label63.Size = New System.Drawing.Size(85, 16)
        Me.Label63.TabIndex = 11
        Me.Label63.Text = "Machine ID"
        '
        'Label64
        '
        Me.Label64.AutoSize = True
        Me.Label64.BackColor = System.Drawing.Color.Transparent
        Me.Label64.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label64.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label64.Location = New System.Drawing.Point(20, 101)
        Me.Label64.Name = "Label64"
        Me.Label64.Size = New System.Drawing.Size(103, 16)
        Me.Label64.TabIndex = 11
        Me.Label64.Text = "Existing Code"
        '
        'TextBox28
        '
        Me.TextBox28.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox28.Location = New System.Drawing.Point(129, 73)
        Me.TextBox28.Name = "TextBox28"
        Me.TextBox28.Size = New System.Drawing.Size(108, 22)
        Me.TextBox28.TabIndex = 2
        '
        'ComboBox19
        '
        Me.ComboBox19.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox19.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox19.FormattingEnabled = True
        Me.ComboBox19.Items.AddRange(New Object() {"Yes", "No"})
        Me.ComboBox19.Location = New System.Drawing.Point(642, 270)
        Me.ComboBox19.Name = "ComboBox19"
        Me.ComboBox19.Size = New System.Drawing.Size(113, 24)
        Me.ComboBox19.TabIndex = 13
        '
        'ComboBox20
        '
        Me.ComboBox20.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox20.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox20.FormattingEnabled = True
        Me.ComboBox20.Items.AddRange(New Object() {"On Roll", "Short Leave", "Long Leave", "Transfer"})
        Me.ComboBox20.Location = New System.Drawing.Point(129, 252)
        Me.ComboBox20.Name = "ComboBox20"
        Me.ComboBox20.Size = New System.Drawing.Size(304, 24)
        Me.ComboBox20.TabIndex = 12
        '
        'ComboBox21
        '
        Me.ComboBox21.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox21.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox21.FormattingEnabled = True
        Me.ComboBox21.Items.AddRange(New Object() {"None", "Single", "Married", "Separated", "Divorced", "Widowed", "Prefer not to answer"})
        Me.ComboBox21.Location = New System.Drawing.Point(129, 225)
        Me.ComboBox21.Name = "ComboBox21"
        Me.ComboBox21.Size = New System.Drawing.Size(304, 24)
        Me.ComboBox21.TabIndex = 11
        '
        'Label65
        '
        Me.Label65.AutoSize = True
        Me.Label65.BackColor = System.Drawing.Color.Transparent
        Me.Label65.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label65.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label65.Location = New System.Drawing.Point(569, 274)
        Me.Label65.Name = "Label65"
        Me.Label65.Size = New System.Drawing.Size(67, 16)
        Me.Label65.TabIndex = 8
        Me.Label65.Text = "Is Active"
        '
        'ComboBox22
        '
        Me.ComboBox22.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox22.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox22.FormattingEnabled = True
        Me.ComboBox22.Items.AddRange(New Object() {"Male", "Female"})
        Me.ComboBox22.Location = New System.Drawing.Point(129, 173)
        Me.ComboBox22.Name = "ComboBox22"
        Me.ComboBox22.Size = New System.Drawing.Size(143, 24)
        Me.ComboBox22.TabIndex = 8
        '
        'Label66
        '
        Me.Label66.AutoSize = True
        Me.Label66.BackColor = System.Drawing.Color.Transparent
        Me.Label66.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label66.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label66.Location = New System.Drawing.Point(37, 256)
        Me.Label66.Name = "Label66"
        Me.Label66.Size = New System.Drawing.Size(86, 16)
        Me.Label66.TabIndex = 8
        Me.Label66.Text = "Emp Status"
        '
        'ComboBox23
        '
        Me.ComboBox23.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox23.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox23.FormattingEnabled = True
        Me.ComboBox23.Items.AddRange(New Object() {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"})
        Me.ComboBox23.Location = New System.Drawing.Point(129, 46)
        Me.ComboBox23.Name = "ComboBox23"
        Me.ComboBox23.Size = New System.Drawing.Size(309, 24)
        Me.ComboBox23.TabIndex = 1
        '
        'Label67
        '
        Me.Label67.AutoSize = True
        Me.Label67.BackColor = System.Drawing.Color.Transparent
        Me.Label67.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label67.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label67.Location = New System.Drawing.Point(21, 229)
        Me.Label67.Name = "Label67"
        Me.Label67.Size = New System.Drawing.Size(102, 16)
        Me.Label67.TabIndex = 8
        Me.Label67.Text = "Marital Status"
        '
        'Label68
        '
        Me.Label68.AutoSize = True
        Me.Label68.BackColor = System.Drawing.Color.Transparent
        Me.Label68.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label68.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label68.Location = New System.Drawing.Point(71, 203)
        Me.Label68.Name = "Label68"
        Me.Label68.Size = New System.Drawing.Size(52, 16)
        Me.Label68.TabIndex = 8
        Me.Label68.Text = "D.O.B."
        '
        'Label69
        '
        Me.Label69.AutoSize = True
        Me.Label69.BackColor = System.Drawing.Color.Transparent
        Me.Label69.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label69.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label69.Location = New System.Drawing.Point(64, 177)
        Me.Label69.Name = "Label69"
        Me.Label69.Size = New System.Drawing.Size(59, 16)
        Me.Label69.TabIndex = 8
        Me.Label69.Text = "Gender"
        '
        'Label70
        '
        Me.Label70.AutoSize = True
        Me.Label70.BackColor = System.Drawing.Color.Transparent
        Me.Label70.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label70.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label70.Location = New System.Drawing.Point(69, 76)
        Me.Label70.Name = "Label70"
        Me.Label70.Size = New System.Drawing.Size(54, 16)
        Me.Label70.TabIndex = 8
        Me.Label70.Text = "Tkt No"
        '
        'Label71
        '
        Me.Label71.AutoSize = True
        Me.Label71.BackColor = System.Drawing.Color.Transparent
        Me.Label71.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label71.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label71.Location = New System.Drawing.Point(76, 50)
        Me.Label71.Name = "Label71"
        Me.Label71.Size = New System.Drawing.Size(47, 16)
        Me.Label71.TabIndex = 8
        Me.Label71.Text = "Prefix"
        '
        'ComboBox24
        '
        Me.ComboBox24.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox24.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox24.FormattingEnabled = True
        Me.ComboBox24.Location = New System.Drawing.Point(129, 19)
        Me.ComboBox24.Name = "ComboBox24"
        Me.ComboBox24.Size = New System.Drawing.Size(309, 24)
        Me.ComboBox24.TabIndex = 0
        '
        'Label72
        '
        Me.Label72.AutoSize = True
        Me.Label72.BackColor = System.Drawing.Color.Transparent
        Me.Label72.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label72.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label72.Location = New System.Drawing.Point(5, 23)
        Me.Label72.Name = "Label72"
        Me.Label72.Size = New System.Drawing.Size(118, 16)
        Me.Label72.TabIndex = 6
        Me.Label72.Text = "Employee Type"
        '
        'cmdPrint
        '
        Me.cmdPrint.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdPrint.Location = New System.Drawing.Point(148, 448)
        Me.cmdPrint.Name = "cmdPrint"
        Me.cmdPrint.Size = New System.Drawing.Size(75, 30)
        Me.cmdPrint.TabIndex = 6
        Me.cmdPrint.Text = "&Print"
        Me.cmdPrint.UseVisualStyleBackColor = True
        Me.cmdPrint.Visible = False
        '
        'btnMachine_ID_Update
        '
        Me.btnMachine_ID_Update.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMachine_ID_Update.Location = New System.Drawing.Point(629, 539)
        Me.btnMachine_ID_Update.Name = "btnMachine_ID_Update"
        Me.btnMachine_ID_Update.Size = New System.Drawing.Size(144, 30)
        Me.btnMachine_ID_Update.TabIndex = 16
        Me.btnMachine_ID_Update.Text = "Machine ID Update"
        Me.btnMachine_ID_Update.UseVisualStyleBackColor = True
        Me.btnMachine_ID_Update.Visible = False
        '
        'PictureBox2
        '
        Me.PictureBox2.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox2.BackgroundImage = CType(resources.GetObject("PictureBox2.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.PictureBox2.Location = New System.Drawing.Point(1, 448)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(41, 32)
        Me.PictureBox2.TabIndex = 23
        Me.PictureBox2.TabStop = False
        '
        'BtnEPay_Save
        '
        Me.BtnEPay_Save.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnEPay_Save.Location = New System.Drawing.Point(586, 449)
        Me.BtnEPay_Save.Name = "BtnEPay_Save"
        Me.BtnEPay_Save.Size = New System.Drawing.Size(187, 30)
        Me.BtnEPay_Save.TabIndex = 93
        Me.BtnEPay_Save.Text = "New Employee E-Pay Save"
        Me.BtnEPay_Save.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(490, 449)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(75, 30)
        Me.Button3.TabIndex = 94
        Me.Button3.Text = "Encrypt"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'frmEmployee
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(780, 481)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.BtnEPay_Save)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.btnMachine_ID_Update)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.btnClear)
        Me.Controls.Add(Me.cmdPrint)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.Experience)
        Me.Controls.Add(Me.GroupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEmployee"
        Me.Text = "Employee Details"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.Experience.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.pbEmployee, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.TabPage3.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.TabPage5.ResumeLayout(False)
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage4.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.TabPage6.ResumeLayout(False)
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cmbLocCode As System.Windows.Forms.ComboBox
    Friend WithEvents cmbCompCode As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Experience As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents cmbEmpType As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cmbPrefix As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtEmpNo As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtMachineID As System.Windows.Forms.TextBox
    Friend WithEvents txtExistingCode As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtFirstName As System.Windows.Forms.TextBox
    Friend WithEvents txtLastName As System.Windows.Forms.TextBox
    Friend WithEvents txtInitial As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents cmbGender As System.Windows.Forms.ComboBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents pbEmployee As System.Windows.Forms.PictureBox
    Friend WithEvents dtpDOB As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtAge As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents cmbIsActive As System.Windows.Forms.ComboBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnClear As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents dlgOpen As System.Windows.Forms.OpenFileDialog
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtStdWrkHrs As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents txtDesignation As System.Windows.Forms.TextBox
    Friend WithEvents cmbOTEligible As System.Windows.Forms.ComboBox
    Friend WithEvents cmbPayPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents cmbDept As System.Windows.Forms.ComboBox
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents DateTimePicker2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents TextBox8 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox9 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox10 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox11 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox12 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox13 As System.Windows.Forms.TextBox
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents TextBox14 As System.Windows.Forms.TextBox
    Friend WithEvents ComboBox7 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox8 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox9 As System.Windows.Forms.ComboBox
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents ComboBox10 As System.Windows.Forms.ComboBox
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents ComboBox11 As System.Windows.Forms.ComboBox
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents ComboBox12 As System.Windows.Forms.ComboBox
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents txtAccountNo As System.Windows.Forms.TextBox
    Friend WithEvents Label78 As System.Windows.Forms.Label
    Friend WithEvents txtBranchCode As System.Windows.Forms.TextBox
    Friend WithEvents Label84 As System.Windows.Forms.Label
    Friend WithEvents Label85 As System.Windows.Forms.Label
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents DateTimePicker4 As System.Windows.Forms.DateTimePicker
    Friend WithEvents TextBox22 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox23 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox24 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox25 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox26 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox27 As System.Windows.Forms.TextBox
    Friend WithEvents Label59 As System.Windows.Forms.Label
    Friend WithEvents Label60 As System.Windows.Forms.Label
    Friend WithEvents Label61 As System.Windows.Forms.Label
    Friend WithEvents Label62 As System.Windows.Forms.Label
    Friend WithEvents Label63 As System.Windows.Forms.Label
    Friend WithEvents Label64 As System.Windows.Forms.Label
    Friend WithEvents TextBox28 As System.Windows.Forms.TextBox
    Friend WithEvents ComboBox19 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox20 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox21 As System.Windows.Forms.ComboBox
    Friend WithEvents Label65 As System.Windows.Forms.Label
    Friend WithEvents ComboBox22 As System.Windows.Forms.ComboBox
    Friend WithEvents Label66 As System.Windows.Forms.Label
    Friend WithEvents ComboBox23 As System.Windows.Forms.ComboBox
    Friend WithEvents Label67 As System.Windows.Forms.Label
    Friend WithEvents Label68 As System.Windows.Forms.Label
    Friend WithEvents Label69 As System.Windows.Forms.Label
    Friend WithEvents Label70 As System.Windows.Forms.Label
    Friend WithEvents Label71 As System.Windows.Forms.Label
    Friend WithEvents ComboBox24 As System.Windows.Forms.ComboBox
    Friend WithEvents Label72 As System.Windows.Forms.Label
    Friend WithEvents txtBaseSalary As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtESINo As System.Windows.Forms.TextBox
    Friend WithEvents txtNominee As System.Windows.Forms.TextBox
    Friend WithEvents txtPFNo As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents txtNationality As System.Windows.Forms.TextBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents txtCertificate As System.Windows.Forms.TextBox
    Friend WithEvents txtQualification As System.Windows.Forms.TextBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents dtpDOJ As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents txtRecuritmentThrough As System.Windows.Forms.TextBox
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents txtFamilyDetails As System.Windows.Forms.TextBox
    Friend WithEvents Label56 As System.Windows.Forms.Label
    Friend WithEvents cmbBloodGroup As System.Windows.Forms.ComboBox
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents txtIDMark2 As System.Windows.Forms.TextBox
    Friend WithEvents txtIDMark1 As System.Windows.Forms.TextBox
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents cmbHandicapped As System.Windows.Forms.ComboBox
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents txtWeight As System.Windows.Forms.TextBox
    Friend WithEvents txtHeight As System.Windows.Forms.TextBox
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents txtAddress2 As System.Windows.Forms.TextBox
    Friend WithEvents txtAddress1 As System.Windows.Forms.TextBox
    Friend WithEvents Label52 As System.Windows.Forms.Label
    Friend WithEvents Label51 As System.Windows.Forms.Label
    Friend WithEvents txtBankName As System.Windows.Forms.TextBox
    Friend WithEvents cmbEmpStatus As System.Windows.Forms.ComboBox
    Friend WithEvents cmbMarital As System.Windows.Forms.ComboBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents cmbSubCatName As System.Windows.Forms.ComboBox
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents cmbCatName As System.Windows.Forms.ComboBox
    Friend WithEvents Label53 As System.Windows.Forms.Label
    Friend WithEvents cmbShiftType As System.Windows.Forms.ComboBox
    Friend WithEvents Label55 As System.Windows.Forms.Label
    Friend WithEvents cmdPrint As System.Windows.Forms.Button
    Friend WithEvents chkNonAdmin As System.Windows.Forms.CheckBox
    Friend WithEvents txtTot_Work_Hours As System.Windows.Forms.TextBox
    Friend WithEvents Label57 As System.Windows.Forms.Label
    Friend WithEvents txtOT_Hours As System.Windows.Forms.TextBox
    Friend WithEvents Label58 As System.Windows.Forms.Label
    Friend WithEvents txtCalculate_Work_Hours As System.Windows.Forms.TextBox
    Friend WithEvents Label73 As System.Windows.Forms.Label
    Friend WithEvents cmbWages As System.Windows.Forms.ComboBox
    Friend WithEvents Label74 As System.Windows.Forms.Label
    Friend WithEvents TabPage5 As System.Windows.Forms.TabPage
    Friend WithEvents txtReligion As System.Windows.Forms.TextBox
    Friend WithEvents Label76 As System.Windows.Forms.Label
    Friend WithEvents txtRecmob1 As System.Windows.Forms.TextBox
    Friend WithEvents txtRecMob As System.Windows.Forms.TextBox
    Friend WithEvents Label75 As System.Windows.Forms.Label
    Friend WithEvents cbPermenant As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents SiNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Years As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Months As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CompanyName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Designation As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Department As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PeriodFrom As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PeriodTo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ContactName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Mobile As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Phone As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txtConll1 As System.Windows.Forms.TextBox
    Friend WithEvents txtConll As System.Windows.Forms.TextBox
    Friend WithEvents Label106 As System.Windows.Forms.Label
    Friend WithEvents txtConMob1 As System.Windows.Forms.TextBox
    Friend WithEvents txtConMob As System.Windows.Forms.TextBox
    Friend WithEvents Label105 As System.Windows.Forms.Label
    Friend WithEvents txtContactname As System.Windows.Forms.TextBox
    Friend WithEvents Label104 As System.Windows.Forms.Label
    Friend WithEvents txtPeriodTo As System.Windows.Forms.TextBox
    Friend WithEvents Label102 As System.Windows.Forms.Label
    Friend WithEvents txtPeriodFrom As System.Windows.Forms.TextBox
    Friend WithEvents Label103 As System.Windows.Forms.Label
    Friend WithEvents txtPreviousDepartment As System.Windows.Forms.TextBox
    Friend WithEvents Label101 As System.Windows.Forms.Label
    Friend WithEvents txtPreviousDesignation As System.Windows.Forms.TextBox
    Friend WithEvents Label100 As System.Windows.Forms.Label
    Friend WithEvents txtCompanyName As System.Windows.Forms.TextBox
    Friend WithEvents Label98 As System.Windows.Forms.Label
    Friend WithEvents cmbMonths As System.Windows.Forms.ComboBox
    Friend WithEvents Label97 As System.Windows.Forms.Label
    Friend WithEvents cmbYears As System.Windows.Forms.ComboBox
    Friend WithEvents Label96 As System.Windows.Forms.Label
    Friend WithEvents txtEmpMob1 As System.Windows.Forms.TextBox
    Friend WithEvents txtEmpMob As System.Windows.Forms.TextBox
    Friend WithEvents Label77 As System.Windows.Forms.Label
    Friend WithEvents txtll1 As System.Windows.Forms.TextBox
    Friend WithEvents txtLL As System.Windows.Forms.TextBox
    Friend WithEvents Label79 As System.Windows.Forms.Label
    Friend WithEvents txtParMob1 As System.Windows.Forms.TextBox
    Friend WithEvents txtParMob As System.Windows.Forms.TextBox
    Friend WithEvents Label80 As System.Windows.Forms.Label
    Friend WithEvents btnMachine_ID_Update As System.Windows.Forms.Button
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents cmbWeakOff As System.Windows.Forms.ComboBox
    Friend WithEvents Label81 As System.Windows.Forms.Label
    Friend WithEvents BtnEPay_Save As System.Windows.Forms.Button
    Friend WithEvents TabPage6 As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    Friend WithEvents txtAdharCard As System.Windows.Forms.TextBox
    Friend WithEvents lblAdharCard As System.Windows.Forms.Label
    Friend WithEvents txtDriving As System.Windows.Forms.TextBox
    Friend WithEvents Label83 As System.Windows.Forms.Label
    Friend WithEvents txtVoterId As System.Windows.Forms.TextBox
    Friend WithEvents Label82 As System.Windows.Forms.Label
    Friend WithEvents txtOthers As System.Windows.Forms.TextBox
    Friend WithEvents Label90 As System.Windows.Forms.Label
    Friend WithEvents txtSmart As System.Windows.Forms.TextBox
    Friend WithEvents Label89 As System.Windows.Forms.Label
    Friend WithEvents txtPanCard As System.Windows.Forms.TextBox
    Friend WithEvents Label88 As System.Windows.Forms.Label
    Friend WithEvents txtRationCard As System.Windows.Forms.TextBox
    Friend WithEvents Label87 As System.Windows.Forms.Label
    Friend WithEvents txtPassport As System.Windows.Forms.TextBox
    Friend WithEvents Label86 As System.Windows.Forms.Label
    Friend WithEvents txtAttn_Inc_Eligible As System.Windows.Forms.ComboBox
    Friend WithEvents Label91 As System.Windows.Forms.Label
    Friend WithEvents Button3 As System.Windows.Forms.Button
End Class
