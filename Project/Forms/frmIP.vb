﻿#Region "Imports"
Imports System
Imports System.Data
Imports System.Data.OleDb
#End Region

Public Class frmIP
    Dim mStatus As Long
    Private Sub frmIP_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormAlign(Me)
        Fill_Company_Details()
        Fill_Grid_Details()
    End Sub
    Public Sub Fill_Grid_Details()
        With fgIP
            .Clear()
            .Rows = 5
            .Row = 0
            .Col = 0
            .Text = "IP Address"
            .Row = 0
            .Col = 1
            .Text = "IP Mode"
            .Row = 0
            .Col = 2
            .Text = "Remarks"
            .CellAlignment = C1.Win.C1FlexGrid.Classic.AlignmentSettings.flexAlignCenterCenter
            .Cell(C1.Win.C1FlexGrid.Classic.CellPropertySettings.flexcpFontBold, 0, 0, 0, 2) = True
            .set_ColWidth(0, 150)
            .set_ColWidth(1, 130)
            .set_ColWidth(2, 100)
            .set_ColComboList(1, "IN|OUT|CANTEEN|SALARY")
        End With
    End Sub
    Public Sub Fill_Company_Details()
        SSQL = ""
        SSQL = "Select CompCode + ' | ' + CompName as [Name] from Company_Mst Order By CompName"

        mDataSet = ReturnMultipleValue(SSQL)

        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                cmbCompCode.Items.Clear()
                For iRow = 0 To .Rows.Count - 1
                    cmbCompCode.Items.Add(.Rows(iRow)(0))
                Next
            End If
        End With
    End Sub
    Private Sub cmbCompCode_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCompCode.LostFocus
        If Trim(cmbCompCode.Text) = "" Then Exit Sub
        Dim iStr() As String
        iStr = Split(Trim(cmbCompCode.Text), " | ")

        SSQL = ""
        SSQL = "Select LocCode + ' | ' + LocName as [Name] from Location_Mst Where Compcode='"
        SSQL = SSQL & "" & iStr(0) & "'"

        mDataSet = ReturnMultipleValue(SSQL)

        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                cmbLocCode.Items.Clear()
                For iRow = 0 To .Rows.Count - 1
                    cmbLocCode.Items.Add(.Rows(iRow)(0))
                Next
            End If
        End With
    End Sub
    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Close()
    End Sub
    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        Fill_Grid_Details()
    End Sub
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Trim(cmbCompCode.Text) = "" Then
            MessageBox.Show("Plase select company.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbCompCode.Focus()
            Exit Sub
        End If
        If Trim(cmbLocCode.Text) = "" Then
            MessageBox.Show("Plase select loaction.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbLocCode.Focus()
            Exit Sub
        End If
        For iRow = 1 To fgIP.Rows - 1
            If Trim(fgIP.get_TextMatrix(iRow, 0)) = "" Then GoTo 1
            If IsAddressValid(fgIP.get_TextMatrix(iRow, 0)) = False Then
                MessageBox.Show("Please Enter Valid IP Address.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
                fgIP.Cell(C1.Win.C1FlexGrid.Classic.CellPropertySettings.flexcpForeColor, iRow, 0) = Color.Red
                Exit Sub
1:          End If
        Next

        For iRow = 1 To fgIP.Rows - 1
            If Trim(fgIP.get_TextMatrix(iRow, 0)) = "" Then GoTo 2
            If My.Computer.Network.Ping(fgIP.get_TextMatrix(iRow, 0)) = False Then
                MessageBox.Show("Please Enter Correct IP.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
                fgIP.Cell(C1.Win.C1FlexGrid.Classic.CellPropertySettings.flexcpForeColor, iRow, 0) = Color.Red
                Exit Sub
2:          End If
        Next
3:
        For irow = 1 To fgIP.Rows - 1
            If Trim(fgIP.get_TextMatrix(irow, 0)) = "" Or Trim(fgIP.get_TextMatrix(irow, 1)) = "" Then
                fgIP.RemoveItem(irow)
                GoTo 3
            End If
        Next
        If fgIP.Rows = 1 Then
            Fill_Grid_Details()
            Exit Sub
        End If

        Dim iStr1() As String
        Dim iStr2() As String

        iStr1 = Split(cmbCompCode.Text, " | ")
        iStr2 = Split(cmbLocCode.Text, " | ")

        For iRow = 1 To fgIP.Rows - 1
            SSQL = ""
            SSQL = "Delete from IPAddress_Mst Where CompCode='" & iStr1(0) & "'"
            SSQL = SSQL & " and LocCode='" & iStr2(0) & "'"
            SSQL = SSQL & " And IPAddress='" & Remove_Single_Quote(fgIP.get_TextMatrix(iRow, 0)) & "'"
            mStatus = InsertDeleteUpdate(SSQL)

            SSQL = ""
            SSQL = "Insert into IPAddress_Mst Values( "
            SSQL = SSQL & "'" & iStr1(0) & "',"
            SSQL = SSQL & "'" & iStr2(0) & "',"
            SSQL = SSQL & "'" & Trim(Remove_Single_Quote(fgIP.get_TextMatrix(iRow, 0))) & "',"
            SSQL = SSQL & "'" & Trim(Remove_Single_Quote(fgIP.get_TextMatrix(iRow, 1))) & "',"
            SSQL = SSQL & "'" & Trim(Remove_Single_Quote(fgIP.get_TextMatrix(iRow, 2))) & "')"
            mStatus = InsertDeleteUpdate(SSQL)
        Next

        If mStatus > 0 Then
            MessageBox.Show("Successfully Saved.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Fill_Grid_Details()
            cmbLocCode.Focus()
            Exit Sub
        End If
    End Sub

    Private Sub cmbLocCode_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbLocCode.LostFocus

        Dim iStr1() As String
        Dim iStr2() As String

        iStr1 = Split(cmbCompCode.Text, " | ")
        iStr2 = Split(cmbLocCode.Text, " | ")

        Fill_Grid_Details()

        SSQL = ""
        SSQL = "Select * from IPAddress_Mst Where CompCode='" & iStr1(0) & "'"
        SSQL = SSQL & " and LocCode='" & iStr2(0) & "'"
        mDataSet = ReturnMultipleValue(SSQL)

        For iRow = 0 To mDataSet.Tables(0).Rows.Count - 1
            fgIP.set_TextMatrix(iRow + 1, 0, mDataSet.Tables(0).Rows(iRow)("IPAddress"))
            fgIP.set_TextMatrix(iRow + 1, 1, mDataSet.Tables(0).Rows(iRow)("IPMode"))
            fgIP.set_TextMatrix(iRow + 1, 2, mDataSet.Tables(0).Rows(iRow)("Remarks"))
        Next
    End Sub

    Private Sub fgIP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fgIP.Click

    End Sub
End Class