﻿Imports System
Imports System.Windows.Forms
Imports System.Threading
Imports System.Globalization
Imports System.Data.OleDb
Imports Microsoft.VisualBasic
Imports System.Net
Imports System.Net.Mail
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Windows.Forms
Imports CrystalDecisions.ReportSource
Public Class mdiMain
    Dim Flag As Boolean = False
    Dim mIpAddress_IN As String = ""
    Dim mIpAddress_OUT As String = ""
    Dim xlTitle As String = ""
    Dim xlFileName As String = ""
    Private shiftCount As Integer = 0
    Private Sub mdiMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim KeyName(3) As String
        Dim KeyValues(3) As String

        Dim Payroll_KeyName(3) As String
        Dim Payroll_KeyValues(3) As String

        mvarServerName = ""
        mvarUserName = ""
        mvarPassword = ""

        KeyName(1) = "ServerName"
        KeyName(2) = "UserName"
        KeyName(3) = "Password"

        ReadINIFile("C:\Canteen_eAlert.ini", "eAlert", KeyName, KeyValues)

        mvarServerName = KeyValues(1)
        mvarUserName = KeyValues(2)
        mvarPassword = KeyValues(3)

        'Payroll_Server
        'ReadINIFile("E:\Altius\Payroll.ini", "VLM-E-PAY", Payroll_KeyName, Payroll_KeyValues)
        ReadINIFile("C:\Canteen_eAlert.ini", "eAlert", Payroll_KeyName, Payroll_KeyValues)
        Payroll_mvarServerName = KeyValues(1)
        Payroll_mvarUserName = KeyValues(2)
        Payroll_mvarPassword = KeyValues(3)

        For Each ctl As Control In Me.Controls
            If TypeOf ctl Is MdiClient Then
                ctl.BackColor = Color.FromArgb(128, 128, 225)
            End If
        Next ctl
        Thread.CurrentThread.CurrentCulture = New CultureInfo("en-GB", False)

        btnLogout.Enabled = False
        With frmLogin
            .MdiParent = Me
            .Show()
            .BringToFront()
        End With
        'companydetails_load()
        'MonthlyRepoort()
    End Sub
    Public Sub Form_Load(ByVal mForm As Object)
        With mForm
            .MdiParent = Me
            .Show()
            .BringToFront()
        End With

    End Sub

    Private Sub btnCompany_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Form_Load(frmCompany)
    End Sub

    Private Sub btnLocation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Form_Load(frmLocation)
    End Sub

    Private Sub btnDepartment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Form_Load(frmDepartment)
    End Sub

    Private Sub btnEmpType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Form_Load(frmEmpType)
    End Sub

    Private Sub btnShift_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Form_Load(frmTimeDelete)
    End Sub

    Private Sub btnEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Form_Load(frmEmployee)
    End Sub

    Private Sub btnIPDetails_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Form_Load(frmIP)
    End Sub

    Private Sub btnMailAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Form_Load(frmIncentive)
    End Sub

    Private Sub btnMailSetup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Form_Load(frmNFGSalary)
    End Sub

    Private Sub btnReports_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Form_Load(frmReports)
    End Sub

    Private Sub btnDownClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Form_Load(frmDownClear)
    End Sub

    Private Sub btnSalDisb_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Form_Load(FrmSalayCover)
    End Sub

    Private Sub btnDesignation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Form_Load(frmDesign)
    End Sub

    Private Sub btnManAttn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Form_Load(frmManAttn)
    End Sub

    Private Sub btnMachineData_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMachineData.Click
        Form_Load(frmMealsDetails)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Form_Load(FrmShiftRecording)
    End Sub

    Private Sub btnShiftmaster_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Form_Load(FrmNFh)
    End Sub

    Private Sub btnUserFPDownload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Form_Load(FrmOTDistribute)
        Form_Load(FormDistributeBtn)
    End Sub

    Private Sub btnNfh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Form_Load(FrmNFh)
    End Sub

    Private Sub btnLogout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLogout.Click
        frmCompany.Dispose()
        frmLocation.Dispose()
        frmDepartment.Dispose()
        frmEmpType.Dispose()
        frmShift.Dispose()
        frmEmployee.Dispose()
        frmIP.Dispose()
        frmMailAdd.Dispose()
        frmMailSetup.Dispose()
        frmReports.Dispose()
        frmDownClear.Dispose()
        frmSalDisb.Dispose()
        frmDesign.Dispose()
        frmManAttn.Dispose()
        'frmMachineData.Dispose()
        FrmShiftRecording.Dispose()
        frmFingerPrintDownload.Dispose()
        FrmNFh.Dispose()
        FrmShiftRep.Dispose()
        FrmNFh.Dispose()
        frmMealsDetails.Dispose()
        Form_Load(frmLogin)
    End Sub
    Private Sub getdaywise(ByVal Ccode As String, ByVal Lcode As String)

        Dim Payroll_DS As New DataSet

        fg.CheckForIllegalCrossThreadCalls = False
        With fg
            .Clear()
            .Rows = 2
            .Cols = 16
            .set_TextMatrix(0, 0, "SNo")
            .set_TextMatrix(0, 1, "Dept")
            .set_TextMatrix(0, 2, "Type")
            .set_TextMatrix(0, 3, "Shift")
            .set_TextMatrix(0, 4, "EmpCode")
            .set_TextMatrix(0, 5, "Ex.Code")
            .set_TextMatrix(0, 6, "Name")
            .set_TextMatrix(0, 7, "TimeIN")
            .set_TextMatrix(0, 8, "TimeOUT")
            .set_TextMatrix(0, 9, "MachineID")
            .set_TextMatrix(0, 10, "Category")
            .set_TextMatrix(0, 11, "SubCategory")
            .set_TextMatrix(0, 12, "TotalMIN")
            .set_TextMatrix(0, 13, "GrandTOT")

            .set_TextMatrix(0, 14, "DayWages")
            .set_TextMatrix(0, 15, "WagesType")
        End With

        SSQL = ""
        SSQL = "Select IPAddress,IPMode from IPAddress_Mst Where Compcode='" & Ccode & "'"
        SSQL = SSQL & " And LocCode='" & Lcode & "'"

        mDataSet = ReturnMultipleValue(SSQL)

        If mDataSet.Tables(0).Rows.Count = 0 Then
            'MessageBox.Show("No Data Please Contact Your Administrator.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If

        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                For iRow = 0 To .Rows.Count - 1
                    If (.Rows(iRow)("IPMode")) = "IN" Then
                        mIpAddress_IN = (.Rows(iRow)("IPAddress"))
                    ElseIf (.Rows(iRow)("IPMode")) = "OUT" Then
                        mIpAddress_OUT = (.Rows(iRow)("IPAddress"))
                    End If
                Next
            End If
        End With



        Dim mLocalDS As New DataSet
        Dim qry As String = ""

        qry = ""
        qry = "Select shiftDesc,StartIN,EndIN,StartOUT,EndOUT,StartIN_Days,"
        qry = qry & " EndIN_Days,StartOUT_Days,EndOUT_Days "
        qry &= " from Shift_Mst Where CompCode='" & Ccode & "'"
        qry &= " And LocCode='" & Lcode & "'"

        'qry &= " And shiftDesc='" & Trim("All") & "'"

        qry &= " And ShiftDesc like '" & "" & "%'" 'Shift%'"
        qry &= " Order By shiftDesc"




        mLocalDS = ReturnMultipleValue(qry)

        If mLocalDS.Tables(0).Rows.Count < 0 Then Exit Sub

        fg.Rows = 1
        Dim mStartINRow1 As Integer = 1
        Dim mStartOUTRow1 As Integer = 1

        For iTabRow = 0 To mLocalDS.Tables(0).Rows.Count - 1

            If fg.Rows <= 1 Then
                mStartOUTRow1 = 1
            Else
                mStartOUTRow1 = fg.Rows - 1
            End If


            SSQL = ""
            SSQL = "Select MachineID,Min(TimeIN) as [TimeIN] from LogTime_IN "
            'SSQL = "Select MachineID,Min(convert( varchar(10), TimeIN, 102) +"
            'SSQL = SSQL & " stuff( right( convert( varchar(26), TimeIN, 109 ), 15 ), 7, 7, ' ' )) as [TimeIN] from LogTime_IN "
            SSQL = SSQL & " Where Compcode='" & Ccode & "'"
            SSQL = SSQL & " And LocCode='" & Lcode & "'"
            SSQL = SSQL & " And IPAddress='" & mIpAddress_IN & "'"

            SSQL = SSQL & " And TimeIN >='" & Date.Today.AddDays(-1).AddDays(mLocalDS.Tables(0).Rows(iTabRow)("StartIN_Days")).ToString("yyyy/MM/dd") _
                                            & " " & mLocalDS.Tables(0).Rows(iTabRow)("StartIN") & "'"

            SSQL = SSQL & " And TimeIN <='" & Date.Today.AddDays(-1).AddDays(mLocalDS.Tables(0).Rows(iTabRow)("EndIN_Days")).ToString("yyyy/MM/dd") _
                                            & " " & mLocalDS.Tables(0).Rows(iTabRow)("EndIN") & "'"
            SSQL = SSQL & " Group By MachineID"
            SSQL = SSQL & " Order By Min(TimeIN)"

            mDataSet = ReturnMultipleValue(SSQL)

            If mDataSet.Tables(0).Rows.Count > 0 Then

                'fg.Rows = fg.Rows + mDataSet.Tables(0).Rows.Count


                With mDataSet.Tables(0)
                    For iRow = 0 To .Rows.Count - 1
                        Dim chkduplicate As Boolean
                        chkduplicate = False
                        'Check Duplicate Entry
                        For ia = 0 To fg.Rows - 1
                            If Decryption(.Rows(iRow)("MachineID")) = fg.get_TextMatrix(ia, 9) Then
                                chkduplicate = True
                                Exit For
                            End If
                        Next
                        If chkduplicate = False Then


                            fg.Rows = fg.Rows + 1
                            fg.set_TextMatrix(mStartINRow1, 0, iRow + 1)
                            fg.set_TextMatrix(mStartINRow1, 3, mLocalDS.Tables(0).Rows(iTabRow)("ShiftDesc"))
                            'fg.set_TextMatrix(mStartINRow, 7, String.Format("{0:hh:mm tt}", (.Rows(iRow)("TimeIN"))))
                            fg.set_TextMatrix(mStartINRow1, 7, (.Rows(iRow)("TimeIN")))
                            fg.set_TextMatrix(mStartINRow1, 9, Decryption(.Rows(iRow)("MachineID")))
                            mStartINRow1 += 1

                        End If

                    Next
                End With
            End If


            '  MsgBox(mStartOUTRow)


            mDataSet = New DataSet

            SSQL = ""
            SSQL = "Select MachineID,Max(TimeOUT) as [TimeOUT] from LogTime_OUT "
            'SSQL = "Select MachineID,Min(convert( varchar(10), TimeOUT, 102) +"
            'SSQL = SSQL & " stuff( right( convert( varchar(26), TimeOUT, 109 ), 15 ), 7, 7, ' ' )) as [TimeOUT] from LogTime_OUT "

            SSQL = SSQL & " Where Compcode='" & Ccode & "'"
            SSQL = SSQL & " And LocCode='" & Lcode & "'"
            SSQL = SSQL & " And IPAddress='" & mIpAddress_OUT & "'"

            SSQL = SSQL & " And TimeOUT >='" & Date.Today.AddDays(-1).AddDays(mLocalDS.Tables(0).Rows(iTabRow)("StartOUT_Days")).ToString("yyyy/MM/dd") _
                                             & " " & mLocalDS.Tables(0).Rows(iTabRow)("StartOUT") & "'"

            SSQL = SSQL & " And TimeOUT <='" & Date.Today.AddDays(-1).AddDays(mLocalDS.Tables(0).Rows(iTabRow)("EndOUT_Days")).ToString("yyyy/MM/dd") _
                                             & " " & mLocalDS.Tables(0).Rows(iTabRow)("EndOUT") & "'"
            SSQL = SSQL & " Group By MachineID"
            SSQL = SSQL & " Order By Max(TimeOUT)"

            mDataSet = ReturnMultipleValue(SSQL)

            Dim InMachine_IP As String = ""
            Dim mLocalDS_out As New DataSet
            With mDataSet.Tables(0)
                'For iRow1 = 0 To .Rows.Count - 1
                ' fg1.set_TextMatrix(iRow1 + 1, 0, Trim(Decryption(.Rows(iRow1)("MachineID"))))
                For iRow2 = mStartOUTRow1 To fg.Rows - 1
                    InMachine_IP = Encryption(Trim(fg.get_TextMatrix(iRow2, 9)))
                    'TimeOUT Get
                    SSQL = "Select MachineID,TimeOUT from LogTime_OUT where MachineID='" & InMachine_IP & "' And Compcode='" & Ccode & "'"
                    SSQL = SSQL & " And LocCode='" & Lcode & "' And IPAddress='" & mIpAddress_OUT & "'"
                    SSQL = SSQL & " And TimeOUT >='" & Date.Today.AddDays(-1).AddDays(mLocalDS.Tables(0).Rows(iTabRow)("StartOUT_Days")).ToString("yyyy/MM/dd") & " " & "00:00" & "'"
                    SSQL = SSQL & " And TimeOUT <='" & Date.Today.AddDays(-1).AddDays(mLocalDS.Tables(0).Rows(iTabRow)("EndOUT_Days")).ToString("yyyy/MM/dd") & " " & "23:59" & "' Order by TimeOUT Desc"
                    mLocalDS_out = ReturnMultipleValue(SSQL)
                    If mLocalDS_out.Tables(0).Rows.Count <= 0 Then
                        'Skip
                    Else
                        If Trim(fg.get_TextMatrix(iRow2, 9)) = Trim(Decryption(mLocalDS_out.Tables(0).Rows(0)(0))) Then
                            fg.set_TextMatrix(iRow2, 8, String.Format("{0:hh:mm tt}", (mLocalDS_out.Tables(0).Rows(0)(1))))
                            'GoTo 1
                        End If
                    End If
                    'If Trim(fg.get_TextMatrix(iRow2, 9)) = Trim(Decryption(.Rows(iRow1)("MachineID"))) Then
                    '    'fg.set_TextMatrix(iRow2, 8, String.Format("{0:hh:mm tt}", (.Rows(iRow1)("TimeOUT"))))
                    '    fg.set_TextMatrix(iRow2, 8, (.Rows(iRow1)("TimeOUT")))
                    '    GoTo 1
                    'End If
                Next
1:
                'Next
            End With

            '            With mDataSet.Tables(0)
            '                For iRow1 = 0 To .Rows.Count - 1
            '                    ' fg1.set_TextMatrix(iRow1 + 1, 0, Trim(Decryption(.Rows(iRow1)("MachineID"))))
            '                    For iRow2 = mStartOUTRow1 To fg.Rows - 1
            '                        If Trim(fg.get_TextMatrix(iRow2, 9)) = Trim(Decryption(.Rows(iRow1)("MachineID"))) Then
            '                            'fg.set_TextMatrix(iRow2, 8, String.Format("{0:hh:mm tt}", (.Rows(iRow1)("TimeOUT"))))
            '                            fg.set_TextMatrix(iRow2, 8, (.Rows(iRow1)("TimeOUT")))
            '                            GoTo 1
            '                        End If
            '                    Next
            '1:
            '                Next
            '            End With

        Next


        Dim mEmployeeDS As New DataSet

        SSQL = ""
        SSQL = "select isnull(MachineID,'') as [MachineID],isnull(DeptName,'') as [DeptName]"
        SSQL = SSQL & ",isnull(TypeName,'') as [TypeName],EmpNo,isnull(ExistingCode,'') as [ExistingCode]"
        SSQL = SSQL & ",isnull(FirstName,'') + '.'+ isnull(MiddleInitial,'') as [FirstName]"
        SSQL = SSQL & ",isnull(CatName,'') as [CatName], isnull(SubCatName,'') as [SubCatName]"
        SSQL = SSQL & " from Employee_Mst Where Compcode='" & Ccode & "'"
        SSQL = SSQL & " And LocCode='" & Lcode & "' And IsActive='Yes'"

        mEmployeeDS = ReturnMultipleValue(SSQL)


        If mEmployeeDS.Tables(0).Rows.Count > 0 Then

            With mEmployeeDS.Tables(0)
                For iRow1 = 0 To .Rows.Count - 1
                    For iRow2 = 1 To fg.Rows - 1
                        If UCase(Trim(fg.get_TextMatrix(iRow2, 9))) = UCase(Trim(.Rows(iRow1)("MachineID"))) Then
                            fg.set_TextMatrix(iRow2, 1, Trim(.Rows(iRow1)("DeptName")))
                            fg.set_TextMatrix(iRow2, 2, Trim(.Rows(iRow1)("TypeName")))
                            fg.set_TextMatrix(iRow2, 4, Trim(.Rows(iRow1)("EmpNo")))
                            fg.set_TextMatrix(iRow2, 5, Trim(.Rows(iRow1)("ExistingCode")))
                            fg.set_TextMatrix(iRow2, 6, Trim(.Rows(iRow1)("FirstName")))
                            fg.set_TextMatrix(iRow2, 10, Trim(.Rows(iRow1)("CatName")))
                            fg.set_TextMatrix(iRow2, 11, Trim(.Rows(iRow1)("SubCatName")))

                            'Day Wages And Type Get
                            SSQL = "Select SM.Base as DaySalary,ED.EmployeeType,MET.EmpType as WagesType from"
                            SSQL = SSQL & " EmployeeDetails ED,SalaryMaster SM,MstEmployeeType MET where"
                            SSQL = SSQL & " ED.EmpNo=SM.EmpNo and MET.EmpTypeCd=ED.EmployeeType"
                            SSQL = SSQL & " and ED.BiometricID='" & fg.get_TextMatrix(iRow2, 9) & "'"
                            Payroll_DS = ReturnMultipleValue_Payroll(SSQL)
                            If Payroll_DS.Tables(0).Rows.Count <> 0 Then
                                If Payroll_DS.Tables(0).Rows(0)("EmployeeType") <> "1" Then
                                    fg.set_TextMatrix(iRow2, 14, Payroll_DS.Tables(0).Rows(0)("DaySalary"))
                                    fg.set_TextMatrix(iRow2, 15, Payroll_DS.Tables(0).Rows(0)("WagesType"))
                                Else
                                    fg.set_TextMatrix(iRow2, 14, "0.00")
                                    fg.set_TextMatrix(iRow2, 15, "")
                                End If
                            Else
                                fg.set_TextMatrix(iRow2, 14, "0.00")
                                fg.set_TextMatrix(iRow2, 15, "")
                            End If
                        End If
                    Next
                Next
                For irow2 = 1 To fg.Rows - 1
                    If UCase(Trim(fg.get_TextMatrix(irow2, 14))) = "" Then
                        fg.set_TextMatrix(irow2, 14, "0.00")
                        fg.set_TextMatrix(irow2, 15, "")
                    End If
                Next
            End With

        End If
        'fg.Cols = fg.Cols + 1
        For iRow = 1 To fg.Rows - 1
            Dim mLocalDS_INTAB As New DataSet()
            Dim mLocalDS_OUTTAB As New DataSet()
            Dim Date_Value_Str As String
            Dim Time_IN_Str As String = ""
            Dim Time_Out_Str As String = ""
            Dim time_Check_dbl As Int32 = 0
            Dim Total_Time_get As String = ""
            Dim Emp_Total_Work_Time_1 As String = "00:00"
            Date_Value_Str = Format(Date.Today.AddDays(-1).Date, "yyyy/MM/dd")
            If fg.get_TextMatrix(iRow, 7) <> "" And fg.get_TextMatrix(iRow, 8) <> "" Then
                Dim date1 As DateTime = System.Convert.ToDateTime(fg.get_TextMatrix(iRow, 7))
                Dim date2 As DateTime = System.Convert.ToDateTime(fg.get_TextMatrix(iRow, 8))
                Dim ts As New TimeSpan()
                ts = date2.Subtract(date1)
                ts = date2.Subtract(date1)
                'Grand Total multi in and Multi out 


                SSQL = "Select TimeIN from LogTime_IN where MachineID='" & Encryption(Trim(fg.get_TextMatrix(iRow, 9))) & "'"
                SSQL = SSQL & " And TimeIN >='" & Date_Value_Str & " " & "00:00' And TimeIN <='" & Date_Value_Str & " " & "23:59' Order by TimeIN ASC"
                mLocalDS_INTAB = ReturnMultipleValue(SSQL)

                SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" & Encryption(Trim(fg.get_TextMatrix(iRow, 9))) & "'"
                SSQL = SSQL & " And TimeOUT >='" & Date_Value_Str & " " & "00:00' And TimeOUT <='" & Date_Value_Str & " " & "23:59' Order by TimeOUT ASC"
                mLocalDS_OUTTAB = ReturnMultipleValue(SSQL)

                If mLocalDS_INTAB.Tables(0).Rows.Count > 1 Then
                    For tin = 0 To mLocalDS_INTAB.Tables(0).Rows.Count - 1
                        Time_IN_Str = mLocalDS_INTAB.Tables(0).Rows(tin)(0)

                        If mLocalDS_OUTTAB.Tables(0).Rows.Count > tin Then
                            Time_Out_Str = mLocalDS_OUTTAB.Tables(0).Rows(tin)(0)
                        ElseIf mLocalDS_OUTTAB.Tables(0).Rows.Count > mLocalDS_INTAB.Tables(0).Rows.Count Then
                            Time_Out_Str = mLocalDS_OUTTAB.Tables(0).Rows(mLocalDS_OUTTAB.Tables(0).Rows.Count - 1)(0)
                        Else
                            Time_Out_Str = ""
                        End If
                        Dim ts4 As New TimeSpan()
                        ts4 = Convert.ToDateTime(String.Format("{0:hh\:mm}", Emp_Total_Work_Time_1)).TimeOfDay()
                        If mLocalDS.Tables(0).Rows.Count <= 0 Then
                            Time_IN_Str = ""
                        Else
                            Time_IN_Str = mLocalDS_INTAB.Tables(0).Rows(tin)(0)
                        End If
                        'For tout = 0 To mLocalDS1.Tables(0).Rows.Count - 1
                        '    If mLocalDS1.Tables(0).Rows.Count <= 0 Then
                        '        Time_Out_Str = ""
                        '    Else
                        '        Time_Out_Str = mLocalDS1.Tables(0).Rows(0)(0)
                        '    End If

                        'Next
                        '    Time_Out_Str = mLocalDS1.Tables(0).Rows(tin)(0)

                        'Emp_Total_Work_Time
                        If Time_IN_Str = "" Or Time_Out_Str = "" Then
                            time_Check_dbl = time_Check_dbl
                        Else
                            Dim date3 As DateTime = System.Convert.ToDateTime(Time_IN_Str)
                            Dim date4 As DateTime = System.Convert.ToDateTime(Time_Out_Str)
                            If date1 > date2 Then

                                date2 = System.Convert.ToDateTime(mLocalDS_OUTTAB.Tables(0).Rows(tin + 1)(0))
                            End If
                            Dim ts1 As New TimeSpan()
                            ts1 = date4.Subtract(date3)
                            ts1 = date4.Subtract(date3)
                            Total_Time_get = Trim(ts1.Hours) '& ":" & Trim(ts.Minutes)
                            ts4 = ts4.Add(ts1)
                            'OT Time Get
                            Emp_Total_Work_Time_1 = Trim(ts4.Hours) & ":" & Trim(ts4.Minutes)
                            'MessageBox.Show(Emp_Total_Work_Time_1.ToString() + "")
                            If Left_Val(Total_Time_get, 1) = "-" Then
                                date4 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1)
                                ts = date2.Subtract(date1)
                                ts = date2.Subtract(date1)
                                Total_Time_get = Trim(ts.Hours) '& ":" & Trim(ts.Minutes)
                                time_Check_dbl = Total_Time_get
                                'Total_Time_get = Right_Val(Total_Time_get, Len(Total_Time_get) - 1)
                                Emp_Total_Work_Time_1 = Trim(ts.Hours) & ":" & Trim(ts.Minutes)
                            Else
                                time_Check_dbl = time_Check_dbl + Total_Time_get
                            End If
                        End If
                    Next

                Else
                    Dim ts4 As New TimeSpan()
                    ts4 = Convert.ToDateTime(String.Format("{0:hh\:mm}", Emp_Total_Work_Time_1)).TimeOfDay()
                    If mLocalDS_INTAB.Tables(0).Rows.Count <= 0 Then
                        Time_IN_Str = ""
                    Else
                        Time_IN_Str = mLocalDS_INTAB.Tables(0).Rows(0)(0)
                    End If
                    For tout = 0 To mLocalDS_OUTTAB.Tables(0).Rows.Count - 1
                        If mLocalDS_OUTTAB.Tables(0).Rows.Count <= 0 Then
                            Time_Out_Str = ""
                        Else
                            Time_Out_Str = mLocalDS_OUTTAB.Tables(0).Rows(0)(0)
                        End If

                    Next
                    'Emp_Total_Work_Time
                    If Time_IN_Str = "" Or Time_Out_Str = "" Then
                        time_Check_dbl = 0
                    Else
                        Dim date3 As DateTime = System.Convert.ToDateTime(Time_IN_Str)
                        Dim date4 As DateTime = System.Convert.ToDateTime(Time_Out_Str)
                        Dim ts1 As New TimeSpan()
                        ts1 = date4.Subtract(date3)
                        ts1 = date4.Subtract(date3)
                        Total_Time_get = Trim(ts.Hours) '& ":" & Trim(ts.Minutes)
                        ts4 = ts4.Add(ts)
                        'OT Time Get
                        Emp_Total_Work_Time_1 = Trim(ts4.Hours) & ":" & Trim(ts4.Minutes)
                        'MessageBox.Show(Emp_Total_Work_Time_1.ToString() + "")
                        If Left_Val(Total_Time_get, 1) = "-" Then
                            date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1)
                            ts = date2.Subtract(date1)
                            ts = date2.Subtract(date1)
                            Total_Time_get = Trim(ts.Hours) '& ":" & Trim(ts.Minutes)
                            time_Check_dbl = Total_Time_get
                            'Total_Time_get = Right_Val(Total_Time_get, Len(Total_Time_get) - 1)
                            Emp_Total_Work_Time_1 = Trim(ts1.Hours) & ":" & Trim(ts1.Minutes)
                        Else
                            time_Check_dbl = Total_Time_get
                        End If
                    End If
                End If

                'End
                If Left_Val(Trim(ts.Hours), 1) = "-" Then
                    date2 = System.Convert.ToDateTime(fg.get_TextMatrix(iRow, 8)).AddDays(1)
                    ts = date2.Subtract(date1)
                    ts = date2.Subtract(date1)
                    fg.set_TextMatrix(iRow, 12, Trim(ts.Hours) & ":" & Trim(ts.Minutes))
                Else
                    fg.set_TextMatrix(iRow, 12, Trim(ts.Hours) & ":" & Trim(ts.Minutes))
                End If

                fg.set_TextMatrix(iRow, 13, Emp_Total_Work_Time_1)
                'fg.set_TextMatrix(iRow, fg.Cols - 1, Trim(ts.Hours) & ":" & Trim(ts.Minutes))
            End If
        Next

    End Sub
    Private Function get_details(ByVal Ccode As String, ByVal Lcode As String) As DataTable
        Dim table As New DataTable

        table.Columns.Add("CompanyName", GetType(String))
        table.Columns.Add("LocationName", GetType(String))
        table.Columns.Add("ShiftDate", GetType(DateTime))
        table.Columns.Add("SNo", GetType(Integer))
        table.Columns.Add("Dept", GetType(String))
        table.Columns.Add("Type", GetType(String))
        table.Columns.Add("Shift", GetType(String))
        table.Columns.Add("Category", GetType(String))
        table.Columns.Add("SubCategory", GetType(String))
        table.Columns.Add("EmpCode", GetType(String))
        table.Columns.Add("ExCode", GetType(String))
        table.Columns.Add("Name", GetType(String))
        table.Columns.Add("TimeIN", GetType(String))
        table.Columns.Add("TimeOUT", GetType(String))
        table.Columns.Add("MachineID", GetType(String))
        table.Columns.Add("PrepBy", GetType(String))
        table.Columns.Add("PrepDate", GetType(DateTime))
        table.Columns.Add("TotalMIN", GetType(String))
        table.Columns.Add("GrandTOT", GetType(String))

        table.Columns.Add("DayWages", GetType(String))
        table.Columns.Add("Wages_Type", GetType(String))


        Dim dtRow As DataRow = Nothing

        For iRow = 1 To fg.Rows - 1
            dtRow = table.NewRow()

            dtRow("CompanyName") = Trim(Ccode)
            dtRow("LocationName") = Trim(Lcode)
            dtRow("ShiftDate") = Date.Today
            dtRow("SNo") = Val(fg.get_TextMatrix(iRow, 0))
            dtRow("Dept") = Trim(fg.get_TextMatrix(iRow, 1))
            dtRow("Type") = Trim(fg.get_TextMatrix(iRow, 2))
            dtRow("Shift") = Trim(fg.get_TextMatrix(iRow, 3))
            dtRow("Category") = Trim(fg.get_TextMatrix(iRow, 10))
            dtRow("SubCategory") = Trim(fg.get_TextMatrix(iRow, 11))
            dtRow("EmpCode") = Trim(fg.get_TextMatrix(iRow, 4))
            dtRow("ExCode") = Trim(fg.get_TextMatrix(iRow, 5))
            dtRow("Name") = Trim(fg.get_TextMatrix(iRow, 6))
            If Trim(fg.get_TextMatrix(iRow, 7)) <> "" Then
                dtRow("TimeIN") = String.Format("{0:hh:mm tt}", CDate(Trim(fg.get_TextMatrix(iRow, 7))))
            Else
                dtRow("TimeIN") = String.Format("{0:hh:mm tt}", Trim(fg.get_TextMatrix(iRow, 7)))
            End If
            If Trim(fg.get_TextMatrix(iRow, 8)) <> "" Then
                dtRow("TimeOUT") = String.Format("{0:hh:mm tt}", CDate(Trim(fg.get_TextMatrix(iRow, 8))))
            Else
                dtRow("TimeOUT") = String.Format("{0:hh:mm tt}", Trim(fg.get_TextMatrix(iRow, 8)))
            End If

            dtRow("MachineID") = Trim(fg.get_TextMatrix(iRow, 9))
            dtRow("PrepBy") = "User"
            dtRow("PrepDate") = Date.Today
            'dtRow("TotalMIN") = Trim(fg.get_TextMatrix(iRow, fg.Cols - 2))
            'dtRow("GrandTOT") = Trim(fg.get_TextMatrix(iRow, fg.Cols - 1))

            dtRow("TotalMIN") = Trim(fg.get_TextMatrix(iRow, 12))
            dtRow("GrandTOT") = Trim(fg.get_TextMatrix(iRow, 13))

            dtRow("DayWages") = Trim(fg.get_TextMatrix(iRow, 14))
            dtRow("Wages_Type") = Trim(fg.get_TextMatrix(iRow, 15))
            table.Rows.Add(dtRow)
        Next
        'table.Rows.Add("CompanyName", "LocationName", Now.Date, 1, "Dept1", "Type", "Shift1", "SubCategory1", _
        '               "Category", "EmpCode", "ExCode", "Name", "11:00 AM", "01:00 PM", "MachineID", "PrepBy", DateTime.Now, br.ReadBytes(br.BaseStream.Length))

        Return table
    End Function
    Private Sub companydetails_load()
        Dim comp As String = "Select CompCode,LocCode from Location_Mst"
        Dim ds_comp As DataSet = ReturnMultipleValue(comp)
        Dim tmr_trigger As Boolean = False
        If ds_comp.Tables(0).Rows.Count > 0 Then
            Dim Dat1 As String = DateTime.Today().ToString("dd-MM-yyyy")
            For b As Integer = 0 To ds_comp.Tables(0).Rows.Count - 1
                Dim value_qry As String = "Select DateVal from MailTrigger where Ccode='" & ds_comp.Tables(0).Rows(b)("CompCode").ToString() & "' and Lcode='" & ds_comp.Tables(0).Rows(b)("LocCode").ToString() & "' and convert(varchar, DateVal,105)='" & Dat1 & "'"

                Dim val As String = ReturnSingleValue(value_qry)
                If (Trim(val) = "") Then
                    downloadLogDetails(ds_comp.Tables(0).Rows(b)("CompCode").ToString(), ds_comp.Tables(0).Rows(b)("LocCode").ToString())
                    tmr_trigger = True
                End If

            Next
            If tmr_trigger = True Then
                Dim timer As System.Timers.Timer = New System.Timers.Timer()
                timer.AutoReset = True
                timer.Interval = 20000
                AddHandler timer.Elapsed, AddressOf Timer1_Tick
                timer.Start()
            Else
                End
            End If

        Else

        End If
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Try

        
            Dim dt As Date = Date.Now()
            Dim strReportPath As String = ""
            Dim table As New DataTable
            Dim imp_table As New DataTable
            Dim cr As New ReportDocument
            Dim imp_cr As New ReportDocument
            Dim dt1 As Integer = Convert.ToInt32(Date.Now.Hour)

            If Flag = False Then
                If dt1 >= 9 Then
                    SSQL = ""
                    SSQL = "Select CompCode,LocCode,FromAdd,FromPWD,ToAdd,CCAdd,BCCAdd,Subject from MailAdd_Mst"
                    mDataSet = ReturnMultipleValue(SSQL)
                    If mDataSet.Tables(0).Rows.Count > 0 Then


                        For i As Integer = 0 To mDataSet.Tables(0).Rows.Count - 1
                            Dim Dat1 As String = DateTime.Today().ToString("dd-MM-yyyy")
                            Dim value_qry As String = "Select DateVal from MailTrigger where Ccode='" & mDataSet.Tables(0).Rows(i)("CompCode").ToString() & "' and Lcode='" & mDataSet.Tables(0).Rows(i)("LocCode").ToString() & "' and convert(varchar, DateVal,105)='" & Dat1 & "'"

                            Dim val As String = ReturnSingleValue(value_qry)
                            If (Trim(val) = "") Then
                                Dim Fromadd As String = mDataSet.Tables(0).Rows(i)("FromAdd").ToString()
                                Dim Frompwd As String = mDataSet.Tables(0).Rows(i)("FromPWD").ToString()
                                Dim ToAdd As String = mDataSet.Tables(0).Rows(i)("ToAdd").ToString()
                                Dim CCAdd1 As String = mDataSet.Tables(0).Rows(i)("CCAdd").ToString()
                                Dim BCCAdd1 As String = mDataSet.Tables(0).Rows(i)("BCCAdd").ToString()
                                Dim Subject As String = mDataSet.Tables(0).Rows(i)("Subject").ToString()
                                Dim Ccode1 As String = mDataSet.Tables(0).Rows(i)("CompCode").ToString()
                                Dim Lcode1 As String = mDataSet.Tables(0).Rows(i)("LocCode").ToString()
                                'downloadLogDetails(Ccode1, Lcode1)
                                getdaywise(Ccode1, Lcode1)
                                table = get_details(Ccode1, Lcode1)
                                strReportPath = Application.StartupPath & "\Reports\" & "Attendance2.rpt"
                                ds = New DataSet()
                                ds.Tables.Add(table)
                                cr.Load(strReportPath)
                                cr.SetDataSource(ds.Tables(0))

                                frmRepView.crViewer.ShowRefreshButton = False
                                frmRepView.crViewer.ShowCloseButton = False
                                frmRepView.crViewer.ShowGroupTreeButton = False
                                frmRepView.crViewer.ReportSource = cr
                                'frmRepView.Show()
                                Dim CrExportOptions As ExportOptions
                                Dim CrDiskFileDestinationOptions As New DiskFileDestinationOptions()
                                Dim CrFormatTypeOptions As New PdfRtfWordFormatOptions()
                                CrDiskFileDestinationOptions.DiskFileName = _
                                                            Application.StartupPath & "\MailReport\DailyAttenance" & "-" & Lcode1 & "-" & Date.Today.AddDays(-1).Day & "-" & Date.Today.Month & "-" & Date.Today.Year & ".pdf"
                                CrExportOptions = cr.ExportOptions
                                With CrExportOptions
                                    .ExportDestinationType = ExportDestinationType.DiskFile
                                    .ExportFormatType = ExportFormatType.PortableDocFormat
                                    .DestinationOptions = CrDiskFileDestinationOptions
                                    .FormatOptions = CrFormatTypeOptions

                                End With
                                cr.Export()

                                Dim myprocess As New System.Diagnostics.Process()
                                'myprocess.Start(Application.StartupPath & "\MailReport\DailyAttenance" & "-" & Date.Today.AddDays(-1).Day & "-" & Date.Today.Month & "-" & Date.Today.Year & ".pdf")
                                'myprocess.Close()

                                System.Diagnostics.Process.Start(Application.StartupPath & "\MailReport\DailyAttenance" & "-" & Lcode1 & "-" & Date.Today.AddDays(-1).Day & "-" & Date.Today.Month & "-" & Date.Today.Year & ".pdf")

                                cr = Nothing
                                'Dim madd1 As MailAddress = New MailAddress(Fromadd)
                                'Dim madd1 As MailAddress = New MailAddress("ramkumar@altius.co.in")


                                'Mail Code Start 
                                Dim msg As New MailMessage
                                'msg.From = (New MailAddress(Fromadd))
                                'msg.To.Add(New MailAddress(ToAdd))
                                'msg = New MailMessage(Fromadd, "padmanabhan@altius.co.in,malik@altius.co.in,sabapathy.m@altius.co.in")
                                msg = New MailMessage(Fromadd, ToAdd)
                                msg.Subject = Subject
                                msg.Body = Subject
                                If Trim(CCAdd1) = "" Then
                                Else
                                    msg.CC.Add(CCAdd1)
                                    'msg.CC.Add("malik@altius.co.in")

                                End If
                                If Trim(BCCAdd1) = "" Then
                                Else
                                    msg.Bcc.Add(BCCAdd1)
                                    'msg.Bcc.Add("raguram@altius.co.in")

                                End If
                                'If (ds.Tables(0).Rows.Count > 0) Then
                                msg.Attachments.Add(New Attachment(Application.StartupPath & "\MailReport\DailyAttenance" & "-" & Lcode1 & "-" & Date.Today.AddDays(-1).Day & "-" & Date.Today.Month & "-" & Date.Today.Year & ".pdf"))
                                'End If
                                msg.IsBodyHtml = True
                                Dim Smtp As New SmtpClient("smtp.gmail.com")
                                Smtp.Port = 587
                                Smtp.Host = "smtp.gmail.com"
                                Smtp.EnableSsl = True
                                Dim uidpwd As New Net.NetworkCredential(Fromadd, Frompwd)
                                Smtp.Credentials = uidpwd

                                Smtp.Send(msg)

                                'Imporper Punch Report
                                imp_table = GetAttdTable(Ccode1, Lcode1)
                                strReportPath = Application.StartupPath & "\Reports\" & "PresentAbstract.rpt"
                                ds = New DataSet()
                                ds.Tables.Add(imp_table)
                                imp_cr.Load(strReportPath)
                                imp_cr.SetDataSource(ds.Tables(0))

                                frmRepView.crViewer.ShowRefreshButton = False
                                frmRepView.crViewer.ShowCloseButton = False
                                frmRepView.crViewer.ShowGroupTreeButton = False
                                frmRepView.crViewer.ReportSource = imp_cr
                                'frmRepView.Show()
                                Dim CrExportOptions1 As ExportOptions
                                Dim CrDiskFileDestinationOptions1 As New DiskFileDestinationOptions()
                                Dim CrFormatTypeOptions1 As New PdfRtfWordFormatOptions()
                                CrDiskFileDestinationOptions1.DiskFileName = _
                                                            Application.StartupPath & "\MailReport\ImproperPunches" & "-" & Lcode1 & "-" & Date.Today.AddDays(-1).Day & "-" & Date.Today.Month & "-" & Date.Today.Year & ".pdf"
                                CrExportOptions1 = imp_cr.ExportOptions
                                With CrExportOptions1
                                    .ExportDestinationType = ExportDestinationType.DiskFile
                                    .ExportFormatType = ExportFormatType.PortableDocFormat
                                    .DestinationOptions = CrDiskFileDestinationOptions1
                                    .FormatOptions = CrFormatTypeOptions1

                                End With
                                imp_cr.Export()
                                Dim myprocess1 As New System.Diagnostics.Process()
                                System.Diagnostics.Process.Start(Application.StartupPath & "\MailReport\ImproperPunches" & "-" & Lcode1 & "-" & Date.Today.AddDays(-1).Day & "-" & Date.Today.Month & "-" & Date.Today.Year & ".pdf")
                                'cr.Close()

                                'Mail Code Start 
                                Dim msg1 As New MailMessage
                                msg1 = New MailMessage(Fromadd, ToAdd)
                                msg1.Subject = Subject
                                msg1.Body = Subject
                                If Trim(CCAdd1) = "" Then
                                Else
                                    msg1.CC.Add(CCAdd1)
                                End If
                                If Trim(BCCAdd1) = "" Then
                                Else
                                    msg1.Bcc.Add(BCCAdd1)
                                End If
                                msg1.Attachments.Add(New Attachment(Application.StartupPath & "\MailReport\ImproperPunches" & "-" & Lcode1 & "-" & Date.Today.AddDays(-1).Day & "-" & Date.Today.Month & "-" & Date.Today.Year & ".pdf"))
                                'End If
                                msg1.IsBodyHtml = True
                                Dim Smtp1 As New SmtpClient("smtp.gmail.com")
                                Smtp1.Port = 587
                                Smtp1.Host = "smtp.gmail.com"
                                Smtp1.EnableSsl = True
                                Dim uidpwd1 As New Net.NetworkCredential(Fromadd, Frompwd)
                                Smtp1.Credentials = uidpwd1

                                Smtp1.Send(msg1)


                                Dim insert_val As String = "Insert into MailTrigger (Ccode,Lcode,DateVal) values ('" & Ccode1 & "','" & Lcode1 & "',convert(datetime,'" & DateTime.Today & "',105))"
                                Dim mStatus As Long
                                mStatus = InsertDeleteUpdate(insert_val)

                                'Mail Code End

                            Else

                            End If


                        Next
                    End If
                    Flag = True
                    Timer1.Stop()
                    End
                Else

                End If
            End If
        Catch ex As Exception
            'MessageBox.Show("No Internet Connection" & ex.Message)
        End Try
    End Sub

    Public Function GetAttdTable(ByVal Ccode As String, ByVal Lcode As String) As DataTable

        Dim table As New DataTable

        table.Columns.Add("CompanyName", GetType(String))
        table.Columns.Add("LocationName", GetType(String))
        table.Columns.Add("ShiftDate", GetType(DateTime))
        table.Columns.Add("SNo", GetType(Integer))
        table.Columns.Add("Dept", GetType(String))
        table.Columns.Add("Type", GetType(String))
        table.Columns.Add("Shift", GetType(String))
        table.Columns.Add("Category", GetType(String))
        table.Columns.Add("SubCategory", GetType(String))
        table.Columns.Add("EmpCode", GetType(String))
        table.Columns.Add("ExCode", GetType(String))
        table.Columns.Add("Name", GetType(String))
        table.Columns.Add("TimeIN", GetType(String))
        table.Columns.Add("TimeOUT", GetType(String))
        table.Columns.Add("MachineID", GetType(String))
        table.Columns.Add("PrepBy", GetType(String))
        table.Columns.Add("PrepDate", GetType(DateTime))
        table.Columns.Add("TotalMIN", GetType(String))
        table.Columns.Add("GrandTOT", GetType(String))



        Dim dtRow As DataRow = Nothing

        For iRow = 1 To fg.Rows - 1
            dtRow = table.NewRow()

            dtRow("CompanyName") = Trim(Ccode)
            dtRow("LocationName") = Trim(Lcode)
            dtRow("ShiftDate") = Date.Today
            dtRow("SNo") = Val(fg.get_TextMatrix(iRow, 0))
            dtRow("Dept") = Trim(fg.get_TextMatrix(iRow, 1))
            dtRow("Type") = Trim(fg.get_TextMatrix(iRow, 2))
            dtRow("Shift") = Trim(fg.get_TextMatrix(iRow, 3))
            dtRow("Category") = Trim(fg.get_TextMatrix(iRow, 10))
            dtRow("SubCategory") = Trim(fg.get_TextMatrix(iRow, 11))
            dtRow("EmpCode") = Trim(fg.get_TextMatrix(iRow, 4))
            dtRow("ExCode") = Trim(fg.get_TextMatrix(iRow, 5))
            dtRow("Name") = Trim(fg.get_TextMatrix(iRow, 6))
            If Trim(fg.get_TextMatrix(iRow, 7)) <> "" Then
                dtRow("TimeIN") = String.Format("{0:hh:mm tt}", CDate(Trim(fg.get_TextMatrix(iRow, 7))))
            Else
                dtRow("TimeIN") = String.Format("{0:hh:mm tt}", Trim(fg.get_TextMatrix(iRow, 7)))
            End If
            If Trim(fg.get_TextMatrix(iRow, 8)) <> "" Then
                dtRow("TimeOUT") = String.Format("{0:hh:mm tt}", CDate(Trim(fg.get_TextMatrix(iRow, 8))))
            Else
                dtRow("TimeOUT") = String.Format("{0:hh:mm tt}", Trim(fg.get_TextMatrix(iRow, 8)))
            End If

            dtRow("MachineID") = Trim(fg.get_TextMatrix(iRow, 9))
            dtRow("PrepBy") = "User"
            dtRow("PrepDate") = Date.Today

            'dtRow("TotalMIN") = Trim(fg.get_TextMatrix(iRow, fg.Cols - 2))
            'dtRow("GrandTOT") = Trim(fg.get_TextMatrix(iRow, fg.Cols - 1))
            dtRow("TotalMIN") = Trim(fg.get_TextMatrix(iRow, 12))
            dtRow("GrandTOT") = Trim(fg.get_TextMatrix(iRow, 13))

            'dtRow("TotalMIN") = Trim(fg.get_TextMatrix(iRow, 12))
            'dtRow("GrandTOT") = Trim(fg.get_TextMatrix(iRow, 13))
            'Dim DayWagesCheck As Double = Format(fg.get_TextMatrix(iRow, 14), "0.00")
            'dtRow("DayWages") = Trim(fg.get_TextMatrix(iRow, 14))
            'dtRow("Wages_Type") = Trim(fg.get_TextMatrix(iRow, 15))

            table.Rows.Add(dtRow)
        Next

        Return table
    End Function

    Public Sub downloadLogDetails(ByVal Ccode As String, ByVal Lcode As String)
        Dim vTMachineNumber As Long
        Dim vSEnrollNumber As String = ""
        Dim vVerifyMode As Long
        Dim vYear As Long
        Dim vMonth As Long
        Dim vDay As Long
        Dim vHour As Long
        Dim vMinute As Long
        Dim vRet As Boolean
        Dim i As Long
        Dim mTableName As String = ""
        Dim bConn As Boolean
        Dim vInOutMode As Long
        Dim vsec As Long
        Dim Wcode As Long
        SSQL = ""
        SSQL = "Select CompCode,LocCode,IPAddress,IPMode from IPAddress_Mst where CompCode='" & Ccode & "' and LocCode='" & Lcode & "'"
        Dim ds As New DataSet
        ds = ReturnMultipleValue(SSQL)
        If ds.Tables(0).Rows.Count > 0 Then
            For a As Integer = 0 To ds.Tables(0).Rows.Count - 1
                If ds.Tables(0).Rows(a)("IPMode").ToString() = "IN" Then
                    mTableName = "LogTime_IN"
                ElseIf ds.Tables(0).Rows(a)("IPMode").ToString() = "OUT" Then
                    mTableName = "LogTime_OUT"
                Else
                    Exit Sub
                End If

                '("", "TMachineNo", "EnrollNo", "EMachineNo", "VeriMode", "DateTime")
                bConn = AxSB100PC1.Connect_Net(Trim(ds.Tables(0).Rows(a)("IPAddress").ToString()), 4370)
                'bConn = mdiMain.AxSB100PC1.Connect_Net(Trim(iStr3(0)), 4370)

                'mdiMain.AxSB100PC1.SetIPAddress(Trim(iStr3(0)), CLng("5005"), CLng("0"))

                If bConn = False Then
                    MessageBox.Show("Not connected.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                    Exit Sub
                End If

                vRet = AxSB100PC1.EnableDevice(1, False)

                If vRet = False Then
                    MessageBox.Show("Not connected.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                    Exit Sub
                End If

                vRet = AxSB100PC1.ReadAllGLogData(1)

                If vRet = False Then
                    ' MessageBox.Show(mdiMain.AxSB100PC1.GetLastError(vErrorCode), "Altius", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                    Exit Sub
                End If

                If vRet = True Then
                    i = 1
                    Do

                        'vRet = mdiMain.AxSB100PC1.GetAllGLogData(1, _
                        '                                 vTMachineNumber, _
                        '                                 vSEnrollNumber, _
                        '                                 vSMachineNumber, _
                        '                                 vVerifyMode, _
                        '                                 vInOutMode, _
                        '                                 vYear, _
                        '                                 vMonth, _
                        '                                 vDay, _
                        '                                 vHour, _
                        '                                 vMinute)



                        vRet = AxSB100PC1.SSR_GetGeneralLogData(vTMachineNumber, vSEnrollNumber, _
                                                        vVerifyMode, vInOutMode, _
                                                        vYear, vMonth, vDay, _
                                                        vHour, vMinute, vsec, Wcode)

                        If vRet = False Then Exit Do

                        ' MsgBox(iStr1(0) & iStr2(0) & iStr3(0) & vSEnrollNumber & CStr(vYear) & "/" & Format(vMonth, "0#") & "/" & Format(vDay, "0#") & _
                        '  " " & Format(vHour, "0#") & ":" & Format(vMinute, "0#"))


                        '================
                        SSQL = ""
                        SSQL = "Delete from " & mTableName & " Where CompCode='" & Trim(Ccode) & "'"
                        SSQL = SSQL & " And LocCode='" & Trim(Lcode) & "'"
                        SSQL = SSQL & " And IPAddress='" & Trim(ds.Tables(0).Rows(a)("IPAddress").ToString()) & "'"
                        SSQL = SSQL & " And MachineID='" & Encryption(vSEnrollNumber) & "'"

                        If mTableName = "LogTime_IN" Then
                            SSQL = SSQL & " And TimeIN='" & CStr(vYear) & "/" & Format(vMonth, "0#") & "/" & Format(vDay, "0#") & _
                                            " " & Format(vHour, "0#") & ":" & Format(vMinute, "0#") & "'"
                        Else
                            SSQL = SSQL & " And TimeOUT='" & CStr(vYear) & "/" & Format(vMonth, "0#") & "/" & Format(vDay, "0#") & _
                                            " " & Format(vHour, "0#") & ":" & Format(vMinute, "0#") & "'"

                        End If

                        Dim mStatus As Long

                        mStatus = InsertDeleteUpdate(SSQL)

                        SSQL = ""
                        SSQL = "Insert into " & mTableName & " Values("
                        SSQL = SSQL & "'" & Trim(Ccode) & "',"
                        SSQL = SSQL & "'" & Trim(Lcode) & "',"
                        SSQL = SSQL & "'" & Trim(ds.Tables(0).Rows(a)("IPAddress").ToString()) & "',"
                        SSQL = SSQL & "'" & Encryption(vSEnrollNumber) & "',"

                        If mTableName = "LogTime_IN" Then
                            SSQL = SSQL & "'" & CStr(vYear) & "/" & Format(vMonth, "0#") & "/" & Format(vDay, "0#") & _
                                            " " & Format(vHour, "0#") & ":" & Format(vMinute, "0#") & "')"
                        Else
                            SSQL = SSQL & "'" & CStr(vYear) & "/" & Format(vMonth, "0#") & "/" & Format(vDay, "0#") & _
                                            " " & Format(vHour, "0#") & ":" & Format(vMinute, "0#") & "')"

                        End If

                        mStatus = InsertDeleteUpdate(SSQL)

                        '=================

                        i = i + 1
                    Loop
                    AxSB100PC1.EnableDevice(1, True)
                    AxSB100PC1.RefreshData(1)
                End If

            Next

        End If
    End Sub
    Private Sub MonthlyRepoort()
        'Dim comp As String = "Select CompCode,LocCode from Location_Mst"
        'Dim ds_comp As DataSet = ReturnMultipleValue(comp)
        'Dim tmr_trigger1 As Boolean = False

        'If ds_comp.Tables(0).Rows.Count > 0 Then
        '    Dim Dat1 As String = DateTime.Today().AddMonths(-1).ToString("dd-MM-yyyy")
        '    For b As Integer = 0 To ds_comp.Tables(0).Rows.Count - 1
        '        Dim val_1 As String = "Select Month from MonthMail where Month='" & Convert.ToDateTime(Dat1).Month.ToString() & "' and Year='" & Convert.ToDateTime(Dat1).Year.ToString() & "' and Ccode='" & ds_comp.Tables(0).Rows(b)("CompCode").ToString() & "' and Lcode='" & ds_comp.Tables(0).Rows(b)("LocCode").ToString() & "'"
        '        Dim val_2 As String = ReturnSingleValue(val_1)
        '        If (Trim(val_2) = "") Then
        '            Download_Monthly(ds_comp.Tables(0).Rows(b)("CompCode").ToString(), ds_comp.Tables(0).Rows(b)("LocCode").ToString())
        '        End If
        '    Next
        'End If
    End Sub
    '    Public Sub Download_Monthly(ByVal Ccode As String, ByVal Lcode As String)
    '        Remove_Empty_Column()
    '        Dim Date2 As String = Date.Today.AddMonths(-1)
    '        Dim dayval As String
    '        Dim dpFrom As String = ("01-" & Convert.ToDateTime(Date2).Month & "-" & Convert.ToDateTime(Date2).Year).ToString()

    '        If (Convert.ToDateTime(Date2).Month.ToString() = "1") Then
    '            dayval = "31"
    '        ElseIf (Convert.ToDateTime(Date2).Month.ToString() = "3") Then
    '            dayval = "31"
    '        ElseIf (Convert.ToDateTime(Date2).Month.ToString() = "5") Then
    '            dayval = "31"
    '        ElseIf (Convert.ToDateTime(Date2).Month.ToString() = "7") Then
    '            dayval = "31"
    '        ElseIf (Convert.ToDateTime(Date2).Month.ToString() = "8") Then
    '            dayval = "31"
    '        ElseIf (Convert.ToDateTime(Date2).Month.ToString() = "10") Then
    '            dayval = "31"
    '        ElseIf (Convert.ToDateTime(Date2).Month.ToString() = "12") Then
    '            dayval = "31"
    '        ElseIf (Convert.ToDateTime(Date2).Month.ToString() = "4") Then
    '            dayval = "30"
    '        ElseIf (Convert.ToDateTime(Date2).Month.ToString() = "6") Then
    '            dayval = "30"
    '        ElseIf (Convert.ToDateTime(Date2).Month.ToString() = "9") Then
    '            dayval = "30"
    '        ElseIf (Convert.ToDateTime(Date2).Month.ToString() = "11") Then
    '            dayval = "30"
    '        ElseIf (Convert.ToDateTime(Date2).Month.ToString() = "2") Then
    '            Dim i As Integer = (Convert.ToInt32(Convert.ToDateTime(Date2).Month.ToString()))
    '            Dim j As Integer = (i Mod 4)
    '            If (j = 0) Then
    '                dayval = "29"
    '            Else
    '                dayval = "28"
    '            End If
    '        End If
    '        Dim dpTo As String = (dayval & "-" & Convert.ToDateTime(Date2).Month & "-" & Convert.ToDateTime(Date2).Year).ToString()
    '        xlFileName = "TOTAL DAYS ATTENDANCE - BETWEEN DATES - " & DateTime.Today.Day.ToString() & "-" & DateTime.Today.Month.ToString() & "-" & DateTime.Today.Year.ToString() & "-" & Lcode
    '        xlTitle = "TOTAL DAYS ATTENDANCE - BETWEEN DATES" & " - " & Lcode & " - " & Convert.ToDateTime(dpFrom.ToString()).ToString("dd/MMM/yyyy")
    '        xlTitle &= " To " & Convert.ToDateTime(dpTo.ToString()).ToString("dd/MMM/yyyy")


    '        Try

    '            If Convert.ToDateTime(dpFrom) > Convert.ToDateTime(dpTo) Then Exit Sub
    '            SSQL = ""
    '            SSQL = "Select IPAddress,IPMode from IPAddress_Mst Where Compcode='" & Ccode & "'"
    '            SSQL = SSQL & " And LocCode='" & Lcode & "'"

    '            mDataSet = ReturnMultipleValue(SSQL)

    '            If mDataSet.Tables(0).Rows.Count = 0 Then
    '                MessageBox.Show("No Data Please Contact Your Administrator.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '                Exit Sub
    '            End If

    '            With mDataSet.Tables(0)
    '                If .Rows.Count > 0 Then
    '                    For iRow = 0 To .Rows.Count - 1
    '                        If (.Rows(iRow)("IPMode")) = "IN" Then
    '                            mIpAddress_IN = (.Rows(iRow)("IPAddress"))
    '                        ElseIf (.Rows(iRow)("IPMode")) = "OUT" Then
    '                            mIpAddress_OUT = (.Rows(iRow)("IPAddress"))
    '                        End If
    '                    Next
    '                End If
    '            End With
    '            'SSQL = ""
    '            'SSQL = "Select MachineID from Employee_Mst Where ShiftType Like 'Shift%' "
    '            'SSQL = SSQL & " And CompCode='" & iStr1(0) & "'"
    '            'SSQL = SSQL & " And LocCode='" & iStr2(0) & "'"
    '            'SSQL = SSQL & " Order By convert(numeric,MachineID)"

    '            'mDataSet = Nothing
    '            'mDataSet = ReturnMultipleValue(SSQL)

    '            SSQL = ""
    '            SSQL = "select isnull(DeptName,'') as [DeptName], Cast(MachineID As int) As MachineID"
    '            SSQL = SSQL & ",EmpNo,isnull(ExistingCode,'') as [ExistingCode]"
    '            SSQL = SSQL & ",isnull(FirstName,'') + '.'+ isnull(MiddleInitial,'') as [FirstName]"
    '            'SSQL = SSQL & ",isnull(CatName,'') as [CatName], isnull(SubCatName,'') as [SubCatName]"
    '            SSQL = SSQL & " from Employee_Mst Where Compcode='" & Ccode & "'"
    '            SSQL = SSQL & " And LocCode='" & Lcode & "' And IsActive='Yes'"
    '            SSQL = SSQL & " Order By DeptName, MachineID"

    '            Dim dsEmployee As DataSet = ReturnMultipleValue(SSQL)

    '            If dsEmployee.Tables(0).Rows.Count <= 0 Then Exit Sub

    '            fg.Rows = 0
    '            fg.Cols = 0

    '            fg.Rows = dsEmployee.Tables(0).Rows.Count + 3
    '            'fg.Cols = 2
    '            fg.Cols = dsEmployee.Tables(0).Columns.Count + 1

    '            For j As Integer = 0 To dsEmployee.Tables(0).Columns.Count - 1
    '                fg.set_TextMatrix(0, j, dsEmployee.Tables(0).Columns(j).ColumnName)
    '            Next

    '            For i As Integer = 0 To dsEmployee.Tables(0).Rows.Count - 1
    '                For j As Integer = 0 To dsEmployee.Tables(0).Columns.Count - 1
    '                    'fg.set_TextMatrix(i + 2, 0, mDataSet.Tables(0).Rows(i)(0))
    '                    fg.set_TextMatrix(i + 3, j, dsEmployee.Tables(0).Rows(i)(j))
    '                Next
    '            Next

    '            SSQL = ""
    '            SSQL = "select ShiftDesc,StartIN,StartIN_Days,EndIN,EndIn_Days,StartOut,StartOut_Days,EndOut,EndOut_Days"
    '            SSQL = SSQL & " from Shift_Mst Where CompCode='" & Ccode & "'"
    '            SSQL = SSQL & " And LocCode='" & Lcode & "'"
    '            SSQL = SSQL & " And (ShiftDesc Like '%Shift%' Or ShiftDesc Like '%GENERAL%')"

    '            mDataSet = Nothing
    '            mDataSet = ReturnMultipleValue(SSQL)

    '            If mDataSet.Tables(0).Rows.Count <= 0 Then Exit Sub
    '            shiftCount = mDataSet.Tables(0).Rows.Count * 2 'For each shift IN and OUT Column should be included


    '            Dim iColVal As Long

    '            iColVal = DateDiff(DateInterval.Day, Convert.ToDateTime(dpFrom), Convert.ToDateTime(dpTo)) + 1

    '            Dim iDate As Date = Convert.ToDateTime(dpFrom)

    '            Dim mStartIN As String = "" : Dim mEndIN As String = ""
    '            Dim mStartIN_Days As Double = 0 : Dim mEndIN_Days As Double = 0

    '            Dim mStartOUT As String = "" : Dim mEndOUT As String = ""
    '            Dim mStartOUT_Days As Double = 0 : Dim mEndOUT_Days As Double = 0

    '            Dim mLocalDS As New DataSet
    '            mLocalDS = Nothing

    '            Do
    '                For iCol2 = 0 To mDataSet.Tables(0).Rows.Count - 1

    '                    mStartIN = mDataSet.Tables(0).Rows(iCol2)("StartIN")
    '                    mEndIN = mDataSet.Tables(0).Rows(iCol2)("EndIN")
    '                    mStartIN_Days = mDataSet.Tables(0).Rows(iCol2)("StartIN_Days")
    '                    mEndIN_Days = mDataSet.Tables(0).Rows(iCol2)("EndIn_Days")


    '                    mStartOUT = mDataSet.Tables(0).Rows(iCol2)("StartOUT")
    '                    mEndOUT = mDataSet.Tables(0).Rows(iCol2)("EndOUT")
    '                    mStartOUT_Days = mDataSet.Tables(0).Rows(iCol2)("StartOUT_Days")
    '                    mEndOUT_Days = mDataSet.Tables(0).Rows(iCol2)("EndOUT_Days")



    '                    fg.set_TextMatrix(0, fg.Cols - 1, iDate.ToString("dd/MM/yyyy"))
    '                    fg.set_TextMatrix(1, fg.Cols - 1, mDataSet.Tables(0).Rows(iCol2)("ShiftDesc"))
    '                    fg.set_TextMatrix(2, fg.Cols - 1, "IN")

    '                    SSQL = ""
    '                    SSQL = "Select MachineID,Min(TimeIN) as [TimeIN] from LogTime_IN Where Compcode='" & Ccode & "'"
    '                    SSQL = SSQL & " And LocCode='" & Lcode & "'"
    '                    SSQL = SSQL & " And IPAddress='" & mIpAddress_IN & "'"
    '                    SSQL = SSQL & " And TimeIN >='" & iDate.AddDays(mStartIN_Days).ToString("yyyy/MM/dd") & " " & Trim(mStartIN) & "'"
    '                    SSQL = SSQL & " And TimeIN <='" & iDate.AddDays(mEndIN_Days).ToString("yyyy/MM/dd") & " " & Trim(mEndIN) & "'"
    '                    SSQL = SSQL & " Group By MachineID"
    '                    SSQL = SSQL & " Order By Min(TimeIN)"

    '                    mLocalDS = Nothing
    '                    mLocalDS = ReturnMultipleValue(SSQL)

    '                    For Each dr As DataRow In mLocalDS.Tables(0).Rows
    '                        dr("MachineID") = Decryption(dr("MachineID"))
    '                    Next

    '                    If mLocalDS.Tables(0).Rows.Count > 0 Then
    '                        For dsRow = 0 To mLocalDS.Tables(0).Rows.Count - 1

    '                            For gridRow = 3 To fg.Rows - 1

    '                                If Trim(fg.get_TextMatrix(gridRow, 1)) = Trim(mLocalDS.Tables(0).Rows(dsRow)("MachineID")) Then
    '                                    fg.set_TextMatrix(gridRow, fg.Cols - 1, String.Format("{0:hh:mm tt}", (mLocalDS.Tables(0).Rows(dsRow)("TimeIN"))))
    '                                    GoTo 1
    '                                End If

    '                            Next
    '1:
    '                        Next
    '                    End If

    '                    fg.Cols = fg.Cols + 1

    '                    fg.set_TextMatrix(0, fg.Cols - 1, iDate.ToString("dd/MM/yyyy"))
    '                    fg.set_TextMatrix(1, fg.Cols - 1, mDataSet.Tables(0).Rows(iCol2)("ShiftDesc"))
    '                    fg.set_TextMatrix(2, fg.Cols - 1, "OUT")

    '                    SSQL = ""
    '                    SSQL = "Select MachineID,Max(TimeOUT) as [TimeOUT] from LogTime_OUT Where Compcode='" & Ccode & "'"
    '                    SSQL = SSQL & " And LocCode='" & Lcode & "'"
    '                    SSQL = SSQL & " And IPAddress='" & mIpAddress_OUT & "'"
    '                    SSQL = SSQL & " And TimeOUT >='" & Convert.ToDateTime(dpFrom).AddDays(mStartOUT_Days).ToString("yyyy/MM/dd") & " " & Trim(mStartOUT) & "'"
    '                    SSQL = SSQL & " And TimeOUT <='" & Convert.ToDateTime(dpTo).AddDays(mEndOUT_Days).ToString("yyyy/MM/dd") & " " & Trim(mEndOUT) & "'"
    '                    SSQL = SSQL & " Group By MachineID"
    '                    SSQL = SSQL & " Order By Max(TimeOUT)"

    '                    mLocalDS = Nothing
    '                    mLocalDS = ReturnMultipleValue(SSQL)

    '                    If mLocalDS.Tables(0).Rows.Count > 0 Then
    '                        For dsRow = 0 To mLocalDS.Tables(0).Rows.Count - 1

    '                            For gridRow = 3 To fg.Rows - 1

    '                                If Trim(fg.get_TextMatrix(gridRow, 0)) = Decryption(Trim(mLocalDS.Tables(0).Rows(dsRow)("MachineID"))) Then
    '                                    fg.set_TextMatrix(gridRow, fg.Cols - 1, String.Format("{0:hh:mm tt}", (mLocalDS.Tables(0).Rows(dsRow)("TimeOUT"))))
    '                                    GoTo 2
    '                                End If

    '                            Next
    '2:
    '                        Next
    '                    End If

    '                    fg.Cols = fg.Cols + 1
    '                Next
    '                iDate = iDate.AddDays(1)

    '                While iDate > Convert.ToDateTime(dpTo)
    '                    Exit Sub
    '                End While
    '            Loop
    '        Catch ex As Exception
    '            MessageBox.Show(ex.Message, "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '            Exit Sub
    '        End Try
    '    End Sub
    '    Public Sub Remove_Empty_Column()

    '        Dim mEmptyColCount As Integer = 0
    '1:
    '        For iRow = 3 To fg.Rows - 1
    '            mEmptyColCount = 0
    '            For iCol = 5 To fg.Cols - 1
    '                If Trim(fg.get_TextMatrix(iRow, iCol)) <> "" Then
    '                    mEmptyColCount = mEmptyColCount + 1
    '                Else
    '                    mEmptyColCount = mEmptyColCount
    '                End If
    '            Next
    '            If mEmptyColCount = 0 Then
    '                fg.RemoveItem(iRow)
    '                GoTo 1
    '            End If
    '        Next
    '    End Sub

    Private Sub btnUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Form_Load(FrmNewUser)
    End Sub

    Private Sub btnRights_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Form_Load(FrmPermission)
    End Sub

    Private Sub fg_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fg.Click

    End Sub
End Class
