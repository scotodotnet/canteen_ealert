﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTimeDelete
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.BtnExit = New System.Windows.Forms.Button
        Me.BtnOutDelete = New System.Windows.Forms.Button
        Me.btnINDelete = New System.Windows.Forms.Button
        Me.flxGrid_Out = New System.Windows.Forms.DataGridView
        Me.OutDummySno = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.OutSelect = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.OutSno = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.OutMachineID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.OutTokenNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.OutEmpName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.LogTimeOut = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.flxGrid_IN = New System.Windows.Forms.DataGridView
        Me.WeekDummySno = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.WeekSelect = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.WSno = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.WeekMachineID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.WeekTokenNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.WEmpName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.LogTimeIN = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.txtAttnDate = New System.Windows.Forms.DateTimePicker
        Me.Label24 = New System.Windows.Forms.Label
        Me.BtnLoad = New System.Windows.Forms.Button
        Me.txtEmpName = New System.Windows.Forms.TextBox
        Me.Label14 = New System.Windows.Forms.Label
        Me.cmbTokenNo = New System.Windows.Forms.ComboBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.cmbMachineID = New System.Windows.Forms.ComboBox
        Me.Label15 = New System.Windows.Forms.Label
        Me.cmbLocCode = New System.Windows.Forms.ComboBox
        Me.cmbCompCode = New System.Windows.Forms.ComboBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.GroupBox1.SuspendLayout()
        CType(Me.flxGrid_Out, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.flxGrid_IN, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.GroupBox1.Controls.Add(Me.BtnExit)
        Me.GroupBox1.Controls.Add(Me.BtnOutDelete)
        Me.GroupBox1.Controls.Add(Me.btnINDelete)
        Me.GroupBox1.Controls.Add(Me.flxGrid_Out)
        Me.GroupBox1.Controls.Add(Me.flxGrid_IN)
        Me.GroupBox1.Controls.Add(Me.txtAttnDate)
        Me.GroupBox1.Controls.Add(Me.Label24)
        Me.GroupBox1.Controls.Add(Me.BtnLoad)
        Me.GroupBox1.Controls.Add(Me.txtEmpName)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.cmbTokenNo)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.cmbMachineID)
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.cmbLocCode)
        Me.GroupBox1.Controls.Add(Me.cmbCompCode)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Location = New System.Drawing.Point(-5, -5)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(672, 465)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        '
        'BtnExit
        '
        Me.BtnExit.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnExit.Location = New System.Drawing.Point(556, 97)
        Me.BtnExit.Name = "BtnExit"
        Me.BtnExit.Size = New System.Drawing.Size(112, 30)
        Me.BtnExit.TabIndex = 78
        Me.BtnExit.Text = "&Exit"
        Me.BtnExit.UseVisualStyleBackColor = True
        '
        'BtnOutDelete
        '
        Me.BtnOutDelete.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnOutDelete.Location = New System.Drawing.Point(556, 431)
        Me.BtnOutDelete.Name = "BtnOutDelete"
        Me.BtnOutDelete.Size = New System.Drawing.Size(112, 30)
        Me.BtnOutDelete.TabIndex = 77
        Me.BtnOutDelete.Text = "&Out Delete"
        Me.BtnOutDelete.UseVisualStyleBackColor = True
        '
        'btnINDelete
        '
        Me.btnINDelete.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnINDelete.Location = New System.Drawing.Point(556, 252)
        Me.btnINDelete.Name = "btnINDelete"
        Me.btnINDelete.Size = New System.Drawing.Size(112, 30)
        Me.btnINDelete.TabIndex = 76
        Me.btnINDelete.Text = "&IN Delete"
        Me.btnINDelete.UseVisualStyleBackColor = True
        '
        'flxGrid_Out
        '
        Me.flxGrid_Out.AllowUserToAddRows = False
        Me.flxGrid_Out.AllowUserToDeleteRows = False
        Me.flxGrid_Out.AllowUserToResizeRows = False
        Me.flxGrid_Out.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.flxGrid_Out.ColumnHeadersHeight = 25
        Me.flxGrid_Out.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.OutDummySno, Me.OutSelect, Me.OutSno, Me.OutMachineID, Me.OutTokenNo, Me.OutEmpName, Me.LogTimeOut})
        Me.flxGrid_Out.Location = New System.Drawing.Point(4, 285)
        Me.flxGrid_Out.Name = "flxGrid_Out"
        Me.flxGrid_Out.ReadOnly = True
        Me.flxGrid_Out.Size = New System.Drawing.Size(664, 144)
        Me.flxGrid_Out.TabIndex = 75
        '
        'OutDummySno
        '
        Me.OutDummySno.HeaderText = "DummySno"
        Me.OutDummySno.Name = "OutDummySno"
        Me.OutDummySno.ReadOnly = True
        Me.OutDummySno.Visible = False
        '
        'OutSelect
        '
        Me.OutSelect.HeaderText = "Select"
        Me.OutSelect.Name = "OutSelect"
        Me.OutSelect.ReadOnly = True
        Me.OutSelect.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.OutSelect.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'OutSno
        '
        Me.OutSno.HeaderText = "SNo"
        Me.OutSno.Name = "OutSno"
        Me.OutSno.ReadOnly = True
        Me.OutSno.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'OutMachineID
        '
        Me.OutMachineID.HeaderText = "MachineID"
        Me.OutMachineID.Name = "OutMachineID"
        Me.OutMachineID.ReadOnly = True
        Me.OutMachineID.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'OutTokenNo
        '
        Me.OutTokenNo.HeaderText = "TokenNo"
        Me.OutTokenNo.Name = "OutTokenNo"
        Me.OutTokenNo.ReadOnly = True
        Me.OutTokenNo.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'OutEmpName
        '
        Me.OutEmpName.HeaderText = "EmpName"
        Me.OutEmpName.Name = "OutEmpName"
        Me.OutEmpName.ReadOnly = True
        Me.OutEmpName.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'LogTimeOut
        '
        Me.LogTimeOut.HeaderText = "TimeOUT"
        Me.LogTimeOut.Name = "LogTimeOut"
        Me.LogTimeOut.ReadOnly = True
        Me.LogTimeOut.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'flxGrid_IN
        '
        Me.flxGrid_IN.AllowUserToAddRows = False
        Me.flxGrid_IN.AllowUserToDeleteRows = False
        Me.flxGrid_IN.AllowUserToResizeRows = False
        Me.flxGrid_IN.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.flxGrid_IN.ColumnHeadersHeight = 25
        Me.flxGrid_IN.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.WeekDummySno, Me.WeekSelect, Me.WSno, Me.WeekMachineID, Me.WeekTokenNo, Me.WEmpName, Me.LogTimeIN})
        Me.flxGrid_IN.Location = New System.Drawing.Point(4, 130)
        Me.flxGrid_IN.Name = "flxGrid_IN"
        Me.flxGrid_IN.ReadOnly = True
        Me.flxGrid_IN.Size = New System.Drawing.Size(664, 119)
        Me.flxGrid_IN.TabIndex = 74
        '
        'WeekDummySno
        '
        Me.WeekDummySno.HeaderText = "DummySno"
        Me.WeekDummySno.Name = "WeekDummySno"
        Me.WeekDummySno.ReadOnly = True
        Me.WeekDummySno.Visible = False
        '
        'WeekSelect
        '
        Me.WeekSelect.HeaderText = "Select"
        Me.WeekSelect.Name = "WeekSelect"
        Me.WeekSelect.ReadOnly = True
        Me.WeekSelect.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.WeekSelect.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'WSno
        '
        Me.WSno.HeaderText = "SNo"
        Me.WSno.Name = "WSno"
        Me.WSno.ReadOnly = True
        Me.WSno.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'WeekMachineID
        '
        Me.WeekMachineID.HeaderText = "MachineID"
        Me.WeekMachineID.Name = "WeekMachineID"
        Me.WeekMachineID.ReadOnly = True
        Me.WeekMachineID.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'WeekTokenNo
        '
        Me.WeekTokenNo.HeaderText = "TokenNo"
        Me.WeekTokenNo.Name = "WeekTokenNo"
        Me.WeekTokenNo.ReadOnly = True
        Me.WeekTokenNo.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'WEmpName
        '
        Me.WEmpName.HeaderText = "EmpName"
        Me.WEmpName.Name = "WEmpName"
        Me.WEmpName.ReadOnly = True
        Me.WEmpName.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'LogTimeIN
        '
        Me.LogTimeIN.HeaderText = "TimeIN"
        Me.LogTimeIN.Name = "LogTimeIN"
        Me.LogTimeIN.ReadOnly = True
        Me.LogTimeIN.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'txtAttnDate
        '
        Me.txtAttnDate.CustomFormat = "dd/MM/yyyy"
        Me.txtAttnDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAttnDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.txtAttnDate.Location = New System.Drawing.Point(98, 69)
        Me.txtAttnDate.Name = "txtAttnDate"
        Me.txtAttnDate.Size = New System.Drawing.Size(246, 22)
        Me.txtAttnDate.TabIndex = 9
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.BackColor = System.Drawing.Color.Transparent
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label24.Location = New System.Drawing.Point(4, 71)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(71, 16)
        Me.Label24.TabIndex = 73
        Me.Label24.Text = "Attn Date"
        '
        'BtnLoad
        '
        Me.BtnLoad.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnLoad.Location = New System.Drawing.Point(442, 97)
        Me.BtnLoad.Name = "BtnLoad"
        Me.BtnLoad.Size = New System.Drawing.Size(112, 30)
        Me.BtnLoad.TabIndex = 22
        Me.BtnLoad.Text = "&View"
        Me.BtnLoad.UseVisualStyleBackColor = True
        '
        'txtEmpName
        '
        Me.txtEmpName.BackColor = System.Drawing.Color.White
        Me.txtEmpName.Enabled = False
        Me.txtEmpName.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmpName.Location = New System.Drawing.Point(98, 40)
        Me.txtEmpName.Name = "txtEmpName"
        Me.txtEmpName.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtEmpName.Size = New System.Drawing.Size(246, 22)
        Me.txtEmpName.TabIndex = 7
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.BackColor = System.Drawing.Color.Transparent
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label14.Location = New System.Drawing.Point(4, 43)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(49, 16)
        Me.Label14.TabIndex = 46
        Me.Label14.Text = "Name"
        '
        'cmbTokenNo
        '
        Me.cmbTokenNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbTokenNo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbTokenNo.FormattingEnabled = True
        Me.cmbTokenNo.Location = New System.Drawing.Point(428, 70)
        Me.cmbTokenNo.Name = "cmbTokenNo"
        Me.cmbTokenNo.Size = New System.Drawing.Size(240, 24)
        Me.cmbTokenNo.TabIndex = 8
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label4.Location = New System.Drawing.Point(349, 74)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(76, 16)
        Me.Label4.TabIndex = 44
        Me.Label4.Text = "Token No"
        '
        'cmbMachineID
        '
        Me.cmbMachineID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbMachineID.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbMachineID.FormattingEnabled = True
        Me.cmbMachineID.Location = New System.Drawing.Point(428, 40)
        Me.cmbMachineID.Name = "cmbMachineID"
        Me.cmbMachineID.Size = New System.Drawing.Size(240, 24)
        Me.cmbMachineID.TabIndex = 6
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.BackColor = System.Drawing.Color.Transparent
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label15.Location = New System.Drawing.Point(349, 43)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(81, 16)
        Me.Label15.TabIndex = 42
        Me.Label15.Text = "MachineID"
        '
        'cmbLocCode
        '
        Me.cmbLocCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbLocCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbLocCode.FormattingEnabled = True
        Me.cmbLocCode.Location = New System.Drawing.Point(428, 11)
        Me.cmbLocCode.Name = "cmbLocCode"
        Me.cmbLocCode.Size = New System.Drawing.Size(240, 24)
        Me.cmbLocCode.TabIndex = 2
        '
        'cmbCompCode
        '
        Me.cmbCompCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbCompCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbCompCode.FormattingEnabled = True
        Me.cmbCompCode.Location = New System.Drawing.Point(98, 10)
        Me.cmbCompCode.Name = "cmbCompCode"
        Me.cmbCompCode.Size = New System.Drawing.Size(246, 24)
        Me.cmbCompCode.TabIndex = 1
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label7.Location = New System.Drawing.Point(349, 15)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(78, 16)
        Me.Label7.TabIndex = 8
        Me.Label7.Text = "Loc. Code"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.BackColor = System.Drawing.Color.Transparent
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label11.Location = New System.Drawing.Point(4, 14)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(93, 16)
        Me.Label11.TabIndex = 9
        Me.Label11.Text = "Comp. Code"
        '
        'frmTimeDelete
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(663, 455)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "frmTimeDelete"
        Me.Text = "frmTimeDelete"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.flxGrid_Out, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.flxGrid_IN, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents BtnExit As System.Windows.Forms.Button
    Friend WithEvents BtnOutDelete As System.Windows.Forms.Button
    Friend WithEvents btnINDelete As System.Windows.Forms.Button
    Friend WithEvents flxGrid_Out As System.Windows.Forms.DataGridView
    Friend WithEvents OutDummySno As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OutSelect As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents OutSno As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OutMachineID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OutTokenNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OutEmpName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LogTimeOut As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents flxGrid_IN As System.Windows.Forms.DataGridView
    Friend WithEvents WeekDummySno As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents WeekSelect As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents WSno As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents WeekMachineID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents WeekTokenNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents WEmpName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LogTimeIN As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txtAttnDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents BtnLoad As System.Windows.Forms.Button
    Friend WithEvents txtEmpName As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents cmbTokenNo As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cmbMachineID As System.Windows.Forms.ComboBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents cmbLocCode As System.Windows.Forms.ComboBox
    Friend WithEvents cmbCompCode As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
End Class
