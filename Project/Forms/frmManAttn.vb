﻿Public Class frmManAttn
    Dim mStartIN, mEndIN, mStartOUT, mEndOUT As String
    Dim mStartIN_Days, mEndIN_Days, mStartOUT_days, mEndOUT_Days As Double
    Dim mLocalDS As New DataSet

    Private Sub frmManAttn_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormAlign(Me)
        Fill_Company_Details()
    End Sub

    Public Sub Fill_Company_Details()
        SSQL = ""
        SSQL = "Select CompCode + ' | ' + CompName as [Name] from Company_Mst Order By CompName"

        mDataSet = ReturnMultipleValue(SSQL)
        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                cmbCompCode.Items.Clear()
                For iRow = 0 To .Rows.Count - 1
                    cmbCompCode.Items.Add(.Rows(iRow)(0))
                Next
            End If
        End With
    End Sub

    Private Sub cmbCompCode_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCompCode.LostFocus
        If Trim(cmbCompCode.Text) = "" Then Exit Sub
        Dim iStr() As String
        iStr = Split(Trim(cmbCompCode.Text), " | ")

        SSQL = ""
        SSQL = "Select LocCode + ' | ' + LocName as [Name] from Location_Mst Where Compcode='"
        SSQL = SSQL & "" & iStr(0) & "'"

        mDataSet = ReturnMultipleValue(SSQL)

        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                cmbLocCode.Items.Clear()
                For iRow = 0 To .Rows.Count - 1
                    cmbLocCode.Items.Add(.Rows(iRow)(0))
                Next
            End If
        End With
    End Sub

    Private Sub Trans_ID_Load()
        If Trim(cmbCompCode.Text) = "" Then Exit Sub
        If Trim(cmbLocCode.Text) = "" Then Exit Sub
        Dim Comp_iStr() As String
        Dim Loc_iStr() As String
        Comp_iStr = Split(Trim(cmbCompCode.Text), " | ")
        Loc_iStr = Split(Trim(cmbLocCode.Text), " | ")

        SSQL = ""
        SSQL = "Select Trans_ID from ManAttn_Details Where CompCode='" & Comp_iStr(0) & "'"
        SSQL = SSQL & " and LocCode='" & Loc_iStr(0) & "'"

        mDataSet = ReturnMultipleValue(SSQL)

        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                txtTransID.Items.Clear()
                For iRow = 0 To .Rows.Count - 1
                    txtTransID.Items.Add(.Rows(iRow)(0))
                Next
            End If
        End With
    End Sub

    Private Sub cmbLocCode_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbLocCode.LostFocus
        If Trim(cmbCompCode.Text) = "" Then Exit Sub
        If Trim(cmbLocCode.Text) = "" Then Exit Sub

        Dim iStr1() As String
        Dim iStr2() As String

        iStr1 = Split(Trim(cmbCompCode.Text), " | ")
        iStr2 = Split(Trim(cmbLocCode.Text), " | ")

        SSQL = ""
        SSQL = "Select ShiftDesc,StartIN ,StartIN_Days,EndIN_Days,StartOUT,StartOUT_days,EndOUT,EndOUT_Days"
        SSQL = SSQL & " From Shift_Mst Where Compcode='"
        SSQL = SSQL & "" & iStr1(0) & "'"
        SSQL = SSQL & " and LocCode='" & iStr2(0) & "'"

        mDataSet = ReturnMultipleValue(SSQL)

        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                cmbShifDesc.Items.Clear()
                For iRow = 0 To .Rows.Count - 1
                    cmbShifDesc.Items.Add(.Rows(iRow)("ShiftDesc"))
                Next
            End If
        End With
    End Sub

    Private Sub cmbPrefix_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbPrefix.LostFocus
        'If Trim(cmbCompCode.Text) = "" Then Exit Sub
        'If Trim(cmbLocCode.Text) = "" Then Exit Sub
        'If Trim(cmbPrefix.Text) = "" Then Exit Sub

        'Dim iStr1() As String
        'Dim iStr2() As String

        'iStr1 = Split(Trim(cmbCompCode.Text), " | ")
        'iStr2 = Split(Trim(cmbLocCode.Text), " | ")

        'If mUserLocation <> "" Then
        '    SSQL = ""
        '    SSQL = "Select EmpNo + ' -> ' + FirstName from Employee_Mst Where Compcode='"
        '    SSQL = SSQL & "" & iStr1(0) & "'"
        '    SSQL = SSQL & " and LocCode='" & iStr2(0) & "'"
        '    SSQL = SSQL & " and EmpPrefix='" & Trim(cmbPrefix.Text) & "'"
        '    SSQL = SSQL & " and IsNonAdmin='0'"
        '    SSQL = SSQL & " Order By convert(numeric,EmpNo)"
        'Else
        '    SSQL = ""
        '    SSQL = "Select convert(varchar(50),EmpNo) + ' -> ' + FirstName from Employee_Mst Where Compcode='"
        '    SSQL = SSQL & "" & iStr1(0) & "'"
        '    SSQL = SSQL & " and LocCode='" & iStr2(0) & "'"
        '    SSQL = SSQL & " and EmpPrefix='" & Trim(cmbPrefix.Text) & "'"
        '    SSQL = SSQL & " Order By convert(numeric,EmpNo)"
        'End If
        ''SSQL = ""
        ''SSQL = "Select EmpNo + ' -> ' + FirstName from Employee_Mst Where Compcode='"
        ''SSQL = SSQL & "" & iStr1(0) & "'"
        ''SSQL = SSQL & " and LocCode='" & iStr2(0) & "'"
        ''SSQL = SSQL & " and EmpPrefix='" & Trim(cmbPrefix.Text) & "'"
        ''SSQL = SSQL & " Order By convert(numeric,EmpNo)"

        'mDataSet = ReturnMultipleValue(SSQL)

        'With mDataSet.Tables(0)
        '    If .Rows.Count > 0 Then
        '        cmbTktNo.Items.Clear()
        '        For iRow = 0 To .Rows.Count - 1
        '            cmbTktNo.Items.Add(.Rows(iRow)(0))
        '        Next
        '    End If
        'End With
    End Sub

    Private Sub cmbTktNo_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cmbTktNo.KeyPress
        If e.KeyChar = ChrW(Keys.Enter) Then
            If Trim(cmbTktNo.Text) <> "" Then cmbTktNo_LostFocus(sender, e)
        End If
    End Sub

    Private Sub cmbTktNo_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbTktNo.LostFocus
        If Trim(cmbCompCode.Text) = "" Then Exit Sub
        If Trim(cmbLocCode.Text) = "" Then Exit Sub
        'If Trim(cmbPrefix.Text) = "" Then Exit Sub
        If Trim(cmbTktNo.Text) = "" Then Exit Sub

        Dim iStr1() As String
        Dim iStr2() As String
        Dim iStr3() As String

        iStr1 = Split(Trim(cmbCompCode.Text), " | ")
        iStr2 = Split(Trim(cmbLocCode.Text), " | ")
        iStr3 = Split(Trim(cmbTktNo.Text), " -> ")

        txtExistingCode.Text = ""

        SSQL = ""
        SSQL = "Select ExistingCode,MachineID,FirstName from Employee_Mst Where Compcode='" & Trim(iStr1(0)) & "'"
        SSQL = SSQL & " and LocCode='" & Trim(iStr2(0)) & "'"
        SSQL = SSQL & " and FirstName like '" & txtName.Text & "%'"
        SSQL = SSQL & " and EmpNo='" & Trim(iStr3(0)) & "'"
        mDataSet = ReturnMultipleValue(SSQL)

        With mDataSet.Tables(0)
            txtExistingCode.Text = .Rows(0)(0)
            txtMachineID.Text = .Rows(0)(1)
            txtEmpName.Text = .Rows(0)(2)
        End With
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            'If Trim(cmbCompCode.Text) = "" Or Trim(cmbLocCode.Text) = "" Or _
            'Trim(cmbPrefix.Text) = "" Or Trim(cmbTktNo.Text) = "" Or Trim(cmbShifDesc.Text) = "" Or Trim(cmbTktNo.Text) = "" Then
            '    Exit Sub
            'End If
            'Check All Condtion
            If Trim(cmbCompCode.Text) = "" Then MsgBox("Select Company Name", MsgBoxStyle.Critical) : cmbCompCode.Focus() : Exit Sub
            If Trim(cmbLocCode.Text) = "" Then MsgBox("Select Location Name", MsgBoxStyle.Critical) : cmbLocCode.Focus() : Exit Sub
            'If Trim(cmbPrefix.Text) = "" Then MsgBox("Select Employee Prefix", MsgBoxStyle.Critical) : cmbPrefix.Focus() : Exit Sub
            If Trim(cmbTktNo.Text) = "" Then MsgBox("Select Employee Token No", MsgBoxStyle.Critical) : cmbTktNo.Focus() : Exit Sub
            If Trim(cmbAttnStatus.Text) = "" Then MsgBox("Select Attn. Status", MsgBoxStyle.Critical) : cmbAttnStatus.Focus() : Exit Sub
            If Trim(txtTimeIN.Text) = "" Then MsgBox("Enter Time IN", MsgBoxStyle.Critical) : txtTimeIN.Focus() : Exit Sub
            If Trim(txtTimeOut.Text) = "" Then MsgBox("Enter Time OUT", MsgBoxStyle.Critical) : txtTimeOut.Focus() : Exit Sub
            If Trim(txtMachineID.Text) = "" Or Trim(txtExistingCode.Text) = "" Then MsgBox("Select Employee Token No Properly", MsgBoxStyle.Critical) : cmbTktNo.Focus() : Exit Sub

            Dim iStr1() As String
            Dim iStr2() As String
            Dim iStr3() As String
            Dim Time_IN_Count_Check() As String
            Dim Time_OUT_Count_Check() As String
            Dim Time_OUT_Format_check As Int32
            Dim MachineID As String
            Dim In_Machine_IP As String = ""
            Dim Out_Machine_IP As String = ""
            Dim LogTime_IN_Date As String
            Dim LogTime_OUT_Date As String
            Dim Date_Join As String = ""
            Dim Month_Val As Long
            Dim Year_Val As Long
            Dim Day_Val As Long
            Dim Day_Val_1 As String = ""
            Dim i As Int32

            Dim Val As String
            Dim Prifix() As String



            Val = cmbTktNo.Text
            Prifix = Val.Split(".")




            Time_IN_Count_Check = Split(Trim(txtTimeIN.Text), ":")
            Time_OUT_Count_Check = Split(Trim(txtTimeOut.Text), ":")
            If Time_IN_Count_Check.Length <> 2 Then MsgBox("Time IN Invalid", MsgBoxStyle.Critical) : txtTimeIN.Focus() : Exit Sub
            If Time_OUT_Count_Check.Length <> 2 Then MsgBox("Time OUT Invalid", MsgBoxStyle.Critical) : txtTimeOut.Focus() : Exit Sub

            iStr1 = Split(Trim(cmbCompCode.Text), " | ")
            iStr2 = Split(Trim(cmbLocCode.Text), " | ")
            iStr3 = Split(Trim(cmbTktNo.Text), " -> ")
            MachineID = Encryption(txtMachineID.Text)
            'MachineID = txtMachineID.Text
            Time_OUT_Format_check = Time_OUT_Count_Check(0)

            Year_Val = Year(CDate(txtAttnDate.Text)) : Month_Val = Month(CDate(txtAttnDate.Text)) : Day_Val = Left_Val(txtAttnDate.Text, 2)
            Date_Join = CStr(Year_Val) & "/" & Format(Month_Val, "0#") & "/" & Format(Day_Val, "0#")
            LogTime_IN_Date = Date_Join & " " & txtTimeIN.Text

            If Time_OUT_Format_check <= 12 Then
                Year_Val = Year(CDate(txtAttnDate.Text)) : Month_Val = Month(CDate(txtAttnDate.Text))
                Day_Val_1 = txtAttnDate.Value.AddDays(1).ToString("yyyy/MM/dd")
                Day_Val = Right_Val(Day_Val_1, 2)
                Date_Join = CStr(Year_Val) & "/" & Format(Month_Val, "0#") & "/" & Format(Day_Val, "0#")
                LogTime_OUT_Date = Date_Join & " " & txtTimeOut.Text
            Else
                LogTime_OUT_Date = Date_Join & " " & txtTimeOut.Text
            End If
            'LogTime_OUT_Date = Date_Join & " " & txtTimeOut.Text & ":00"
            'If CheckForEmployee(Encryption(Trim(iStr3(0)))) Then
            '    MessageBox.Show("Already Exists.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            '    Exit Sub
            'End If

            'Get IP Address
            SSQL = "Select * from IPAddress_Mst where CompCode = '" & iStr1(0) & "' and LocCode='" & iStr2(0) & "'"
            mDataSet = ReturnMultipleValue(SSQL)
            For i = 0 To mDataSet.Tables(0).Rows.Count - 1
                If UCase(mDataSet.Tables(0).Rows(i)("IPMode").ToString) = UCase("IN") Then In_Machine_IP = mDataSet.Tables(0).Rows(i)("IPAddress").ToString
                If UCase(mDataSet.Tables(0).Rows(i)("IPMode").ToString) = UCase("OUT") Then Out_Machine_IP = mDataSet.Tables(0).Rows(i)("IPAddress").ToString
            Next
            'FInd OLD Record
            SSQL = "Select * from ManAttn_Details where CompCode='" & iStr1(0) & "'"
            SSQL = SSQL & " and LocCode='" & iStr2(0) & "'"
            SSQL = SSQL & " and EmpNo='" & iStr3(0) & "' and AttnDateStr='" & txtAttnDate.Text & "'"
            mDataSet = ReturnMultipleValue(SSQL)
            If mDataSet.Tables(0).Rows.Count = 0 Then
                'Insert
                If Trim(cmbAttnStatus.Text) = "A" Then
                    'Delete LogTime_IN Record
                    SSQL = "Delete from LogTime_IN where CompCode='" & iStr1(0) & "' And LocCode= '" & iStr2(0) & "'"
                    SSQL = SSQL & " and IPAddress='" & In_Machine_IP & "' and MachineID='" & MachineID & "' and TimeIN = '" & LogTime_IN_Date & "%'"
                    ReturnSingleValue(SSQL)
                    'Delete LogTime_OUT Record
                    SSQL = "Delete from LogTime_OUT where CompCode='" & iStr1(0) & "' And LocCode= '" & iStr2(0) & "'"
                    SSQL = SSQL & " and IPAddress='" & Out_Machine_IP & "' and MachineID='" & MachineID & "' and TimeOUT like '%" & LogTime_OUT_Date & "%'"
                    ReturnSingleValue(SSQL)
                    'Delete ManAttn_Details Record
                    SSQL = "Delete from ManAttn_Details where CompCode='" & iStr1(0) & "' and LocCode='" & iStr2(0) & "'"
                    SSQL = SSQL & " and EmpNo='" & iStr3(0) & "' and AttnDateStr='" & txtAttnDate.Text & "'"
                    ReturnSingleValue(SSQL)
                    MsgBox("Manual Attn. Deleted Successfully", MsgBoxStyle.Exclamation)
                    btnClear_Click(sender, e)
                Else
                    'Insert logtime in and logtime out
                    'Insert Manual Attn. Record
                    SSQL = "Insert Into ManAttn_Details(CompCode,LocCode,EmpPrefix,EmpNo,ExistingCode,Machine_No,EmpName,AttnStatus,AttnDate,AttnDateStr,TimeIn,TimeOut,LogTimeIn,LogTimeOut,Enter_Date,UserName) Values('" & _
                    iStr1(0) & "','" & iStr2(0) & "','" & Prifix(1) & "','" & iStr3(0) & "','" & txtExistingCode.Text & "','" & txtMachineID.Text & "','" & txtEmpName.Text & "','" & cmbAttnStatus.Text & "','" & _
                    txtAttnDate.Text & "','" & txtAttnDate.Text & "','" & txtTimeIN.Text & "','" & txtTimeOut.Text & "','" & LogTime_IN_Date & "','" & LogTime_OUT_Date & "','" & CDate(Now) & "','" & mvarLoginUser & "')"
                    ReturnSingleValue(SSQL)
                    'Insert LogTime_IN
                    SSQL = "Insert Into LogTime_IN(CompCode,LocCode,IPAddress,MachineID,TimeIN) Values('" & _
                    iStr1(0) & "','" & iStr2(0) & "','" & In_Machine_IP & "','" & MachineID & "','" & LogTime_IN_Date & "')"
                    ReturnSingleValue(SSQL)
                    'Insert LogTime_OUT
                    SSQL = "Insert Into LogTime_OUT(CompCode,LocCode,IPAddress,MachineID,TimeOUT) Values('" & _
                    iStr1(0) & "','" & iStr2(0) & "','" & Out_Machine_IP & "','" & MachineID & "','" & LogTime_OUT_Date & "')"
                    ReturnSingleValue(SSQL)
                    MsgBox("Manual Attn. Inserted Successfully", MsgBoxStyle.Exclamation)
                    btnClear_Click(sender, e)
                End If
            Else
                'Modify
                'Delete OLD Record
                'Delete LogTime_IN Record
                SSQL = "Delete from LogTime_IN where CompCode='" & iStr1(0) & "' And LocCode= '" & iStr2(0) & "'"
                SSQL = SSQL & " and IPAddress='" & In_Machine_IP & "' and MachineID='" & MachineID & "' and TimeIN = '" & mDataSet.Tables(0).Rows(0)("LogTimeIn") & "'"
                ReturnSingleValue(SSQL)
                'Delete LogTime_OUT Record
                SSQL = "Delete from LogTime_OUT where CompCode='" & iStr1(0) & "' And LocCode= '" & iStr2(0) & "'"
                SSQL = SSQL & " and IPAddress='" & Out_Machine_IP & "' and MachineID='" & MachineID & "' and TimeOUT = '" & mDataSet.Tables(0).Rows(0)("LogTimeOut") & "'"
                ReturnSingleValue(SSQL)
                'Delete ManAttn_Details Record
                SSQL = "Delete from ManAttn_Details where CompCode='" & iStr1(0) & "' and LocCode='" & iStr2(0) & "'"
                SSQL = SSQL & " and EmpPrefix='" & Prifix(1) & "' and EmpNo='" & iStr3(0) & "' and AttnDateStr='" & txtAttnDate.Text & "'"
                ReturnSingleValue(SSQL)

                If Trim(cmbAttnStatus.Text) <> "A" Then
                    'Insert Manual Attn. Record
                    SSQL = "Insert Into ManAttn_Details(CompCode,LocCode,EmpPrefix,EmpNo,ExistingCode,Machine_No,EmpName,AttnStatus,AttnDate,AttnDateStr,TimeIn,TimeOut,LogTimeIn,LogTimeOut,Enter_Date,UserName) Values('" & _
                    iStr1(0) & "','" & iStr2(0) & "','" & Prifix(1) & "','" & iStr3(0) & "','" & txtExistingCode.Text & "','" & txtMachineID.Text & "','" & txtEmpName.Text & "','" & cmbAttnStatus.Text & "','" & _
                    txtAttnDate.Text & "','" & txtAttnDate.Text & "','" & txtTimeIN.Text & "','" & txtTimeOut.Text & "','" & LogTime_IN_Date & "','" & LogTime_OUT_Date & "','" & CDate(Now) & "','" & mvarLoginUser & "')"
                    ReturnSingleValue(SSQL)
                    'Insert LogTime_IN
                    SSQL = "Insert Into LogTime_IN(CompCode,LocCode,IPAddress,MachineID,TimeIN) Values('" & _
                    iStr1(0) & "','" & iStr2(0) & "','" & In_Machine_IP & "','" & MachineID & "','" & LogTime_IN_Date & "')"
                    ReturnSingleValue(SSQL)
                    'Insert LogTime_OUT
                    SSQL = "Insert Into LogTime_OUT(CompCode,LocCode,IPAddress,MachineID,TimeOUT) Values('" & _
                    iStr1(0) & "','" & iStr2(0) & "','" & Out_Machine_IP & "','" & MachineID & "','" & LogTime_OUT_Date & "')"
                    ReturnSingleValue(SSQL)
                    MsgBox("Manual Attn. Updated Successfully", MsgBoxStyle.Exclamation)
                    btnClear_Click(sender, e)
                Else
                    MsgBox("Manual Attn. Deleted Successfully", MsgBoxStyle.Exclamation)
                    btnClear_Click(sender, e)
                End If
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End Try
    End Sub

    Private Function CheckForEmployee(ByVal searchMachineID As String) As Boolean
        Dim drMatchRow As DataRow = Nothing

        'drMatchRow = mLocalDS.Tables(0).Rows.Find(searchMachineID)

        'If drMatchRow Is Nothing Then
        'Return False
        'Else
        'Return True
        'End If

        MsgBox(mLocalDS.Tables(0).Rows.Count)
        For Each dRow In mLocalDS.Tables(0).Rows
            If Trim(dRow.Item("MachineID")) = searchMachineID Then
                Return True
                Exit Function
            End If
        Next
        Return False
    End Function

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        cmbLocCode.Text = ""
        txtTransID.Text = ""
        txtTransID.Items.Clear()
        cmbPrefix.SelectedIndex = 0
        cmbTktNo.Text = ""
        cmbTktNo.Items.Clear()
        txtExistingCode.Text = ""
        txtMachineID.Text = ""
        txtEmpName.Text = ""
        cmbAttnStatus.SelectedIndex = 0
        txtTimeIN.Text = ""
        txtTimeOut.Text = ""
        cmbPrefix.Focus()
        'cmbShifDesc.Text = ""
        'cmbAttnStatus.SelectedIndex = 0
        'cmbLocCode.Focus()
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Close()
    End Sub

    Private Sub cmbLocCode_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbLocCode.SelectedIndexChanged
        If Trim(cmbLocCode.Text) = "" Then Exit Sub
        Call Trans_ID_Load()
    End Sub

    Private Sub txtExistingCode_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtExistingCode.KeyPress
        e.Handled = True
    End Sub

    Private Sub txtMachineID_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtMachineID.KeyPress
        e.Handled = True
    End Sub

    Private Sub txtEmpName_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtEmpName.KeyPress
        e.Handled = True
    End Sub

    Private Sub txtTimeIN_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtTimeIN.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 39 Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtTimeOut_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtTimeOut.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 39 Then
            e.Handled = True
        End If
    End Sub

    Private Sub cmbPrefix_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbPrefix.SelectedIndexChanged

    End Sub

    Private Sub txtName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtName.TextChanged
        If Trim(cmbCompCode.Text) = "" Then Exit Sub
        If Trim(cmbLocCode.Text) = "" Then Exit Sub
        'If Trim(cmbPrefix.Text) = "" Then Exit Sub

        Dim iStr1() As String
        Dim iStr2() As String

        iStr1 = Split(Trim(cmbCompCode.Text), " | ")
        iStr2 = Split(Trim(cmbLocCode.Text), " | ")

        If mUserLocation <> "" Then
            SSQL = ""
            SSQL = "Select EmpNo + ' -> ' + (FirstName + '.' + MiddleInitial) as FirstName from Employee_Mst Where Compcode='"
            SSQL = SSQL & "" & iStr1(0) & "'"
            SSQL = SSQL & " and LocCode='" & iStr2(0) & "'"
            SSQL = SSQL & " and FirstName='" & txtName.Text & "%'"
            SSQL = SSQL & " and IsNonAdmin='0'"
            SSQL = SSQL & " Order By convert(numeric,EmpNo)"
        Else
            SSQL = ""
            SSQL = "Select convert(varchar(50),EmpNo) + ' -> ' + (FirstName + '.' + MiddleInitial) as FirstName from Employee_Mst Where Compcode='"
            SSQL = SSQL & "" & iStr1(0) & "'"
            SSQL = SSQL & " and LocCode='" & iStr2(0) & "'"
            SSQL = SSQL & " and FirstName like '" & txtName.Text & "%'"
            SSQL = SSQL & " Order By convert(numeric,EmpNo)"
        End If
        'SSQL = ""
        'SSQL = "Select EmpNo + ' -> ' + FirstName from Employee_Mst Where Compcode='"
        'SSQL = SSQL & "" & iStr1(0) & "'"
        'SSQL = SSQL & " and LocCode='" & iStr2(0) & "'"
        'SSQL = SSQL & " and EmpPrefix='" & Trim(cmbPrefix.Text) & "'"
        'SSQL = SSQL & " Order By convert(numeric,EmpNo)"

        mDataSet = ReturnMultipleValue(SSQL)

        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                cmbTktNo.Items.Clear()
                For iRow = 0 To .Rows.Count - 1
                    cmbTktNo.Items.Add(.Rows(iRow)(0))
                Next
            End If
        End With
    End Sub

    Private Sub cmbTktNo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbTktNo.SelectedIndexChanged
        If Trim(cmbCompCode.Text) = "" Then Exit Sub
        If Trim(cmbLocCode.Text) = "" Then Exit Sub
        'If Trim(cmbPrefix.Text) = "" Then Exit Sub
        If Trim(cmbTktNo.Text) = "" Then Exit Sub

        Dim iStr1() As String
        Dim iStr2() As String
        Dim iStr3() As String

        iStr1 = Split(Trim(cmbCompCode.Text), " | ")
        iStr2 = Split(Trim(cmbLocCode.Text), " | ")
        iStr3 = Split(Trim(cmbTktNo.Text), " -> ")

        txtExistingCode.Text = ""

        SSQL = ""
        SSQL = "Select ExistingCode,MachineID,FirstName from Employee_Mst Where Compcode='" & Trim(iStr1(0)) & "'"
        SSQL = SSQL & " and LocCode='" & Trim(iStr2(0)) & "'"
        'SSQL = SSQL & " and FirstName like '" & txtName.Text & "%'"
        SSQL = SSQL & " and EmpNo='" & Trim(iStr3(0)) & "' and IsActive='" & "Yes" & "'"
        mDataSet = ReturnMultipleValue(SSQL)

        With mDataSet.Tables(0)
            txtExistingCode.Text = .Rows(0)(0)
            txtMachineID.Text = .Rows(0)(1)
            txtEmpName.Text = .Rows(0)(2)
        End With
    End Sub
End Class