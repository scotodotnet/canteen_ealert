﻿#Region "Imports"
Imports System
Imports System.Data
Imports System.Data.OleDb
#End Region
Public Class frmDownClear
    Dim iStr1() As String
    Dim iStr2() As String
    Dim iStr3() As String
    Private Sub frmDownClear_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormAlign(Me)
        Fill_Company_Details()
        cmbCompCode.SelectedIndex = 0
        cmbCompCode.Enabled = False
        cmbCompCode_LostFocus(sender, e)
    End Sub

    Public Sub Fill_Company_Details()
        SSQL = ""
        SSQL = "Select CompCode + ' | ' + CompName as [Name] from Company_Mst Order By CompName"

        mDataSet = ReturnMultipleValue(SSQL)
        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                cmbCompCode.Items.Clear()
                For iRow = 0 To .Rows.Count - 1
                    cmbCompCode.Items.Add(.Rows(iRow)(0))
                Next
            End If
        End With
    End Sub

    Private Sub cmbCompCode_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCompCode.LostFocus
        If Trim(cmbCompCode.Text) = "" Then Exit Sub
        Dim iStr() As String
        iStr = Split(Trim(cmbCompCode.Text), " | ")

        SSQL = ""
        SSQL = "Select LocCode + ' | ' + LocName as [Name] from Location_Mst Where Compcode='"
        SSQL = SSQL & "" & iStr(0) & "'"

        mDataSet = ReturnMultipleValue(SSQL)

        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                cmbLocCode.Items.Clear()
                For iRow = 0 To .Rows.Count - 1
                    cmbLocCode.Items.Add(.Rows(iRow)(0))
                Next
            End If
        End With
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Close()
    End Sub

    Private Sub cmbLocCode_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbLocCode.LostFocus
        If Trim(cmbCompCode.Text) = "" Then Exit Sub
        If Trim(cmbLocCode.Text) = "" Then Exit Sub

        Dim iStr1() As String
        Dim iStr2() As String

        iStr1 = Split(Trim(cmbCompCode.Text), " | ")
        iStr2 = Split(Trim(cmbLocCode.Text), " | ")

        SSQL = ""
        SSQL = "Select IPAddress + ' | ' + IPMode as [Name] from IPAddress_Mst Where Compcode='"
        SSQL = SSQL & "" & iStr1(0) & "'"
        SSQL = SSQL & " And LocCode='" & iStr2(0) & "'"

        mDataSet = ReturnMultipleValue(SSQL)

        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                cmbIPAddress.Items.Clear()
                For iRow = 0 To .Rows.Count - 1
                    cmbIPAddress.Items.Add(.Rows(iRow)(0))
                Next
            End If
        End With
    End Sub

    Private Sub btnDownload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDownload.Click

        btnDownload.Enabled = False

        cmbCompCode.Enabled = False
        cmbLocCode.Enabled = False
        cmbIPAddress.Enabled = False

        Me.Text = "Started... Please Wait..."

        If Trim(cmbCompCode.Text) = "" Then
            MessageBox.Show("Please select company code.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbCompCode.Focus()
            Exit Sub
        End If

        If Trim(cmbLocCode.Text) = "" Then
            MessageBox.Show("Please select location code.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbLocCode.Focus()
            Exit Sub
        End If

        If Trim(cmbIPAddress.Text) = "" Then
            MessageBox.Show("Please select IP Address.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbIPAddress.Focus()
            Exit Sub
        End If

        iStr1 = Split(cmbCompCode.Text, " | ")
        iStr2 = Split(cmbLocCode.Text, " | ")
        iStr3 = Split(cmbIPAddress.Text, " | ")

        '--------------- HISTORY DOWNLOAD ------------------------------'
        ''downloadHistoryDetails()

        '--------------- HISTORY CLEAR ---------------------------------'
        '' clearHistoryDetails()

        '--------------- LOG DOWNLOAD ----------------------------------'
        downloadLogDetails()

        '--------------- LOG CLEAR -------------------------------------'
        ''clearLogDetails()


        'Clear Attn Log
        'TFT_Machine_Attn_Log_Clear()


        Me.Text = "Download / Clear"

        cmbCompCode.Enabled = True
        cmbLocCode.Enabled = True
        cmbIPAddress.Enabled = True

        btnDownload.Enabled = True

    End Sub

    Private Sub TFT_Machine_Attn_Log_Clear()
        Dim bConn As Boolean
        Dim idwErrorCode As Integer
        Dim iMachineNumber As Integer 'the serial number of the device.After connecting the device ,this value will be changed.

        bConn = mdiMain.AxSB100PC1.Connect_Net(Trim(iStr3(0)), 4370)

        'mdiMain.AxSB100PC1.SetIPAddress(Trim(iStr3(0)), CLng("5005"), CLng("0"))

        If bConn = False Then
            'MessageBox.Show("Not connected.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        iMachineNumber = 1
        mdiMain.AxSB100PC1.EnableDevice(iMachineNumber, False) 'disable the device
        If mdiMain.AxSB100PC1.ClearGLog(iMachineNumber) = True Then
            mdiMain.AxSB100PC1.RefreshData(iMachineNumber) 'the data in the device should be refreshed
            'MsgBox("All att Logs have been cleared from teiminal!", MsgBoxStyle.Information, "Success")
        Else
            mdiMain.AxSB100PC1.GetLastError(idwErrorCode)
            'MsgBox("Operation failed,ErrorCode=" & idwErrorCode, MsgBoxStyle.Exclamation, "Error")
        End If
        mdiMain.AxSB100PC1.EnableDevice(iMachineNumber, True) 'enable the device

    End Sub

    'Public Sub clearHistoryDetails()

    '    Dim vRet As Boolean
    '    Dim vErrorCode As Long

    '    mdiMain.AxSB100PC1.SetIPAddress(Trim(iStr3(0)), CLng("5005"), CLng("0"))

    '    If (mdiMain.AxSB100PC1.OpenCommPort(1)) = False Then
    '        MessageBox.Show("Not connected.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Stop)
    '        Exit Sub
    '    End If

    '    vRet = mdiMain.AxSB100PC1.EnableDevice(1, False)

    '    If vRet = False Then
    '        MessageBox.Show("Not connected.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Stop)
    '        Exit Sub
    '    End If

    '    vRet = mdiMain.AxSB100PC1.EmptySuperLogData(1)

    '    If vRet = False Then
    '        mdiMain.AxSB100PC1.GetLastError(vErrorCode)
    '        MessageBox.Show(ErrorPrint(vErrorCode), "Altius", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '    End If

    '    mdiMain.AxSB100PC1.EnableDevice(1, True)

    'End Sub

    'Public Sub clearLogDetails()

    '    Dim vRet As Boolean
    '    Dim vErrorCode As Long

    '    mdiMain.AxSB100PC1.SetIPAddress(Trim(iStr3(0)), CLng("5005"), CLng("0"))

    '    If (mdiMain.AxSB100PC1.OpenCommPort(1)) = False Then
    '        MessageBox.Show("Not connected.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Stop)
    '        Exit Sub
    '    End If

    '    vRet = mdiMain.AxSB100PC1.EnableDevice(1, False)

    '    If vRet = False Then
    '        MessageBox.Show("Not connected.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Stop)
    '        Exit Sub
    '    End If

    '    vRet = mdiMain.AxSB100PC1.EmptyGeneralLogData(1)

    '    If vRet = False Then
    '        mdiMain.AxSB100PC1.GetLastError(vErrorCode)
    '        MessageBox.Show(ErrorPrint(vErrorCode), "Altius", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '    End If

    '    mdiMain.AxSB100PC1.EnableDevice(1, True)
    'End Sub

    Public Sub downloadLogDetails()
        Dim vTMachineNumber As Long
        Dim vSEnrollNumber As String = ""
        Dim vVerifyMode As Long
        Dim vYear As Long
        Dim vMonth As Long
        Dim vDay As Long
        Dim vHour As Long
        Dim vMinute As Long
        Dim vRet As Boolean
        Dim i As Long
        Dim mTableName As String = ""
        Dim bConn As Boolean
        Dim vInOutMode As Long
        Dim vsec As Long
        Dim Wcode As Long


        If iStr3(1) = "IN" Then
            mTableName = "LogTime_IN"
        ElseIf iStr3(1) = "OUT" Then
            mTableName = "LogTime_OUT"
        Else
            Exit Sub
        End If

        '("", "TMachineNo", "EnrollNo", "EMachineNo", "VeriMode", "DateTime")

        bConn = mdiMain.AxSB100PC1.Connect_Net(Trim(iStr3(0)), 4370)

        'mdiMain.AxSB100PC1.SetIPAddress(Trim(iStr3(0)), CLng("5005"), CLng("0"))

        If bConn = False Then
            MessageBox.Show("Not connected.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        vRet = mdiMain.AxSB100PC1.EnableDevice(1, False)

        If vRet = False Then
            MessageBox.Show("Not connected.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        vRet = mdiMain.AxSB100PC1.ReadAllGLogData(1)

        If vRet = False Then
            ' MessageBox.Show(mdiMain.AxSB100PC1.GetLastError(vErrorCode), "Altius", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If
        If vRet = True Then
            i = 1
            Do

                'vRet = mdiMain.AxSB100PC1.GetAllGLogData(1, _
                '                                 vTMachineNumber, _
                '                                 vSEnrollNumber, _
                '                                 vSMachineNumber, _
                '                                 vVerifyMode, _
                '                                 vInOutMode, _
                '                                 vYear, _
                '                                 vMonth, _
                '                                 vDay, _
                '                                 vHour, _
                '                                 vMinute)



                vRet = mdiMain.AxSB100PC1.SSR_GetGeneralLogData(vTMachineNumber, vSEnrollNumber, _
                                                vVerifyMode, vInOutMode, _
                                                vYear, vMonth, vDay, _
                                                vHour, vMinute, vsec, Wcode)

                If vRet = False Then Exit Do

                ' MsgBox(iStr1(0) & iStr2(0) & iStr3(0) & vSEnrollNumber & CStr(vYear) & "/" & Format(vMonth, "0#") & "/" & Format(vDay, "0#") & _
                '  " " & Format(vHour, "0#") & ":" & Format(vMinute, "0#"))


                '================                
                SSQL = ""
                SSQL = "Delete from " & mTableName & " Where CompCode='" & Trim(iStr1(0)) & "'"
                SSQL = SSQL & " And LocCode='" & Trim(iStr2(0)) & "'"
                SSQL = SSQL & " And IPAddress='" & Trim(iStr3(0)) & "'"
                SSQL = SSQL & " And MachineID='" & Encryption(vSEnrollNumber) & "'"

                If mTableName = "LogTime_IN" Then
                    SSQL = SSQL & " And TimeIN='" & CStr(vYear) & "/" & Format(vMonth, "0#") & "/" & Format(vDay, "0#") & _
                                    " " & Format(vHour, "0#") & ":" & Format(vMinute, "0#") & "'"
                Else
                    SSQL = SSQL & " And TimeOUT='" & CStr(vYear) & "/" & Format(vMonth, "0#") & "/" & Format(vDay, "0#") & _
                                    " " & Format(vHour, "0#") & ":" & Format(vMinute, "0#") & "'"

                End If
                

                

                Dim mStatus As Long

                mStatus = InsertDeleteUpdate(SSQL)

                SSQL = ""
                SSQL = "Insert into " & mTableName & " Values("
                SSQL = SSQL & "'" & Trim(iStr1(0)) & "',"
                SSQL = SSQL & "'" & Trim(iStr2(0)) & "',"
                SSQL = SSQL & "'" & Trim(iStr3(0)) & "',"
                SSQL = SSQL & "'" & Encryption(vSEnrollNumber) & "',"

                If mTableName = "LogTime_IN" Then
                    SSQL = SSQL & "'" & CStr(vYear) & "/" & Format(vMonth, "0#") & "/" & Format(vDay, "0#") & _
                                    " " & Format(vHour, "0#") & ":" & Format(vMinute, "0#") & "')"
                Else
                    SSQL = SSQL & "'" & CStr(vYear) & "/" & Format(vMonth, "0#") & "/" & Format(vDay, "0#") & _
                                    " " & Format(vHour, "0#") & ":" & Format(vMinute, "0#") & "')"

                End If

                mStatus = InsertDeleteUpdate(SSQL)

                '=================

                i = i + 1
            Loop
            mdiMain.AxSB100PC1.EnableDevice(1, True)
            mdiMain.AxSB100PC1.RefreshData(1)
        End If
        
    End Sub

    'Public Sub downloadHistoryDetails()
    '    Dim vTMachineNumber As Long
    '    Dim vSMachineNumber As Long
    '    Dim vSEnrollNumber As Long
    '    Dim vGEnrollNumber As Long
    '    Dim vGMachineNumber As Long
    '    Dim vManipulation As Long
    '    Dim vFingerNumber As Long
    '    Dim vYear As Long
    '    Dim vMonth As Long
    '    Dim vDay As Long
    '    Dim vHour As Long
    '    Dim vMinute As Long
    '    Dim vRet As Boolean
    '    Dim vErrorCode As Long
    '    Dim i As Long


    '    '"TMNo", "SEnlNo", "SMNo", "GEnlNo", "GMNo", "Manipulation", "FpNo", "DateTime"

    '    mdiMain.AxSB100PC1.SetIPAddress(Trim(iStr3(0)), CLng("5005"), CLng("0"))

    '    If (mdiMain.AxSB100PC1.OpenCommPort(1)) = False Then
    '        MessageBox.Show("Not connected.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Stop)
    '        Exit Sub
    '    End If

    '    vRet = mdiMain.AxSB100PC1.EnableDevice(1, False)

    '    If vRet = False Then
    '        MessageBox.Show("Not connected.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Stop)
    '        Exit Sub
    '    End If

    '    vRet = mdiMain.AxSB100PC1.ReadAllSLogData(1)

    '    If vRet = False Then
    '        MessageBox.Show(mdiMain.AxSB100PC1.GetLastError(vErrorCode), "Altius", MessageBoxButtons.OK, MessageBoxIcon.Stop)
    '        Exit Sub
    '    End If

    '    If vRet = True Then
    '        i = 1
    '        Do
    '            vRet = mdiMain.AxSB100PC1.GetAllSLogData(1, _
    '                                                    vTMachineNumber, _
    '                                                    vSEnrollNumber, _
    '                                                    vSMachineNumber, _
    '                                                    vGEnrollNumber, _
    '                                                    vGMachineNumber, _
    '                                                    vManipulation, _
    '                                                    vFingerNumber, _
    '                                                    vYear, _
    '                                                    vMonth, _
    '                                                    vDay, _
    '                                                    vHour, _
    '                                                    vMinute)
    '            If vRet = False Then Exit Do

    '            SSQL = ""
    '            SSQL = "Delete from LogHistory_Details Where CompCode='" & Trim(iStr1(0)) & "'"
    '            SSQL = SSQL & " And LocCode='" & Trim(iStr2(0)) & "'"
    '            SSQL = SSQL & " And IPAddress='" & Trim(iStr3(0)) & "'"
    '            SSQL = SSQL & " And TMNo='" & vTMachineNumber & "'"
    '            SSQL = SSQL & " And SEnlNo='" & vSEnrollNumber & "'"
    '            SSQL = SSQL & " And SMNo='" & vSMachineNumber & "'"
    '            SSQL = SSQL & " And GEnlNo='" & vGEnrollNumber & "'"
    '            SSQL = SSQL & " And GMNo='" & vGMachineNumber & "'"

    '            Select Case vManipulation
    '                Case 1
    '                Case 2
    '                Case 3
    '                    SSQL = SSQL & " And Manipulation='" & vManipulation & "--" & "Enroll User" & "'"
    '                Case 4
    '                    SSQL = SSQL & " And Manipulation='" & vManipulation & "--" & "Enroll Manager" & "'"
    '                Case 5
    '                    SSQL = SSQL & " And Manipulation='" & vManipulation & "--" & "Delete Fp Data" & "'"
    '                Case 6
    '                    SSQL = SSQL & " And Manipulation='" & vManipulation & "--" & "Delete Password" & "'"
    '                Case 7
    '                    SSQL = SSQL & " And Manipulation='" & vManipulation & "--" & "Delete Card Data" & "'"
    '                Case 8
    '                    SSQL = SSQL & " And Manipulation='" & vManipulation & "--" & "Delete All LogData" & "'"
    '                Case 9
    '                    SSQL = SSQL & " And Manipulation='" & vManipulation & "--" & "Modify System Info" & "'"
    '                Case 10
    '                    SSQL = SSQL & " And Manipulation='" & vManipulation & "--" & "Modify System Time" & "'"
    '                Case 11
    '                    SSQL = SSQL & " And Manipulation='" & vManipulation & "--" & "Modify Log Setting" & "'"
    '                Case 12
    '                    SSQL = SSQL & " And Manipulation='" & vManipulation & "--" & "Modify Comm Setting" & "'"
    '            End Select

    '            If vFingerNumber < 10 Then
    '                SSQL = SSQL & " And FpNo='" & vFingerNumber & "'"
    '            ElseIf vFingerNumber = 10 Then
    '                SSQL = SSQL & " And FpNo='" & "Password" & "'"
    '            Else
    '                SSQL = SSQL & " And FpNo='" & "Card" & "'"
    '            End If
    '            SSQL = SSQL & " And HDateTime='" & CStr(vYear) & "/" & Format(vMonth, "0#") & "/" & Format(vDay, "0#") & _
    '            " " & Format(vHour, "0#") & ":" & Format(vMinute, "0#") & "'"

    '            Dim mStatus As Long
    '            mStatus = InsertDeleteUpdate(SSQL)


    '            SSQL = ""
    '            SSQL = "Insert into LogHistory_Details Values("
    '            SSQL = SSQL & "'" & Trim(iStr1(0)) & "',"
    '            SSQL = SSQL & "'" & Trim(iStr2(0)) & "',"
    '            SSQL = SSQL & "'" & Trim(iStr3(0)) & "',"
    '            SSQL = SSQL & "'" & vTMachineNumber & "',"
    '            SSQL = SSQL & "'" & vSEnrollNumber & "',"
    '            SSQL = SSQL & "'" & vSMachineNumber & "',"
    '            SSQL = SSQL & "'" & vGEnrollNumber & "',"
    '            SSQL = SSQL & "'" & vGMachineNumber & "',"

    '            Select Case vManipulation
    '                Case 1
    '                Case 2
    '                Case 3
    '                    SSQL = SSQL & "'" & vManipulation & "--" & "Enroll User" & "',"
    '                Case 4
    '                    SSQL = SSQL & "'" & vManipulation & "--" & "Enroll Manager" & "',"
    '                Case 5
    '                    SSQL = SSQL & "'" & vManipulation & "--" & "Delete Fp Data" & "',"
    '                Case 6
    '                    SSQL = SSQL & "'" & vManipulation & "--" & "Delete Password" & "',"
    '                Case 7
    '                    SSQL = SSQL & "'" & vManipulation & "--" & "Delete Card Data" & "',"
    '                Case 8
    '                    SSQL = SSQL & "'" & vManipulation & "--" & "Delete All LogData" & "',"
    '                Case 9
    '                    SSQL = SSQL & "'" & vManipulation & "--" & "Modify System Info" & "',"
    '                Case 10
    '                    SSQL = SSQL & "'" & vManipulation & "--" & "Modify System Time" & "',"
    '                Case 11
    '                    SSQL = SSQL & "'" & vManipulation & "--" & "Modify Log Setting" & "',"
    '                Case 12
    '                    SSQL = SSQL & "'" & vManipulation & "--" & "Modify Comm Setting" & "',"
    '            End Select
    '            'Debug.Print(SSQL)

    '            If vFingerNumber < 10 Then
    '                SSQL = SSQL & "'" & vFingerNumber & "',"
    '            ElseIf vFingerNumber = 10 Then
    '                SSQL = SSQL & "'" & "Password" & "',"
    '            Else
    '                SSQL = SSQL & "'" & "Card" & "',"
    '            End If
    '            SSQL = SSQL & "'" & CStr(vYear) & "/" & Format(vMonth, "0#") & "/" & Format(vDay, "0#") & _
    '            " " & Format(vHour, "0#") & ":" & Format(vMinute, "0#") & "')"

    '            mStatus = InsertDeleteUpdate(SSQL)
    '            i = i + 1
    '        Loop
    '    End If
    '    mdiMain.AxSB100PC1.EnableDevice(1, True)
    'End Sub

    'Public Function getData(ByVal IPAddress As String, ByVal Mode As String) As DataTable
    '    Dim dt As New DataTable
    '    Dim dr As DataRow

    '    Dim vTMachineNumber As Long
    '    Dim vSMachineNumber As Long
    '    Dim vSEnrollNumber As Long
    '    Dim vVerifyMode As Long
    '    Dim vYear As Long
    '    Dim vMonth As Long
    '    Dim vDay As Long
    '    Dim vHour As Long
    '    Dim vMinute As Long
    '    Dim vRet As Boolean
    '    Dim i As Long
    '    Dim vMaxLogCnt As Long
    '    vMaxLogCnt = 30000
    '    'dt.Columns.Add("IP", GetType(String))
    '    dt.Columns.Add("Emp Code", GetType(String))
    '    dt.Columns.Add("Shift Date", GetType(Date))
    '    If Mode = "IN" Then
    '        dt.Columns.Add("Time In", GetType(Date))
    '    ElseIf Mode = "OUT" Then
    '        dt.Columns.Add("Time Out", GetType(Date))
    '    End If



    '    mdiMain.AxSB100PC1.SetIPAddress("192.168.1.224", CLng("5005"), CLng("0"))

    '    If (mdiMain.AxSB100PC1.OpenCommPort(1)) = False Then
    '        MessageBox.Show("Error.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '        Return Nothing
    '    End If
    '    vRet = mdiMain.AxSB100PC1.ReadAllGLogData(1)


    '    i = 1
    '    Do

    '        vRet = mdiMain.AxSB100PC1.GetAllGLogData(1, _
    '        vTMachineNumber, _
    '        vSEnrollNumber, _
    '        vSMachineNumber, _
    '        vVerifyMode, _
    '        vYear, _
    '        vMonth, _
    '        vDay, _
    '        vHour, _
    '        vMinute)
    '        If vRet = False Then Exit Do
    '        If vRet = True And i <> 1 Then
    '            dr = dt.NewRow()
    '        End If
    '        Dim strTime As String = CStr(vYear) & "/" & Format(vMonth, "0#") & "/" & Format(vDay, "0#") & _
    '        " " & Format(vHour, "0#") & ":" & Format(vMinute, "0#")
    '        Dim strDate As String = Format(vDay, "0#") & "/" & Format(vMonth, "0#") & "/" & CStr(vYear)

    '        dt.Rows.Add(New Object() {vSEnrollNumber, strDate, Format(CDate(strTime), "hh:mm tt")})
    '        'ds.Tables(0).Rows.Add(New Object() {vSEnrollNumber, strDate, Format(CDate(strTime), "hh:mm tt")})

    '        ' '' ''.Row = i
    '        ' '' ''.Col = 0
    '        ' '' ''.Text = i
    '        ' '' ''.Col = 1
    '        ' '' ''.Text = vTMachineNumber
    '        ' '' ''.Col = 2
    '        ' '' ''.Text = vSEnrollNumber
    '        ' '' ''.Col = 3
    '        ' '' ''.Text = vSMachineNumber
    '        ' '' ''.Col = 4
    '        ' '' ''If vVerifyMode = 1 Then
    '        ' '' '' .Text = "Fp"
    '        ' '' ''ElseIf vVerifyMode = 2 Then
    '        ' '' '' .Text = "Password"
    '        ' '' ''ElseIf vVerifyMode = 3 Then
    '        ' '' '' .Text = "Card"
    '        ' '' ''ElseIf vVerifyMode = 10 Then
    '        ' '' '' .Text = "Hand Lock"
    '        ' '' ''ElseIf vVerifyMode = 11 Then
    '        ' '' '' .Text = "Prog Lock"
    '        ' '' ''ElseIf vVerifyMode = 12 Then
    '        ' '' '' .Text = "Prog Open"
    '        ' '' ''ElseIf vVerifyMode = 13 Then
    '        ' '' '' .Text = "Prog Close"
    '        ' '' ''ElseIf vVerifyMode = 14 Then
    '        ' '' '' .Text = "Auto Recover"
    '        ' '' ''ElseIf vVerifyMode = 20 Then
    '        ' '' '' .Text = "Lock Over"
    '        ' '' ''ElseIf vVerifyMode = 21 Then
    '        ' '' '' .Text = "Illegal Open"


    '        ' '' ''ElseIf vVerifyMode = 23 Then
    '        ' '' '' .Text = "FP_IN"
    '        ' '' ''ElseIf vVerifyMode = 24 Then
    '        ' '' '' .Text = "PWD_IN"
    '        ' '' ''ElseIf vVerifyMode = 25 Then
    '        ' '' '' .Text = "CARD_IN"
    '        ' '' ''ElseIf vVerifyMode = 26 Then
    '        ' '' '' .Text = "FP_OUT"
    '        ' '' ''ElseIf vVerifyMode = 27 Then
    '        ' '' '' .Text = "PWD_OUT"
    '        ' '' ''ElseIf vVerifyMode = 28 Then
    '        ' '' '' .Text = "CARD_OUT"

    '        ' '' ''Else
    '        ' '' '' .Text = "--"
    '        ' '' ''End If
    '        ' '' ''.Col = 5
    '        '' '' ''Dim strTime As String = CStr(vYear) & "/" & Format(vMonth, "0#") & "/" & Format(vDay, "0#") & _
    '        '' '' '' " " & Format(vHour, "0#") & ":" & Format(vMinute, "0#")
    '        ' '' ''.Text = strTime


    '        'ds.Tables(0).Rows.Add(New Object() {vTMachineNumber, vSEnrollNumber, vSMachineNumber, gridSLogData(i, 4), strTime})

    '        i = i + 1
    '        If i > vMaxLogCnt Then Exit Do
    '    Loop


    '    mdiMain.AxSB100PC1.EnableDevice(1, True)

    '    ' '' ''If dt Is Nothing OrElse dt.Rows.Count = 0 Then
    '    ' '' '' MsgBox("No data found!")
    '    ' '' '' Exit Function
    '    ' '' ''End If

    '    Return dt
    'End Function


    Private Sub btnclear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclear.Click
        btnDownload.Enabled = False

        cmbCompCode.Enabled = False
        cmbLocCode.Enabled = False
        cmbIPAddress.Enabled = False

        Me.Text = "Started... Please Wait..."

        If Trim(cmbCompCode.Text) = "" Then
            MessageBox.Show("Please select company code.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbCompCode.Focus()
            Exit Sub
        End If

        If Trim(cmbLocCode.Text) = "" Then
            MessageBox.Show("Please select location code.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbLocCode.Focus()
            Exit Sub
        End If

        If Trim(cmbIPAddress.Text) = "" Then
            MessageBox.Show("Please select IP Address.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbIPAddress.Focus()
            Exit Sub
        End If

        iStr1 = Split(cmbCompCode.Text, " | ")
        iStr2 = Split(cmbLocCode.Text, " | ")
        iStr3 = Split(cmbIPAddress.Text, " | ")

        '--------------- HISTORY DOWNLOAD ------------------------------'
        ''downloadHistoryDetails()

        '--------------- HISTORY CLEAR ---------------------------------'
        '' clearHistoryDetails()

        '--------------- LOG DOWNLOAD ----------------------------------'
        downloadLogDetails()

        '--------------- LOG CLEAR -------------------------------------'
        ''clearLogDetails()


        'Clear Attn Log
        TFT_Machine_Attn_Log_Clear()


        Me.Text = "Download / Clear"

        cmbCompCode.Enabled = True
        cmbLocCode.Enabled = True
        cmbIPAddress.Enabled = True

        btnDownload.Enabled = True
    End Sub
End Class