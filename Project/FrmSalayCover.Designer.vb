﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSalayCover
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.cmbWages = New System.Windows.Forms.ComboBox
        Me.Label74 = New System.Windows.Forms.Label
        Me.lblDeductionsAmt = New System.Windows.Forms.Label
        Me.lblAllowanceAmt = New System.Windows.Forms.Label
        Me.lblWEarnedAmt = New System.Windows.Forms.Label
        Me.lblDeduction = New System.Windows.Forms.Label
        Me.lblAllowance = New System.Windows.Forms.Label
        Me.lblWagesEarn = New System.Windows.Forms.Label
        Me.lblRupees = New System.Windows.Forms.Label
        Me.lblNetAmt = New System.Windows.Forms.Label
        Me.lblWagesType = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtToDate = New System.Windows.Forms.DateTimePicker
        Me.txtFromDate = New System.Windows.Forms.DateTimePicker
        Me.Label23 = New System.Windows.Forms.Label
        Me.Label24 = New System.Windows.Forms.Label
        Me.cmbFinYear = New System.Windows.Forms.ComboBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.cmbMonth = New System.Windows.Forms.ComboBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.lblDesg = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.lblDept = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.lblEmpName = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.lblMachineID = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.lblTokenNo = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.pbEmployee = New System.Windows.Forms.PictureBox
        Me.cmbIPAddress = New System.Windows.Forms.ComboBox
        Me.cmbLocCode = New System.Windows.Forms.ComboBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.cmbCompCode = New System.Windows.Forms.ComboBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.BtnClear = New System.Windows.Forms.Button
        Me.btnExit = New System.Windows.Forms.Button
        Me.btnConnect = New System.Windows.Forms.Button
        Me.btnReport = New System.Windows.Forms.Button
        Me.btnupload = New System.Windows.Forms.Button
        Me.lblAtten_Inecn_Amt = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.GroupBox1.SuspendLayout()
        CType(Me.pbEmployee, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.GroupBox1.Controls.Add(Me.lblAtten_Inecn_Amt)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.cmbWages)
        Me.GroupBox1.Controls.Add(Me.Label74)
        Me.GroupBox1.Controls.Add(Me.lblDeductionsAmt)
        Me.GroupBox1.Controls.Add(Me.lblAllowanceAmt)
        Me.GroupBox1.Controls.Add(Me.lblWEarnedAmt)
        Me.GroupBox1.Controls.Add(Me.lblDeduction)
        Me.GroupBox1.Controls.Add(Me.lblAllowance)
        Me.GroupBox1.Controls.Add(Me.lblWagesEarn)
        Me.GroupBox1.Controls.Add(Me.lblRupees)
        Me.GroupBox1.Controls.Add(Me.lblNetAmt)
        Me.GroupBox1.Controls.Add(Me.lblWagesType)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.txtToDate)
        Me.GroupBox1.Controls.Add(Me.txtFromDate)
        Me.GroupBox1.Controls.Add(Me.Label23)
        Me.GroupBox1.Controls.Add(Me.Label24)
        Me.GroupBox1.Controls.Add(Me.cmbFinYear)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.cmbMonth)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.lblDesg)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.lblDept)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.lblEmpName)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.lblMachineID)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.lblTokenNo)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.pbEmployee)
        Me.GroupBox1.Controls.Add(Me.cmbIPAddress)
        Me.GroupBox1.Controls.Add(Me.cmbLocCode)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.cmbCompCode)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Location = New System.Drawing.Point(-4, -6)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(814, 487)
        Me.GroupBox1.TabIndex = 25
        Me.GroupBox1.TabStop = False
        '
        'cmbWages
        '
        Me.cmbWages.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbWages.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbWages.FormattingEnabled = True
        Me.cmbWages.Items.AddRange(New Object() {"MONTHLY TEMP WORKER", "MONTHLY C-WORKER", "MONTHLY STAFF-II", "MONTHLY M-WORKER", "MONTHLY STAFF-I", "MONTHLY TEN DAYS", "WEEKLY", "WEEKLY II", "NEW COMMERS"})
        Me.cmbWages.Location = New System.Drawing.Point(126, 101)
        Me.cmbWages.Name = "cmbWages"
        Me.cmbWages.Size = New System.Drawing.Size(379, 24)
        Me.cmbWages.TabIndex = 101
        '
        'Label74
        '
        Me.Label74.AutoSize = True
        Me.Label74.BackColor = System.Drawing.Color.Transparent
        Me.Label74.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label74.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label74.Location = New System.Drawing.Point(6, 104)
        Me.Label74.Name = "Label74"
        Me.Label74.Size = New System.Drawing.Size(97, 16)
        Me.Label74.TabIndex = 100
        Me.Label74.Text = "Wages Type"
        '
        'lblDeductionsAmt
        '
        Me.lblDeductionsAmt.BackColor = System.Drawing.Color.Transparent
        Me.lblDeductionsAmt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblDeductionsAmt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDeductionsAmt.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.lblDeductionsAmt.Location = New System.Drawing.Point(340, 333)
        Me.lblDeductionsAmt.Name = "lblDeductionsAmt"
        Me.lblDeductionsAmt.Size = New System.Drawing.Size(165, 22)
        Me.lblDeductionsAmt.TabIndex = 89
        '
        'lblAllowanceAmt
        '
        Me.lblAllowanceAmt.BackColor = System.Drawing.Color.Transparent
        Me.lblAllowanceAmt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAllowanceAmt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAllowanceAmt.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.lblAllowanceAmt.Location = New System.Drawing.Point(340, 306)
        Me.lblAllowanceAmt.Name = "lblAllowanceAmt"
        Me.lblAllowanceAmt.Size = New System.Drawing.Size(165, 22)
        Me.lblAllowanceAmt.TabIndex = 88
        '
        'lblWEarnedAmt
        '
        Me.lblWEarnedAmt.BackColor = System.Drawing.Color.Transparent
        Me.lblWEarnedAmt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblWEarnedAmt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWEarnedAmt.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.lblWEarnedAmt.Location = New System.Drawing.Point(340, 278)
        Me.lblWEarnedAmt.Name = "lblWEarnedAmt"
        Me.lblWEarnedAmt.Size = New System.Drawing.Size(165, 22)
        Me.lblWEarnedAmt.TabIndex = 87
        '
        'lblDeduction
        '
        Me.lblDeduction.BackColor = System.Drawing.Color.Transparent
        Me.lblDeduction.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblDeduction.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDeduction.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.lblDeduction.Location = New System.Drawing.Point(189, 334)
        Me.lblDeduction.Name = "lblDeduction"
        Me.lblDeduction.Size = New System.Drawing.Size(147, 21)
        Me.lblDeduction.TabIndex = 86
        Me.lblDeduction.Text = "Deductions :"
        Me.lblDeduction.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblAllowance
        '
        Me.lblAllowance.BackColor = System.Drawing.Color.Transparent
        Me.lblAllowance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAllowance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAllowance.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.lblAllowance.Location = New System.Drawing.Point(189, 306)
        Me.lblAllowance.Name = "lblAllowance"
        Me.lblAllowance.Size = New System.Drawing.Size(147, 21)
        Me.lblAllowance.TabIndex = 85
        Me.lblAllowance.Text = "Allowances  :"
        Me.lblAllowance.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblWagesEarn
        '
        Me.lblWagesEarn.BackColor = System.Drawing.Color.Transparent
        Me.lblWagesEarn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblWagesEarn.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWagesEarn.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.lblWagesEarn.Location = New System.Drawing.Point(189, 278)
        Me.lblWagesEarn.Name = "lblWagesEarn"
        Me.lblWagesEarn.Size = New System.Drawing.Size(147, 21)
        Me.lblWagesEarn.TabIndex = 84
        Me.lblWagesEarn.Text = "Wages Earned  :"
        Me.lblWagesEarn.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblRupees
        '
        Me.lblRupees.BackColor = System.Drawing.Color.Transparent
        Me.lblRupees.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblRupees.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRupees.ForeColor = System.Drawing.Color.Red
        Me.lblRupees.Location = New System.Drawing.Point(6, 438)
        Me.lblRupees.Name = "lblRupees"
        Me.lblRupees.Size = New System.Drawing.Size(802, 40)
        Me.lblRupees.TabIndex = 83
        Me.lblRupees.Text = "RUPEES :"
        Me.lblRupees.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblNetAmt
        '
        Me.lblNetAmt.BackColor = System.Drawing.Color.Transparent
        Me.lblNetAmt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNetAmt.Font = New System.Drawing.Font("Arial", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNetAmt.ForeColor = System.Drawing.Color.Red
        Me.lblNetAmt.Location = New System.Drawing.Point(5, 387)
        Me.lblNetAmt.Name = "lblNetAmt"
        Me.lblNetAmt.Size = New System.Drawing.Size(500, 48)
        Me.lblNetAmt.TabIndex = 82
        Me.lblNetAmt.Text = "NET AMOUNT : 0.00"
        Me.lblNetAmt.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblWagesType
        '
        Me.lblWagesType.BackColor = System.Drawing.Color.Transparent
        Me.lblWagesType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblWagesType.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWagesType.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.lblWagesType.Location = New System.Drawing.Point(126, 249)
        Me.lblWagesType.Name = "lblWagesType"
        Me.lblWagesType.Size = New System.Drawing.Size(104, 22)
        Me.lblWagesType.TabIndex = 80
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label7.Location = New System.Drawing.Point(9, 250)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(111, 21)
        Me.Label7.TabIndex = 79
        Me.Label7.Text = "Wages Type  :"
        '
        'txtToDate
        '
        Me.txtToDate.CustomFormat = "dd/MM/yyyy"
        Me.txtToDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.txtToDate.Location = New System.Drawing.Point(364, 162)
        Me.txtToDate.Name = "txtToDate"
        Me.txtToDate.Size = New System.Drawing.Size(140, 22)
        Me.txtToDate.TabIndex = 76
        '
        'txtFromDate
        '
        Me.txtFromDate.CustomFormat = "dd/MM/yyyy"
        Me.txtFromDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.txtFromDate.Location = New System.Drawing.Point(125, 162)
        Me.txtFromDate.Name = "txtFromDate"
        Me.txtFromDate.Size = New System.Drawing.Size(159, 22)
        Me.txtFromDate.TabIndex = 75
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.BackColor = System.Drawing.Color.Transparent
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label23.Location = New System.Drawing.Point(290, 164)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(64, 16)
        Me.Label23.TabIndex = 78
        Me.Label23.Text = "To Date"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.BackColor = System.Drawing.Color.Transparent
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label24.Location = New System.Drawing.Point(5, 164)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(80, 16)
        Me.Label24.TabIndex = 77
        Me.Label24.Text = "From Date"
        '
        'cmbFinYear
        '
        Me.cmbFinYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbFinYear.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbFinYear.FormattingEnabled = True
        Me.cmbFinYear.Location = New System.Drawing.Point(364, 132)
        Me.cmbFinYear.Name = "cmbFinYear"
        Me.cmbFinYear.Size = New System.Drawing.Size(140, 24)
        Me.cmbFinYear.TabIndex = 42
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label4.Location = New System.Drawing.Point(290, 135)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(70, 16)
        Me.Label4.TabIndex = 44
        Me.Label4.Text = "Fin. Year"
        '
        'cmbMonth
        '
        Me.cmbMonth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbMonth.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbMonth.FormattingEnabled = True
        Me.cmbMonth.Items.AddRange(New Object() {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"})
        Me.cmbMonth.Location = New System.Drawing.Point(125, 130)
        Me.cmbMonth.Name = "cmbMonth"
        Me.cmbMonth.Size = New System.Drawing.Size(159, 24)
        Me.cmbMonth.TabIndex = 41
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label6.Location = New System.Drawing.Point(5, 133)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(49, 16)
        Me.Label6.TabIndex = 43
        Me.Label6.Text = "Month"
        '
        'lblDesg
        '
        Me.lblDesg.BackColor = System.Drawing.Color.Transparent
        Me.lblDesg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblDesg.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDesg.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.lblDesg.Location = New System.Drawing.Point(340, 249)
        Me.lblDesg.Name = "lblDesg"
        Me.lblDesg.Size = New System.Drawing.Size(165, 22)
        Me.lblDesg.TabIndex = 25
        '
        'Label13
        '
        Me.Label13.BackColor = System.Drawing.Color.Transparent
        Me.Label13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label13.Location = New System.Drawing.Point(234, 249)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(102, 22)
        Me.Label13.TabIndex = 24
        Me.Label13.Text = "Desg     :"
        '
        'lblDept
        '
        Me.lblDept.BackColor = System.Drawing.Color.Transparent
        Me.lblDept.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblDept.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDept.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.lblDept.Location = New System.Drawing.Point(340, 225)
        Me.lblDept.Name = "lblDept"
        Me.lblDept.Size = New System.Drawing.Size(165, 21)
        Me.lblDept.TabIndex = 23
        '
        'Label8
        '
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        Me.Label8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label8.Location = New System.Drawing.Point(234, 225)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(102, 21)
        Me.Label8.TabIndex = 22
        Me.Label8.Text = "Dept      :"
        '
        'lblEmpName
        '
        Me.lblEmpName.BackColor = System.Drawing.Color.Transparent
        Me.lblEmpName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblEmpName.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpName.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.lblEmpName.Location = New System.Drawing.Point(340, 198)
        Me.lblEmpName.Name = "lblEmpName"
        Me.lblEmpName.Size = New System.Drawing.Size(165, 22)
        Me.lblEmpName.TabIndex = 21
        '
        'Label10
        '
        Me.Label10.BackColor = System.Drawing.Color.Transparent
        Me.Label10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label10.Location = New System.Drawing.Point(234, 198)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(102, 22)
        Me.Label10.TabIndex = 20
        Me.Label10.Text = "Emp. Name  : "
        '
        'lblMachineID
        '
        Me.lblMachineID.BackColor = System.Drawing.Color.Transparent
        Me.lblMachineID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblMachineID.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMachineID.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.lblMachineID.Location = New System.Drawing.Point(126, 225)
        Me.lblMachineID.Name = "lblMachineID"
        Me.lblMachineID.Size = New System.Drawing.Size(104, 21)
        Me.lblMachineID.TabIndex = 19
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label5.Location = New System.Drawing.Point(9, 225)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(111, 21)
        Me.Label5.TabIndex = 18
        Me.Label5.Text = "MG. No : "
        '
        'lblTokenNo
        '
        Me.lblTokenNo.BackColor = System.Drawing.Color.Transparent
        Me.lblTokenNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblTokenNo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTokenNo.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.lblTokenNo.Location = New System.Drawing.Point(126, 198)
        Me.lblTokenNo.Name = "lblTokenNo"
        Me.lblTokenNo.Size = New System.Drawing.Size(104, 22)
        Me.lblTokenNo.TabIndex = 17
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label3.Location = New System.Drawing.Point(9, 198)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(111, 22)
        Me.Label3.TabIndex = 16
        Me.Label3.Text = "Token No       : "
        '
        'pbEmployee
        '
        Me.pbEmployee.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.pbEmployee.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pbEmployee.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pbEmployee.Location = New System.Drawing.Point(508, 11)
        Me.pbEmployee.Name = "pbEmployee"
        Me.pbEmployee.Size = New System.Drawing.Size(300, 424)
        Me.pbEmployee.TabIndex = 15
        Me.pbEmployee.TabStop = False
        '
        'cmbIPAddress
        '
        Me.cmbIPAddress.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbIPAddress.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbIPAddress.FormattingEnabled = True
        Me.cmbIPAddress.Location = New System.Drawing.Point(126, 71)
        Me.cmbIPAddress.Name = "cmbIPAddress"
        Me.cmbIPAddress.Size = New System.Drawing.Size(379, 24)
        Me.cmbIPAddress.TabIndex = 2
        '
        'cmbLocCode
        '
        Me.cmbLocCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbLocCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbLocCode.FormattingEnabled = True
        Me.cmbLocCode.Location = New System.Drawing.Point(126, 41)
        Me.cmbLocCode.Name = "cmbLocCode"
        Me.cmbLocCode.Size = New System.Drawing.Size(379, 24)
        Me.cmbLocCode.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label2.Location = New System.Drawing.Point(6, 75)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(84, 16)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "IP Address"
        '
        'cmbCompCode
        '
        Me.cmbCompCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbCompCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbCompCode.FormattingEnabled = True
        Me.cmbCompCode.Location = New System.Drawing.Point(126, 11)
        Me.cmbCompCode.Name = "cmbCompCode"
        Me.cmbCompCode.Size = New System.Drawing.Size(379, 24)
        Me.cmbCompCode.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label1.Location = New System.Drawing.Point(6, 45)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(108, 16)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Location Code"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.BackColor = System.Drawing.Color.Transparent
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label11.Location = New System.Drawing.Point(6, 15)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(114, 16)
        Me.Label11.TabIndex = 4
        Me.Label11.Text = "Company Code"
        '
        'BtnClear
        '
        Me.BtnClear.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnClear.Location = New System.Drawing.Point(117, 487)
        Me.BtnClear.Name = "BtnClear"
        Me.BtnClear.Size = New System.Drawing.Size(102, 30)
        Me.BtnClear.TabIndex = 29
        Me.BtnClear.Text = "&Clear"
        Me.BtnClear.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExit.Location = New System.Drawing.Point(222, 487)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(102, 30)
        Me.btnExit.TabIndex = 28
        Me.btnExit.Text = "E&xit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnConnect
        '
        Me.btnConnect.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnConnect.Location = New System.Drawing.Point(1, 487)
        Me.btnConnect.Name = "btnConnect"
        Me.btnConnect.Size = New System.Drawing.Size(112, 30)
        Me.btnConnect.TabIndex = 27
        Me.btnConnect.Text = "&Connect"
        Me.btnConnect.UseVisualStyleBackColor = True
        '
        'btnReport
        '
        Me.btnReport.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReport.Location = New System.Drawing.Point(329, 487)
        Me.btnReport.Name = "btnReport"
        Me.btnReport.Size = New System.Drawing.Size(102, 30)
        Me.btnReport.TabIndex = 90
        Me.btnReport.Text = "R&eport"
        Me.btnReport.UseVisualStyleBackColor = True
        '
        'btnupload
        '
        Me.btnupload.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnupload.Location = New System.Drawing.Point(504, 487)
        Me.btnupload.Name = "btnupload"
        Me.btnupload.Size = New System.Drawing.Size(300, 30)
        Me.btnupload.TabIndex = 91
        Me.btnupload.Text = "&Salary Transfer"
        Me.btnupload.UseVisualStyleBackColor = True
        '
        'lblAtten_Inecn_Amt
        '
        Me.lblAtten_Inecn_Amt.BackColor = System.Drawing.Color.Transparent
        Me.lblAtten_Inecn_Amt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAtten_Inecn_Amt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAtten_Inecn_Amt.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.lblAtten_Inecn_Amt.Location = New System.Drawing.Point(340, 361)
        Me.lblAtten_Inecn_Amt.Name = "lblAtten_Inecn_Amt"
        Me.lblAtten_Inecn_Amt.Size = New System.Drawing.Size(165, 22)
        Me.lblAtten_Inecn_Amt.TabIndex = 103
        '
        'Label12
        '
        Me.Label12.BackColor = System.Drawing.Color.Transparent
        Me.Label12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label12.Location = New System.Drawing.Point(189, 362)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(147, 21)
        Me.Label12.TabIndex = 102
        Me.Label12.Text = "Attn. Incentive :"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'FrmSalayCover
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(805, 519)
        Me.Controls.Add(Me.btnupload)
        Me.Controls.Add(Me.btnReport)
        Me.Controls.Add(Me.BtnClear)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnConnect)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "FrmSalayCover"
        Me.Text = "FrmSalayCover"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.pbEmployee, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lblRupees As System.Windows.Forms.Label
    Friend WithEvents lblNetAmt As System.Windows.Forms.Label
    Friend WithEvents lblWagesType As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtToDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtFromDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents cmbFinYear As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cmbMonth As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents lblDesg As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents lblDept As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents lblEmpName As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents lblMachineID As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lblTokenNo As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents pbEmployee As System.Windows.Forms.PictureBox
    Friend WithEvents cmbIPAddress As System.Windows.Forms.ComboBox
    Friend WithEvents cmbLocCode As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cmbCompCode As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents BtnClear As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnConnect As System.Windows.Forms.Button
    Friend WithEvents lblDeduction As System.Windows.Forms.Label
    Friend WithEvents lblAllowance As System.Windows.Forms.Label
    Friend WithEvents lblWagesEarn As System.Windows.Forms.Label
    Friend WithEvents lblDeductionsAmt As System.Windows.Forms.Label
    Friend WithEvents lblAllowanceAmt As System.Windows.Forms.Label
    Friend WithEvents lblWEarnedAmt As System.Windows.Forms.Label
    Friend WithEvents btnReport As System.Windows.Forms.Button
    Friend WithEvents btnupload As System.Windows.Forms.Button
    Friend WithEvents cmbWages As System.Windows.Forms.ComboBox
    Friend WithEvents Label74 As System.Windows.Forms.Label
    Friend WithEvents lblAtten_Inecn_Amt As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
End Class
