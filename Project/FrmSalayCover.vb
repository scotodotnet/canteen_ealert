﻿#Region "Imports"
Imports System
Imports System.Data
Imports System.Data.OleDb
Imports CrystalDecisions.CrystalReports.Engine

#End Region
Public Class FrmSalayCover

    Private iStr1() As String
    Private iStr2() As String
    Private iStr3() As String
    Private iYear() As String
    Dim mStatus As Long
    Private Check_Download_Clear_Error As Boolean = False
    Public axCZKEM1 As New zkemkeeper.CZKEM
    Private bIsConnected = False 'the boolean value identifies whether the device is connected
    Private iMachineNumber As Integer

    Private Sub FrmSalayCover_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormAlign(Me)
        Fill_Company_Details()
        cmbCompCode.SelectedIndex = 0
        cmbCompCode.Enabled = False
        cmbCompCode_LostFocus(sender, e)

        If Trim(mUserLocation) <> "" Then
            cmbLocCode.SelectedIndex = cmbLocCode.FindString(Trim(mUserLocation))
            cmbLocCode.Enabled = False

            Dim iStr1() As String
            Dim iStr2() As String
            Dim iYear() As String

            iStr1 = Split(Trim(cmbCompCode.Text), " | ")
            iStr2 = Split(Trim(cmbLocCode.Text), " | ")
            iYear = Split(Trim(cmbFinYear.Text), " - ")
        Else
            cmbLocCode.SelectedIndex = 0
        End If
        cmbLocCode_LostFocus(sender, e)
        Check_Download_Clear_Error = False

        Call Fin_Year_Add()
    End Sub
    Public Sub Fill_Company_Details()
        SSQL = ""
        SSQL = "Select CompCode + ' | ' + CompName as [Name] from Company_Mst Order By CompName"

        mDataSet = ReturnMultipleValue(SSQL)
        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                cmbCompCode.Items.Clear()
                For iRow = 0 To .Rows.Count - 1
                    cmbCompCode.Items.Add(.Rows(iRow)(0))
                Next
            End If
        End With
    End Sub

    Private Sub Fin_Year_Add()

        Dim CurrentYear As Int32
        Dim i As Int32
        'Financial Year Add
        CurrentYear = Year(Now)
        cmbFinYear.Items.Clear()
        For i = 0 To 11
            cmbFinYear.Items.Add(Convert.ToString(CurrentYear) & "-" & Convert.ToString(CurrentYear + 1))
            CurrentYear = CurrentYear - 1
        Next i
        cmbFinYear.SelectedIndex = 0
    End Sub
    Private Sub cmbCompCode_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCompCode.LostFocus
        If Trim(cmbCompCode.Text) = "" Then Exit Sub
        Dim iStr() As String
        iStr = Split(Trim(cmbCompCode.Text), " | ")

        SSQL = ""
        SSQL = "Select LocCode + ' | ' + LocName as [Name] from Location_Mst Where Compcode='"
        SSQL = SSQL & "" & iStr(0) & "'"

        mDataSet = ReturnMultipleValue(SSQL)

        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                cmbLocCode.Items.Clear()
                For iRow = 0 To .Rows.Count - 1
                    cmbLocCode.Items.Add(.Rows(iRow)(0))
                Next
            End If
        End With
    End Sub
    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Close()
    End Sub

    Private Sub cmbLocCode_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbLocCode.LostFocus
        If Trim(cmbCompCode.Text) = "" Then Exit Sub
        If Trim(cmbLocCode.Text) = "" Then Exit Sub

        Dim iStr1() As String
        Dim iStr2() As String

        iStr1 = Split(Trim(cmbCompCode.Text), " | ")
        iStr2 = Split(Trim(cmbLocCode.Text), " | ")

        SSQL = ""
        SSQL = "Select IPAddress + ' | ' + IPMode as [Name] from IPAddress_Mst Where Compcode='"
        SSQL = SSQL & "" & iStr1(0) & "'"
        SSQL = SSQL & " And LocCode='" & iStr2(0) & "'"

        mDataSet = ReturnMultipleValue(SSQL)

        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                cmbIPAddress.Items.Clear()
                For iRow = 0 To .Rows.Count - 1
                    cmbIPAddress.Items.Add(.Rows(iRow)(0))
                Next
            End If
        End With
    End Sub

    Private Sub btnConnect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConnect.Click

        If Trim(cmbCompCode.Text) = "" Then
            MessageBox.Show("Please select company code.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbCompCode.Focus()
            Exit Sub
        End If

        If Trim(cmbLocCode.Text) = "" Then
            MessageBox.Show("Please select location code.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbLocCode.Focus()
            Exit Sub
        End If

        If Trim(cmbIPAddress.Text) = "" Then
            MessageBox.Show("Please select IP Address.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbIPAddress.Focus()
            Exit Sub
        End If

        iStr1 = Split(cmbCompCode.Text, " | ")
        iStr2 = Split(cmbLocCode.Text, " | ")
        iStr3 = Split(cmbIPAddress.Text, " | ")

        Connect_Machine_RTEvents()
    End Sub

    Private Sub Connect_Machine_RTEvents()
        Dim idwErrorCode As Integer
        Cursor = Cursors.WaitCursor
        If btnConnect.Text = "Disconnect" Then
            axCZKEM1.Disconnect()

            RemoveHandler axCZKEM1.OnAttTransactionEx, AddressOf AxCZKEM1_OnAttTransactionEx

            bIsConnected = False
            btnConnect.Text = "Connect"
            Cursor = Cursors.Default
            Return
        End If

        bIsConnected = axCZKEM1.Connect_Net(Trim(iStr3(0)), 4370)
        If bIsConnected = True Then
            btnConnect.Text = "Disconnect"
            btnConnect.Refresh()
            iMachineNumber = 1 'In fact,when you are using the tcp/ip communication,this parameter will be ignored,that is any integer will all right.Here we use 1.

            If axCZKEM1.RegEvent(iMachineNumber, 65535) = True Then 'Here you can register the realtime events that you want to be triggered(the parameters 65535 means registering all)

                AddHandler axCZKEM1.OnAttTransactionEx, AddressOf AxCZKEM1_OnAttTransactionEx

            End If
        Else
            axCZKEM1.GetLastError(idwErrorCode)
            MsgBox("Unable to connect the device,ErrorCode=" & idwErrorCode, MsgBoxStyle.Exclamation, "Error")
        End If
        Cursor = Cursors.Default
    End Sub

    'If your fingerprint(or your card) passes the verification,this event will be triggered
    Private Sub AxCZKEM1_OnAttTransactionEx(ByVal sEnrollNumber As String, ByVal iIsInValid As Integer, ByVal iAttState As Integer, ByVal iVerifyMethod As Integer, _
                      ByVal iYear As Integer, ByVal iMonth As Integer, ByVal iDay As Integer, ByVal iHour As Integer, ByVal iMinute As Integer, ByVal iSecond As Integer, ByVal iWorkCode As Integer)

        Dim User_Machine_ID As String
        Dim AmountInWords_Str As String
        Dim Class_Check As New clsConversion
        Dim mDataSal As New DataSet
        User_Machine_ID = sEnrollNumber
        Dim MonthHead As String = cmbMonth.Text
        Dim Heading As String = MonthHead.Substring(0, 3)
        Dim GetYear() As String = Split(Trim(cmbFinYear.Text), "-")

        'Get Employee Details
        SSQL = ""
        SSQL = "Select * from Employee_Mst Where CompCode='" & iStr1(0) & "'"
        SSQL = SSQL & " And LocCode='" & iStr2(0) & "'"
        SSQL = SSQL & " And MachineID='" & Val(Remove_Single_Quote(Trim(User_Machine_ID))) & "'"
        mDataSet = ReturnMultipleValue(SSQL)
        If mDataSet.Tables(0).Rows.Count <> 0 Then
            lblTokenNo.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("ExistingCode")), " ", mDataSet.Tables(0).Rows(0)("ExistingCode"))
            lblMachineID.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("MachineID")), " ", mDataSet.Tables(0).Rows(0)("MachineID"))
            lblEmpName.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("FirstName")), " ", mDataSet.Tables(0).Rows(0)("FirstName"))
            lblDept.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("DeptName")), "", mDataSet.Tables(0).Rows(0)("DeptName"))
            lblDesg.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Designation")), "", mDataSet.Tables(0).Rows(0)("Designation"))
            lblWagesType.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Designation")), "", mDataSet.Tables(0).Rows(0)("Wages"))

            'Get Photo
            Dim sFilePath As String = ""
            Dim mUnitName As String = ""
            Dim NetAmt As String = ""
            Dim WagesType As String = ""


            If (cmbWages.Text = "MONTHLY STAFF-I") Then
                WagesType = "1"
            ElseIf (cmbWages.Text = "MONTHLY STAFF-II") Then
                WagesType = "2"
            ElseIf (cmbWages.Text = "MONTHLY TEMP WORKER") Then
                WagesType = "3"
            ElseIf (cmbWages.Text = "MONTHLY TEN DAYS") Then
                WagesType = "4"
            ElseIf (cmbWages.Text = "MONTHLY C-WORKER") Then
                WagesType = "5"
            ElseIf (cmbWages.Text = "MONTHLY M-WORKER") Then
                WagesType = "6"
            ElseIf (cmbWages.Text = "WEEKLY") Then
                WagesType = "7"
            ElseIf (cmbWages.Text = "WEEKLY II") Then
                WagesType = "8"
            ElseIf (cmbWages.Text = "NEW COMMERS") Then
                WagesType = "9"
            End If


            If UCase(Trim(iStr2(0))) = "UNIT I" Then
                mUnitName = "Unit1-Photos"
            ElseIf UCase(Trim(iStr2(0))) = "UNIT II" Then
                mUnitName = "Unit2-Photos"
            ElseIf UCase(Trim(iStr2(0))) = "UNIT III" Then
                mUnitName = "Unit3-Photos"
            ElseIf UCase(Trim(iStr2(0))) = "UNIT IV" Then
                mUnitName = "Unit4-Photos"
            Else
                mUnitName = "Tapes-Photos"
            End If

            sFilePath = mvarPhotoFilePath & "\" & Trim(mUnitName) _
                                     & "\" & Trim(lblMachineID.Text) & ".JPG"

            If Not IO.File.Exists(sFilePath) Then
                sFilePath = Application.StartupPath & "\Tapes-Photos\No-Iamge.JPG"
            End If

            If sFilePath = "" Then Exit Sub

            pbEmployee.BackgroundImage = Image.FromFile(sFilePath)

            'Get Payroll EmpNo
            Dim Payroll_EmpNo As String = ""
            Dim Fin_Year_Split() As String = Split(Trim(cmbFinYear.Text), "-")


            SSQL = "Select * from [JLM-Epay]..EmployeeDetails where BiometricID='" & mDataSet.Tables(0).Rows(0)("MachineID") & "'"
            SSQL = SSQL & " And Ccode='" & iStr1(0) & "' And Lcode='" & iStr2(0) & "'"
            mDataSet = ReturnMultipleValue(SSQL)

            If mDataSet.Tables(0).Rows.Count <> 0 Then
                Payroll_EmpNo = mDataSet.Tables(0).Rows(0)("EmpNo")

                SSQL = "Select * from [JLM-Epay]..SalaryDetails where Ccode='" & iStr1(0) & "' And Lcode='" & iStr2(0) & "'"
                SSQL = SSQL & " And EmpNo='" & Payroll_EmpNo & "' And Month='" & cmbMonth.Text & "' And FinancialYear='" & Fin_Year_Split(0) & "'"
                SSQL = SSQL & " And FromDate='" & Format(CDate(txtFromDate.Text), "yyyy/MM/dd") & "'"
                SSQL = SSQL & " And ToDate='" & Format(CDate(txtToDate.Text), "yyyy/MM/dd") & "'"
                mDataSet = ReturnMultipleValue(SSQL)
                If mDataSet.Tables(0).Rows.Count <> 0 Then

                    SSQL = "Select * from Salary_Distribute where TknNo='" & lblMachineID.Text & "' and Status='1' and FromDate=convert(datetime,'" & txtFromDate.Text & "',103) and "
                    SSQL = SSQL & "ToDate=convert(datetime,'" & txtToDate.Text & "',103) and Ccode='" & iStr1(0) & "' and Lcode='" & iStr2(0) & "'"
                    mDataSal = ReturnMultipleValue(SSQL)

                    If mDataSal.Tables(0).Rows.Count <> 0 Then
                        MessageBox.Show("Already this Employee got salary.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)

                        lblWEarnedAmt.Text = ""
                        lblAllowanceAmt.Text = ""
                        lblDeductionsAmt.Text = ""
                        lblNetAmt.Text = ""

                    Else

                        lblNetAmt.Text = "NET AMOUNT : " & IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("NetPay")), "", mDataSet.Tables(0).Rows(0)("NetPay"))
                        NetAmt = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("NetPay")), "", mDataSet.Tables(0).Rows(0)("NetPay"))

                        lblWEarnedAmt.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("BasicandDA")), "", mDataSet.Tables(0).Rows(0)("BasicandDA"))
                        lblAllowanceAmt.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("Hosteldeduction")), "", mDataSet.Tables(0).Rows(0)("Hosteldeduction"))
                        lblDeductionsAmt.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("TotalDeductions")), "", mDataSet.Tables(0).Rows(0)("TotalDeductions"))

                        lblAtten_Inecn_Amt.Text = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("SpinningAmt")), "", mDataSet.Tables(0).Rows(0)("SpinningAmt"))

                        AmountInWords_Str = Class_Check.ConvertNumberToWords(IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("NetPay")), "0.0", mDataSet.Tables(0).Rows(0)("NetPay")))

                        'Update
                        SSQL = "Update Salary_Distribute set Status='1' where TknNo='" & lblMachineID.Text & "' and FromDate=convert(datetime,'" & txtFromDate.Text & "',103) and"
                        SSQL = SSQL & " ToDate=convert(datetime,'" & txtToDate.Text & "',103) and Ccode='" & iStr1(0) & "' and Lcode='" & iStr2(0) & "'"
                        mStatus = InsertDeleteUpdate(SSQL)

                        'Print Salary
                        SSQL = "select TknNo,MachineID,EmpName,BankAccNo,WagesType,Attendance,PLdays,NfhDays,FromDate,ToDate,WagesEarn,Allowance,Deduction,NetAmt,Status,convert(varchar(10),YEAR(FromDate),103) as Year,Attn_Incentive from Salary_Distribute "

                        SSQL = SSQL & "where FromDate=convert(datetime,'" & txtFromDate.Text & "',103)"
                        SSQL = SSQL & " and ToDate=convert(datetime,'" & txtToDate.Text & "',103)"
                        SSQL = SSQL & " and TknNo='" & lblMachineID.Text & "'"
                        mDataSet = Nothing
                        mDataSet = ReturnMultipleValue(SSQL)

                        Dim cryRep As New ReportDocument
                        Dim cryView As New frmRepView
                        Dim YearHead As String = GetYear(0)
                        Dim Titles As String = "Salary Slip for " & Heading & " " & GetYear(0)


                        If mDataSet Is Nothing OrElse mDataSet.Tables.Count <= 0 Then Exit Sub

                        

                        cryRep.Load(Application.StartupPath & "\Reports\" & "Printer.rpt")
                        cryRep.SetDataSource(mDataSet.Tables(0))
                        cryRep.DataDefinition.FormulaFields("Company").Text = iStr1(0)
                        cryRep.DataDefinition.FormulaFields("Month").Text = "'" & Titles & "'"

                        'Get Attendance Incentive Amount
                        Dim Attn_Inc_Amt As String = "0"
                        Attn_Inc_Amt = mDataSet.Tables(0).Rows(0)("Attn_Incentive")
                        cryRep.DataDefinition.FormulaFields("Attn_Source_Val").Text = "'" & Attn_Inc_Amt & "'"

                        'cryRep.DataDefinition.FormulaFields("Year").Text = Titles

                        cryRep.PrintToPrinter(1, False, 0, 0)

                        'cryView.crViewer.ReportSource = cryRep
                        'cryView.crViewer.Refresh()
                        'cryView.Show()


                    End If
                Else
                    lblNetAmt.Text = "NET AMOUNT : 0.00"
                    lblWEarnedAmt.Text = "0.0"
                    lblAllowanceAmt.Text = "0.0"
                    lblDeductionsAmt.Text = "0.0"

                    AmountInWords_Str = Class_Check.ConvertNumberToWords("0.0")
                End If
            Else
                lblNetAmt.Text = "NET AMOUNT : 0.00"
                lblWEarnedAmt.Text = "0.0"
                lblAllowanceAmt.Text = "0.0"
                lblDeductionsAmt.Text = "0.0"
                AmountInWords_Str = Class_Check.ConvertNumberToWords("0.0")
            End If
        Else
            lblTokenNo.Text = ""
            lblMachineID.Text = ""
            lblEmpName.Text = ""
            lblDept.Text = ""
            lblDesg.Text = ""
            lblWagesType.Text = ""
            lblNetAmt.Text = "NET AMOUNT : 0.00"
            lblWEarnedAmt.Text = ""
            lblAllowanceAmt.Text = ""
            lblDeductionsAmt.Text = ""
            AmountInWords_Str = Class_Check.ConvertNumberToWords("0.0")
            Dim sFilePath_Clear As String = ""

            sFilePath_Clear = Application.StartupPath & "\Tapes-Photos\No-Iamge.JPG"
            pbEmployee.BackgroundImage = Image.FromFile(sFilePath_Clear)
        End If

        lblRupees.Text = "RUPEES : " & UCase(AmountInWords_Str)

        'lbRTShow.Items.Add("...UserID:" & sEnrollNumber)
        'lbRTShow.Items.Add("...isInvalid:" & iIsInValid.ToString())
        'lbRTShow.Items.Add("...attState:" & iAttState.ToString())
        'lbRTShow.Items.Add("...VerifyMethod:" & iVerifyMethod.ToString())
        'lbRTShow.Items.Add("...Workcode:" & iWorkCode.ToString()) 'the difference between the event OnAttTransaction and OnAttTransactionEx
        'lbRTShow.Items.Add("...Time:" & iYear.ToString() & "-" & iMonth.ToString() & "-" & iDay.ToString() & " " & iHour.ToString() & ":" & iMinute.ToString() & ":" & iSecond.ToString())
    End Sub

    Private Sub BtnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnClear.Click

        Dim AmountInWords_Str As String
        Dim Class_Check As New clsConversion

        lblTokenNo.Text = ""
        lblMachineID.Text = ""
        lblEmpName.Text = ""
        lblDept.Text = ""
        lblDesg.Text = ""
        lblWagesType.Text = ""
        lblNetAmt.Text = "NET AMOUNT : 0.00"
        Dim sFilePath_Clear As String = ""

        sFilePath_Clear = Application.StartupPath & "\Tapes-Photos\No-Iamge.JPG"
        pbEmployee.BackgroundImage = Image.FromFile(sFilePath_Clear)

        AmountInWords_Str = Class_Check.ConvertNumberToWords("0.0")
        lblRupees.Text = "RUPEES : " & UCase(AmountInWords_Str)

    End Sub


    Private Sub btnReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReport.Click
        If Trim(cmbCompCode.Text) = "" Then
            MessageBox.Show("Please select company code.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbCompCode.Focus()
            Exit Sub
        End If
        If Trim(cmbLocCode.Text) = "" Then
            MessageBox.Show("Please select Location code.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbCompCode.Focus()
            Exit Sub
        End If
        If Trim(cmbMonth.Text) = "" Then
            MessageBox.Show("Please select Month.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbCompCode.Focus()
            Exit Sub
        End If

        If Trim(txtFromDate.Text) = "" Then
            MessageBox.Show("Please Enter From Date.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbCompCode.Focus()
            Exit Sub
        End If

        If Trim(txtToDate.Text) = "" Then
            MessageBox.Show("Please Enter To Date.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbCompCode.Focus()
            Exit Sub
        End If


        Dim SdataSet As New DataSet
        Dim iStr1() As String
        Dim iStr2() As String
        Dim strPhotoPath As String

        iStr1 = Split(Trim(cmbCompCode.Text), " | ")
        iStr2 = Split(Trim(cmbLocCode.Text), " | ")


        SSQL = "select ED.MachineID,(ED.FirstName + '.' + ED.MiddleInitial) as EmpName,ED.DeptName,SD.NetAmt,SD.Status from Employee_Mst ED inner join Salary_Distribute SD on ED.EmpNo=SD.TknNo "
        SSQL = SSQL & "where FromDate=convert(datetime,'" & txtFromDate.Text & "',103) and ToDate=convert(datetime,'" & txtToDate.Text & "',103) order by ED.MachineID asc"
        mDataSet = Nothing
        mDataSet = ReturnMultipleValue(SSQL)

        Dim cryRep As New ReportDocument
        Dim cryView As New frmRepView

        If mDataSet Is Nothing OrElse mDataSet.Tables.Count <= 0 Then Exit Sub
        cryRep.Load(Application.StartupPath & "\Reports\" & "New_SalaryCover.rpt")
        cryRep.SetDataSource(mDataSet.Tables(0))
        cryRep.DataDefinition.FormulaFields("Company").Text = iStr1(0)
        cryView.crViewer.ReportSource = cryRep
        cryView.crViewer.Refresh()
        cryView.Show()

    End Sub

    Private Sub btnupload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupload.Click
        Dim WagesType As String = ""

        If Trim(cmbCompCode.Text) = "" Then
            MessageBox.Show("Please select company code.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbCompCode.Focus()
            Exit Sub
        End If
        If Trim(cmbLocCode.Text) = "" Then
            MessageBox.Show("Please select Location code.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbCompCode.Focus()
            Exit Sub
        End If
        If Trim(cmbMonth.Text) = "" Then
            MessageBox.Show("Please select Month.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbCompCode.Focus()
            Exit Sub
        End If

        If Trim(txtFromDate.Text) = "" Then
            MessageBox.Show("Please Enter From Date.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbCompCode.Focus()
            Exit Sub
        End If

        If Trim(txtToDate.Text) = "" Then
            MessageBox.Show("Please Enter To Date.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbCompCode.Focus()
            Exit Sub
        End If
        iStr1 = Split(Trim(cmbCompCode.Text), " | ")
        iStr2 = Split(Trim(cmbLocCode.Text), " | ")
        iYear = Split(Trim(cmbFinYear.Text), "-")

        If (cmbWages.Text = "MONTHLY STAFF-I") Then
            WagesType = "1"
        ElseIf (cmbWages.Text = "MONTHLY STAFF-II") Then
            WagesType = "2"
        ElseIf (cmbWages.Text = "MONTHLY TEMP WORKER") Then
            WagesType = "3"
        ElseIf (cmbWages.Text = "MONTHLY TEN DAYS") Then
            WagesType = "4"
        ElseIf (cmbWages.Text = "MONTHLY C-WORKER") Then
            WagesType = "5"
        ElseIf (cmbWages.Text = "MONTHLY M-WORKER") Then
            WagesType = "6"
        ElseIf (cmbWages.Text = "WEEKLY") Then
            WagesType = "7"
        ElseIf (cmbWages.Text = "WEEKLY II") Then
            WagesType = "8"
        ElseIf (cmbWages.Text = "NEW COMMERS") Then
            WagesType = "9"
        End If

        'Check Doublecate data available or Not
        SSQL = "select * from Salary_Distribute where WagesType='" & WagesType & "' and Ccode='" & iStr1(0) & "' and Lcode='" & iStr2(0) & "' and "
        SSQL = SSQL & "FromDate=convert(datetime,'" & txtFromDate.Text & "',103) and ToDate=convert(datetime,'" & txtToDate.Text & "',103)"
        mDataSet = ReturnMultipleValue(SSQL)
        If mDataSet.Tables(0).Rows.Count <> 0 Then

            MessageBox.Show("Already Transfer this catagory .", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub

        End If


        If (cmbWages.Text = "MONTHLY STAFF-I") And (cmbWages.Text = "MONTHLY STAFF-II") Then

            SSQL = "select ED.BiometricID,ED.ExisistingCode,(ED.EmpName + '.' + ED.Initial) as EmpName,OP.BankACCNo,SD.Availabledays, "
            SSQL = SSQL & "SD.Losspay,SD.NFh,SD.BasicandDA,SD.Hosteldeduction,SD.TotalDeductions,SD.NetPay,SD.SpinningAmt "
            SSQL = SSQL & "from [JLM-Epay]..SalaryDetails SD inner join [JLM-Epay]..EmployeeDetails ED on ED.EmpNo=SD.EmpNo "
            SSQL = SSQL & "inner Join [JLM-Epay]..OfficialProfile OP on OP.EmpNo=SD.EmpNo "
            SSQL = SSQL & "where ED.EmployeeType='" & WagesType & "' and SD.month='" & cmbMonth.Text & "'  and SD.FromDate=convert(datetime,'" & txtFromDate.Text & "',103) and "
            SSQL = SSQL & "SD.ToDate=convert(datetime,'" & txtToDate.Text & "',103) and SD.FinancialYear='" & iYear(0) & "' and "
            SSQL = SSQL & "SD.Ccode='" & iStr1(0) & "' and SD.Lcode='" & iStr2(0) & "' and SD.NetPay<>'0' order by cast(BiometricID as int) asc "
            mDataSet = ReturnMultipleValue(SSQL)

            If mDataSet.Tables(0).Rows.Count <> 0 Then
                For intRow = 0 To mDataSet.Tables(0).Rows.Count - 1




                    'Insert Salary_Distribute Table
                    SSQL = "Insert into Salary_Distribute(TknNo,MachineID,EmpName,BankAccNo,WagesType,Attendance,PLdays,NfhDays,"
                    SSQL = SSQL & "FromDate,ToDate,WagesEarn,Allowance,Deduction,NetAmt,Status,Ccode,Lcode,Attn_Incentive) values("
                    SSQL = SSQL & "'" & mDataSet.Tables(0).Rows(intRow)(0) & "','" & mDataSet.Tables(0).Rows(intRow)(1) & "','" & mDataSet.Tables(0).Rows(intRow)(2) & "','" & mDataSet.Tables(0).Rows(intRow)(3) & "', "
                    SSQL = SSQL & "'" & WagesType & "','" & mDataSet.Tables(0).Rows(intRow)(4) & "','" & mDataSet.Tables(0).Rows(intRow)(5) & "','" & mDataSet.Tables(0).Rows(intRow)(6) & "', "
                    SSQL = SSQL & "convert(datetime,'" & txtFromDate.Text & "',103),convert(datetime,'" & txtToDate.Text & "',103),'" & mDataSet.Tables(0).Rows(intRow)(7) & "','" & mDataSet.Tables(0).Rows(intRow)(8) & "', "
                    SSQL = SSQL & "'" & mDataSet.Tables(0).Rows(intRow)(9) & "','" & mDataSet.Tables(0).Rows(intRow)(10) & "', "
                    SSQL = SSQL & "'2','" & iStr1(0) & "','" & iStr2(0) & "','" & mDataSet.Tables(0).Rows(intRow)("SpinningAmt") & "')"
                    mStatus = InsertDeleteUpdate(SSQL)

                Next
            Else
                MessageBox.Show("Please give correct inputs .", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub

            End If
        Else

            SSQL = "select ED.BiometricID,ED.ExisistingCode,(ED.EmpName + '.' + ED.Initial) as EmpName,OP.BankACCNo,SD.Availabledays, "
            SSQL = SSQL & "SD.Losspay,SD.NFh,SD.BasicandDA,SD.Hosteldeduction,SD.TotalDeductions,SD.NetPay,SD.SpinningAmt "
            SSQL = SSQL & "from [JLM-Epay]..SalaryDetails SD inner join [JLM-Epay]..EmployeeDetails ED on ED.EmpNo=SD.EmpNo "
            SSQL = SSQL & "inner Join [JLM-Epay]..OfficialProfile OP on OP.EmpNo=SD.EmpNo "
            SSQL = SSQL & "where ED.EmployeeType='" & WagesType & "' and SD.month='" & cmbMonth.Text & "'  and SD.FromDate=convert(datetime,'" & txtFromDate.Text & "',103) and "
            SSQL = SSQL & "SD.ToDate=convert(datetime,'" & txtToDate.Text & "',103) and SD.FinancialYear='" & iYear(0) & "' and "
            SSQL = SSQL & "SD.Ccode='" & iStr1(0) & "' and SD.Lcode='" & iStr2(0) & "'  and SD.NetPay<>'0' order by cast(BiometricID as int) asc "
            mDataSet = ReturnMultipleValue(SSQL)

            If mDataSet.Tables(0).Rows.Count <> 0 Then
                For intRow = 0 To mDataSet.Tables(0).Rows.Count - 1




                    'Insert Salary_Distribute Table
                    SSQL = "Insert into Salary_Distribute(TknNo,MachineID,EmpName,BankAccNo,WagesType,Attendance,PLdays,NfhDays,"
                    SSQL = SSQL & "FromDate,ToDate,WagesEarn,Allowance,Deduction,NetAmt,Status,Ccode,Lcode,Attn_Incentive) values("
                    SSQL = SSQL & "'" & mDataSet.Tables(0).Rows(intRow)(0) & "','" & mDataSet.Tables(0).Rows(intRow)(1) & "','" & mDataSet.Tables(0).Rows(intRow)(2) & "','" & mDataSet.Tables(0).Rows(intRow)(3) & "', "
                    SSQL = SSQL & "'" & WagesType & "','" & mDataSet.Tables(0).Rows(intRow)(4) & "','" & mDataSet.Tables(0).Rows(intRow)(5) & "','" & mDataSet.Tables(0).Rows(intRow)(6) & "', "
                    SSQL = SSQL & "convert(datetime,'" & txtFromDate.Text & "',103),convert(datetime,'" & txtToDate.Text & "',103),'" & mDataSet.Tables(0).Rows(intRow)(7) & "','" & mDataSet.Tables(0).Rows(intRow)(8) & "', "
                    SSQL = SSQL & "'" & mDataSet.Tables(0).Rows(intRow)(9) & "','" & mDataSet.Tables(0).Rows(intRow)(10) & "', "
                    SSQL = SSQL & "'2','" & iStr1(0) & "','" & iStr2(0) & "','" & mDataSet.Tables(0).Rows(intRow)("SpinningAmt") & "')"
                    mStatus = InsertDeleteUpdate(SSQL)

                Next
            Else
                MessageBox.Show("Please give correct inputs .", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub

            End If
        End If


        

        MessageBox.Show("Successfully Transfer Salary .", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)

    End Sub

    Private Sub btnGet_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
End Class