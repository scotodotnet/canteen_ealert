﻿#Region "Imports"
Imports System
Imports System.Data
Imports System.Data.OleDb
#End Region

Module modConnection
    Public Function GetConnection(ByVal mvarDatabaseName As String) As String
        Dim mvarConnectionString As String = ""
        Try
            mvarConnectionString = "Provider=sqloledb;"
            mvarConnectionString &= "Data Source=" & mvarServerName & ";"
            mvarConnectionString &= "Initial Catalog= " & "Raajco_Spay" & ";"
            mvarConnectionString &= "User ID= " & mvarUserName & ";"
            mvarConnectionString &= "Password= " & mvarPassword & ""

            'saba Connection string
            'mvarConnectionString = "Provider=sqloledb;"
            'mvarConnectionString &= "Data Source=" & mvarServerName & ";Network Library=DBMSSOCN;"
            'mvarConnectionString &= "Initial Catalog=eAlert;"
            'mvarConnectionString &= "User ID=" & mvarUserName & ";Password=" & mvarPassword & ";"

            Return mvarConnectionString
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
        Return mvarConnectionString
    End Function

    Public Function GetConnection_Payroll(ByVal mvarDatabaseName As String) As String
        Dim mvarConnectionString As String = ""
        Try
            mvarConnectionString = "Provider=sqloledb;"
            mvarConnectionString &= "Data Source=" & Payroll_mvarServerName & ";"
            mvarConnectionString &= "Initial Catalog= " & mvarDatabaseName & ";"
            mvarConnectionString &= "User ID= " & mvarUserName & ";"
            mvarConnectionString &= "Password= " & mvarPassword & ""

            ''saba Connection string
            'mvarConnectionString = "Provider=sqloledb;"
            'mvarConnectionString &= "Data Source=" & Payroll_mvarServerName & ";Network Library=DBMSSOCN;"
            'mvarConnectionString &= "Initial Catalog=" & mvarDatabaseName & ";"
            'mvarConnectionString &= "User ID=sa;Password=Altius2005;"

            Return mvarConnectionString
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
        Return mvarConnectionString
    End Function

    Public Function ReturnSingleValue(ByVal MyQuery As String) As Object
        Try
            con = New OleDbConnection(GetConnection("eAlert"))
            com = New OleDbCommand(MyQuery, con)
            con.Open()
            Dim result As Object = com.ExecuteScalar
            con.Close()
            If IsDBNull(result) Then
                Return Nothing
            End If

            Return result
        Catch ex As Exception
            Throw New Exception(ex.Message)
            If con.State = ConnectionState.Open Then
                con.Close()
            End If
        End Try
        Return Nothing
    End Function

    Public Function InsertDeleteUpdate(ByVal MyQuery As String) As Long
        Try
            con = New OleDbConnection(GetConnection("eAlert"))
            com = New OleDbCommand(MyQuery, con)
            con.Open()
            Dim RowsAffected As Long
            RowsAffected = com.ExecuteNonQuery()
            con.Close()
            Return RowsAffected
        Catch ex As Exception
            Throw New Exception(ex.Message)
            If con.State = ConnectionState.Open Then
                con.Close()
            End If
        End Try
        Return Nothing
    End Function
    Public Function ReturnMultipleValue_payrollAtt(ByVal MyQuery As String) As DataSet
        Dim ds As New DataSet
        Try
            con = New OleDbConnection(GetConnection_Payroll("JLM-Epay"))
            da = New OleDbDataAdapter(MyQuery, con)
            ds = New DataSet
            con.Open()
            da.Fill(ds)
            con.Close()
            Return ds
        Catch ex As Exception
            Throw New Exception(ex.Message)
            If con.State = ConnectionState.Open Then
                con.Close()
            End If
        End Try
        Return ds
    End Function
    Public Function ReturnMultipleValue(ByVal MyQuery As String) As DataSet
        Dim ds As New DataSet
        Try
            con = New OleDbConnection(GetConnection("eAlert"))
            da = New OleDbDataAdapter(MyQuery, con)
            ds = New DataSet
            con.Open()
            da.Fill(ds)
            con.Close()
            Return ds
        Catch ex As Exception
            Throw New Exception(ex.Message)
            If con.State = ConnectionState.Open Then
                con.Close()
            End If
        End Try
        Return ds
    End Function

    Public Function ReturnMultipleValue_Payroll(ByVal MyQuery As String) As DataSet
        Dim ds As New DataSet
        Try
            con = New OleDbConnection(GetConnection_Payroll("JLM-Epay"))
            da = New OleDbDataAdapter(MyQuery, con)
            ds = New DataSet
            con.Open()
            da.Fill(ds)
            con.Close()
            Return ds
        Catch ex As Exception
            Throw New Exception(ex.Message)
            If con.State = ConnectionState.Open Then
                con.Close()
            End If
        End Try
        Return ds
    End Function

    Public Sub ExecuteCommandQuery(ByVal comm As OleDbCommand)
        Try
            con = New OleDbConnection(GetConnection("eAlert"))
            comm.Connection = con
            con.Open()
            comm.ExecuteNonQuery()
            con.Close()
        Catch ex As Exception
            Throw New Exception(ex.Message)
            If con.State = ConnectionState.Open Then
                con.Close()
            End If
        End Try
    End Sub
End Module
