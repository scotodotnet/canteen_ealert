﻿#Region "Imports"
Imports System
Imports System.Data
Imports System.Data.OleDb
Imports System.Text.RegularExpressions
Imports System.Net
#End Region

Module modCommonfun
    Public Function ErrorPrint(ByVal aErrorCode As Long) As String
        Dim mLocalString = ""
        Select Case aErrorCode
            Case 0
                mLocalString = "SUCCESS"
            Case 1
                mLocalString = "ERR_COMPORT_ERROR"
            Case 2
                mLocalString = "ERR_WRITE_FAIL"
            Case 3
                mLocalString = "ERR_READ_FAIL"
            Case 4
                mLocalString = "ERR_INVALID_PARAM"
            Case 5
                mLocalString = "ERR_NON_CARRYOUT"
            Case 6
                mLocalString = "ERR_LOG_END"
            Case 7
                mLocalString = "ERR_MEMORY"
            Case 8
                mLocalString = "ERR_MULTIUSER"
        End Select
        Return mLocalString
    End Function

    Public Function TimeDiff(ByVal t1, ByVal t2)

        'Purpose: Calculate the difference between two given times
        ' t1 and t2. Returns t1 - t2.

        Dim sec1, sec2, secOut, minsOut, hoursOut

        'Convert times to seconds
        sec1 = (Hour(t1) * 3600) + (Minute(t1) * 60) + Second(t1)
        sec2 = (Hour(t2) * 3600) + (Minute(t2) * 60) + Second(t2)

        'Subtract one from the other
        secOut = sec1 - sec2 ' or Add together to sum two times

        'Calc hours
        hoursOut = Int(secOut / 3600)
        secOut = secOut - (hoursOut * 3600)

        'Calc minutes
        minsOut = Int(secOut / 60)
        secOut = secOut - (minsOut * 60)

        'Convert back to HH:MM:SS
        TimeDiff = TimeValue(Format(hoursOut, "00") + ":" + Format(minsOut, "00") + ":" + Format(secOut, "00"))

    End Function
    Public Function EmailAddressCheck(ByVal emailAddress As String) As Boolean
        Dim pattern As String = "^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"
        Dim emailAddressMatch As Match = Regex.Match(emailAddress, pattern)
        If emailAddressMatch.Success Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Function IsIpValid(ByVal ipAddress As String) As Boolean
        Dim expr As String = "^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$"
        Dim reg As Regex = New Regex(expr)
        If (reg.IsMatch(ipAddress)) Then
            Dim parts() As String = ipAddress.Split(".")
            If Convert.ToInt32(parts(0)) = 0 Then
                Return False
            ElseIf Convert.ToInt32(parts(3)) = 0 Then
                Return False
            End If
            For i As Integer = 1 To 4
                If i > 255 Then
                    Return False
                End If
            Next
            Return True
        Else
            Return False
        End If
    End Function
    Public Function IsAddressValid(ByVal addrString As String) As Boolean
        Dim address As IPAddress
        Return IPAddress.TryParse(addrString, address)
    End Function
    Public Sub FormAlign(ByVal objForm As Form)
        objForm.Top = ((Screen.PrimaryScreen.WorkingArea.Height / 2) - (objForm.Height / 2)) - 100
        objForm.Left = ((Screen.PrimaryScreen.WorkingArea.Width / 2) - (objForm.Width / 2)) - 90
    End Sub
    Public Function Remove_Single_Quote(ByVal mValue As String) As String
        Return Replace(mValue, "'", " ")
    End Function
    Public Sub Show_Message(ByVal MessageBoxMessage As String, Optional ByVal MessageBoxTitle As String = Nothing)
        If MessageBoxTitle = Nothing Then
            MsgBox(MessageBoxMessage, MsgBoxStyle.Information, MessageBoxTitle)
        Else
            MsgBox(MessageBoxMessage, MsgBoxStyle.Information, MessageBoxTitle)
        End If
    End Sub
    Public Function DateConverterPost(ByVal e_date As String) As String
        Dim l_len As Int16
        ' Dim index As Int16
        Dim l_date As String
        Dim l_char As String
        Dim l_value As Int16
        Dim l_month As Int16
        Dim l_day As Int16
        Dim l_year As Int16
        Dim l_slash As Int16
        l_month = Now.Month
        l_day = Now.Day
        l_year = Now.Year
        l_date = e_date
        l_len = Len(e_date)
        Select Case l_len
            Case 1
                l_char = e_date
                If IsNumeric(l_char) Then
                    l_value = Val(l_char)
                    Select Case l_value
                        Case 0
                            l_date = "0"
                        Case Else
                            l_date = l_char & "/" & CStr(l_day) & "/" & CStr(l_year)
                    End Select
                Else
                    l_date = "0"
                End If
            Case 2
                If IsNumeric(l_date) Then
                    If Val(l_date) > 0 And Val(l_date) < 13 Then
                        l_date = l_date & "/" & CStr(l_day) & "/" & CStr(l_year)
                    Else
                        l_value = Val(Mid(l_date, 1, 1))
                        If l_value > 1 And l_value < 10 Then
                            l_month = l_value
                            l_day = Mid(l_date, 2, 1)
                            l_date = CStr(l_month) & "/" & CStr(l_day) & "/" & CStr(l_year)
                        Else
                            l_date = "0"
                        End If
                    End If
                Else
                    If Mid(l_date, 1, 2) = "/" And IsNumeric(Mid(l_date, 1, 1)) Then
                        l_date = l_date & CStr(l_day) & "/" & CStr(l_year)
                    Else
                        l_date = "0"
                    End If
                End If
            Case 3
                l_value = Val(Mid(l_date, 1, 1))
                If l_value > 1 And l_value < 13 Then
                    l_month = l_value
                    l_date = Mid(l_date, 2, 2)
                Else
                    l_month = Val(Mid(l_date, 1, 2))
                    l_date = Mid(l_date, 3, 1)
                End If
                If IsNumeric(l_date) Then
                    l_day = Val(l_date)
                End If
                l_date = CStr(l_month) & "/" & CStr(l_day) & "/" & CStr(l_year)
            Case 4
                l_slash = InStr(l_date, "/", CompareMethod.Binary)
                If l_slash = 0 Then
                    l_slash = InStr(l_date, ".", CompareMethod.Binary)
                End If
                Select Case l_slash
                    Case 0
                        l_value = Val(Mid(l_date, 1, 1))
                        If l_value > 1 And l_value < 10 Then
                            l_month = l_value
                            l_value = Val(Mid(l_date, 2, 1))
                            If l_value > 3 Then
                                l_day = Val(Mid(l_date, 2, 1))
                            Else
                                l_day = Val(Mid(l_date, 2, 2))
                                l_year = Val(Mid(l_date, 3, 2))
                            End If
                            l_day = Mid(l_date, 2, 1)
                            l_date = CStr(l_month) & "/" & CStr(l_day) & "/" & CStr(l_year)
                        Else
                            l_month = Val(Mid(l_date, 1, 2))
                            l_day = Val(Mid(l_date, 3, 2))
                            l_date = CStr(l_month) & "/" & CStr(l_day) & "/" & CStr(l_year)
                        End If
                    Case 3
                        l_month = Val(Mid(l_date, 1, 2))
                        l_day = Val(Mid(l_date, 4, 1))
                        l_date = CStr(l_month) & "/" & CStr(l_day) & "/" & CStr(l_year)
                    Case Else
                        l_date = "0"
                End Select
                If l_date <> "0" Then
                    l_date = CStr(l_month) & "/" & CStr(l_day) & "/" & CStr(l_year)
                End If
            Case 5
                l_slash = InStr(l_date, "/", CompareMethod.Binary)
                If l_slash = 0 Then
                    l_slash = InStr(l_date, ".", CompareMethod.Binary)
                End If
                Select Case l_slash
                    Case 0
                        l_char = Mid(l_date, 1, 1)
                        If Val(l_char) > 1 Then
                            l_month = Val(Mid(l_date, 1, 1))
                            l_day = Val(Mid(l_date, 2, 2))
                            l_year = Val(Mid(l_date, 4, 2))
                            l_year = l_year + 2000
                        Else
                            l_month = Val(Mid(l_date, 1, 2))
                            l_day = Val(Mid(l_date, 3, 2))
                            l_year = Val(Mid(l_date, 5, 1))
                            l_year = l_year + 2000
                        End If
                        l_date = CStr(l_month) & "/" & CStr(l_day) & "/" & CStr(l_year)
                    Case 2
                        l_month = Val(Mid(l_date, 1, 1))
                        l_day = Val(Mid(l_date, 3, 2))
                        l_year = Val(Mid(l_date, 5, 1))
                        l_year = l_year + 2000
                        l_date = CStr(l_month) & "/" & CStr(l_day) & "/" & CStr(l_year)
                    Case 3
                        l_month = Val(Mid(l_date, 1, 2))
                        l_day = Val(Mid(l_date, 4, 2))
                        l_date = CStr(l_month) & "/" & CStr(l_day) & "/" & CStr(l_year)
                    Case Else
                        l_date = "0"
                End Select
                If l_date <> "0" Then
                    l_date = CStr(l_month) & "/" & CStr(l_day) & "/" & CStr(l_year)
                End If
            Case 6
                l_slash = InStr(l_date, "/", CompareMethod.Binary)
                If l_slash = 0 Then
                    l_slash = InStr(l_date, ".", CompareMethod.Binary)
                End If
                Select Case l_slash
                    Case 0
                        l_month = Val(Mid(l_date, 1, 2))
                        l_day = Val(Mid(l_date, 3, 2))
                        l_year = Val(Mid(l_date, 5, 2))
                        l_year = l_year + 2000
                        l_date = CStr(l_month) & "/" & CStr(l_day) & "/" & CStr(l_year)
                    Case 2
                        l_month = Val(Mid(l_date, 1, 1))
                        l_day = Val(Mid(l_date, 3, 2))
                        l_year = Val(Mid(l_date, 5, 2))
                        l_year = l_year + 2000
                        l_date = CStr(l_month) & "/" & CStr(l_day) & "/" & CStr(l_year)
                    Case 3
                        l_month = Val(Mid(l_date, 1, 2))
                        l_day = Val(Mid(l_date, 4, 2))
                        l_date = CStr(l_month) & "/" & CStr(l_day) & "/" & CStr(l_year)
                    Case Else
                        l_date = "0"
                End Select
            Case 7
                l_slash = InStr(l_date, "/", CompareMethod.Binary)
                If l_slash = 0 Then
                    l_slash = InStr(l_date, ".", CompareMethod.Binary)
                End If
                Select Case l_slash
                    Case 0
                        If Val(Mid(l_date, 1, 1)) > 1 Then
                            l_month = Val(Mid(l_date, 1, 1))
                            l_day = Val(Mid(l_date, 2, 2))
                            l_year = Val(Mid(l_date, 4, 4))
                            l_year = l_year + 2000
                        Else
                            l_month = Val(Mid(l_date, 1, 2))
                            l_day = Val(Mid(l_date, 3, 2))
                            l_year = Val(Mid(l_date, 5, 3))
                            l_year = l_year + 2000
                        End If
                        l_date = CStr(l_month) & "/" & CStr(l_day) & "/" & CStr(l_year)
                    Case 2
                        l_month = Val(Mid(l_date, 1, 1))
                        l_day = Val(Mid(l_date, 3, 2))
                        l_year = Val(Mid(l_date, 6, 2))
                        l_year = l_year + 2000
                        l_date = CStr(l_month) & "/" & CStr(l_day) & "/" & CStr(l_year)
                    Case 3
                        l_month = Val(Mid(l_date, 1, 2))
                        l_day = Val(Mid(l_date, 4, 2))
                        l_year = Val(Mid(l_date, 7, 1))
                        l_year = l_year + 2000
                        l_date = CStr(l_month) & "/" & CStr(l_day) & "/" & CStr(l_year)
                    Case Else
                        l_date = "0"
                End Select
            Case 8
                l_slash = InStr(l_date, "/", CompareMethod.Binary)
                If l_slash = 0 Then
                    l_slash = InStr(l_date, ".", CompareMethod.Binary)
                End If
                Select Case l_slash
                    Case 0
                        l_month = Val(Mid(l_date, 1, 2))
                        l_day = Val(Mid(l_date, 3, 2))
                        l_year = Val(Mid(l_date, 5, 4))
                        l_date = CStr(l_month) & "/" & CStr(l_day) & "/" & CStr(l_year)
                    Case 2
                        l_month = Val(Mid(l_date, 1, 1))
                        l_day = Val(Mid(l_date, 3, 2))
                        l_year = Val(Mid(l_date, 6, 3))
                        l_year = l_year + 2000
                        l_date = CStr(l_month) & "/" & CStr(l_day) & "/" & CStr(l_year)
                    Case 3
                        l_month = Val(Mid(l_date, 1, 2))
                        l_day = Val(Mid(l_date, 4, 2))
                        l_year = Val(Mid(l_date, 7, 2))
                        l_year = l_year + 2000
                        l_date = CStr(l_month) & "/" & CStr(l_day) & "/" & CStr(l_year)
                    Case Else
                        l_date = "0"
                End Select
            Case 9, 10
            Case Else
                l_date = "0"
        End Select
        Return l_date
    End Function
    Public Function retWord(ByVal Num As Decimal) As String
        'This two dimensional array store the primary word convertion of number.
        retWord = ""
        Dim ArrWordList(,) As Object = {{0, ""}, {1, "One"}, {2, "Two"}, {3, "Three"}, {4, "Four"}, _
                                        {5, "Five"}, {6, "Six"}, {7, "Seven"}, {8, "Eight"}, {9, "Nine"}, _
                                        {10, "Ten"}, {11, "Eleven"}, {12, "Twelve"}, {13, "Thirteen"}, {14, "Fourteen"}, _
                                        {15, "Fifteen"}, {16, "Sixteen"}, {17, "Seventeen"}, {18, "Eighteen"}, {19, "Nineteen"}, _
                                        {20, "Twenty"}, {30, "Thirty"}, {40, "Forty"}, {50, "Fifty"}, {60, "Sixty"}, _
                                        {70, "Seventy"}, {80, "Eighty"}, {90, "Ninety"}, {100, "Hundred"}, {1000, "Thousand"}, _
                                        {100000, "Lakh"}, {10000000, "Crore"}}

        Dim i As Integer
        For i = 0 To UBound(ArrWordList)
            If Num = ArrWordList(i, 0) Then
                retWord = ArrWordList(i, 1)
                Exit For
            End If
        Next
        Return retWord
    End Function

    Public Function Left_Val(ByVal Value As String, ByVal Length As Integer) As String
        ' Rereate a LEFT function for string manipulation
        If Value.Length >= Length Then
            Return Value.Substring(0, Length)
        Else
            Return Value
        End If
    End Function
    Public Function GetRandom(ByVal Min As Integer, ByVal Max As Integer) As Integer
        ' by making Generator static, we preserve the same instance '
        ' (i.e., do not create new instances with the same seed over and over) '
        ' between calls '
        Static Generator As System.Random = New System.Random()
        Return Generator.Next(Min, Max)
    End Function

    Public Function Right_Val(ByVal Value As String, ByVal Length As Integer) As String
        ' Recreate a RIGHT function for string manipulation
        Dim i As Integer
        i = 0
        If Value.Length >= Length Then
            'i = Value.Length - Length
            Return Value.Substring(Value.Length - Length, Length)
        Else
            Return Value
        End If
    End Function

End Module