﻿#Region "Imports"
Imports System
Imports System.Data
Imports System.Data.OleDb
#End Region
Module modReports
    Public Function GetLateComers_Details(ByVal mDate As Date, ByVal mCompCode As String, ByVal mLocCode As String, ByVal mIpAddress As String) As DataSet
        Try
            SSQL = ""
            SSQL = "select ShiftDet,MachineID,TimeIN from"
            SSQL &= "("
            SSQL &= " Select 'Shift1' as [ShiftDet],MachineID,Min(TimeIN) as [TimeIN] from LogTime_IN"
            SSQL &= " Where Compcode='" & mCompCode & "'"
            SSQL &= " And LocCode='" & mLocCode & "'"
            SSQL &= " And IPAddress='" & mIpAddress & "'"
            SSQL &= " And TimeIN  >'" & mDate.ToString("yyyy/MM/dd") & " 09:30'"
            SSQL &= " And TimeIN <='" & mDate.ToString("yyyy/MM/dd") & " 12:00'"
            SSQL &= " Group By MachineID"
            SSQL &= " union"
            SSQL &= " Select 'Shift2' as [ShiftDet],MachineID,Min(TimeIN) as [TimeIN] from LogTime_IN"
            SSQL &= " Where Compcode='" & mCompCode & "'"
            SSQL &= " And LocCode='" & mLocCode & "'"
            SSQL &= " And IPAddress='" & mIpAddress & "'"
            SSQL &= " And TimeIN  >'" & mDate.ToString("yyyy/MM/dd") & " 17:30'"
            SSQL &= " And TimeIN <='" & mDate.ToString("yyyy/MM/dd") & " 19:30'"
            SSQL &= " Group By MachineID"
            SSQL &= " union"
            SSQL &= " Select 'Shift3' as [ShiftDet],MachineID,Min(TimeIN) as [TimeIN] from LogTime_IN"
            SSQL &= " Where Compcode='" & mCompCode & "'"
            SSQL &= " And LocCode='" & mLocCode & "'"
            SSQL &= " And IPAddress='" & mIpAddress & "'"
            SSQL &= " And TimeIN >'" & mDate.AddDays(1).ToString("yyyy/MM/dd") & " 01:00'"
            SSQL &= " And TimeIN <='" & mDate.AddDays(1).ToString("yyyy/MM/dd") & " 03:30'"
            SSQL &= " Group By MachineID) a "
            SSQL &= " Order By ShiftDet,TimeIN"
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
        Return (ReturnMultipleValue(SSQL))
    End Function

    Public Function GetEarlyGoers_Details(ByVal mDate As Date, ByVal mCompCode As String, ByVal mLocCode As String, ByVal mIpAddress As String) As DataSet
        Try
            SSQL = ""
            SSQL = "select ShiftDet,MachineID,TimeOUT from"
            SSQL &= "("
            SSQL &= " Select 'Shift1' as [ShiftDet],MachineID,Max(TimeOUT) as [TimeOUT] from LogTime_OUT"
            SSQL &= " Where Compcode='" & mCompCode & "'"
            SSQL &= " And LocCode='" & mLocCode & "'"
            SSQL &= " And IPAddress='" & mIpAddress & "'"
            SSQL &= " And TimeOUT  >'" & mDate.ToString("yyyy/MM/dd") & " 10:00'"
            SSQL &= " And TimeOUT <='" & mDate.ToString("yyyy/MM/dd") & " 16:30'"
            SSQL &= " Group By MachineID"
            SSQL &= " union"
            SSQL &= " Select 'Shift2' as [ShiftDet],MachineID,Max(TimeOUT) as [TimeOUT] from LogTime_OUT"
            SSQL &= " Where Compcode='" & mCompCode & "'"
            SSQL &= " And LocCode='" & mLocCode & "'"
            SSQL &= " And IPAddress='" & mIpAddress & "'"
            SSQL &= " And TimeOUT  >'" & mDate.ToString("yyyy/MM/dd") & " 17:30'"
            SSQL &= " And TimeOUT <='" & mDate.ToString("yyyy/MM/dd") & " 00:00'"
            SSQL &= " Group By MachineID"
            SSQL &= " union"
            SSQL &= " Select 'Shift3' as [ShiftDet],MachineID,Max(TimeOUT) as [TimeOUT] from LogTime_OUT"
            SSQL &= " Where Compcode='" & mCompCode & "'"
            SSQL &= " And LocCode='" & mLocCode & "'"
            SSQL &= " And IPAddress='" & mIpAddress & "'"
            SSQL &= " And TimeOUT >'" & mDate.AddDays(1).ToString("yyyy/MM/dd") & " 01:00'"
            SSQL &= " And TimeOUT <='" & mDate.AddDays(1).ToString("yyyy/MM/dd") & " 08:00'"
            SSQL &= " Group By MachineID) a "
            SSQL &= " Order By ShiftDet,TimeOUT"
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
        Return (ReturnMultipleValue(SSQL))
    End Function
End Module
